// ignore_for_file: must_be_immutable

part of 'light_fill_profile_blank_form_bloc.dart';

/// Represents the state of LightFillProfileBlankForm in the application.
class LightFillProfileBlankFormState extends Equatable {
  LightFillProfileBlankFormState({
    this.fullnameController,
    this.nameController,
    this.emailController,
    this.phonenumberController,
    this.selectedDropDownValue,
    this.lightFillProfileBlankFormModelObj,
  });

  TextEditingController? fullnameController;

  TextEditingController? nameController;

  TextEditingController? emailController;

  TextEditingController? phonenumberController;

  SelectionPopupModel? selectedDropDownValue;

  LightFillProfileBlankFormModel? lightFillProfileBlankFormModelObj;

  @override
  List<Object?> get props => [
        fullnameController,
        nameController,
        emailController,
        phonenumberController,
        selectedDropDownValue,
        lightFillProfileBlankFormModelObj,
      ];
  LightFillProfileBlankFormState copyWith({
    TextEditingController? fullnameController,
    TextEditingController? nameController,
    TextEditingController? emailController,
    TextEditingController? phonenumberController,
    SelectionPopupModel? selectedDropDownValue,
    LightFillProfileBlankFormModel? lightFillProfileBlankFormModelObj,
  }) {
    return LightFillProfileBlankFormState(
      fullnameController: fullnameController ?? this.fullnameController,
      nameController: nameController ?? this.nameController,
      emailController: emailController ?? this.emailController,
      phonenumberController:
          phonenumberController ?? this.phonenumberController,
      selectedDropDownValue:
          selectedDropDownValue ?? this.selectedDropDownValue,
      lightFillProfileBlankFormModelObj: lightFillProfileBlankFormModelObj ??
          this.lightFillProfileBlankFormModelObj,
    );
  }
}
