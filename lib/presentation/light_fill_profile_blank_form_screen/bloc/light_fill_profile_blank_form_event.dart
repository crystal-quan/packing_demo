// ignore_for_file: must_be_immutable

part of 'light_fill_profile_blank_form_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightFillProfileBlankForm widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightFillProfileBlankFormEvent extends Equatable {}

/// Event that is dispatched when the LightFillProfileBlankForm widget is first created.
class LightFillProfileBlankFormInitialEvent
    extends LightFillProfileBlankFormEvent {
  @override
  List<Object?> get props => [];
}

///event for dropdown selection
class ChangeDropDownEvent extends LightFillProfileBlankFormEvent {
  ChangeDropDownEvent({required this.value});

  SelectionPopupModel value;

  @override
  List<Object?> get props => [
        value,
      ];
}
