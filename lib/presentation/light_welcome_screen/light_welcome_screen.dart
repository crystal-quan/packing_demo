import 'bloc/light_welcome_bloc.dart';
import 'models/light_welcome_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_elevated_button.dart';

class LightWelcomeScreen extends StatelessWidget {
  const LightWelcomeScreen({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LightWelcomeBloc>(
      create: (context) => LightWelcomeBloc(LightWelcomeState(
        lightWelcomeModelObj: LightWelcomeModel(),
      ))
        ..add(LightWelcomeInitialEvent()),
      child: LightWelcomeScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return BlocBuilder<LightWelcomeBloc, LightWelcomeState>(
      builder: (context, state) {
        return SafeArea(
          child: Scaffold(
            backgroundColor: appTheme.gray5005,
            body: Container(
              width: double.maxFinite,
              padding: getPadding(
                left: 23,
                top: 51,
                right: 23,
                bottom: 51,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Spacer(),
                  Container(
                    height: getVerticalSize(
                      441,
                    ),
                    width: getHorizontalSize(
                      334,
                    ),
                    margin: getMargin(
                      left: 1,
                    ),
                    child: Stack(
                      alignment: Alignment.topRight,
                      children: [
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                "lbl_welcome_to".tr,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.left,
                                style: theme.textTheme.displayMedium,
                              ),
                              Padding(
                                padding: getPadding(
                                  top: 17,
                                ),
                                child: Text(
                                  "lbl_parkingrid".tr,
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.left,
                                  style: CustomTextStyles.urbanistIndigo60001,
                                ),
                              ),
                            ],
                          ),
                        ),
                        CustomImageView(
                          imagePath: ImageConstant.imgHellorafiki1,
                          height: getSize(
                            286,
                          ),
                          width: getSize(
                            286,
                          ),
                          alignment: Alignment.topRight,
                          margin: getMargin(
                            right: 5,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: getHorizontalSize(
                      332,
                    ),
                    margin: getMargin(
                      left: 10,
                      top: 8,
                      right: 40,
                    ),
                    child: Text(
                      "msg_the_best_parking".tr,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                      style: CustomTextStyles.titleMediumIndigo60001SemiBold
                          .copyWith(
                        letterSpacing: getHorizontalSize(
                          0.2,
                        ),
                      ),
                    ),
                  ),
                  CustomElevatedButton(
                    width: getHorizontalSize(
                      380,
                    ),
                    height: getVerticalSize(
                      58,
                    ),
                    text: "lbl_next".tr,
                    margin: getMargin(
                      left: 1,
                      top: 14,
                    ),
                    buttonStyle: CustomButtonStyles
                        .gradientnameprimarynameindigoA100
                        .copyWith(
                            fixedSize: MaterialStateProperty.all<Size>(Size(
                      double.maxFinite,
                      getVerticalSize(
                        58,
                      ),
                    ))),
                    decoration: CustomButtonStyles
                        .gradientnameprimarynameindigoA100Decoration,
                    buttonTextStyle: CustomTextStyles.titleMediumWhiteA7000116,
                  ),
                  CustomElevatedButton(
                    text: "lbl_skip".tr,
                    margin: getMargin(
                      left: 2,
                      top: 17,
                    ),
                    buttonStyle: CustomButtonStyles.fillIndigo10066.copyWith(
                        fixedSize: MaterialStateProperty.all<Size>(Size(
                      double.maxFinite,
                      getVerticalSize(
                        58,
                      ),
                    ))),
                    buttonTextStyle: CustomTextStyles.titleMediumIndigoA400,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
