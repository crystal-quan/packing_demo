// ignore_for_file: must_be_immutable

part of 'light_welcome_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightWelcome widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightWelcomeEvent extends Equatable {}

/// Event that is dispatched when the LightWelcome widget is first created.
class LightWelcomeInitialEvent extends LightWelcomeEvent {
  @override
  List<Object?> get props => [];
}
