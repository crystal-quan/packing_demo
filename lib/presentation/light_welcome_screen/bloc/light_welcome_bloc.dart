import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:parking_app/presentation/light_welcome_screen/models/light_welcome_model.dart';
part 'light_welcome_event.dart';
part 'light_welcome_state.dart';

/// A bloc that manages the state of a LightWelcome according to the event that is dispatched to it.
class LightWelcomeBloc extends Bloc<LightWelcomeEvent, LightWelcomeState> {
  LightWelcomeBloc(LightWelcomeState initialState) : super(initialState) {
    on<LightWelcomeInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightWelcomeInitialEvent event,
    Emitter<LightWelcomeState> emit,
  ) async {}
}
