// ignore_for_file: must_be_immutable

part of 'light_welcome_bloc.dart';

/// Represents the state of LightWelcome in the application.
class LightWelcomeState extends Equatable {
  LightWelcomeState({this.lightWelcomeModelObj});

  LightWelcomeModel? lightWelcomeModelObj;

  @override
  List<Object?> get props => [
        lightWelcomeModelObj,
      ];
  LightWelcomeState copyWith({LightWelcomeModel? lightWelcomeModelObj}) {
    return LightWelcomeState(
      lightWelcomeModelObj: lightWelcomeModelObj ?? this.lightWelcomeModelObj,
    );
  }
}
