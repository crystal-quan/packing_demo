// ignore_for_file: must_be_immutable

part of 'light_sign_up_type_form_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightSignUpTypeForm widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightSignUpTypeFormEvent extends Equatable {}

/// Event that is dispatched when the LightSignUpTypeForm widget is first created.
class LightSignUpTypeFormInitialEvent extends LightSignUpTypeFormEvent {
  @override
  List<Object?> get props => [];
}

///Event for changing password visibility
class ChangePasswordVisibilityEvent extends LightSignUpTypeFormEvent {
  ChangePasswordVisibilityEvent({required this.value});

  bool value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing checkbox
class ChangeCheckBoxEvent extends LightSignUpTypeFormEvent {
  ChangeCheckBoxEvent({required this.value});

  bool value;

  @override
  List<Object?> get props => [
        value,
      ];
}
