import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_sign_up_type_form_screen/models/light_sign_up_type_form_model.dart';part 'light_sign_up_type_form_event.dart';part 'light_sign_up_type_form_state.dart';/// A bloc that manages the state of a LightSignUpTypeForm according to the event that is dispatched to it.
class LightSignUpTypeFormBloc extends Bloc<LightSignUpTypeFormEvent, LightSignUpTypeFormState> {LightSignUpTypeFormBloc(LightSignUpTypeFormState initialState) : super(initialState) { on<LightSignUpTypeFormInitialEvent>(_onInitialize); on<ChangePasswordVisibilityEvent>(_changePasswordVisibility); on<ChangeCheckBoxEvent>(_changeCheckBox); }

_changePasswordVisibility(ChangePasswordVisibilityEvent event, Emitter<LightSignUpTypeFormState> emit, ) { emit(state.copyWith(isShowPassword: event.value)); } 
_changeCheckBox(ChangeCheckBoxEvent event, Emitter<LightSignUpTypeFormState> emit, ) { emit(state.copyWith(isCheckbox: event.value)); } 
_onInitialize(LightSignUpTypeFormInitialEvent event, Emitter<LightSignUpTypeFormState> emit, ) async  { emit(state.copyWith(emailController: TextEditingController(), languageController: TextEditingController(), isShowPassword: true, isCheckbox: false)); } 
 }
