// ignore_for_file: must_be_immutable

part of 'light_sign_up_type_form_bloc.dart';

/// Represents the state of LightSignUpTypeForm in the application.
class LightSignUpTypeFormState extends Equatable {
  LightSignUpTypeFormState({
    this.emailController,
    this.languageController,
    this.isShowPassword = true,
    this.isCheckbox = false,
    this.lightSignUpTypeFormModelObj,
  });

  TextEditingController? emailController;

  TextEditingController? languageController;

  LightSignUpTypeFormModel? lightSignUpTypeFormModelObj;

  bool isShowPassword;

  bool isCheckbox;

  @override
  List<Object?> get props => [
        emailController,
        languageController,
        isShowPassword,
        isCheckbox,
        lightSignUpTypeFormModelObj,
      ];
  LightSignUpTypeFormState copyWith({
    TextEditingController? emailController,
    TextEditingController? languageController,
    bool? isShowPassword,
    bool? isCheckbox,
    LightSignUpTypeFormModel? lightSignUpTypeFormModelObj,
  }) {
    return LightSignUpTypeFormState(
      emailController: emailController ?? this.emailController,
      languageController: languageController ?? this.languageController,
      isShowPassword: isShowPassword ?? this.isShowPassword,
      isCheckbox: isCheckbox ?? this.isCheckbox,
      lightSignUpTypeFormModelObj:
          lightSignUpTypeFormModelObj ?? this.lightSignUpTypeFormModelObj,
    );
  }
}
