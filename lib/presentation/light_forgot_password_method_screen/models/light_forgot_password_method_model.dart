// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_forgot_password_method_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightForgotPasswordMethodModel extends Equatable {LightForgotPasswordMethodModel copyWith() { return LightForgotPasswordMethodModel(
); } 
@override List<Object?> get props => [];
 }
