import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_forgot_password_method_screen/models/light_forgot_password_method_model.dart';part 'light_forgot_password_method_event.dart';part 'light_forgot_password_method_state.dart';/// A bloc that manages the state of a LightForgotPasswordMethod according to the event that is dispatched to it.
class LightForgotPasswordMethodBloc extends Bloc<LightForgotPasswordMethodEvent, LightForgotPasswordMethodState> {LightForgotPasswordMethodBloc(LightForgotPasswordMethodState initialState) : super(initialState) { on<LightForgotPasswordMethodInitialEvent>(_onInitialize); }

_onInitialize(LightForgotPasswordMethodInitialEvent event, Emitter<LightForgotPasswordMethodState> emit, ) async  {  } 
 }
