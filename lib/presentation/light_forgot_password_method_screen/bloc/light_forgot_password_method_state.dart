// ignore_for_file: must_be_immutable

part of 'light_forgot_password_method_bloc.dart';

/// Represents the state of LightForgotPasswordMethod in the application.
class LightForgotPasswordMethodState extends Equatable {
  LightForgotPasswordMethodState({this.lightForgotPasswordMethodModelObj});

  LightForgotPasswordMethodModel? lightForgotPasswordMethodModelObj;

  @override
  List<Object?> get props => [
        lightForgotPasswordMethodModelObj,
      ];
  LightForgotPasswordMethodState copyWith(
      {LightForgotPasswordMethodModel? lightForgotPasswordMethodModelObj}) {
    return LightForgotPasswordMethodState(
      lightForgotPasswordMethodModelObj: lightForgotPasswordMethodModelObj ??
          this.lightForgotPasswordMethodModelObj,
    );
  }
}
