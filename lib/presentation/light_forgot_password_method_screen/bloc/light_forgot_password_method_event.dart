// ignore_for_file: must_be_immutable

part of 'light_forgot_password_method_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightForgotPasswordMethod widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightForgotPasswordMethodEvent extends Equatable {}

/// Event that is dispatched when the LightForgotPasswordMethod widget is first created.
class LightForgotPasswordMethodInitialEvent
    extends LightForgotPasswordMethodEvent {
  @override
  List<Object?> get props => [];
}
