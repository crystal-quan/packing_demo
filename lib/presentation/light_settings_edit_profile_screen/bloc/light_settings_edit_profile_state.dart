// ignore_for_file: must_be_immutable

part of 'light_settings_edit_profile_bloc.dart';

/// Represents the state of LightSettingsEditProfile in the application.
class LightSettingsEditProfileState extends Equatable {
  LightSettingsEditProfileState({
    this.statusfilltypedController,
    this.statusfilltypedController1,
    this.emailController,
    this.statusfilltypepController,
    this.selectedDropDownValue,
    this.selectedDropDownValue1,
    this.lightSettingsEditProfileModelObj,
  });

  TextEditingController? statusfilltypedController;

  TextEditingController? statusfilltypedController1;

  TextEditingController? emailController;

  TextEditingController? statusfilltypepController;

  SelectionPopupModel? selectedDropDownValue;

  SelectionPopupModel? selectedDropDownValue1;

  LightSettingsEditProfileModel? lightSettingsEditProfileModelObj;

  @override
  List<Object?> get props => [
        statusfilltypedController,
        statusfilltypedController1,
        emailController,
        statusfilltypepController,
        selectedDropDownValue,
        selectedDropDownValue1,
        lightSettingsEditProfileModelObj,
      ];
  LightSettingsEditProfileState copyWith({
    TextEditingController? statusfilltypedController,
    TextEditingController? statusfilltypedController1,
    TextEditingController? emailController,
    TextEditingController? statusfilltypepController,
    SelectionPopupModel? selectedDropDownValue,
    SelectionPopupModel? selectedDropDownValue1,
    LightSettingsEditProfileModel? lightSettingsEditProfileModelObj,
  }) {
    return LightSettingsEditProfileState(
      statusfilltypedController:
          statusfilltypedController ?? this.statusfilltypedController,
      statusfilltypedController1:
          statusfilltypedController1 ?? this.statusfilltypedController1,
      emailController: emailController ?? this.emailController,
      statusfilltypepController:
          statusfilltypepController ?? this.statusfilltypepController,
      selectedDropDownValue:
          selectedDropDownValue ?? this.selectedDropDownValue,
      selectedDropDownValue1:
          selectedDropDownValue1 ?? this.selectedDropDownValue1,
      lightSettingsEditProfileModelObj: lightSettingsEditProfileModelObj ??
          this.lightSettingsEditProfileModelObj,
    );
  }
}
