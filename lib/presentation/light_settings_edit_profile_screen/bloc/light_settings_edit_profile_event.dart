// ignore_for_file: must_be_immutable

part of 'light_settings_edit_profile_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightSettingsEditProfile widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightSettingsEditProfileEvent extends Equatable {}

/// Event that is dispatched when the LightSettingsEditProfile widget is first created.
class LightSettingsEditProfileInitialEvent
    extends LightSettingsEditProfileEvent {
  @override
  List<Object?> get props => [];
}

///event for dropdown selection
class ChangeDropDownEvent extends LightSettingsEditProfileEvent {
  ChangeDropDownEvent({required this.value});

  SelectionPopupModel value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///event for dropdown selection
class ChangeDropDown1Event extends LightSettingsEditProfileEvent {
  ChangeDropDown1Event({required this.value});

  SelectionPopupModel value;

  @override
  List<Object?> get props => [
        value,
      ];
}
