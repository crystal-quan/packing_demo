// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'package:parking_app/data/models/selectionPopupModel/selection_popup_model.dart';/// This class defines the variables used in the [light_settings_edit_profile_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightSettingsEditProfileModel extends Equatable {LightSettingsEditProfileModel({this.dropdownItemList = const [], this.dropdownItemList1 = const [], });

List<SelectionPopupModel> dropdownItemList;

List<SelectionPopupModel> dropdownItemList1;

LightSettingsEditProfileModel copyWith({List<SelectionPopupModel>? dropdownItemList, List<SelectionPopupModel>? dropdownItemList1, }) { return LightSettingsEditProfileModel(
dropdownItemList : dropdownItemList ?? this.dropdownItemList,
dropdownItemList1 : dropdownItemList1 ?? this.dropdownItemList1,
); } 
@override List<Object?> get props => [dropdownItemList,dropdownItemList1];
 }
