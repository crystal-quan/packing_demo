// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'autolayoutverti3_item_model.dart';/// This class defines the variables used in the [light_my_parking_reservation_completed_page],
/// and is typically used to hold data that is passed between different parts of the application.
class LightMyParkingReservationCompletedModel extends Equatable {LightMyParkingReservationCompletedModel({this.autolayoutverti3ItemList = const []});

List<Autolayoutverti3ItemModel> autolayoutverti3ItemList;

LightMyParkingReservationCompletedModel copyWith({List<Autolayoutverti3ItemModel>? autolayoutverti3ItemList}) { return LightMyParkingReservationCompletedModel(
autolayoutverti3ItemList : autolayoutverti3ItemList ?? this.autolayoutverti3ItemList,
); } 
@override List<Object?> get props => [autolayoutverti3ItemList];
 }
