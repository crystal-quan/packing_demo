import '../light_my_parking_reservation_completed_page/widgets/autolayoutverti3_item_widget.dart';
import 'bloc/light_my_parking_reservation_completed_bloc.dart';
import 'models/autolayoutverti3_item_model.dart';
import 'models/light_my_parking_reservation_completed_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

// ignore_for_file: must_be_immutable
class LightMyParkingReservationCompletedPage extends StatefulWidget {
  const LightMyParkingReservationCompletedPage({Key? key})
      : super(
          key: key,
        );

  @override
  LightMyParkingReservationCompletedPageState createState() =>
      LightMyParkingReservationCompletedPageState();
  static Widget builder(BuildContext context) {
    return BlocProvider<LightMyParkingReservationCompletedBloc>(
      create: (context) => LightMyParkingReservationCompletedBloc(
          LightMyParkingReservationCompletedState(
        lightMyParkingReservationCompletedModelObj:
            LightMyParkingReservationCompletedModel(),
      ))
        ..add(LightMyParkingReservationCompletedInitialEvent()),
      child: LightMyParkingReservationCompletedPage(),
    );
  }
}

class LightMyParkingReservationCompletedPageState
    extends State<LightMyParkingReservationCompletedPage>
    with AutomaticKeepAliveClientMixin<LightMyParkingReservationCompletedPage> {
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SizedBox(
          width: double.maxFinite,
          child: Padding(
            padding: getPadding(
              left: 24,
              top: 24,
              right: 24,
            ),
            child: BlocSelector<
                LightMyParkingReservationCompletedBloc,
                LightMyParkingReservationCompletedState,
                LightMyParkingReservationCompletedModel?>(
              selector: (state) =>
                  state.lightMyParkingReservationCompletedModelObj,
              builder: (context, lightMyParkingReservationCompletedModelObj) {
                return ListView.separated(
                  physics: BouncingScrollPhysics(),
                  shrinkWrap: true,
                  separatorBuilder: (
                    context,
                    index,
                  ) {
                    return Padding(
                      padding: getPadding(
                        top: 10.0,
                        bottom: 10.0,
                      ),
                      child: SizedBox(
                        width: getHorizontalSize(
                          340,
                        ),
                        child: Divider(
                          height: getVerticalSize(
                            1,
                          ),
                          thickness: getVerticalSize(
                            1,
                          ),
                          color: appTheme.gray200,
                        ),
                      ),
                    );
                  },
                  itemCount: lightMyParkingReservationCompletedModelObj
                          ?.autolayoutverti3ItemList.length ??
                      0,
                  itemBuilder: (context, index) {
                    Autolayoutverti3ItemModel model =
                        lightMyParkingReservationCompletedModelObj
                                ?.autolayoutverti3ItemList[index] ??
                            Autolayoutverti3ItemModel();
                    return Autolayoutverti3ItemWidget(
                      model,
                    );
                  },
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
