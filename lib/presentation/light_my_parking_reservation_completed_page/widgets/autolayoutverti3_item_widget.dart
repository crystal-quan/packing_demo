import '../models/autolayoutverti3_item_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_outlined_button.dart';

// ignore: must_be_immutable
class Autolayoutverti3ItemWidget extends StatelessWidget {
  Autolayoutverti3ItemWidget(
    this.autolayoutverti3ItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  Autolayoutverti3ItemModel autolayoutverti3ItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: getPadding(
        all: 20,
      ),
      decoration: AppDecoration.outline4.copyWith(
        borderRadius: BorderRadiusStyle.roundedBorder16,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            children: [
              CustomImageView(
                imagePath: ImageConstant.imgRectangle9,
                height: getSize(
                  100,
                ),
                width: getSize(
                  100,
                ),
                radius: BorderRadius.circular(
                  getHorizontalSize(
                    10,
                  ),
                ),
              ),
              Padding(
                padding: getPadding(
                  left: 16,
                  top: 8,
                  bottom: 6,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      autolayoutverti3ItemModelObj.locationTxt,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                      style: theme.textTheme.titleLarge,
                    ),
                    Padding(
                      padding: getPadding(
                        top: 9,
                      ),
                      child: Text(
                        autolayoutverti3ItemModelObj.addressTxt,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: CustomTextStyles.bodyMediumGray700.copyWith(
                          letterSpacing: getHorizontalSize(
                            0.2,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: getPadding(
                        top: 11,
                      ),
                      child: Row(
                        children: [
                          Padding(
                            padding: getPadding(
                              top: 2,
                              bottom: 1,
                            ),
                            child: Text(
                              autolayoutverti3ItemModelObj.priceTxt,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.left,
                              style: CustomTextStyles.titleMediumIndigo60001
                                  .copyWith(
                                letterSpacing: getHorizontalSize(
                                  0.2,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: getPadding(
                              left: 4,
                              top: 8,
                              bottom: 3,
                            ),
                            child: Text(
                              autolayoutverti3ItemModelObj.durationTxt,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.left,
                              style: CustomTextStyles.bodySmallGray700.copyWith(
                                letterSpacing: getHorizontalSize(
                                  0.2,
                                ),
                              ),
                            ),
                          ),
                          CustomOutlinedButton(
                            text: "lbl_completed".tr,
                            margin: getMargin(
                              left: 15,
                            ),
                            buttonStyle: CustomButtonStyles.outlineGreen400
                                .copyWith(
                                    fixedSize:
                                        MaterialStateProperty.all<Size>(Size(
                              getHorizontalSize(
                                72,
                              ),
                              getVerticalSize(
                                24,
                              ),
                            ))),
                            buttonTextStyle:
                                CustomTextStyles.labelMediumGreen400,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          CustomOutlinedButton(
            text: "lbl_view_ticket".tr,
            margin: getMargin(
              top: 40,
            ),
            buttonStyle: CustomButtonStyles.outlineIndigo60001.copyWith(
                fixedSize: MaterialStateProperty.all<Size>(Size(
              double.maxFinite,
              getVerticalSize(
                38,
              ),
            ))),
            buttonTextStyle: CustomTextStyles.titleMediumIndigo60001SemiBold16,
          ),
        ],
      ),
    );
  }
}
