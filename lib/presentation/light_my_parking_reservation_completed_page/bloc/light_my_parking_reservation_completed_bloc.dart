import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/autolayoutverti3_item_model.dart';
import 'package:parking_app/presentation/light_my_parking_reservation_completed_page/models/light_my_parking_reservation_completed_model.dart';
part 'light_my_parking_reservation_completed_event.dart';
part 'light_my_parking_reservation_completed_state.dart';

/// A bloc that manages the state of a LightMyParkingReservationCompleted according to the event that is dispatched to it.
class LightMyParkingReservationCompletedBloc extends Bloc<
    LightMyParkingReservationCompletedEvent,
    LightMyParkingReservationCompletedState> {
  LightMyParkingReservationCompletedBloc(
      LightMyParkingReservationCompletedState initialState)
      : super(initialState) {
    on<LightMyParkingReservationCompletedInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightMyParkingReservationCompletedInitialEvent event,
    Emitter<LightMyParkingReservationCompletedState> emit,
  ) async {
    emit(state.copyWith(
        lightMyParkingReservationCompletedModelObj:
            state.lightMyParkingReservationCompletedModelObj?.copyWith(
      autolayoutverti3ItemList: fillAutolayoutverti3ItemList(),
    )));
  }

  List<Autolayoutverti3ItemModel> fillAutolayoutverti3ItemList() {
    return List.generate(3, (index) => Autolayoutverti3ItemModel());
  }
}
