// ignore_for_file: must_be_immutable

part of 'light_my_parking_reservation_completed_bloc.dart';

/// Represents the state of LightMyParkingReservationCompleted in the application.
class LightMyParkingReservationCompletedState extends Equatable {
  LightMyParkingReservationCompletedState(
      {this.lightMyParkingReservationCompletedModelObj});

  LightMyParkingReservationCompletedModel?
      lightMyParkingReservationCompletedModelObj;

  @override
  List<Object?> get props => [
        lightMyParkingReservationCompletedModelObj,
      ];
  LightMyParkingReservationCompletedState copyWith(
      {LightMyParkingReservationCompletedModel?
          lightMyParkingReservationCompletedModelObj}) {
    return LightMyParkingReservationCompletedState(
      lightMyParkingReservationCompletedModelObj:
          lightMyParkingReservationCompletedModelObj ??
              this.lightMyParkingReservationCompletedModelObj,
    );
  }
}
