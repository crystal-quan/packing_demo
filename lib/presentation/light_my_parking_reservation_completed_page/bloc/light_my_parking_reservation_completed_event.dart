// ignore_for_file: must_be_immutable

part of 'light_my_parking_reservation_completed_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightMyParkingReservationCompleted widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightMyParkingReservationCompletedEvent extends Equatable {}

/// Event that is dispatched when the LightMyParkingReservationCompleted widget is first created.
class LightMyParkingReservationCompletedInitialEvent
    extends LightMyParkingReservationCompletedEvent {
  @override
  List<Object?> get props => [];
}
