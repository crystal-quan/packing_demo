import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:parking_app/presentation/light_my_parking_reservation_refund_successful_dialog/models/light_my_parking_reservation_refund_successful_model.dart';
part 'light_my_parking_reservation_refund_successful_event.dart';
part 'light_my_parking_reservation_refund_successful_state.dart';

/// A bloc that manages the state of a LightMyParkingReservationRefundSuccessful according to the event that is dispatched to it.
class LightMyParkingReservationRefundSuccessfulBloc extends Bloc<
    LightMyParkingReservationRefundSuccessfulEvent,
    LightMyParkingReservationRefundSuccessfulState> {
  LightMyParkingReservationRefundSuccessfulBloc(
      LightMyParkingReservationRefundSuccessfulState initialState)
      : super(initialState) {
    on<LightMyParkingReservationRefundSuccessfulInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightMyParkingReservationRefundSuccessfulInitialEvent event,
    Emitter<LightMyParkingReservationRefundSuccessfulState> emit,
  ) async {}
}
