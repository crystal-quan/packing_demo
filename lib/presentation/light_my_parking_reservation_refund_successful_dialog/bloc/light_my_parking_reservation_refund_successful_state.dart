// ignore_for_file: must_be_immutable

part of 'light_my_parking_reservation_refund_successful_bloc.dart';

/// Represents the state of LightMyParkingReservationRefundSuccessful in the application.
class LightMyParkingReservationRefundSuccessfulState extends Equatable {
  LightMyParkingReservationRefundSuccessfulState(
      {this.lightMyParkingReservationRefundSuccessfulModelObj});

  LightMyParkingReservationRefundSuccessfulModel?
      lightMyParkingReservationRefundSuccessfulModelObj;

  @override
  List<Object?> get props => [
        lightMyParkingReservationRefundSuccessfulModelObj,
      ];
  LightMyParkingReservationRefundSuccessfulState copyWith(
      {LightMyParkingReservationRefundSuccessfulModel?
          lightMyParkingReservationRefundSuccessfulModelObj}) {
    return LightMyParkingReservationRefundSuccessfulState(
      lightMyParkingReservationRefundSuccessfulModelObj:
          lightMyParkingReservationRefundSuccessfulModelObj ??
              this.lightMyParkingReservationRefundSuccessfulModelObj,
    );
  }
}
