import 'bloc/light_my_parking_reservation_refund_successful_bloc.dart';
import 'models/light_my_parking_reservation_refund_successful_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_elevated_button.dart';

// ignore_for_file: must_be_immutable
class LightMyParkingReservationRefundSuccessfulDialog extends StatelessWidget {
  const LightMyParkingReservationRefundSuccessfulDialog({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LightMyParkingReservationRefundSuccessfulBloc>(
      create: (context) => LightMyParkingReservationRefundSuccessfulBloc(
          LightMyParkingReservationRefundSuccessfulState(
        lightMyParkingReservationRefundSuccessfulModelObj:
            LightMyParkingReservationRefundSuccessfulModel(),
      ))
        ..add(LightMyParkingReservationRefundSuccessfulInitialEvent()),
      child: LightMyParkingReservationRefundSuccessfulDialog(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return Container(
      width: getHorizontalSize(
        340,
      ),
      padding: getPadding(
        all: 32,
      ),
      decoration: AppDecoration.fill.copyWith(
        borderRadius: BorderRadiusStyle.roundedBorder24,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: getVerticalSize(
              342,
            ),
            width: getHorizontalSize(
              268,
            ),
            child: Stack(
              alignment: Alignment.topCenter,
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "lbl_successfull".tr,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: CustomTextStyles.headlineSmallIndigo60001,
                      ),
                      Container(
                        width: getHorizontalSize(
                          268,
                        ),
                        margin: getMargin(
                          top: 17,
                        ),
                        child: Text(
                          "msg_you_have_successfully".tr,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: CustomTextStyles.bodyLarge18.copyWith(
                            letterSpacing: getHorizontalSize(
                              0.2,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                CustomImageView(
                  imagePath: ImageConstant.imgCardrivingbro256x256,
                  height: getSize(
                    256,
                  ),
                  width: getSize(
                    256,
                  ),
                  alignment: Alignment.topCenter,
                ),
              ],
            ),
          ),
          CustomElevatedButton(
            width: getHorizontalSize(
              276,
            ),
            height: getVerticalSize(
              58,
            ),
            text: "lbl_ok".tr,
            margin: getMargin(
              top: 29,
            ),
            buttonStyle: CustomButtonStyles.outlineDeeppurple100TL101.copyWith(
                fixedSize: MaterialStateProperty.all<Size>(Size(
              double.maxFinite,
              getVerticalSize(
                58,
              ),
            ))),
            decoration: CustomButtonStyles.outlineDeeppurple100TL101Decoration,
            buttonTextStyle: CustomTextStyles.titleMediumWhiteA7000116,
          ),
        ],
      ),
    );
  }
}
