// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_my_parking_reservation_refund_successful_dialog],
/// and is typically used to hold data that is passed between different parts of the application.
class LightMyParkingReservationRefundSuccessfulModel extends Equatable {LightMyParkingReservationRefundSuccessfulModel copyWith() { return LightMyParkingReservationRefundSuccessfulModel(
); } 
@override List<Object?> get props => [];
 }
