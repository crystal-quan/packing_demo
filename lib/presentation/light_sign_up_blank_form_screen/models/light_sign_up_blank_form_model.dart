// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_sign_up_blank_form_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightSignUpBlankFormModel extends Equatable {LightSignUpBlankFormModel copyWith() { return LightSignUpBlankFormModel(
); } 
@override List<Object?> get props => [];
 }
