// ignore_for_file: must_be_immutable

part of 'light_sign_up_blank_form_bloc.dart';

/// Represents the state of LightSignUpBlankForm in the application.
class LightSignUpBlankFormState extends Equatable {
  LightSignUpBlankFormState({
    this.emailController,
    this.passwordController,
    this.isShowPassword = true,
    this.isCheckbox = false,
    this.lightSignUpBlankFormModelObj,
  });

  TextEditingController? emailController;

  TextEditingController? passwordController;

  LightSignUpBlankFormModel? lightSignUpBlankFormModelObj;

  bool isShowPassword;

  bool isCheckbox;

  @override
  List<Object?> get props => [
        emailController,
        passwordController,
        isShowPassword,
        isCheckbox,
        lightSignUpBlankFormModelObj,
      ];
  LightSignUpBlankFormState copyWith({
    TextEditingController? emailController,
    TextEditingController? passwordController,
    bool? isShowPassword,
    bool? isCheckbox,
    LightSignUpBlankFormModel? lightSignUpBlankFormModelObj,
  }) {
    return LightSignUpBlankFormState(
      emailController: emailController ?? this.emailController,
      passwordController: passwordController ?? this.passwordController,
      isShowPassword: isShowPassword ?? this.isShowPassword,
      isCheckbox: isCheckbox ?? this.isCheckbox,
      lightSignUpBlankFormModelObj:
          lightSignUpBlankFormModelObj ?? this.lightSignUpBlankFormModelObj,
    );
  }
}
