import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_sign_up_blank_form_screen/models/light_sign_up_blank_form_model.dart';part 'light_sign_up_blank_form_event.dart';part 'light_sign_up_blank_form_state.dart';/// A bloc that manages the state of a LightSignUpBlankForm according to the event that is dispatched to it.
class LightSignUpBlankFormBloc extends Bloc<LightSignUpBlankFormEvent, LightSignUpBlankFormState> {LightSignUpBlankFormBloc(LightSignUpBlankFormState initialState) : super(initialState) { on<LightSignUpBlankFormInitialEvent>(_onInitialize); on<ChangePasswordVisibilityEvent>(_changePasswordVisibility); on<ChangeCheckBoxEvent>(_changeCheckBox); }

_changePasswordVisibility(ChangePasswordVisibilityEvent event, Emitter<LightSignUpBlankFormState> emit, ) { emit(state.copyWith(isShowPassword: event.value)); } 
_changeCheckBox(ChangeCheckBoxEvent event, Emitter<LightSignUpBlankFormState> emit, ) { emit(state.copyWith(isCheckbox: event.value)); } 
_onInitialize(LightSignUpBlankFormInitialEvent event, Emitter<LightSignUpBlankFormState> emit, ) async  { emit(state.copyWith(emailController: TextEditingController(), passwordController: TextEditingController(), isShowPassword: true, isCheckbox: false)); } 
 }
