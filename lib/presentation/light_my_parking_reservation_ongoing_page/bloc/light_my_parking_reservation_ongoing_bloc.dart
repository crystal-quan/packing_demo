import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/autolayoutverti4_item_model.dart';
import 'package:parking_app/presentation/light_my_parking_reservation_ongoing_page/models/light_my_parking_reservation_ongoing_model.dart';
part 'light_my_parking_reservation_ongoing_event.dart';
part 'light_my_parking_reservation_ongoing_state.dart';

/// A bloc that manages the state of a LightMyParkingReservationOngoing according to the event that is dispatched to it.
class LightMyParkingReservationOngoingBloc extends Bloc<
    LightMyParkingReservationOngoingEvent,
    LightMyParkingReservationOngoingState> {
  LightMyParkingReservationOngoingBloc(
      LightMyParkingReservationOngoingState initialState)
      : super(initialState) {
    on<LightMyParkingReservationOngoingInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightMyParkingReservationOngoingInitialEvent event,
    Emitter<LightMyParkingReservationOngoingState> emit,
  ) async {
    emit(state.copyWith(
        lightMyParkingReservationOngoingModelObj:
            state.lightMyParkingReservationOngoingModelObj?.copyWith(
      autolayoutverti4ItemList: fillAutolayoutverti4ItemList(),
    )));
  }

  List<Autolayoutverti4ItemModel> fillAutolayoutverti4ItemList() {
    return List.generate(2, (index) => Autolayoutverti4ItemModel());
  }
}
