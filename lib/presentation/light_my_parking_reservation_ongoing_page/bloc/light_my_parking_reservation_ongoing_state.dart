// ignore_for_file: must_be_immutable

part of 'light_my_parking_reservation_ongoing_bloc.dart';

/// Represents the state of LightMyParkingReservationOngoing in the application.
class LightMyParkingReservationOngoingState extends Equatable {
  LightMyParkingReservationOngoingState(
      {this.lightMyParkingReservationOngoingModelObj});

  LightMyParkingReservationOngoingModel?
      lightMyParkingReservationOngoingModelObj;

  @override
  List<Object?> get props => [
        lightMyParkingReservationOngoingModelObj,
      ];
  LightMyParkingReservationOngoingState copyWith(
      {LightMyParkingReservationOngoingModel?
          lightMyParkingReservationOngoingModelObj}) {
    return LightMyParkingReservationOngoingState(
      lightMyParkingReservationOngoingModelObj:
          lightMyParkingReservationOngoingModelObj ??
              this.lightMyParkingReservationOngoingModelObj,
    );
  }
}
