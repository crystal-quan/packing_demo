// ignore_for_file: must_be_immutable

part of 'light_my_parking_reservation_ongoing_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightMyParkingReservationOngoing widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightMyParkingReservationOngoingEvent extends Equatable {}

/// Event that is dispatched when the LightMyParkingReservationOngoing widget is first created.
class LightMyParkingReservationOngoingInitialEvent
    extends LightMyParkingReservationOngoingEvent {
  @override
  List<Object?> get props => [];
}
