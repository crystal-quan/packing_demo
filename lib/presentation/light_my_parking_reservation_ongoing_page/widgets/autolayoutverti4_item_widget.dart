import '../models/autolayoutverti4_item_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_elevated_button.dart';
import 'package:parking_app/widgets/custom_outlined_button.dart';

// ignore: must_be_immutable
class Autolayoutverti4ItemWidget extends StatelessWidget {
  Autolayoutverti4ItemWidget(
    this.autolayoutverti4ItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  Autolayoutverti4ItemModel autolayoutverti4ItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: getPadding(
        all: 20,
      ),
      decoration: AppDecoration.outline4.copyWith(
        borderRadius: BorderRadiusStyle.roundedBorder16,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            children: [
              CustomImageView(
                imagePath: ImageConstant.imgRectangle12,
                height: getSize(
                  100,
                ),
                width: getSize(
                  100,
                ),
                radius: BorderRadius.circular(
                  getHorizontalSize(
                    10,
                  ),
                ),
              ),
              Padding(
                padding: getPadding(
                  left: 16,
                  top: 5,
                  bottom: 6,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      autolayoutverti4ItemModelObj.nameTxt,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                      style: theme.textTheme.titleLarge,
                    ),
                    Padding(
                      padding: getPadding(
                        top: 12,
                      ),
                      child: Text(
                        autolayoutverti4ItemModelObj.addressTxt,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: CustomTextStyles.bodyMediumGray700.copyWith(
                          letterSpacing: getHorizontalSize(
                            0.2,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: getPadding(
                        top: 11,
                      ),
                      child: Row(
                        children: [
                          Padding(
                            padding: getPadding(
                              top: 2,
                              bottom: 1,
                            ),
                            child: Text(
                              autolayoutverti4ItemModelObj.priceTxt,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.left,
                              style: CustomTextStyles.titleMediumIndigo60001
                                  .copyWith(
                                letterSpacing: getHorizontalSize(
                                  0.2,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: getPadding(
                              left: 4,
                              top: 8,
                              bottom: 3,
                            ),
                            child: Text(
                              autolayoutverti4ItemModelObj.durationTxt,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.left,
                              style: CustomTextStyles.bodySmallGray700.copyWith(
                                letterSpacing: getHorizontalSize(
                                  0.2,
                                ),
                              ),
                            ),
                          ),
                          CustomElevatedButton(
                            text: "lbl_now_active".tr,
                            margin: getMargin(
                              left: 15,
                            ),
                            buttonStyle: CustomButtonStyles.fillIndigo60001TL6
                                .copyWith(
                                    fixedSize:
                                        MaterialStateProperty.all<Size>(Size(
                              getHorizontalSize(
                                73,
                              ),
                              getVerticalSize(
                                24,
                              ),
                            ))),
                            buttonTextStyle:
                                CustomTextStyles.labelMediumWhiteA70001,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Padding(
            padding: getPadding(
              top: 40,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: CustomOutlinedButton(
                    text: "lbl_view_timer".tr,
                    margin: getMargin(
                      right: 6,
                    ),
                    buttonStyle: CustomButtonStyles.outlineIndigo60001.copyWith(
                        fixedSize: MaterialStateProperty.all<Size>(Size(
                      double.maxFinite,
                      getVerticalSize(
                        38,
                      ),
                    ))),
                    buttonTextStyle:
                        CustomTextStyles.titleMediumIndigo60001SemiBold16,
                  ),
                ),
                Expanded(
                  child: CustomElevatedButton(
                    width: getHorizontalSize(
                      164,
                    ),
                    height: getVerticalSize(
                      38,
                    ),
                    text: "lbl_view_ticket".tr,
                    margin: getMargin(
                      left: 6,
                    ),
                    buttonStyle: CustomButtonStyles
                        .gradientnameprimarynameindigoA100
                        .copyWith(
                            fixedSize: MaterialStateProperty.all<Size>(Size(
                      double.maxFinite,
                      getVerticalSize(
                        38,
                      ),
                    ))),
                    decoration: CustomButtonStyles
                        .gradientnameprimarynameindigoA100Decoration,
                    buttonTextStyle:
                        CustomTextStyles.titleMediumWhiteA70001SemiBold,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
