import '../light_my_parking_reservation_ongoing_page/widgets/autolayoutverti4_item_widget.dart';
import 'bloc/light_my_parking_reservation_ongoing_bloc.dart';
import 'models/autolayoutverti4_item_model.dart';
import 'models/light_my_parking_reservation_ongoing_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

// ignore_for_file: must_be_immutable
class LightMyParkingReservationOngoingPage extends StatefulWidget {
  const LightMyParkingReservationOngoingPage({Key? key})
      : super(
          key: key,
        );

  @override
  LightMyParkingReservationOngoingPageState createState() =>
      LightMyParkingReservationOngoingPageState();
  static Widget builder(BuildContext context) {
    return BlocProvider<LightMyParkingReservationOngoingBloc>(
      create: (context) => LightMyParkingReservationOngoingBloc(
          LightMyParkingReservationOngoingState(
        lightMyParkingReservationOngoingModelObj:
            LightMyParkingReservationOngoingModel(),
      ))
        ..add(LightMyParkingReservationOngoingInitialEvent()),
      child: LightMyParkingReservationOngoingPage(),
    );
  }
}

class LightMyParkingReservationOngoingPageState
    extends State<LightMyParkingReservationOngoingPage>
    with AutomaticKeepAliveClientMixin<LightMyParkingReservationOngoingPage> {
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SizedBox(
          width: double.maxFinite,
          child: Padding(
            padding: getPadding(
              left: 24,
              top: 24,
              right: 24,
            ),
            child: BlocSelector<
                LightMyParkingReservationOngoingBloc,
                LightMyParkingReservationOngoingState,
                LightMyParkingReservationOngoingModel?>(
              selector: (state) =>
                  state.lightMyParkingReservationOngoingModelObj,
              builder: (context, lightMyParkingReservationOngoingModelObj) {
                return ListView.separated(
                  physics: BouncingScrollPhysics(),
                  shrinkWrap: true,
                  separatorBuilder: (
                    context,
                    index,
                  ) {
                    return Padding(
                      padding: getPadding(
                        top: 10.0,
                        bottom: 10.0,
                      ),
                      child: SizedBox(
                        width: getHorizontalSize(
                          340,
                        ),
                        child: Divider(
                          height: getVerticalSize(
                            1,
                          ),
                          thickness: getVerticalSize(
                            1,
                          ),
                          color: appTheme.gray200,
                        ),
                      ),
                    );
                  },
                  itemCount: lightMyParkingReservationOngoingModelObj
                          ?.autolayoutverti4ItemList.length ??
                      0,
                  itemBuilder: (context, index) {
                    Autolayoutverti4ItemModel model =
                        lightMyParkingReservationOngoingModelObj
                                ?.autolayoutverti4ItemList[index] ??
                            Autolayoutverti4ItemModel();
                    return Autolayoutverti4ItemWidget(
                      model,
                    );
                  },
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
