// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'autolayoutverti4_item_model.dart';/// This class defines the variables used in the [light_my_parking_reservation_ongoing_page],
/// and is typically used to hold data that is passed between different parts of the application.
class LightMyParkingReservationOngoingModel extends Equatable {LightMyParkingReservationOngoingModel({this.autolayoutverti4ItemList = const []});

List<Autolayoutverti4ItemModel> autolayoutverti4ItemList;

LightMyParkingReservationOngoingModel copyWith({List<Autolayoutverti4ItemModel>? autolayoutverti4ItemList}) { return LightMyParkingReservationOngoingModel(
autolayoutverti4ItemList : autolayoutverti4ItemList ?? this.autolayoutverti4ItemList,
); } 
@override List<Object?> get props => [autolayoutverti4ItemList];
 }
