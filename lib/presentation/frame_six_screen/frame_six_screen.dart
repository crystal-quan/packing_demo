import 'bloc/frame_six_bloc.dart';
import 'models/frame_six_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

class FrameSixScreen extends StatelessWidget {
  const FrameSixScreen({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<FrameSixBloc>(
      create: (context) => FrameSixBloc(FrameSixState(
        frameSixModelObj: FrameSixModel(),
      ))
        ..add(FrameSixInitialEvent()),
      child: FrameSixScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return BlocBuilder<FrameSixBloc, FrameSixState>(
      builder: (context, state) {
        return SafeArea(
          child: Scaffold(
            backgroundColor: appTheme.whiteA70001,
            body: SizedBox(
              height: getSize(
                600,
              ),
              width: getSize(
                600,
              ),
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  CustomImageView(
                    imagePath: ImageConstant.imgRectangle140,
                    height: getVerticalSize(
                      622,
                    ),
                    width: getHorizontalSize(
                      600,
                    ),
                    alignment: Alignment.center,
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: SizedBox(
                      height: getVerticalSize(
                        853,
                      ),
                      width: getHorizontalSize(
                        600,
                      ),
                      child: Stack(
                        alignment: Alignment.topCenter,
                        children: [
                          CustomImageView(
                            imagePath: ImageConstant.imgRectangle31,
                            height: getVerticalSize(
                              441,
                            ),
                            width: getHorizontalSize(
                              219,
                            ),
                            radius: BorderRadius.circular(
                              getHorizontalSize(
                                40,
                              ),
                            ),
                            alignment: Alignment.bottomLeft,
                            margin: getMargin(
                              left: 178,
                            ),
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: SizedBox(
                              height: getVerticalSize(
                                759,
                              ),
                              width: getHorizontalSize(
                                600,
                              ),
                              child: Stack(
                                alignment: Alignment.topLeft,
                                children: [
                                  CustomImageView(
                                    imagePath: ImageConstant.imgRectangle13,
                                    height: getVerticalSize(
                                      277,
                                    ),
                                    width: getHorizontalSize(
                                      142,
                                    ),
                                    radius: BorderRadius.circular(
                                      getHorizontalSize(
                                        40,
                                      ),
                                    ),
                                    alignment: Alignment.topLeft,
                                  ),
                                  CustomImageView(
                                    imagePath: ImageConstant.imgRectangle30,
                                    height: getVerticalSize(
                                      349,
                                    ),
                                    width: getHorizontalSize(
                                      219,
                                    ),
                                    radius: BorderRadius.circular(
                                      getHorizontalSize(
                                        40,
                                      ),
                                    ),
                                    alignment: Alignment.topLeft,
                                    margin: getMargin(
                                      left: 178,
                                    ),
                                  ),
                                  CustomImageView(
                                    imagePath: ImageConstant.imgRectangle34,
                                    height: getVerticalSize(
                                      218,
                                    ),
                                    width: getHorizontalSize(
                                      166,
                                    ),
                                    radius: BorderRadius.circular(
                                      getHorizontalSize(
                                        40,
                                      ),
                                    ),
                                    alignment: Alignment.topRight,
                                  ),
                                  CustomImageView(
                                    imagePath: ImageConstant.imgRectangle15,
                                    height: getVerticalSize(
                                      440,
                                    ),
                                    width: getHorizontalSize(
                                      142,
                                    ),
                                    radius: BorderRadius.circular(
                                      getHorizontalSize(
                                        40,
                                      ),
                                    ),
                                    alignment: Alignment.bottomLeft,
                                  ),
                                  CustomImageView(
                                    imagePath: ImageConstant.imgRectangle35,
                                    height: getVerticalSize(
                                      440,
                                    ),
                                    width: getHorizontalSize(
                                      166,
                                    ),
                                    radius: BorderRadius.circular(
                                      getHorizontalSize(
                                        40,
                                      ),
                                    ),
                                    alignment: Alignment.bottomRight,
                                    margin: getMargin(
                                      bottom: 59,
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      margin: getMargin(
                                        left: 63,
                                        top: 227,
                                      ),
                                      padding: getPadding(
                                        left: 33,
                                        top: 11,
                                        right: 33,
                                        bottom: 11,
                                      ),
                                      decoration:
                                          AppDecoration.outline.copyWith(
                                        borderRadius:
                                            BorderRadiusStyle.roundedBorder10,
                                      ),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            width: getHorizontalSize(
                                              320,
                                            ),
                                            margin: getMargin(
                                              right: 17,
                                            ),
                                            child: RichText(
                                              text: TextSpan(
                                                children: [
                                                  TextSpan(
                                                    text: "\n".tr,
                                                    style: CustomTextStyles
                                                        .displayMediumMontserratBluegray900,
                                                  ),
                                                  TextSpan(
                                                    text:
                                                        "msg_download_more_free2"
                                                            .tr,
                                                    style: CustomTextStyles
                                                        .displayMediumMontserratIndigo60001,
                                                  ),
                                                ],
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                          Padding(
                                            padding: getPadding(
                                              bottom: 18,
                                            ),
                                            child: Text(
                                              "msg_zeeuiux_gumroad_com".tr,
                                              overflow: TextOverflow.ellipsis,
                                              textAlign: TextAlign.left,
                                              style: CustomTextStyles
                                                  .headlineLargeMontserratOnPrimary
                                                  .copyWith(
                                                decoration:
                                                    TextDecoration.underline,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
