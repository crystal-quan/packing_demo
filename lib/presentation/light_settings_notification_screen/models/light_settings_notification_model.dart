// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_settings_notification_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightSettingsNotificationModel extends Equatable {LightSettingsNotificationModel copyWith() { return LightSettingsNotificationModel(
); } 
@override List<Object?> get props => [];
 }
