// ignore_for_file: must_be_immutable

part of 'light_settings_notification_bloc.dart';

/// Represents the state of LightSettingsNotification in the application.
class LightSettingsNotificationState extends Equatable {
  LightSettingsNotificationState({this.lightSettingsNotificationModelObj});

  LightSettingsNotificationModel? lightSettingsNotificationModelObj;

  @override
  List<Object?> get props => [
        lightSettingsNotificationModelObj,
      ];
  LightSettingsNotificationState copyWith(
      {LightSettingsNotificationModel? lightSettingsNotificationModelObj}) {
    return LightSettingsNotificationState(
      lightSettingsNotificationModelObj: lightSettingsNotificationModelObj ??
          this.lightSettingsNotificationModelObj,
    );
  }
}
