import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_settings_notification_screen/models/light_settings_notification_model.dart';part 'light_settings_notification_event.dart';part 'light_settings_notification_state.dart';/// A bloc that manages the state of a LightSettingsNotification according to the event that is dispatched to it.
class LightSettingsNotificationBloc extends Bloc<LightSettingsNotificationEvent, LightSettingsNotificationState> {LightSettingsNotificationBloc(LightSettingsNotificationState initialState) : super(initialState) { on<LightSettingsNotificationInitialEvent>(_onInitialize); }

_onInitialize(LightSettingsNotificationInitialEvent event, Emitter<LightSettingsNotificationState> emit, ) async  {  } 
 }
