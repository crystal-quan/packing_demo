// ignore_for_file: must_be_immutable

part of 'light_settings_notification_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightSettingsNotification widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightSettingsNotificationEvent extends Equatable {}

/// Event that is dispatched when the LightSettingsNotification widget is first created.
class LightSettingsNotificationInitialEvent
    extends LightSettingsNotificationEvent {
  @override
  List<Object?> get props => [];
}
