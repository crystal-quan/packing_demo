// ignore_for_file: must_be_immutable

part of 'light_parking_confirm_payment_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightParkingConfirmPayment widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightParkingConfirmPaymentEvent extends Equatable {}

/// Event that is dispatched when the LightParkingConfirmPayment widget is first created.
class LightParkingConfirmPaymentInitialEvent
    extends LightParkingConfirmPaymentEvent {
  @override
  List<Object?> get props => [];
}
