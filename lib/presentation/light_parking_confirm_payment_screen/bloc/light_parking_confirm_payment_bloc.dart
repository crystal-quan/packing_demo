import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_parking_confirm_payment_screen/models/light_parking_confirm_payment_model.dart';part 'light_parking_confirm_payment_event.dart';part 'light_parking_confirm_payment_state.dart';/// A bloc that manages the state of a LightParkingConfirmPayment according to the event that is dispatched to it.
class LightParkingConfirmPaymentBloc extends Bloc<LightParkingConfirmPaymentEvent, LightParkingConfirmPaymentState> {LightParkingConfirmPaymentBloc(LightParkingConfirmPaymentState initialState) : super(initialState) { on<LightParkingConfirmPaymentInitialEvent>(_onInitialize); }

_onInitialize(LightParkingConfirmPaymentInitialEvent event, Emitter<LightParkingConfirmPaymentState> emit, ) async  {  } 
 }
