// ignore_for_file: must_be_immutable

part of 'light_parking_confirm_payment_bloc.dart';

/// Represents the state of LightParkingConfirmPayment in the application.
class LightParkingConfirmPaymentState extends Equatable {
  LightParkingConfirmPaymentState({this.lightParkingConfirmPaymentModelObj});

  LightParkingConfirmPaymentModel? lightParkingConfirmPaymentModelObj;

  @override
  List<Object?> get props => [
        lightParkingConfirmPaymentModelObj,
      ];
  LightParkingConfirmPaymentState copyWith(
      {LightParkingConfirmPaymentModel? lightParkingConfirmPaymentModelObj}) {
    return LightParkingConfirmPaymentState(
      lightParkingConfirmPaymentModelObj: lightParkingConfirmPaymentModelObj ??
          this.lightParkingConfirmPaymentModelObj,
    );
  }
}
