// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'autolayouthoriz1_item_model.dart';/// This class defines the variables used in the [light_home_search_filter_bottomsheet],
/// and is typically used to hold data that is passed between different parts of the application.
class LightHomeSearchFilterModel extends Equatable {LightHomeSearchFilterModel({this.autolayouthoriz1ItemList = const []});

List<Autolayouthoriz1ItemModel> autolayouthoriz1ItemList;

LightHomeSearchFilterModel copyWith({List<Autolayouthoriz1ItemModel>? autolayouthoriz1ItemList}) { return LightHomeSearchFilterModel(
autolayouthoriz1ItemList : autolayouthoriz1ItemList ?? this.autolayouthoriz1ItemList,
); } 
@override List<Object?> get props => [autolayouthoriz1ItemList];
 }
