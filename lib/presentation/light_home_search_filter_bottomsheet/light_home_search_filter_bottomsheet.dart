import '../light_home_search_filter_bottomsheet/widgets/autolayouthoriz1_item_widget.dart';
import 'bloc/light_home_search_filter_bloc.dart';
import 'models/autolayouthoriz1_item_model.dart';
import 'models/light_home_search_filter_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_elevated_button.dart';

// ignore_for_file: must_be_immutable
class LightHomeSearchFilterBottomsheet extends StatelessWidget {
  const LightHomeSearchFilterBottomsheet({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LightHomeSearchFilterBloc>(
      create: (context) => LightHomeSearchFilterBloc(LightHomeSearchFilterState(
        lightHomeSearchFilterModelObj: LightHomeSearchFilterModel(),
      ))
        ..add(LightHomeSearchFilterInitialEvent()),
      child: LightHomeSearchFilterBottomsheet(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SingleChildScrollView(
      child: Container(
        width: double.maxFinite,
        padding: getPadding(
          top: 8,
          bottom: 8,
        ),
        decoration: AppDecoration.outline5.copyWith(
          borderRadius: BorderRadiusStyle.customBorderTL40,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            CustomImageView(
              svgPath: ImageConstant.imgFrameGray30001,
              height: getVerticalSize(
                3,
              ),
              width: getHorizontalSize(
                38,
              ),
            ),
            Padding(
              padding: getPadding(
                top: 23,
              ),
              child: Text(
                "lbl_filter".tr,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.left,
                style: theme.textTheme.headlineSmall,
              ),
            ),
            Padding(
              padding: getPadding(
                top: 24,
              ),
              child: Divider(
                height: getVerticalSize(
                  1,
                ),
                thickness: getVerticalSize(
                  1,
                ),
                color: appTheme.gray200,
                indent: getHorizontalSize(
                  24,
                ),
                endIndent: getHorizontalSize(
                  24,
                ),
              ),
            ),
            Padding(
              padding: getPadding(
                left: 24,
                top: 24,
                right: 24,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "lbl_sort_by".tr,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.left,
                    style: theme.textTheme.titleMedium,
                  ),
                  Padding(
                    padding: getPadding(
                      bottom: 2,
                    ),
                    child: Text(
                      "lbl_see_all".tr,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                      style: CustomTextStyles.titleMediumIndigo60001.copyWith(
                        letterSpacing: getHorizontalSize(
                          0.2,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: SizedBox(
                height: getVerticalSize(
                  56,
                ),
                child: BlocSelector<LightHomeSearchFilterBloc,
                    LightHomeSearchFilterState, LightHomeSearchFilterModel?>(
                  selector: (state) => state.lightHomeSearchFilterModelObj,
                  builder: (context, lightHomeSearchFilterModelObj) {
                    return ListView.separated(
                      padding: getPadding(
                        left: 24,
                        top: 18,
                      ),
                      scrollDirection: Axis.horizontal,
                      separatorBuilder: (
                        context,
                        index,
                      ) {
                        return SizedBox(
                          width: getHorizontalSize(
                            12,
                          ),
                        );
                      },
                      itemCount: lightHomeSearchFilterModelObj
                              ?.autolayouthoriz1ItemList.length ??
                          0,
                      itemBuilder: (context, index) {
                        Autolayouthoriz1ItemModel model =
                            lightHomeSearchFilterModelObj
                                    ?.autolayouthoriz1ItemList[index] ??
                                Autolayouthoriz1ItemModel();
                        return Autolayouthoriz1ItemWidget(
                          model,
                        );
                      },
                    );
                  },
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: getPadding(
                  left: 24,
                  top: 23,
                ),
                child: Text(
                  "lbl_distance".tr,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.left,
                  style: theme.textTheme.titleMedium,
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                height: getVerticalSize(
                  25,
                ),
                width: getHorizontalSize(
                  46,
                ),
                margin: getMargin(
                  top: 20,
                ),
                child: Stack(
                  alignment: Alignment.topCenter,
                  children: [
                    CustomImageView(
                      svgPath: ImageConstant.imgVector,
                      height: getVerticalSize(
                        25,
                      ),
                      width: getHorizontalSize(
                        46,
                      ),
                      alignment: Alignment.center,
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Padding(
                        padding: getPadding(
                          top: 3,
                        ),
                        child: Text(
                          "lbl_10_km".tr,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.left,
                          style: theme.textTheme.labelLarge!.copyWith(
                            letterSpacing: getHorizontalSize(
                              0.2,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: getPadding(
                left: 24,
                top: 2,
                right: 24,
              ),
              child: SliderTheme(
                data: SliderThemeData(
                  trackShape: RoundedRectSliderTrackShape(),
                  activeTrackColor: appTheme.indigo60001,
                  inactiveTrackColor: appTheme.gray200,
                  thumbColor: appTheme.whiteA70001,
                  thumbShape: RoundSliderThumbShape(),
                ),
                child: Slider(
                  value: 47.37,
                  min: 0.0,
                  max: 100.0,
                  onChanged: (value) {},
                ),
              ),
            ),
            Padding(
              padding: getPadding(
                left: 24,
                top: 24,
                right: 24,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: getPadding(
                      top: 1,
                    ),
                    child: Text(
                      "lbl_valet_parking".tr,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                      style: theme.textTheme.titleMedium,
                    ),
                  ),
                  Container(
                    decoration: AppDecoration.fill8.copyWith(
                      borderRadius: BorderRadiusStyle.roundedBorder10,
                    ),
                    child: Container(
                      height: getSize(
                        24,
                      ),
                      width: getSize(
                        24,
                      ),
                      decoration: BoxDecoration(
                        color: appTheme.whiteA70001,
                        borderRadius: BorderRadius.circular(
                          getHorizontalSize(
                            12,
                          ),
                        ),
                        border: Border.all(
                          color: appTheme.indigoA400,
                          width: getHorizontalSize(
                            2,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: getPadding(
                top: 24,
              ),
              child: Divider(
                height: getVerticalSize(
                  1,
                ),
                thickness: getVerticalSize(
                  1,
                ),
                color: appTheme.gray200,
                indent: getHorizontalSize(
                  24,
                ),
                endIndent: getHorizontalSize(
                  24,
                ),
              ),
            ),
            Padding(
              padding: getPadding(
                left: 24,
                top: 23,
                bottom: 40,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomElevatedButton(
                    text: "lbl_reset".tr,
                    buttonStyle: CustomButtonStyles.fillGray5003.copyWith(
                        fixedSize: MaterialStateProperty.all<Size>(Size(
                      getHorizontalSize(
                        184,
                      ),
                      getVerticalSize(
                        58,
                      ),
                    ))),
                    buttonTextStyle: CustomTextStyles.titleMediumIndigo60001,
                  ),
                  CustomElevatedButton(
                    width: getHorizontalSize(
                      184,
                    ),
                    height: getVerticalSize(
                      58,
                    ),
                    text: "lbl_apply_filter".tr,
                    margin: getMargin(
                      left: 12,
                    ),
                    buttonStyle:
                        CustomButtonStyles.outlineDeeppurple100.copyWith(
                            fixedSize: MaterialStateProperty.all<Size>(Size(
                      getHorizontalSize(
                        184,
                      ),
                      getVerticalSize(
                        58,
                      ),
                    ))),
                    decoration:
                        CustomButtonStyles.outlineDeeppurple100Decoration,
                    buttonTextStyle: CustomTextStyles.titleMediumWhiteA7000116,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
