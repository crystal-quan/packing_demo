// ignore_for_file: must_be_immutable

part of 'light_home_search_filter_bloc.dart';

/// Represents the state of LightHomeSearchFilter in the application.
class LightHomeSearchFilterState extends Equatable {
  LightHomeSearchFilterState({this.lightHomeSearchFilterModelObj});

  LightHomeSearchFilterModel? lightHomeSearchFilterModelObj;

  @override
  List<Object?> get props => [
        lightHomeSearchFilterModelObj,
      ];
  LightHomeSearchFilterState copyWith(
      {LightHomeSearchFilterModel? lightHomeSearchFilterModelObj}) {
    return LightHomeSearchFilterState(
      lightHomeSearchFilterModelObj:
          lightHomeSearchFilterModelObj ?? this.lightHomeSearchFilterModelObj,
    );
  }
}
