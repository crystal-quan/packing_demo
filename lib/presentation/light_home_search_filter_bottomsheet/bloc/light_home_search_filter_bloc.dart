import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/autolayouthoriz1_item_model.dart';
import 'package:parking_app/presentation/light_home_search_filter_bottomsheet/models/light_home_search_filter_model.dart';
part 'light_home_search_filter_event.dart';
part 'light_home_search_filter_state.dart';

/// A bloc that manages the state of a LightHomeSearchFilter according to the event that is dispatched to it.
class LightHomeSearchFilterBloc
    extends Bloc<LightHomeSearchFilterEvent, LightHomeSearchFilterState> {
  LightHomeSearchFilterBloc(LightHomeSearchFilterState initialState)
      : super(initialState) {
    on<LightHomeSearchFilterInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightHomeSearchFilterInitialEvent event,
    Emitter<LightHomeSearchFilterState> emit,
  ) async {
    emit(state.copyWith(
        lightHomeSearchFilterModelObj:
            state.lightHomeSearchFilterModelObj?.copyWith(
      autolayouthoriz1ItemList: fillAutolayouthoriz1ItemList(),
    )));
  }

  List<Autolayouthoriz1ItemModel> fillAutolayouthoriz1ItemList() {
    return List.generate(4, (index) => Autolayouthoriz1ItemModel());
  }
}
