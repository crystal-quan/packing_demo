// ignore_for_file: must_be_immutable

part of 'light_home_search_filter_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightHomeSearchFilter widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightHomeSearchFilterEvent extends Equatable {}

/// Event that is dispatched when the LightHomeSearchFilter widget is first created.
class LightHomeSearchFilterInitialEvent extends LightHomeSearchFilterEvent {
  @override
  List<Object?> get props => [];
}
