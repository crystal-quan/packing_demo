import '../models/autolayouthoriz1_item_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

// ignore: must_be_immutable
class Autolayouthoriz1ItemWidget extends StatelessWidget {
  Autolayouthoriz1ItemWidget(
    this.autolayouthoriz1ItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  Autolayouthoriz1ItemModel autolayouthoriz1ItemModelObj;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: getHorizontalSize(
        103,
      ),
      child: Align(
        alignment: Alignment.centerRight,
        child: Container(
          width: getHorizontalSize(
            103,
          ),
          padding: getPadding(
            left: 20,
            top: 8,
            right: 20,
            bottom: 8,
          ),
          decoration: AppDecoration.txtFill1.copyWith(
            borderRadius: BorderRadiusStyle.txtCircleBorder19,
          ),
          child: Text(
            autolayouthoriz1ItemModelObj.sizemediumtypefTxt,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.left,
            style: CustomTextStyles.titleMediumWhiteA70001SemiBold.copyWith(
              letterSpacing: getHorizontalSize(
                0.2,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
