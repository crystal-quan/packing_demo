// ignore_for_file: must_be_immutable

part of 'light_home_notification_bloc.dart';

/// Represents the state of LightHomeNotification in the application.
class LightHomeNotificationState extends Equatable {
  LightHomeNotificationState({this.lightHomeNotificationModelObj});

  LightHomeNotificationModel? lightHomeNotificationModelObj;

  @override
  List<Object?> get props => [
        lightHomeNotificationModelObj,
      ];
  LightHomeNotificationState copyWith(
      {LightHomeNotificationModel? lightHomeNotificationModelObj}) {
    return LightHomeNotificationState(
      lightHomeNotificationModelObj:
          lightHomeNotificationModelObj ?? this.lightHomeNotificationModelObj,
    );
  }
}
