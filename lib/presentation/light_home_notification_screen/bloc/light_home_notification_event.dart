// ignore_for_file: must_be_immutable

part of 'light_home_notification_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightHomeNotification widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightHomeNotificationEvent extends Equatable {}

/// Event that is dispatched when the LightHomeNotification widget is first created.
class LightHomeNotificationInitialEvent extends LightHomeNotificationEvent {
  @override
  List<Object?> get props => [];
}
