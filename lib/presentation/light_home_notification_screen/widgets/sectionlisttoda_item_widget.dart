import '../models/sectionlisttoda_item_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

// ignore: must_be_immutable
class SectionlisttodaItemWidget extends StatelessWidget {
  SectionlisttodaItemWidget(
    this.sectionlisttodaItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  SectionlisttodaItemModel sectionlisttodaItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: getPadding(
        all: 16,
      ),
      decoration: AppDecoration.outline4.copyWith(
        borderRadius: BorderRadiusStyle.roundedBorder16,
      ),
      child: Row(
        children: [
          SizedBox(
            width: getHorizontalSize(
              83,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: getPadding(
                        left: 4,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: getSize(
                              9,
                            ),
                            width: getSize(
                              9,
                            ),
                            decoration: BoxDecoration(
                              color: appTheme.indigo60001,
                              borderRadius: BorderRadius.circular(
                                getHorizontalSize(
                                  4,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            height: getSize(
                              2,
                            ),
                            width: getSize(
                              2,
                            ),
                            margin: getMargin(
                              left: 33,
                              top: 1,
                              bottom: 6,
                            ),
                            decoration: BoxDecoration(
                              color: appTheme.indigo60001,
                              borderRadius: BorderRadius.circular(
                                getHorizontalSize(
                                  1,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: getSize(
                            1,
                          ),
                          width: getSize(
                            1,
                          ),
                          margin: getMargin(
                            top: 24,
                            bottom: 38,
                          ),
                          decoration: BoxDecoration(
                            color: appTheme.green400,
                            borderRadius: BorderRadius.circular(
                              getHorizontalSize(
                                1,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          height: getSize(
                            4,
                          ),
                          width: getSize(
                            4,
                          ),
                          margin: getMargin(
                            left: 1,
                            top: 48,
                            bottom: 11,
                          ),
                          decoration: BoxDecoration(
                            color: appTheme.indigo60001,
                            borderRadius: BorderRadius.circular(
                              getHorizontalSize(
                                2,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: getMargin(
                            left: 5,
                          ),
                          decoration: AppDecoration
                              .gradientnamedeeppurple100nameindigo60001
                              .copyWith(
                            borderRadius: BorderRadiusStyle.roundedBorder31,
                          ),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              CustomImageView(
                                svgPath: ImageConstant.imgCheckmarkWhiteA70001,
                                height: getSize(
                                  26,
                                ),
                                width: getSize(
                                  26,
                                ),
                                margin: getMargin(
                                  top: 18,
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  height: getSize(
                                    2,
                                  ),
                                  width: getSize(
                                    2,
                                  ),
                                  margin: getMargin(
                                    top: 17,
                                  ),
                                  decoration: BoxDecoration(
                                    color: appTheme.indigo60001,
                                    borderRadius: BorderRadius.circular(
                                      getHorizontalSize(
                                        1,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                        height: getSize(
                          1,
                        ),
                        width: getSize(
                          1,
                        ),
                        margin: getMargin(
                          top: 4,
                          right: 19,
                        ),
                        decoration: BoxDecoration(
                          color: appTheme.green400,
                          borderRadius: BorderRadius.circular(
                            getHorizontalSize(
                              1,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: getSize(
                        3,
                      ),
                      width: getSize(
                        3,
                      ),
                      margin: getMargin(
                        left: 26,
                      ),
                      decoration: BoxDecoration(
                        color: appTheme.indigo60001,
                        borderRadius: BorderRadius.circular(
                          getHorizontalSize(
                            1,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: getPadding(
                    left: 1,
                    top: 9,
                    bottom: 30,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          height: getSize(
                            7,
                          ),
                          width: getSize(
                            7,
                          ),
                          decoration: BoxDecoration(
                            color: appTheme.indigo60001,
                            borderRadius: BorderRadius.circular(
                              getHorizontalSize(
                                3,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: getSize(
                          2,
                        ),
                        width: getSize(
                          2,
                        ),
                        margin: getMargin(
                          top: 32,
                        ),
                        decoration: BoxDecoration(
                          color: appTheme.indigo60001,
                          borderRadius: BorderRadius.circular(
                            getHorizontalSize(
                              1,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: getPadding(
              left: 20,
              top: 17,
              bottom: 16,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "msg_payment_successful".tr,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.left,
                  style: theme.textTheme.titleMedium,
                ),
                Padding(
                  padding: getPadding(
                    top: 7,
                  ),
                  child: Text(
                    "msg_parking_booking".tr,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.left,
                    style: CustomTextStyles.titleSmallGray700.copyWith(
                      letterSpacing: getHorizontalSize(
                        0.2,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
