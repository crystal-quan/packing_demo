// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'listellipse_item_model.dart';import 'sectionlisttoda_item_model.dart';/// This class defines the variables used in the [light_home_notification_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightHomeNotificationModel extends Equatable {LightHomeNotificationModel({this.listellipseItemList = const [], this.sectionlisttodaItemList = const [], });

List<ListellipseItemModel> listellipseItemList;

List<SectionlisttodaItemModel> sectionlisttodaItemList;

LightHomeNotificationModel copyWith({List<ListellipseItemModel>? listellipseItemList, List<SectionlisttodaItemModel>? sectionlisttodaItemList, }) { return LightHomeNotificationModel(
listellipseItemList : listellipseItemList ?? this.listellipseItemList,
sectionlisttodaItemList : sectionlisttodaItemList ?? this.sectionlisttodaItemList,
); } 
@override List<Object?> get props => [listellipseItemList,sectionlisttodaItemList];
 }
