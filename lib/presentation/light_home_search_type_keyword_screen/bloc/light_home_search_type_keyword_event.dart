// ignore_for_file: must_be_immutable

part of 'light_home_search_type_keyword_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightHomeSearchTypeKeyword widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightHomeSearchTypeKeywordEvent extends Equatable {}

/// Event that is dispatched when the LightHomeSearchTypeKeyword widget is first created.
class LightHomeSearchTypeKeywordInitialEvent
    extends LightHomeSearchTypeKeywordEvent {
  @override
  List<Object?> get props => [];
}
