import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:parking_app/presentation/light_home_search_type_keyword_screen/models/light_home_search_type_keyword_model.dart';
part 'light_home_search_type_keyword_event.dart';
part 'light_home_search_type_keyword_state.dart';

/// A bloc that manages the state of a LightHomeSearchTypeKeyword according to the event that is dispatched to it.
class LightHomeSearchTypeKeywordBloc extends Bloc<
    LightHomeSearchTypeKeywordEvent, LightHomeSearchTypeKeywordState> {
  LightHomeSearchTypeKeywordBloc(LightHomeSearchTypeKeywordState initialState)
      : super(initialState) {
    on<LightHomeSearchTypeKeywordInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightHomeSearchTypeKeywordInitialEvent event,
    Emitter<LightHomeSearchTypeKeywordState> emit,
  ) async {
    emit(state.copyWith(
      stateactivesearController: TextEditingController(),
    ));
  }
}
