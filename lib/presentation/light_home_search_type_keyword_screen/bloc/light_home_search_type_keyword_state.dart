// ignore_for_file: must_be_immutable

part of 'light_home_search_type_keyword_bloc.dart';

/// Represents the state of LightHomeSearchTypeKeyword in the application.
class LightHomeSearchTypeKeywordState extends Equatable {
  LightHomeSearchTypeKeywordState({
    this.stateactivesearController,
    this.lightHomeSearchTypeKeywordModelObj,
  });

  TextEditingController? stateactivesearController;

  LightHomeSearchTypeKeywordModel? lightHomeSearchTypeKeywordModelObj;

  @override
  List<Object?> get props => [
        stateactivesearController,
        lightHomeSearchTypeKeywordModelObj,
      ];
  LightHomeSearchTypeKeywordState copyWith({
    TextEditingController? stateactivesearController,
    LightHomeSearchTypeKeywordModel? lightHomeSearchTypeKeywordModelObj,
  }) {
    return LightHomeSearchTypeKeywordState(
      stateactivesearController:
          stateactivesearController ?? this.stateactivesearController,
      lightHomeSearchTypeKeywordModelObj: lightHomeSearchTypeKeywordModelObj ??
          this.lightHomeSearchTypeKeywordModelObj,
    );
  }
}
