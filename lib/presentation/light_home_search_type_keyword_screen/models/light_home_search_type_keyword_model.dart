// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_home_search_type_keyword_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightHomeSearchTypeKeywordModel extends Equatable {LightHomeSearchTypeKeywordModel copyWith() { return LightHomeSearchTypeKeywordModel(
); } 
@override List<Object?> get props => [];
 }
