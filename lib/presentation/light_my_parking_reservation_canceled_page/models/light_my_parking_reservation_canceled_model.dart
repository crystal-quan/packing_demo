// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'autolayoutverti2_item_model.dart';/// This class defines the variables used in the [light_my_parking_reservation_canceled_page],
/// and is typically used to hold data that is passed between different parts of the application.
class LightMyParkingReservationCanceledModel extends Equatable {LightMyParkingReservationCanceledModel({this.autolayoutverti2ItemList = const []});

List<Autolayoutverti2ItemModel> autolayoutverti2ItemList;

LightMyParkingReservationCanceledModel copyWith({List<Autolayoutverti2ItemModel>? autolayoutverti2ItemList}) { return LightMyParkingReservationCanceledModel(
autolayoutverti2ItemList : autolayoutverti2ItemList ?? this.autolayoutverti2ItemList,
); } 
@override List<Object?> get props => [autolayoutverti2ItemList];
 }
