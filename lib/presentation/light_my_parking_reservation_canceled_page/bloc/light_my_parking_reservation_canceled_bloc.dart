import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/autolayoutverti2_item_model.dart';
import 'package:parking_app/presentation/light_my_parking_reservation_canceled_page/models/light_my_parking_reservation_canceled_model.dart';
part 'light_my_parking_reservation_canceled_event.dart';
part 'light_my_parking_reservation_canceled_state.dart';

/// A bloc that manages the state of a LightMyParkingReservationCanceled according to the event that is dispatched to it.
class LightMyParkingReservationCanceledBloc extends Bloc<
    LightMyParkingReservationCanceledEvent,
    LightMyParkingReservationCanceledState> {
  LightMyParkingReservationCanceledBloc(
      LightMyParkingReservationCanceledState initialState)
      : super(initialState) {
    on<LightMyParkingReservationCanceledInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightMyParkingReservationCanceledInitialEvent event,
    Emitter<LightMyParkingReservationCanceledState> emit,
  ) async {
    emit(state.copyWith(
        lightMyParkingReservationCanceledModelObj:
            state.lightMyParkingReservationCanceledModelObj?.copyWith(
      autolayoutverti2ItemList: fillAutolayoutverti2ItemList(),
    )));
  }

  List<Autolayoutverti2ItemModel> fillAutolayoutverti2ItemList() {
    return List.generate(4, (index) => Autolayoutverti2ItemModel());
  }
}
