// ignore_for_file: must_be_immutable

part of 'light_my_parking_reservation_canceled_bloc.dart';

/// Represents the state of LightMyParkingReservationCanceled in the application.
class LightMyParkingReservationCanceledState extends Equatable {
  LightMyParkingReservationCanceledState(
      {this.lightMyParkingReservationCanceledModelObj});

  LightMyParkingReservationCanceledModel?
      lightMyParkingReservationCanceledModelObj;

  @override
  List<Object?> get props => [
        lightMyParkingReservationCanceledModelObj,
      ];
  LightMyParkingReservationCanceledState copyWith(
      {LightMyParkingReservationCanceledModel?
          lightMyParkingReservationCanceledModelObj}) {
    return LightMyParkingReservationCanceledState(
      lightMyParkingReservationCanceledModelObj:
          lightMyParkingReservationCanceledModelObj ??
              this.lightMyParkingReservationCanceledModelObj,
    );
  }
}
