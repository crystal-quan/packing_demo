// ignore_for_file: must_be_immutable

part of 'light_my_parking_reservation_canceled_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightMyParkingReservationCanceled widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightMyParkingReservationCanceledEvent extends Equatable {}

/// Event that is dispatched when the LightMyParkingReservationCanceled widget is first created.
class LightMyParkingReservationCanceledInitialEvent
    extends LightMyParkingReservationCanceledEvent {
  @override
  List<Object?> get props => [];
}
