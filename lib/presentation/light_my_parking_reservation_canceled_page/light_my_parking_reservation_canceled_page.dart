import '../light_my_parking_reservation_canceled_page/widgets/autolayoutverti2_item_widget.dart';
import 'bloc/light_my_parking_reservation_canceled_bloc.dart';
import 'models/autolayoutverti2_item_model.dart';
import 'models/light_my_parking_reservation_canceled_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

// ignore_for_file: must_be_immutable
class LightMyParkingReservationCanceledPage extends StatefulWidget {
  const LightMyParkingReservationCanceledPage({Key? key})
      : super(
          key: key,
        );

  @override
  LightMyParkingReservationCanceledPageState createState() =>
      LightMyParkingReservationCanceledPageState();
  static Widget builder(BuildContext context) {
    return BlocProvider<LightMyParkingReservationCanceledBloc>(
      create: (context) => LightMyParkingReservationCanceledBloc(
          LightMyParkingReservationCanceledState(
        lightMyParkingReservationCanceledModelObj:
            LightMyParkingReservationCanceledModel(),
      ))
        ..add(LightMyParkingReservationCanceledInitialEvent()),
      child: LightMyParkingReservationCanceledPage(),
    );
  }
}

class LightMyParkingReservationCanceledPageState
    extends State<LightMyParkingReservationCanceledPage>
    with AutomaticKeepAliveClientMixin<LightMyParkingReservationCanceledPage> {
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SizedBox(
          width: double.maxFinite,
          child: Padding(
            padding: getPadding(
              left: 24,
              top: 24,
              right: 24,
            ),
            child: BlocSelector<
                LightMyParkingReservationCanceledBloc,
                LightMyParkingReservationCanceledState,
                LightMyParkingReservationCanceledModel?>(
              selector: (state) =>
                  state.lightMyParkingReservationCanceledModelObj,
              builder: (context, lightMyParkingReservationCanceledModelObj) {
                return ListView.separated(
                  physics: BouncingScrollPhysics(),
                  shrinkWrap: true,
                  separatorBuilder: (
                    context,
                    index,
                  ) {
                    return SizedBox(
                      height: getVerticalSize(
                        20,
                      ),
                    );
                  },
                  itemCount: lightMyParkingReservationCanceledModelObj
                          ?.autolayoutverti2ItemList.length ??
                      0,
                  itemBuilder: (context, index) {
                    Autolayoutverti2ItemModel model =
                        lightMyParkingReservationCanceledModelObj
                                ?.autolayoutverti2ItemList[index] ??
                            Autolayoutverti2ItemModel();
                    return Autolayoutverti2ItemWidget(
                      model,
                    );
                  },
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
