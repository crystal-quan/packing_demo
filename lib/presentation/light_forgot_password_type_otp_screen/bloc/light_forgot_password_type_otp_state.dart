// ignore_for_file: must_be_immutable

part of 'light_forgot_password_type_otp_bloc.dart';

/// Represents the state of LightForgotPasswordTypeOtp in the application.
class LightForgotPasswordTypeOtpState extends Equatable {
  LightForgotPasswordTypeOtpState({
    this.otpController,
    this.lightForgotPasswordTypeOtpModelObj,
  });

  TextEditingController? otpController;

  LightForgotPasswordTypeOtpModel? lightForgotPasswordTypeOtpModelObj;

  @override
  List<Object?> get props => [
        otpController,
        lightForgotPasswordTypeOtpModelObj,
      ];
  LightForgotPasswordTypeOtpState copyWith({
    TextEditingController? otpController,
    LightForgotPasswordTypeOtpModel? lightForgotPasswordTypeOtpModelObj,
  }) {
    return LightForgotPasswordTypeOtpState(
      otpController: otpController ?? this.otpController,
      lightForgotPasswordTypeOtpModelObj: lightForgotPasswordTypeOtpModelObj ??
          this.lightForgotPasswordTypeOtpModelObj,
    );
  }
}
