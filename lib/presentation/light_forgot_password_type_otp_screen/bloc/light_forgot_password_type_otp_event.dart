// ignore_for_file: must_be_immutable

part of 'light_forgot_password_type_otp_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightForgotPasswordTypeOtp widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightForgotPasswordTypeOtpEvent extends Equatable {}

/// Event that is dispatched when the LightForgotPasswordTypeOtp widget is first created.
class LightForgotPasswordTypeOtpInitialEvent
    extends LightForgotPasswordTypeOtpEvent {
  @override
  List<Object?> get props => [];
}

///event for OTP auto fill
class ChangeOTPEvent extends LightForgotPasswordTypeOtpEvent {
  ChangeOTPEvent({required this.code});

  String code;

  @override
  List<Object?> get props => [
        code,
      ];
}
