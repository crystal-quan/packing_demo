import '../models/listautolayoutv_item_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

// ignore: must_be_immutable
class ListautolayoutvItemWidget extends StatelessWidget {
  ListautolayoutvItemWidget(
    this.listautolayoutvItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  ListautolayoutvItemModel listautolayoutvItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          width: getHorizontalSize(
            129,
          ),
          padding: getPadding(
            left: 30,
            top: 13,
            right: 30,
            bottom: 13,
          ),
          decoration: AppDecoration.txtFill.copyWith(
            borderRadius: BorderRadiusStyle.txtRoundedBorder12,
          ),
          child: Text(
            listautolayoutvItemModelObj.autolayoutvertiTxt,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.left,
            style: CustomTextStyles.headlineSmallSFProDisplay,
          ),
        ),
        Container(
          width: getHorizontalSize(
            129,
          ),
          padding: getPadding(
            left: 30,
            top: 13,
            right: 30,
            bottom: 13,
          ),
          decoration: AppDecoration.txtFill.copyWith(
            borderRadius: BorderRadiusStyle.txtRoundedBorder12,
          ),
          child: Text(
            listautolayoutvItemModelObj.autolayoutvertiTxt1,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.left,
            style: CustomTextStyles.headlineSmallSFProDisplay,
          ),
        ),
        Container(
          width: getHorizontalSize(
            129,
          ),
          padding: getPadding(
            left: 30,
            top: 13,
            right: 30,
            bottom: 13,
          ),
          decoration: AppDecoration.txtFill.copyWith(
            borderRadius: BorderRadiusStyle.txtRoundedBorder12,
          ),
          child: Text(
            listautolayoutvItemModelObj.autolayoutvertiTxt12,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.left,
            style: CustomTextStyles.headlineSmallSFProDisplay,
          ),
        ),
      ],
    );
  }
}
