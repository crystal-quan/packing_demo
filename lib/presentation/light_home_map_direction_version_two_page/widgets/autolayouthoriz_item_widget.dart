import '../models/autolayouthoriz_item_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

// ignore: must_be_immutable
class AutolayouthorizItemWidget extends StatelessWidget {
  AutolayouthorizItemWidget(
    this.autolayouthorizItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  AutolayouthorizItemModel autolayouthorizItemModelObj;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: getHorizontalSize(
        107,
      ),
      child: Align(
        alignment: Alignment.bottomRight,
        child: Container(
          padding: getPadding(
            left: 20,
            top: 8,
            right: 20,
            bottom: 8,
          ),
          decoration: AppDecoration.outline3.copyWith(
            borderRadius: BorderRadiusStyle.circleBorder19,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomImageView(
                svgPath: ImageConstant.imgLocation,
                height: getSize(
                  16,
                ),
                width: getSize(
                  16,
                ),
                margin: getMargin(
                  top: 2,
                  bottom: 2,
                ),
              ),
              Padding(
                padding: getPadding(
                  left: 8,
                  top: 1,
                ),
                child: Text(
                  autolayouthorizItemModelObj.typeTxt,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.left,
                  style: CustomTextStyles.titleMediumIndigo60001SemiBold16
                      .copyWith(
                    letterSpacing: getHorizontalSize(
                      0.2,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
