import '../light_home_map_direction_version_two_page/widgets/autolayouthoriz_item_widget.dart';
import 'bloc/light_home_map_direction_version_two_bloc.dart';
import 'models/autolayouthoriz_item_model.dart';
import 'models/light_home_map_direction_version_two_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_floating_button.dart';
import 'package:parking_app/widgets/custom_icon_button.dart';

// ignore_for_file: must_be_immutable
class LightHomeMapDirectionVersionTwoPage extends StatelessWidget {
  const LightHomeMapDirectionVersionTwoPage({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LightHomeMapDirectionVersionTwoBloc>(
      create: (context) => LightHomeMapDirectionVersionTwoBloc(
          LightHomeMapDirectionVersionTwoState(
        lightHomeMapDirectionVersionTwoModelObj:
            LightHomeMapDirectionVersionTwoModel(),
      ))
        ..add(LightHomeMapDirectionVersionTwoInitialEvent()),
      child: LightHomeMapDirectionVersionTwoPage(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.whiteA70001,
        body: Container(
          height: getVerticalSize(
            792,
          ),
          width: double.maxFinite,
          decoration: AppDecoration.fill,
          child: Stack(
            alignment: Alignment.center,
            children: [
              CustomImageView(
                imagePath: ImageConstant.imgVectorIndigoA10004,
                height: getVerticalSize(
                  882,
                ),
                width: getHorizontalSize(
                  428,
                ),
                alignment: Alignment.center,
              ),
              Align(
                alignment: Alignment.center,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: getVerticalSize(
                        792,
                      ),
                      width: double.maxFinite,
                      child: Stack(
                        alignment: Alignment.bottomRight,
                        children: [
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              height: mediaQueryData.size.height,
                              width: double.maxFinite,
                              decoration: BoxDecoration(
                                color: appTheme.gray5002,
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: SizedBox(
                              height: mediaQueryData.size.height,
                              child: BlocSelector<
                                  LightHomeMapDirectionVersionTwoBloc,
                                  LightHomeMapDirectionVersionTwoState,
                                  LightHomeMapDirectionVersionTwoModel?>(
                                selector: (state) => state
                                    .lightHomeMapDirectionVersionTwoModelObj,
                                builder: (context,
                                    lightHomeMapDirectionVersionTwoModelObj) {
                                  return ListView.separated(
                                    padding: getPadding(
                                      left: 24,
                                      top: 726,
                                      bottom: 118,
                                    ),
                                    scrollDirection: Axis.horizontal,
                                    separatorBuilder: (
                                      context,
                                      index,
                                    ) {
                                      return SizedBox(
                                        width: getHorizontalSize(
                                          12,
                                        ),
                                      );
                                    },
                                    itemCount:
                                        lightHomeMapDirectionVersionTwoModelObj
                                                ?.autolayouthorizItemList
                                                .length ??
                                            0,
                                    itemBuilder: (context, index) {
                                      AutolayouthorizItemModel model =
                                          lightHomeMapDirectionVersionTwoModelObj
                                                      ?.autolayouthorizItemList[
                                                  index] ??
                                              AutolayouthorizItemModel();
                                      return AutolayouthorizItemWidget(
                                        model,
                                      );
                                    },
                                  );
                                },
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              margin: getMargin(
                                right: 51,
                                bottom: 249,
                              ),
                              padding: getPadding(
                                all: 80,
                              ),
                              decoration: AppDecoration.fill5.copyWith(
                                borderRadius: BorderRadiusStyle.circleBorder150,
                              ),
                              child: Card(
                                clipBehavior: Clip.antiAlias,
                                elevation: 0,
                                margin: EdgeInsets.all(0),
                                color: appTheme.indigoA40063,
                                shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadiusStyle.circleBorder70,
                                ),
                                child: Container(
                                  height: getSize(
                                    140,
                                  ),
                                  width: getSize(
                                    140,
                                  ),
                                  padding: getPadding(
                                    left: 39,
                                    top: 24,
                                    right: 39,
                                    bottom: 24,
                                  ),
                                  decoration: AppDecoration.fill5.copyWith(
                                    borderRadius:
                                        BorderRadiusStyle.circleBorder70,
                                  ),
                                  child: Stack(
                                    children: [
                                      CustomImageView(
                                        imagePath: ImageConstant.imgImage,
                                        height: getVerticalSize(
                                          91,
                                        ),
                                        width: getHorizontalSize(
                                          60,
                                        ),
                                        alignment: Alignment.center,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topRight,
                            child: Padding(
                              padding: getPadding(
                                top: 22,
                                right: 24,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  CustomIconButton(
                                    height: 52,
                                    width: 52,
                                    padding: getPadding(
                                      all: 12,
                                    ),
                                    decoration:
                                        IconButtonStyleHelper.fillWhiteA70001,
                                    child: CustomImageView(
                                      svgPath: ImageConstant.imgSearch,
                                    ),
                                  ),
                                  CustomIconButton(
                                    height: 52,
                                    width: 52,
                                    margin: getMargin(
                                      left: 20,
                                    ),
                                    padding: getPadding(
                                      all: 12,
                                    ),
                                    decoration:
                                        IconButtonStyleHelper.fillWhiteA70001,
                                    child: CustomImageView(
                                      svgPath: ImageConstant
                                          .imgIconlycurvednotification,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: Padding(
                              padding: getPadding(
                                left: 26,
                                top: 34,
                                right: 37,
                              ),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: getPadding(
                                      left: 4,
                                    ),
                                    child: Row(
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Align(
                                              alignment: Alignment.center,
                                              child: Card(
                                                clipBehavior: Clip.antiAlias,
                                                elevation: 0,
                                                margin: EdgeInsets.all(0),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadiusStyle
                                                          .circleBorder19,
                                                ),
                                                child: Container(
                                                  height: getSize(
                                                    40,
                                                  ),
                                                  width: getSize(
                                                    40,
                                                  ),
                                                  padding: getPadding(
                                                    all: 6,
                                                  ),
                                                  decoration: AppDecoration
                                                      .gradientnameredA20001opacity025nameredA100opacity025
                                                      .copyWith(
                                                    borderRadius:
                                                        BorderRadiusStyle
                                                            .circleBorder19,
                                                  ),
                                                  child: Stack(
                                                    children: [
                                                      CustomIconButton(
                                                        height: 28,
                                                        width: 28,
                                                        padding: getPadding(
                                                          all: 6,
                                                        ),
                                                        alignment:
                                                            Alignment.center,
                                                        child: CustomImageView(
                                                          svgPath: ImageConstant
                                                              .imgFrameWhiteA70001,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.centerRight,
                                              child: Padding(
                                                padding: getPadding(
                                                  top: 79,
                                                ),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.end,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    CustomImageView(
                                                      svgPath: ImageConstant
                                                          .imgTypemuseumcomponentmaps,
                                                      height: getSize(
                                                        28,
                                                      ),
                                                      width: getSize(
                                                        28,
                                                      ),
                                                      margin: getMargin(
                                                        bottom: 30,
                                                      ),
                                                    ),
                                                    Card(
                                                      clipBehavior:
                                                          Clip.antiAlias,
                                                      elevation: 0,
                                                      margin: getMargin(
                                                        top: 18,
                                                      ),
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadiusStyle
                                                                .circleBorder19,
                                                      ),
                                                      child: Container(
                                                        height: getSize(
                                                          40,
                                                        ),
                                                        width: getSize(
                                                          40,
                                                        ),
                                                        padding: getPadding(
                                                          all: 6,
                                                        ),
                                                        decoration: AppDecoration
                                                            .gradientnameredA20001opacity025nameredA100opacity025
                                                            .copyWith(
                                                          borderRadius:
                                                              BorderRadiusStyle
                                                                  .circleBorder19,
                                                        ),
                                                        child: Stack(
                                                          children: [
                                                            CustomIconButton(
                                                              height: 28,
                                                              width: 28,
                                                              padding:
                                                                  getPadding(
                                                                all: 6,
                                                              ),
                                                              alignment:
                                                                  Alignment
                                                                      .center,
                                                              child:
                                                                  CustomImageView(
                                                                svgPath:
                                                                    ImageConstant
                                                                        .imgFrameWhiteA70001,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: getPadding(
                                                top: 5,
                                              ),
                                              child: Text(
                                                "lbl_ffordd_owain".tr,
                                                overflow: TextOverflow.ellipsis,
                                                textAlign: TextAlign.left,
                                                style: CustomTextStyles
                                                    .bodySmallGray700_1
                                                    .copyWith(
                                                  letterSpacing:
                                                      getHorizontalSize(
                                                    0.2,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Padding(
                                          padding: getPadding(
                                            left: 34,
                                            top: 158,
                                            bottom: 21,
                                          ),
                                          child: Text(
                                            "lbl_portley_lane".tr,
                                            overflow: TextOverflow.ellipsis,
                                            textAlign: TextAlign.left,
                                            style: CustomTextStyles
                                                .bodySmallGray700_1
                                                .copyWith(
                                              letterSpacing: getHorizontalSize(
                                                0.2,
                                              ),
                                            ),
                                          ),
                                        ),
                                        CustomImageView(
                                          svgPath: ImageConstant
                                              .imgTypehospitalcomponentmaps,
                                          height: getSize(
                                            28,
                                          ),
                                          width: getSize(
                                            28,
                                          ),
                                          margin: getMargin(
                                            top: 91,
                                            bottom: 86,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.centerRight,
                                    child: Padding(
                                      padding: getPadding(
                                        left: 54,
                                        top: 51,
                                        right: 33,
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Card(
                                            clipBehavior: Clip.antiAlias,
                                            elevation: 0,
                                            margin: getMargin(
                                              top: 8,
                                            ),
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadiusStyle
                                                  .circleBorder19,
                                            ),
                                            child: Container(
                                              height: getSize(
                                                40,
                                              ),
                                              width: getSize(
                                                40,
                                              ),
                                              padding: getPadding(
                                                all: 6,
                                              ),
                                              decoration: AppDecoration
                                                  .gradientnameredA20001opacity025nameredA100opacity025
                                                  .copyWith(
                                                borderRadius: BorderRadiusStyle
                                                    .circleBorder19,
                                              ),
                                              child: Stack(
                                                children: [
                                                  CustomIconButton(
                                                    height: 28,
                                                    width: 28,
                                                    padding: getPadding(
                                                      all: 6,
                                                    ),
                                                    alignment: Alignment.center,
                                                    child: CustomImageView(
                                                      svgPath: ImageConstant
                                                          .imgFrameWhiteA70001,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          CustomImageView(
                                            svgPath: ImageConstant
                                                .imgTypetrainstation,
                                            height: getSize(
                                              28,
                                            ),
                                            width: getSize(
                                              28,
                                            ),
                                            margin: getMargin(
                                              bottom: 20,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: getPadding(
                                      left: 4,
                                      top: 16,
                                    ),
                                    child: Text(
                                      "lbl_hopton_hollies".tr,
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.left,
                                      style: CustomTextStyles.bodySmallGray700_1
                                          .copyWith(
                                        letterSpacing: getHorizontalSize(
                                          0.2,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: getPadding(
                                      top: 59,
                                      right: 19,
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        CustomImageView(
                                          svgPath: ImageConstant
                                              .imgTypefitnesscomponentmaps,
                                          height: getSize(
                                            28,
                                          ),
                                          width: getSize(
                                            28,
                                          ),
                                          margin: getMargin(
                                            bottom: 37,
                                          ),
                                        ),
                                        Card(
                                          clipBehavior: Clip.antiAlias,
                                          elevation: 0,
                                          margin: getMargin(
                                            top: 25,
                                          ),
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadiusStyle
                                                .circleBorder19,
                                          ),
                                          child: Container(
                                            height: getSize(
                                              40,
                                            ),
                                            width: getSize(
                                              40,
                                            ),
                                            padding: getPadding(
                                              all: 6,
                                            ),
                                            decoration: AppDecoration
                                                .gradientnameredA20001opacity025nameredA100opacity025
                                                .copyWith(
                                              borderRadius: BorderRadiusStyle
                                                  .circleBorder19,
                                            ),
                                            child: Stack(
                                              children: [
                                                CustomIconButton(
                                                  height: 28,
                                                  width: 28,
                                                  padding: getPadding(
                                                    all: 6,
                                                  ),
                                                  alignment: Alignment.center,
                                                  child: CustomImageView(
                                                    svgPath: ImageConstant
                                                        .imgFrameWhiteA70001,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.centerRight,
                                    child: Padding(
                                      padding: getPadding(
                                        left: 79,
                                        top: 83,
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Card(
                                            clipBehavior: Clip.antiAlias,
                                            elevation: 0,
                                            margin: EdgeInsets.all(0),
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadiusStyle
                                                  .circleBorder19,
                                            ),
                                            child: Container(
                                              height: getSize(
                                                40,
                                              ),
                                              width: getSize(
                                                40,
                                              ),
                                              padding: getPadding(
                                                all: 6,
                                              ),
                                              decoration: AppDecoration
                                                  .gradientnameredA20001opacity025nameredA100opacity025
                                                  .copyWith(
                                                borderRadius: BorderRadiusStyle
                                                    .circleBorder19,
                                              ),
                                              child: Stack(
                                                children: [
                                                  CustomIconButton(
                                                    height: 28,
                                                    width: 28,
                                                    padding: getPadding(
                                                      all: 6,
                                                    ),
                                                    alignment: Alignment.center,
                                                    child: CustomImageView(
                                                      svgPath: ImageConstant
                                                          .imgFrameWhiteA70001,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          CustomImageView(
                                            svgPath: ImageConstant
                                                .imgTypebankcomponentmaps,
                                            height: getSize(
                                              28,
                                            ),
                                            width: getSize(
                                              28,
                                            ),
                                            margin: getMargin(
                                              top: 12,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: getPadding(
                                      left: 7,
                                      top: 22,
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        CustomImageView(
                                          svgPath: ImageConstant
                                              .imgTypeofficecomponentmaps,
                                          height: getSize(
                                            28,
                                          ),
                                          width: getSize(
                                            28,
                                          ),
                                          margin: getMargin(
                                            top: 2,
                                            bottom: 14,
                                          ),
                                        ),
                                        Text(
                                          "lbl_broughton_woods".tr,
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.left,
                                          style: CustomTextStyles
                                              .bodySmallGray700_1
                                              .copyWith(
                                            letterSpacing: getHorizontalSize(
                                              0.2,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          CustomImageView(
                            imagePath: ImageConstant.imgVector454x132,
                            height: getVerticalSize(
                              454,
                            ),
                            width: getHorizontalSize(
                              132,
                            ),
                            radius: BorderRadius.circular(
                              getHorizontalSize(
                                20,
                              ),
                            ),
                            alignment: Alignment.topCenter,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: CustomFloatingButton(
          height: 52,
          width: 52,
          backgroundColor: appTheme.indigo60001,
          child: CustomImageView(
            svgPath: ImageConstant.imgFrameWhiteA7000152x52,
            height: getVerticalSize(
              26.0,
            ),
            width: getHorizontalSize(
              26.0,
            ),
          ),
        ),
      ),
    );
  }
}
