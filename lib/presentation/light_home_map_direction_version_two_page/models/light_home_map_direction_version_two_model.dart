// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'autolayouthoriz_item_model.dart';/// This class defines the variables used in the [light_home_map_direction_version_two_page],
/// and is typically used to hold data that is passed between different parts of the application.
class LightHomeMapDirectionVersionTwoModel extends Equatable {LightHomeMapDirectionVersionTwoModel({this.autolayouthorizItemList = const []});

List<AutolayouthorizItemModel> autolayouthorizItemList;

LightHomeMapDirectionVersionTwoModel copyWith({List<AutolayouthorizItemModel>? autolayouthorizItemList}) { return LightHomeMapDirectionVersionTwoModel(
autolayouthorizItemList : autolayouthorizItemList ?? this.autolayouthorizItemList,
); } 
@override List<Object?> get props => [autolayouthorizItemList];
 }
