// ignore_for_file: must_be_immutable

part of 'light_home_map_direction_version_two_bloc.dart';

/// Represents the state of LightHomeMapDirectionVersionTwo in the application.
class LightHomeMapDirectionVersionTwoState extends Equatable {
  LightHomeMapDirectionVersionTwoState(
      {this.lightHomeMapDirectionVersionTwoModelObj});

  LightHomeMapDirectionVersionTwoModel? lightHomeMapDirectionVersionTwoModelObj;

  @override
  List<Object?> get props => [
        lightHomeMapDirectionVersionTwoModelObj,
      ];
  LightHomeMapDirectionVersionTwoState copyWith(
      {LightHomeMapDirectionVersionTwoModel?
          lightHomeMapDirectionVersionTwoModelObj}) {
    return LightHomeMapDirectionVersionTwoState(
      lightHomeMapDirectionVersionTwoModelObj:
          lightHomeMapDirectionVersionTwoModelObj ??
              this.lightHomeMapDirectionVersionTwoModelObj,
    );
  }
}
