import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/autolayouthoriz_item_model.dart';
import 'package:parking_app/presentation/light_home_map_direction_version_two_page/models/light_home_map_direction_version_two_model.dart';
part 'light_home_map_direction_version_two_event.dart';
part 'light_home_map_direction_version_two_state.dart';

/// A bloc that manages the state of a LightHomeMapDirectionVersionTwo according to the event that is dispatched to it.
class LightHomeMapDirectionVersionTwoBloc extends Bloc<
    LightHomeMapDirectionVersionTwoEvent,
    LightHomeMapDirectionVersionTwoState> {
  LightHomeMapDirectionVersionTwoBloc(
      LightHomeMapDirectionVersionTwoState initialState)
      : super(initialState) {
    on<LightHomeMapDirectionVersionTwoInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightHomeMapDirectionVersionTwoInitialEvent event,
    Emitter<LightHomeMapDirectionVersionTwoState> emit,
  ) async {
    emit(state.copyWith(
        lightHomeMapDirectionVersionTwoModelObj:
            state.lightHomeMapDirectionVersionTwoModelObj?.copyWith(
      autolayouthorizItemList: fillAutolayouthorizItemList(),
    )));
  }

  List<AutolayouthorizItemModel> fillAutolayouthorizItemList() {
    return List.generate(4, (index) => AutolayouthorizItemModel());
  }
}
