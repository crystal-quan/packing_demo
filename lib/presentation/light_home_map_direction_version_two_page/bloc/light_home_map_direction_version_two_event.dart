// ignore_for_file: must_be_immutable

part of 'light_home_map_direction_version_two_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightHomeMapDirectionVersionTwo widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightHomeMapDirectionVersionTwoEvent extends Equatable {}

/// Event that is dispatched when the LightHomeMapDirectionVersionTwo widget is first created.
class LightHomeMapDirectionVersionTwoInitialEvent
    extends LightHomeMapDirectionVersionTwoEvent {
  @override
  List<Object?> get props => [];
}
