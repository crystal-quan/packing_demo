// ignore_for_file: must_be_immutable

part of 'light_parking_add_new_card_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightParkingAddNewCard widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightParkingAddNewCardEvent extends Equatable {}

/// Event that is dispatched when the LightParkingAddNewCard widget is first created.
class LightParkingAddNewCardInitialEvent extends LightParkingAddNewCardEvent {
  @override
  List<Object?> get props => [];
}
