// ignore_for_file: must_be_immutable

part of 'light_parking_add_new_card_bloc.dart';

/// Represents the state of LightParkingAddNewCard in the application.
class LightParkingAddNewCardState extends Equatable {
  LightParkingAddNewCardState({
    this.statusfilltypedController,
    this.statusfilltypedController1,
    this.statusfilltypedController2,
    this.statusfilltypedController3,
    this.lightParkingAddNewCardModelObj,
  });

  TextEditingController? statusfilltypedController;

  TextEditingController? statusfilltypedController1;

  TextEditingController? statusfilltypedController2;

  TextEditingController? statusfilltypedController3;

  LightParkingAddNewCardModel? lightParkingAddNewCardModelObj;

  @override
  List<Object?> get props => [
        statusfilltypedController,
        statusfilltypedController1,
        statusfilltypedController2,
        statusfilltypedController3,
        lightParkingAddNewCardModelObj,
      ];
  LightParkingAddNewCardState copyWith({
    TextEditingController? statusfilltypedController,
    TextEditingController? statusfilltypedController1,
    TextEditingController? statusfilltypedController2,
    TextEditingController? statusfilltypedController3,
    LightParkingAddNewCardModel? lightParkingAddNewCardModelObj,
  }) {
    return LightParkingAddNewCardState(
      statusfilltypedController:
          statusfilltypedController ?? this.statusfilltypedController,
      statusfilltypedController1:
          statusfilltypedController1 ?? this.statusfilltypedController1,
      statusfilltypedController2:
          statusfilltypedController2 ?? this.statusfilltypedController2,
      statusfilltypedController3:
          statusfilltypedController3 ?? this.statusfilltypedController3,
      lightParkingAddNewCardModelObj:
          lightParkingAddNewCardModelObj ?? this.lightParkingAddNewCardModelObj,
    );
  }
}
