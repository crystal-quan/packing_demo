import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_parking_select_vehicle_screen/models/light_parking_select_vehicle_model.dart';part 'light_parking_select_vehicle_event.dart';part 'light_parking_select_vehicle_state.dart';/// A bloc that manages the state of a LightParkingSelectVehicle according to the event that is dispatched to it.
class LightParkingSelectVehicleBloc extends Bloc<LightParkingSelectVehicleEvent, LightParkingSelectVehicleState> {LightParkingSelectVehicleBloc(LightParkingSelectVehicleState initialState) : super(initialState) { on<LightParkingSelectVehicleInitialEvent>(_onInitialize); on<ChangeRadioButtonEvent>(_changeRadioButton); on<ChangeRadioButton1Event>(_changeRadioButton1); on<ChangeRadioButton2Event>(_changeRadioButton2); on<ChangeRadioButton3Event>(_changeRadioButton3); }

_changeRadioButton(ChangeRadioButtonEvent event, Emitter<LightParkingSelectVehicleState> emit, ) { emit(state.copyWith(radioGroup: event.value)); } 
_changeRadioButton1(ChangeRadioButton1Event event, Emitter<LightParkingSelectVehicleState> emit, ) { emit(state.copyWith(radioGroup1: event.value)); } 
_changeRadioButton2(ChangeRadioButton2Event event, Emitter<LightParkingSelectVehicleState> emit, ) { emit(state.copyWith(radioGroup2: event.value)); } 
_changeRadioButton3(ChangeRadioButton3Event event, Emitter<LightParkingSelectVehicleState> emit, ) { emit(state.copyWith(radioGroup3: event.value)); } 
_onInitialize(LightParkingSelectVehicleInitialEvent event, Emitter<LightParkingSelectVehicleState> emit, ) async  { emit(state.copyWith(radioGroup: "", radioGroup1: "", radioGroup2: "", radioGroup3: "")); } 
 }
