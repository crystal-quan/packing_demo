// ignore_for_file: must_be_immutable

part of 'light_parking_select_vehicle_bloc.dart';

/// Represents the state of LightParkingSelectVehicle in the application.
class LightParkingSelectVehicleState extends Equatable {
  LightParkingSelectVehicleState({
    this.radioGroup = "",
    this.radioGroup1 = "",
    this.radioGroup2 = "",
    this.radioGroup3 = "",
    this.lightParkingSelectVehicleModelObj,
  });

  LightParkingSelectVehicleModel? lightParkingSelectVehicleModelObj;

  String radioGroup;

  String radioGroup1;

  String radioGroup2;

  String radioGroup3;

  @override
  List<Object?> get props => [
        radioGroup,
        radioGroup1,
        radioGroup2,
        radioGroup3,
        lightParkingSelectVehicleModelObj,
      ];
  LightParkingSelectVehicleState copyWith({
    String? radioGroup,
    String? radioGroup1,
    String? radioGroup2,
    String? radioGroup3,
    LightParkingSelectVehicleModel? lightParkingSelectVehicleModelObj,
  }) {
    return LightParkingSelectVehicleState(
      radioGroup: radioGroup ?? this.radioGroup,
      radioGroup1: radioGroup1 ?? this.radioGroup1,
      radioGroup2: radioGroup2 ?? this.radioGroup2,
      radioGroup3: radioGroup3 ?? this.radioGroup3,
      lightParkingSelectVehicleModelObj: lightParkingSelectVehicleModelObj ??
          this.lightParkingSelectVehicleModelObj,
    );
  }
}
