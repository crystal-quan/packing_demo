// ignore_for_file: must_be_immutable

part of 'light_parking_select_vehicle_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightParkingSelectVehicle widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightParkingSelectVehicleEvent extends Equatable {}

/// Event that is dispatched when the LightParkingSelectVehicle widget is first created.
class LightParkingSelectVehicleInitialEvent
    extends LightParkingSelectVehicleEvent {
  @override
  List<Object?> get props => [];
}

///Event for changing radio button
class ChangeRadioButtonEvent extends LightParkingSelectVehicleEvent {
  ChangeRadioButtonEvent({required this.value});

  String value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing radio button
class ChangeRadioButton1Event extends LightParkingSelectVehicleEvent {
  ChangeRadioButton1Event({required this.value});

  String value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing radio button
class ChangeRadioButton2Event extends LightParkingSelectVehicleEvent {
  ChangeRadioButton2Event({required this.value});

  String value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing radio button
class ChangeRadioButton3Event extends LightParkingSelectVehicleEvent {
  ChangeRadioButton3Event({required this.value});

  String value;

  @override
  List<Object?> get props => [
        value,
      ];
}
