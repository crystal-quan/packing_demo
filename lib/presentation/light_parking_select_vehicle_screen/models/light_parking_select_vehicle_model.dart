// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_parking_select_vehicle_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightParkingSelectVehicleModel extends Equatable {LightParkingSelectVehicleModel copyWith() { return LightParkingSelectVehicleModel(
); } 
@override List<Object?> get props => [];
 }
