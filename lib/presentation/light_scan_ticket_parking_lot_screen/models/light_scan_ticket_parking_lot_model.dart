// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'listname1_item_model.dart';/// This class defines the variables used in the [light_scan_ticket_parking_lot_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightScanTicketParkingLotModel extends Equatable {LightScanTicketParkingLotModel({this.listname1ItemList = const []});

List<Listname1ItemModel> listname1ItemList;

LightScanTicketParkingLotModel copyWith({List<Listname1ItemModel>? listname1ItemList}) { return LightScanTicketParkingLotModel(
listname1ItemList : listname1ItemList ?? this.listname1ItemList,
); } 
@override List<Object?> get props => [listname1ItemList];
 }
