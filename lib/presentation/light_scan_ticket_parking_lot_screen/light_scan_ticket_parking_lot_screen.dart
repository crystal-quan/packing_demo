import '../light_scan_ticket_parking_lot_screen/widgets/listname1_item_widget.dart';
import 'bloc/light_scan_ticket_parking_lot_bloc.dart';
import 'models/light_scan_ticket_parking_lot_model.dart';
import 'models/listname1_item_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart' as fs;
import 'package:parking_app/core/app_export.dart';

class LightScanTicketParkingLotScreen extends StatelessWidget {
  const LightScanTicketParkingLotScreen({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LightScanTicketParkingLotBloc>(
      create: (context) =>
          LightScanTicketParkingLotBloc(LightScanTicketParkingLotState(
        lightScanTicketParkingLotModelObj: LightScanTicketParkingLotModel(),
      ))
            ..add(LightScanTicketParkingLotInitialEvent()),
      child: LightScanTicketParkingLotScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.indigoA10004,
        body: Container(
          width: double.maxFinite,
          padding: getPadding(
            left: 24,
            top: 99,
            right: 24,
          ),
          child: Container(
            margin: getMargin(
              bottom: 5,
            ),
            padding: getPadding(
              left: 22,
              top: 28,
              right: 22,
              bottom: 28,
            ),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: fs.Svg(
                  ImageConstant.imgGroup,
                ),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "lbl_a05_1st_floor".tr,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.left,
                  style: theme.textTheme.headlineSmall,
                ),
                Container(
                  height: getSize(
                    256,
                  ),
                  width: getSize(
                    256,
                  ),
                  margin: getMargin(
                    top: 9,
                  ),
                  padding: getPadding(
                    all: 16,
                  ),
                  decoration: AppDecoration.fill,
                  child: Stack(
                    children: [
                      CustomImageView(
                        svgPath: ImageConstant.imgVectorBlack900,
                        height: getSize(
                          224,
                        ),
                        width: getSize(
                          224,
                        ),
                        alignment: Alignment.center,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: getPadding(
                    top: 9,
                  ),
                  child: Divider(
                    height: getVerticalSize(
                      2,
                    ),
                    thickness: getVerticalSize(
                      2,
                    ),
                    color: appTheme.gray200,
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: getPadding(
                      left: 17,
                      top: 35,
                      right: 24,
                    ),
                    child: BlocSelector<
                        LightScanTicketParkingLotBloc,
                        LightScanTicketParkingLotState,
                        LightScanTicketParkingLotModel?>(
                      selector: (state) =>
                          state.lightScanTicketParkingLotModelObj,
                      builder: (context, lightScanTicketParkingLotModelObj) {
                        return ListView.separated(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          separatorBuilder: (
                            context,
                            index,
                          ) {
                            return SizedBox(
                              height: getVerticalSize(
                                21,
                              ),
                            );
                          },
                          itemCount: lightScanTicketParkingLotModelObj
                                  ?.listname1ItemList.length ??
                              0,
                          itemBuilder: (context, index) {
                            Listname1ItemModel model =
                                lightScanTicketParkingLotModelObj
                                        ?.listname1ItemList[index] ??
                                    Listname1ItemModel();
                            return Listname1ItemWidget(
                              model,
                            );
                          },
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
