import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/listname1_item_model.dart';
import 'package:parking_app/presentation/light_scan_ticket_parking_lot_screen/models/light_scan_ticket_parking_lot_model.dart';
part 'light_scan_ticket_parking_lot_event.dart';
part 'light_scan_ticket_parking_lot_state.dart';

/// A bloc that manages the state of a LightScanTicketParkingLot according to the event that is dispatched to it.
class LightScanTicketParkingLotBloc extends Bloc<LightScanTicketParkingLotEvent,
    LightScanTicketParkingLotState> {
  LightScanTicketParkingLotBloc(LightScanTicketParkingLotState initialState)
      : super(initialState) {
    on<LightScanTicketParkingLotInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightScanTicketParkingLotInitialEvent event,
    Emitter<LightScanTicketParkingLotState> emit,
  ) async {
    emit(state.copyWith(
        lightScanTicketParkingLotModelObj:
            state.lightScanTicketParkingLotModelObj?.copyWith(
      listname1ItemList: fillListname1ItemList(),
    )));
  }

  List<Listname1ItemModel> fillListname1ItemList() {
    return List.generate(4, (index) => Listname1ItemModel());
  }
}
