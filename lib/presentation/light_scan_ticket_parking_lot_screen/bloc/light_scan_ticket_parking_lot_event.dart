// ignore_for_file: must_be_immutable

part of 'light_scan_ticket_parking_lot_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightScanTicketParkingLot widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightScanTicketParkingLotEvent extends Equatable {}

/// Event that is dispatched when the LightScanTicketParkingLot widget is first created.
class LightScanTicketParkingLotInitialEvent
    extends LightScanTicketParkingLotEvent {
  @override
  List<Object?> get props => [];
}
