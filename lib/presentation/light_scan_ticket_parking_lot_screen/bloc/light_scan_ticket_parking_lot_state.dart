// ignore_for_file: must_be_immutable

part of 'light_scan_ticket_parking_lot_bloc.dart';

/// Represents the state of LightScanTicketParkingLot in the application.
class LightScanTicketParkingLotState extends Equatable {
  LightScanTicketParkingLotState({this.lightScanTicketParkingLotModelObj});

  LightScanTicketParkingLotModel? lightScanTicketParkingLotModelObj;

  @override
  List<Object?> get props => [
        lightScanTicketParkingLotModelObj,
      ];
  LightScanTicketParkingLotState copyWith(
      {LightScanTicketParkingLotModel? lightScanTicketParkingLotModelObj}) {
    return LightScanTicketParkingLotState(
      lightScanTicketParkingLotModelObj: lightScanTicketParkingLotModelObj ??
          this.lightScanTicketParkingLotModelObj,
    );
  }
}
