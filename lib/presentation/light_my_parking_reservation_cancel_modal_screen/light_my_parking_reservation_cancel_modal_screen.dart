import 'bloc/light_my_parking_reservation_cancel_modal_bloc.dart';
import 'models/light_my_parking_reservation_cancel_modal_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/presentation/light_home_map_direction_version_two_page/light_home_map_direction_version_two_page.dart';
import 'package:parking_app/presentation/light_my_bookmark_page/light_my_bookmark_page.dart';
import 'package:parking_app/presentation/light_my_parking_reservation_completed_tab_container_page/light_my_parking_reservation_completed_tab_container_page.dart';
import 'package:parking_app/presentation/light_profile_settings_page/light_profile_settings_page.dart';
import 'package:parking_app/widgets/app_bar/appbar_image.dart';
import 'package:parking_app/widgets/app_bar/appbar_title.dart';
import 'package:parking_app/widgets/app_bar/custom_app_bar.dart';
import 'package:parking_app/widgets/custom_bottom_bar.dart';
import 'package:parking_app/widgets/custom_elevated_button.dart';
import 'package:parking_app/widgets/custom_outlined_button.dart';

class LightMyParkingReservationCancelModalScreen extends StatelessWidget {
  LightMyParkingReservationCancelModalScreen({Key? key})
      : super(
          key: key,
        );

  GlobalKey<NavigatorState> navigatorKey = GlobalKey();

  static Widget builder(BuildContext context) {
    return BlocProvider<LightMyParkingReservationCancelModalBloc>(
      create: (context) => LightMyParkingReservationCancelModalBloc(
          LightMyParkingReservationCancelModalState(
        lightMyParkingReservationCancelModalModelObj:
            LightMyParkingReservationCancelModalModel(),
      ))
        ..add(LightMyParkingReservationCancelModalInitialEvent()),
      child: LightMyParkingReservationCancelModalScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return BlocBuilder<LightMyParkingReservationCancelModalBloc,
        LightMyParkingReservationCancelModalState>(
      builder: (context, state) {
        return SafeArea(
          child: Scaffold(
            backgroundColor: appTheme.whiteA70001,
            appBar: CustomAppBar(
              height: getVerticalSize(
                65,
              ),
              title: AppbarTitle(
                text: "lbl_my_parking".tr,
                margin: getMargin(
                  left: 24,
                ),
              ),
              actions: [
                AppbarImage(
                  height: getSize(
                    28,
                  ),
                  width: getSize(
                    28,
                  ),
                  svgPath: ImageConstant.imgSearchGray900,
                  margin: getMargin(
                    left: 24,
                    right: 24,
                    bottom: 3,
                  ),
                ),
              ],
            ),
            body: SizedBox(
              height: getVerticalSize(
                727,
              ),
              width: double.maxFinite,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Padding(
                      padding: getPadding(
                        left: 24,
                        top: 31,
                        right: 24,
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                width: getHorizontalSize(
                                  119,
                                ),
                                padding: getPadding(
                                  left: 24,
                                  top: 9,
                                  right: 24,
                                  bottom: 9,
                                ),
                                decoration: AppDecoration
                                    .txtGradientnameprimarynameindigoA100
                                    .copyWith(
                                  borderRadius:
                                      BorderRadiusStyle.txtRoundedBorder22,
                                ),
                                child: Text(
                                  "lbl_ongoing".tr,
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.left,
                                  style: CustomTextStyles.titleMediumWhiteA70001
                                      .copyWith(
                                    letterSpacing: getHorizontalSize(
                                      0.2,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: getHorizontalSize(
                                  119,
                                ),
                                padding: getPadding(
                                  left: 13,
                                  top: 9,
                                  right: 13,
                                  bottom: 9,
                                ),
                                decoration: AppDecoration.txtOutline1.copyWith(
                                  borderRadius:
                                      BorderRadiusStyle.txtRoundedBorder22,
                                ),
                                child: Text(
                                  "lbl_completed".tr,
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.left,
                                  style: CustomTextStyles
                                      .titleMediumIndigo60001_1
                                      .copyWith(
                                    letterSpacing: getHorizontalSize(
                                      0.2,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: getHorizontalSize(
                                  118,
                                ),
                                padding: getPadding(
                                  left: 20,
                                  top: 11,
                                  right: 20,
                                  bottom: 11,
                                ),
                                decoration: AppDecoration.txtOutline1.copyWith(
                                  borderRadius:
                                      BorderRadiusStyle.txtRoundedBorder22,
                                ),
                                child: Text(
                                  "lbl_canceled".tr,
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.left,
                                  style: CustomTextStyles
                                      .titleMediumIndigo60001_1
                                      .copyWith(
                                    letterSpacing: getHorizontalSize(
                                      0.2,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            margin: getMargin(
                              top: 24,
                            ),
                            padding: getPadding(
                              all: 20,
                            ),
                            decoration: AppDecoration.outline4.copyWith(
                              borderRadius: BorderRadiusStyle.roundedBorder16,
                            ),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    CustomImageView(
                                      imagePath: ImageConstant.imgRectangle12,
                                      height: getSize(
                                        100,
                                      ),
                                      width: getSize(
                                        100,
                                      ),
                                      radius: BorderRadius.circular(
                                        getHorizontalSize(
                                          10,
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: getPadding(
                                        left: 16,
                                        top: 5,
                                        bottom: 6,
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Text(
                                            "lbl_san_manolia".tr,
                                            overflow: TextOverflow.ellipsis,
                                            textAlign: TextAlign.left,
                                            style: theme.textTheme.titleLarge,
                                          ),
                                          Padding(
                                            padding: getPadding(
                                              top: 12,
                                            ),
                                            child: Text(
                                              "msg_9569_trantow_courts3".tr,
                                              overflow: TextOverflow.ellipsis,
                                              textAlign: TextAlign.left,
                                              style: CustomTextStyles
                                                  .bodyMediumGray700
                                                  .copyWith(
                                                letterSpacing:
                                                    getHorizontalSize(
                                                  0.2,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: getPadding(
                                              top: 11,
                                            ),
                                            child: Row(
                                              children: [
                                                Padding(
                                                  padding: getPadding(
                                                    top: 2,
                                                    bottom: 1,
                                                  ),
                                                  child: Text(
                                                    "lbl_8_08".tr,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    textAlign: TextAlign.left,
                                                    style: CustomTextStyles
                                                        .titleMediumIndigoA400
                                                        .copyWith(
                                                      letterSpacing:
                                                          getHorizontalSize(
                                                        0.2,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Padding(
                                                  padding: getPadding(
                                                    left: 4,
                                                    top: 8,
                                                    bottom: 3,
                                                  ),
                                                  child: Text(
                                                    "lbl_4_hour".tr,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    textAlign: TextAlign.left,
                                                    style: CustomTextStyles
                                                        .bodySmallGray700
                                                        .copyWith(
                                                      letterSpacing:
                                                          getHorizontalSize(
                                                        0.2,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                CustomElevatedButton(
                                                  text: "lbl_now_active".tr,
                                                  margin: getMargin(
                                                    left: 14,
                                                  ),
                                                  buttonStyle: CustomButtonStyles
                                                      .fillIndigoA400
                                                      .copyWith(
                                                          fixedSize:
                                                              MaterialStateProperty
                                                                  .all<Size>(
                                                                      Size(
                                                    getHorizontalSize(
                                                      73,
                                                    ),
                                                    getVerticalSize(
                                                      24,
                                                    ),
                                                  ))),
                                                  buttonTextStyle:
                                                      CustomTextStyles
                                                          .labelMediumWhiteA70001,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: getPadding(
                                    top: 20,
                                  ),
                                  child: Divider(
                                    height: getVerticalSize(
                                      1,
                                    ),
                                    thickness: getVerticalSize(
                                      1,
                                    ),
                                    color: appTheme.gray200,
                                  ),
                                ),
                                Padding(
                                  padding: getPadding(
                                    top: 19,
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Expanded(
                                        child: CustomOutlinedButton(
                                          text: "lbl_view_timer".tr,
                                          margin: getMargin(
                                            right: 6,
                                          ),
                                          buttonStyle: CustomButtonStyles
                                              .outlineIndigo60001
                                              .copyWith(
                                                  fixedSize:
                                                      MaterialStateProperty.all<
                                                          Size>(Size(
                                            double.maxFinite,
                                            getVerticalSize(
                                              38,
                                            ),
                                          ))),
                                          buttonTextStyle: CustomTextStyles
                                              .titleMediumIndigo60001SemiBold16,
                                        ),
                                      ),
                                      Expanded(
                                        child: CustomElevatedButton(
                                          width: getHorizontalSize(
                                            164,
                                          ),
                                          height: getVerticalSize(
                                            38,
                                          ),
                                          text: "lbl_view_ticket".tr,
                                          margin: getMargin(
                                            left: 6,
                                          ),
                                          buttonStyle: CustomButtonStyles
                                              .gradientnameprimarynameindigoA100
                                              .copyWith(
                                                  fixedSize:
                                                      MaterialStateProperty.all<
                                                          Size>(Size(
                                            double.maxFinite,
                                            getVerticalSize(
                                              38,
                                            ),
                                          ))),
                                          decoration: CustomButtonStyles
                                              .gradientnameprimarynameindigoA100Decoration,
                                          buttonTextStyle: CustomTextStyles
                                              .titleMediumWhiteA70001SemiBold,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: getMargin(
                              top: 20,
                            ),
                            padding: getPadding(
                              all: 20,
                            ),
                            decoration: AppDecoration.outline4.copyWith(
                              borderRadius: BorderRadiusStyle.roundedBorder16,
                            ),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    CustomImageView(
                                      imagePath: ImageConstant.imgRectangle14,
                                      height: getSize(
                                        100,
                                      ),
                                      width: getSize(
                                        100,
                                      ),
                                      radius: BorderRadius.circular(
                                        getHorizontalSize(
                                          10,
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: getPadding(
                                        left: 16,
                                        top: 5,
                                        bottom: 6,
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Text(
                                            "lbl_buxton_path".tr,
                                            overflow: TextOverflow.ellipsis,
                                            textAlign: TextAlign.left,
                                            style: theme.textTheme.titleLarge,
                                          ),
                                          Padding(
                                            padding: getPadding(
                                              top: 13,
                                            ),
                                            child: Text(
                                              "msg_9_evergreen_drive".tr,
                                              overflow: TextOverflow.ellipsis,
                                              textAlign: TextAlign.left,
                                              style: CustomTextStyles
                                                  .bodyMediumGray700
                                                  .copyWith(
                                                letterSpacing:
                                                    getHorizontalSize(
                                                  0.2,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: getPadding(
                                              top: 9,
                                            ),
                                            child: Row(
                                              children: [
                                                Padding(
                                                  padding: getPadding(
                                                    top: 2,
                                                    bottom: 1,
                                                  ),
                                                  child: Text(
                                                    "lbl_5_70".tr,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    textAlign: TextAlign.left,
                                                    style: CustomTextStyles
                                                        .titleMediumIndigoA400
                                                        .copyWith(
                                                      letterSpacing:
                                                          getHorizontalSize(
                                                        0.2,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Padding(
                                                  padding: getPadding(
                                                    left: 4,
                                                    top: 8,
                                                    bottom: 3,
                                                  ),
                                                  child: Text(
                                                    "lbl_5_hour".tr,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    textAlign: TextAlign.left,
                                                    style: CustomTextStyles
                                                        .bodySmallGray700
                                                        .copyWith(
                                                      letterSpacing:
                                                          getHorizontalSize(
                                                        0.2,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  margin: getMargin(
                                                    left: 14,
                                                  ),
                                                  padding: getPadding(
                                                    left: 10,
                                                    top: 5,
                                                    right: 10,
                                                    bottom: 5,
                                                  ),
                                                  decoration: AppDecoration
                                                      .outline12
                                                      .copyWith(
                                                    borderRadius:
                                                        BorderRadiusStyle
                                                            .roundedBorder4,
                                                  ),
                                                  child: Text(
                                                    "lbl_paid".tr,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    textAlign: TextAlign.left,
                                                    style: CustomTextStyles
                                                        .labelMediumIndigoA400
                                                        .copyWith(
                                                      letterSpacing:
                                                          getHorizontalSize(
                                                        0.2,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: getPadding(
                                    top: 20,
                                  ),
                                  child: Divider(
                                    height: getVerticalSize(
                                      1,
                                    ),
                                    thickness: getVerticalSize(
                                      1,
                                    ),
                                    color: appTheme.gray200,
                                  ),
                                ),
                                Padding(
                                  padding: getPadding(
                                    top: 19,
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        margin: getMargin(
                                          right: 6,
                                        ),
                                        padding: getPadding(
                                          left: 26,
                                          top: 7,
                                          right: 26,
                                          bottom: 7,
                                        ),
                                        decoration:
                                            AppDecoration.outline3.copyWith(
                                          borderRadius:
                                              BorderRadiusStyle.circleBorder19,
                                        ),
                                        child: Padding(
                                          padding: getPadding(
                                            top: 4,
                                          ),
                                          child: Text(
                                            "lbl_cancel_booking".tr,
                                            overflow: TextOverflow.ellipsis,
                                            textAlign: TextAlign.left,
                                            style: CustomTextStyles
                                                .titleMediumIndigo60001SemiBold16
                                                .copyWith(
                                              letterSpacing: getHorizontalSize(
                                                0.2,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: getMargin(
                                          left: 6,
                                        ),
                                        padding: getPadding(
                                          left: 39,
                                          top: 8,
                                          right: 39,
                                          bottom: 8,
                                        ),
                                        decoration: AppDecoration
                                            .gradientnameprimarynameindigoA100
                                            .copyWith(
                                          borderRadius:
                                              BorderRadiusStyle.roundedBorder10,
                                        ),
                                        child: Text(
                                          "lbl_view_ticket".tr,
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.left,
                                          style: CustomTextStyles
                                              .titleMediumWhiteA70001SemiBold
                                              .copyWith(
                                            letterSpacing: getHorizontalSize(
                                              0.2,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  CustomImageView(
                    imagePath: ImageConstant.imgRectangle865x422,
                    height: getVerticalSize(
                      865,
                    ),
                    width: getHorizontalSize(
                      422,
                    ),
                    alignment: Alignment.center,
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      padding: getPadding(
                        left: 24,
                        top: 8,
                        right: 24,
                        bottom: 8,
                      ),
                      decoration: AppDecoration.outline5.copyWith(
                        borderRadius: BorderRadiusStyle.customBorderTL40,
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CustomImageView(
                            svgPath: ImageConstant.imgFrameGray30001,
                            height: getVerticalSize(
                              3,
                            ),
                            width: getHorizontalSize(
                              38,
                            ),
                          ),
                          Padding(
                            padding: getPadding(
                              top: 26,
                            ),
                            child: Text(
                              "lbl_cancel_parking".tr,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.left,
                              style: CustomTextStyles.headlineSmallRedA200,
                            ),
                          ),
                          Padding(
                            padding: getPadding(
                              top: 21,
                            ),
                            child: Divider(
                              height: getVerticalSize(
                                1,
                              ),
                              thickness: getVerticalSize(
                                1,
                              ),
                              color: appTheme.gray200,
                            ),
                          ),
                          Container(
                            width: getHorizontalSize(
                              340,
                            ),
                            margin: getMargin(
                              left: 19,
                              top: 22,
                              right: 19,
                            ),
                            child: Text(
                              "msg_are_you_sure_you".tr,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.center,
                              style: CustomTextStyles.titleLargeGray800,
                            ),
                          ),
                          Container(
                            width: getHorizontalSize(
                              372,
                            ),
                            margin: getMargin(
                              left: 3,
                              top: 8,
                              right: 3,
                            ),
                            child: Text(
                              "msg_only_80_of_the".tr,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.center,
                              style:
                                  CustomTextStyles.bodyMediumGray800.copyWith(
                                letterSpacing: getHorizontalSize(
                                  0.2,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: getPadding(
                              top: 22,
                              bottom: 40,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: CustomElevatedButton(
                                    text: "lbl_cancel".tr,
                                    margin: getMargin(
                                      right: 6,
                                    ),
                                    buttonStyle: CustomButtonStyles.fillBlue50
                                        .copyWith(
                                            fixedSize:
                                                MaterialStateProperty.all<Size>(
                                                    Size(
                                      double.maxFinite,
                                      getVerticalSize(
                                        58,
                                      ),
                                    ))),
                                    buttonTextStyle:
                                        CustomTextStyles.titleMediumIndigo60001,
                                  ),
                                ),
                                Expanded(
                                  child: CustomElevatedButton(
                                    width: getHorizontalSize(
                                      184,
                                    ),
                                    height: getVerticalSize(
                                      58,
                                    ),
                                    text: "lbl_yes_continue".tr,
                                    margin: getMargin(
                                      left: 6,
                                    ),
                                    buttonStyle: CustomButtonStyles
                                        .outlineDeeppurple100
                                        .copyWith(
                                            fixedSize:
                                                MaterialStateProperty.all<Size>(
                                                    Size(
                                      double.maxFinite,
                                      getVerticalSize(
                                        58,
                                      ),
                                    ))),
                                    decoration: CustomButtonStyles
                                        .outlineDeeppurple100Decoration,
                                    buttonTextStyle: CustomTextStyles
                                        .titleMediumWhiteA7000116,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            bottomNavigationBar: CustomBottomBar(
              onChanged: (BottomBarEnum type) {
                Navigator.pushNamed(
                    navigatorKey.currentContext!, getCurrentRoute(type));
              },
            ),
          ),
        );
      },
    );
  }

  ///Handling route based on bottom click actions
  String getCurrentRoute(BottomBarEnum type) {
    switch (type) {
      case BottomBarEnum.Home:
        return AppRoutes.lightHomeMapDirectionVersionTwoPage;
      case BottomBarEnum.Saved:
        return AppRoutes.lightMyBookmarkPage;
      case BottomBarEnum.Booking:
        return AppRoutes.lightMyParkingReservationCompletedTabContainerPage;
      case BottomBarEnum.Profile:
        return AppRoutes.lightProfileSettingsPage;
      default:
        return "/";
    }
  }

  ///Handling page based on route
  Widget getCurrentPage(
    BuildContext context,
    String currentRoute,
  ) {
    switch (currentRoute) {
      case AppRoutes.lightHomeMapDirectionVersionTwoPage:
        return LightHomeMapDirectionVersionTwoPage.builder(context);
      case AppRoutes.lightMyBookmarkPage:
        return LightMyBookmarkPage.builder(context);
      case AppRoutes.lightMyParkingReservationCompletedTabContainerPage:
        return LightMyParkingReservationCompletedTabContainerPage.builder(
            context);
      case AppRoutes.lightProfileSettingsPage:
        return LightProfileSettingsPage.builder(context);
      default:
        return DefaultWidget();
    }
  }
}
