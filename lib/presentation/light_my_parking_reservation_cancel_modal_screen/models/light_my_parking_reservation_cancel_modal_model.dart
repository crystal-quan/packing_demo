// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_my_parking_reservation_cancel_modal_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightMyParkingReservationCancelModalModel extends Equatable {LightMyParkingReservationCancelModalModel copyWith() { return LightMyParkingReservationCancelModalModel(
); } 
@override List<Object?> get props => [];
 }
