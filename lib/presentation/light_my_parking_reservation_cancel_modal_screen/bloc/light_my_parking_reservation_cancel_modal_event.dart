// ignore_for_file: must_be_immutable

part of 'light_my_parking_reservation_cancel_modal_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightMyParkingReservationCancelModal widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightMyParkingReservationCancelModalEvent extends Equatable {}

/// Event that is dispatched when the LightMyParkingReservationCancelModal widget is first created.
class LightMyParkingReservationCancelModalInitialEvent
    extends LightMyParkingReservationCancelModalEvent {
  @override
  List<Object?> get props => [];
}
