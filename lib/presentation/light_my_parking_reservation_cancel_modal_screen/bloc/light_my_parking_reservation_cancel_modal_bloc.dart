import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:parking_app/presentation/light_my_parking_reservation_cancel_modal_screen/models/light_my_parking_reservation_cancel_modal_model.dart';
part 'light_my_parking_reservation_cancel_modal_event.dart';
part 'light_my_parking_reservation_cancel_modal_state.dart';

/// A bloc that manages the state of a LightMyParkingReservationCancelModal according to the event that is dispatched to it.
class LightMyParkingReservationCancelModalBloc extends Bloc<
    LightMyParkingReservationCancelModalEvent,
    LightMyParkingReservationCancelModalState> {
  LightMyParkingReservationCancelModalBloc(
      LightMyParkingReservationCancelModalState initialState)
      : super(initialState) {
    on<LightMyParkingReservationCancelModalInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightMyParkingReservationCancelModalInitialEvent event,
    Emitter<LightMyParkingReservationCancelModalState> emit,
  ) async {}
}
