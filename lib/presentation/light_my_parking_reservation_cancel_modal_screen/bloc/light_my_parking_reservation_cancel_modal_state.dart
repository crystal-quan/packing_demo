// ignore_for_file: must_be_immutable

part of 'light_my_parking_reservation_cancel_modal_bloc.dart';

/// Represents the state of LightMyParkingReservationCancelModal in the application.
class LightMyParkingReservationCancelModalState extends Equatable {
  LightMyParkingReservationCancelModalState(
      {this.lightMyParkingReservationCancelModalModelObj});

  LightMyParkingReservationCancelModalModel?
      lightMyParkingReservationCancelModalModelObj;

  @override
  List<Object?> get props => [
        lightMyParkingReservationCancelModalModelObj,
      ];
  LightMyParkingReservationCancelModalState copyWith(
      {LightMyParkingReservationCancelModalModel?
          lightMyParkingReservationCancelModalModelObj}) {
    return LightMyParkingReservationCancelModalState(
      lightMyParkingReservationCancelModalModelObj:
          lightMyParkingReservationCancelModalModelObj ??
              this.lightMyParkingReservationCancelModalModelObj,
    );
  }
}
