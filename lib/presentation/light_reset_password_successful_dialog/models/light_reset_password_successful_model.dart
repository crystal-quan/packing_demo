// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_reset_password_successful_dialog],
/// and is typically used to hold data that is passed between different parts of the application.
class LightResetPasswordSuccessfulModel extends Equatable {LightResetPasswordSuccessfulModel copyWith() { return LightResetPasswordSuccessfulModel(
); } 
@override List<Object?> get props => [];
 }
