import 'bloc/light_reset_password_successful_bloc.dart';
import 'models/light_reset_password_successful_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_elevated_button.dart';

// ignore_for_file: must_be_immutable
class LightResetPasswordSuccessfulDialog extends StatelessWidget {
  const LightResetPasswordSuccessfulDialog({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LightResetPasswordSuccessfulBloc>(
      create: (context) =>
          LightResetPasswordSuccessfulBloc(LightResetPasswordSuccessfulState(
        lightResetPasswordSuccessfulModelObj:
            LightResetPasswordSuccessfulModel(),
      ))
            ..add(LightResetPasswordSuccessfulInitialEvent()),
      child: LightResetPasswordSuccessfulDialog(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return Container(
      width: getHorizontalSize(
        340,
      ),
      padding: getPadding(
        all: 32,
      ),
      decoration: AppDecoration.fill.copyWith(
        borderRadius: BorderRadiusStyle.roundedBorder24,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: getVerticalSize(
              295,
            ),
            width: getHorizontalSize(
              256,
            ),
            child: Stack(
              alignment: Alignment.topCenter,
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: getPadding(
                      left: 26,
                      right: 26,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "msg_congratulations".tr,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.left,
                          style: CustomTextStyles.headlineSmallIndigo60001,
                        ),
                        Padding(
                          padding: getPadding(
                            top: 16,
                          ),
                          child: Text(
                            "msg_your_account_is".tr,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.left,
                            style: theme.textTheme.bodyLarge!.copyWith(
                              letterSpacing: getHorizontalSize(
                                0.2,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                CustomImageView(
                  svgPath: ImageConstant.imgMobileloginbroWhiteA70001,
                  height: getSize(
                    256,
                  ),
                  width: getSize(
                    256,
                  ),
                  alignment: Alignment.topCenter,
                ),
              ],
            ),
          ),
          CustomElevatedButton(
            width: getHorizontalSize(
              276,
            ),
            height: getVerticalSize(
              58,
            ),
            text: "lbl_go_to_homepage".tr,
            margin: getMargin(
              top: 24,
            ),
            buttonStyle:
                CustomButtonStyles.gradientnameprimarynameindigoA100.copyWith(
                    fixedSize: MaterialStateProperty.all<Size>(Size(
              double.maxFinite,
              getVerticalSize(
                58,
              ),
            ))),
            decoration:
                CustomButtonStyles.gradientnameprimarynameindigoA100Decoration,
            buttonTextStyle: CustomTextStyles.titleMediumWhiteA7000116,
          ),
        ],
      ),
    );
  }
}
