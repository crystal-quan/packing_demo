// ignore_for_file: must_be_immutable

part of 'light_reset_password_successful_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightResetPasswordSuccessful widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightResetPasswordSuccessfulEvent extends Equatable {}

/// Event that is dispatched when the LightResetPasswordSuccessful widget is first created.
class LightResetPasswordSuccessfulInitialEvent
    extends LightResetPasswordSuccessfulEvent {
  @override
  List<Object?> get props => [];
}
