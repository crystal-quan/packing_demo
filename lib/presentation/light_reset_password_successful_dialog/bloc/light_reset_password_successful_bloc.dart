import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:parking_app/presentation/light_reset_password_successful_dialog/models/light_reset_password_successful_model.dart';
part 'light_reset_password_successful_event.dart';
part 'light_reset_password_successful_state.dart';

/// A bloc that manages the state of a LightResetPasswordSuccessful according to the event that is dispatched to it.
class LightResetPasswordSuccessfulBloc extends Bloc<
    LightResetPasswordSuccessfulEvent, LightResetPasswordSuccessfulState> {
  LightResetPasswordSuccessfulBloc(
      LightResetPasswordSuccessfulState initialState)
      : super(initialState) {
    on<LightResetPasswordSuccessfulInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightResetPasswordSuccessfulInitialEvent event,
    Emitter<LightResetPasswordSuccessfulState> emit,
  ) async {}
}
