// ignore_for_file: must_be_immutable

part of 'light_reset_password_successful_bloc.dart';

/// Represents the state of LightResetPasswordSuccessful in the application.
class LightResetPasswordSuccessfulState extends Equatable {
  LightResetPasswordSuccessfulState(
      {this.lightResetPasswordSuccessfulModelObj});

  LightResetPasswordSuccessfulModel? lightResetPasswordSuccessfulModelObj;

  @override
  List<Object?> get props => [
        lightResetPasswordSuccessfulModelObj,
      ];
  LightResetPasswordSuccessfulState copyWith(
      {LightResetPasswordSuccessfulModel?
          lightResetPasswordSuccessfulModelObj}) {
    return LightResetPasswordSuccessfulState(
      lightResetPasswordSuccessfulModelObj:
          lightResetPasswordSuccessfulModelObj ??
              this.lightResetPasswordSuccessfulModelObj,
    );
  }
}
