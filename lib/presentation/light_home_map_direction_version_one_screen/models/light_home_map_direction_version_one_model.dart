// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'autolayouthoriz2_item_model.dart';/// This class defines the variables used in the [light_home_map_direction_version_one_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightHomeMapDirectionVersionOneModel extends Equatable {LightHomeMapDirectionVersionOneModel({this.autolayouthoriz2ItemList = const []});

List<Autolayouthoriz2ItemModel> autolayouthoriz2ItemList;

LightHomeMapDirectionVersionOneModel copyWith({List<Autolayouthoriz2ItemModel>? autolayouthoriz2ItemList}) { return LightHomeMapDirectionVersionOneModel(
autolayouthoriz2ItemList : autolayouthoriz2ItemList ?? this.autolayouthoriz2ItemList,
); } 
@override List<Object?> get props => [autolayouthoriz2ItemList];
 }
