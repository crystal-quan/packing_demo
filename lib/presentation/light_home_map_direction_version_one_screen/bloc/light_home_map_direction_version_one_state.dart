// ignore_for_file: must_be_immutable

part of 'light_home_map_direction_version_one_bloc.dart';

/// Represents the state of LightHomeMapDirectionVersionOne in the application.
class LightHomeMapDirectionVersionOneState extends Equatable {
  LightHomeMapDirectionVersionOneState(
      {this.lightHomeMapDirectionVersionOneModelObj});

  LightHomeMapDirectionVersionOneModel? lightHomeMapDirectionVersionOneModelObj;

  @override
  List<Object?> get props => [
        lightHomeMapDirectionVersionOneModelObj,
      ];
  LightHomeMapDirectionVersionOneState copyWith(
      {LightHomeMapDirectionVersionOneModel?
          lightHomeMapDirectionVersionOneModelObj}) {
    return LightHomeMapDirectionVersionOneState(
      lightHomeMapDirectionVersionOneModelObj:
          lightHomeMapDirectionVersionOneModelObj ??
              this.lightHomeMapDirectionVersionOneModelObj,
    );
  }
}
