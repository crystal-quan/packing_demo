// ignore_for_file: must_be_immutable

part of 'light_home_map_direction_version_one_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightHomeMapDirectionVersionOne widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightHomeMapDirectionVersionOneEvent extends Equatable {}

/// Event that is dispatched when the LightHomeMapDirectionVersionOne widget is first created.
class LightHomeMapDirectionVersionOneInitialEvent
    extends LightHomeMapDirectionVersionOneEvent {
  @override
  List<Object?> get props => [];
}
