import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/autolayouthoriz2_item_model.dart';
import 'package:parking_app/presentation/light_home_map_direction_version_one_screen/models/light_home_map_direction_version_one_model.dart';
part 'light_home_map_direction_version_one_event.dart';
part 'light_home_map_direction_version_one_state.dart';

/// A bloc that manages the state of a LightHomeMapDirectionVersionOne according to the event that is dispatched to it.
class LightHomeMapDirectionVersionOneBloc extends Bloc<
    LightHomeMapDirectionVersionOneEvent,
    LightHomeMapDirectionVersionOneState> {
  LightHomeMapDirectionVersionOneBloc(
      LightHomeMapDirectionVersionOneState initialState)
      : super(initialState) {
    on<LightHomeMapDirectionVersionOneInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightHomeMapDirectionVersionOneInitialEvent event,
    Emitter<LightHomeMapDirectionVersionOneState> emit,
  ) async {
    emit(state.copyWith(
        lightHomeMapDirectionVersionOneModelObj:
            state.lightHomeMapDirectionVersionOneModelObj?.copyWith(
      autolayouthoriz2ItemList: fillAutolayouthoriz2ItemList(),
    )));
  }

  List<Autolayouthoriz2ItemModel> fillAutolayouthoriz2ItemList() {
    return List.generate(4, (index) => Autolayouthoriz2ItemModel());
  }
}
