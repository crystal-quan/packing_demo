// ignore_for_file: must_be_immutable

part of 'light_forgot_password_filled_otp_bloc.dart';

/// Represents the state of LightForgotPasswordFilledOtp in the application.
class LightForgotPasswordFilledOtpState extends Equatable {
  LightForgotPasswordFilledOtpState({
    this.otpController,
    this.lightForgotPasswordFilledOtpModelObj,
  });

  TextEditingController? otpController;

  LightForgotPasswordFilledOtpModel? lightForgotPasswordFilledOtpModelObj;

  @override
  List<Object?> get props => [
        otpController,
        lightForgotPasswordFilledOtpModelObj,
      ];
  LightForgotPasswordFilledOtpState copyWith({
    TextEditingController? otpController,
    LightForgotPasswordFilledOtpModel? lightForgotPasswordFilledOtpModelObj,
  }) {
    return LightForgotPasswordFilledOtpState(
      otpController: otpController ?? this.otpController,
      lightForgotPasswordFilledOtpModelObj:
          lightForgotPasswordFilledOtpModelObj ??
              this.lightForgotPasswordFilledOtpModelObj,
    );
  }
}
