// ignore_for_file: must_be_immutable

part of 'light_forgot_password_filled_otp_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightForgotPasswordFilledOtp widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightForgotPasswordFilledOtpEvent extends Equatable {}

/// Event that is dispatched when the LightForgotPasswordFilledOtp widget is first created.
class LightForgotPasswordFilledOtpInitialEvent
    extends LightForgotPasswordFilledOtpEvent {
  @override
  List<Object?> get props => [];
}

///event for OTP auto fill
class ChangeOTPEvent extends LightForgotPasswordFilledOtpEvent {
  ChangeOTPEvent({required this.code});

  String code;

  @override
  List<Object?> get props => [
        code,
      ];
}
