import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_forgot_password_filled_otp_screen/models/light_forgot_password_filled_otp_model.dart';import 'package:sms_autofill/sms_autofill.dart';part 'light_forgot_password_filled_otp_event.dart';part 'light_forgot_password_filled_otp_state.dart';/// A bloc that manages the state of a LightForgotPasswordFilledOtp according to the event that is dispatched to it.
class LightForgotPasswordFilledOtpBloc extends Bloc<LightForgotPasswordFilledOtpEvent, LightForgotPasswordFilledOtpState> with  CodeAutoFill {LightForgotPasswordFilledOtpBloc(LightForgotPasswordFilledOtpState initialState) : super(initialState) { on<LightForgotPasswordFilledOtpInitialEvent>(_onInitialize); on<ChangeOTPEvent>(_changeOTP); }

@override codeUpdated() { add(ChangeOTPEvent(code: code!)); } 
_changeOTP(ChangeOTPEvent event, Emitter<LightForgotPasswordFilledOtpState> emit, ) { emit(state.copyWith(otpController: TextEditingController(text: event.code))); } 
_onInitialize(LightForgotPasswordFilledOtpInitialEvent event, Emitter<LightForgotPasswordFilledOtpState> emit, ) async  { emit(state.copyWith(otpController: TextEditingController())); listenForCode(); } 
 }
