import 'bloc/light_bookmark_remove_modal_bloc.dart';
import 'models/light_bookmark_remove_modal_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_elevated_button.dart';

// ignore_for_file: must_be_immutable
class LightBookmarkRemoveModalBottomsheet extends StatelessWidget {
  const LightBookmarkRemoveModalBottomsheet({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LightBookmarkRemoveModalBloc>(
      create: (context) =>
          LightBookmarkRemoveModalBloc(LightBookmarkRemoveModalState(
        lightBookmarkRemoveModalModelObj: LightBookmarkRemoveModalModel(),
      ))
            ..add(LightBookmarkRemoveModalInitialEvent()),
      child: LightBookmarkRemoveModalBottomsheet(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SingleChildScrollView(
      child: SingleChildScrollView(
        child: Container(
          padding: getPadding(
            left: 24,
            top: 8,
            right: 24,
            bottom: 8,
          ),
          decoration: AppDecoration.outline11.copyWith(
            borderRadius: BorderRadiusStyle.customBorderTL40,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              CustomImageView(
                svgPath: ImageConstant.imgFrameGray30001,
                height: getVerticalSize(
                  3,
                ),
                width: getHorizontalSize(
                  38,
                ),
              ),
              Padding(
                padding: getPadding(
                  top: 23,
                ),
                child: Text(
                  "msg_remove_from_bookmark".tr,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.left,
                  style: theme.textTheme.headlineSmall,
                ),
              ),
              Padding(
                padding: getPadding(
                  top: 24,
                ),
                child: Divider(
                  height: getVerticalSize(
                    1,
                  ),
                  thickness: getVerticalSize(
                    1,
                  ),
                  color: appTheme.gray200,
                ),
              ),
              Container(
                margin: getMargin(
                  top: 23,
                ),
                padding: getPadding(
                  all: 16,
                ),
                decoration: AppDecoration.outline4.copyWith(
                  borderRadius: BorderRadiusStyle.roundedBorder16,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomImageView(
                      imagePath: ImageConstant.imgRectangle5,
                      height: getSize(
                        70,
                      ),
                      width: getSize(
                        70,
                      ),
                      radius: BorderRadius.circular(
                        getHorizontalSize(
                          20,
                        ),
                      ),
                    ),
                    Padding(
                      padding: getPadding(
                        left: 24,
                        top: 8,
                        bottom: 9,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "lbl_welbeck_north".tr,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.left,
                            style: theme.textTheme.titleLarge,
                          ),
                          Padding(
                            padding: getPadding(
                              top: 11,
                            ),
                            child: Text(
                              "msg_7159_washington".tr,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.left,
                              style:
                                  CustomTextStyles.titleSmallGray700.copyWith(
                                letterSpacing: getHorizontalSize(
                                  0.2,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Spacer(),
                    CustomImageView(
                      svgPath: ImageConstant.imgBookmarkIndigo6000128x28,
                      height: getSize(
                        28,
                      ),
                      width: getSize(
                        28,
                      ),
                      margin: getMargin(
                        top: 21,
                        right: 4,
                        bottom: 21,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: getPadding(
                  top: 24,
                  bottom: 40,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: CustomElevatedButton(
                        text: "lbl_cancel".tr,
                        margin: getMargin(
                          right: 6,
                        ),
                        buttonStyle: CustomButtonStyles.fillBlue50.copyWith(
                            fixedSize: MaterialStateProperty.all<Size>(Size(
                          double.maxFinite,
                          getVerticalSize(
                            58,
                          ),
                        ))),
                        buttonTextStyle:
                            CustomTextStyles.titleMediumIndigo60001,
                      ),
                    ),
                    Expanded(
                      child: CustomElevatedButton(
                        width: getHorizontalSize(
                          184,
                        ),
                        height: getVerticalSize(
                          58,
                        ),
                        text: "lbl_yes_remove".tr,
                        margin: getMargin(
                          left: 6,
                        ),
                        buttonStyle:
                            CustomButtonStyles.outlineDeeppurple100.copyWith(
                                fixedSize: MaterialStateProperty.all<Size>(Size(
                          double.maxFinite,
                          getVerticalSize(
                            58,
                          ),
                        ))),
                        decoration:
                            CustomButtonStyles.outlineDeeppurple100Decoration,
                        buttonTextStyle:
                            CustomTextStyles.titleMediumWhiteA7000116,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
