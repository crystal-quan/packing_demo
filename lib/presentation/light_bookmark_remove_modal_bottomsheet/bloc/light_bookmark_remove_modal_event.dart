// ignore_for_file: must_be_immutable

part of 'light_bookmark_remove_modal_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightBookmarkRemoveModal widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightBookmarkRemoveModalEvent extends Equatable {}

/// Event that is dispatched when the LightBookmarkRemoveModal widget is first created.
class LightBookmarkRemoveModalInitialEvent
    extends LightBookmarkRemoveModalEvent {
  @override
  List<Object?> get props => [];
}
