import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:parking_app/presentation/light_bookmark_remove_modal_bottomsheet/models/light_bookmark_remove_modal_model.dart';
part 'light_bookmark_remove_modal_event.dart';
part 'light_bookmark_remove_modal_state.dart';

/// A bloc that manages the state of a LightBookmarkRemoveModal according to the event that is dispatched to it.
class LightBookmarkRemoveModalBloc
    extends Bloc<LightBookmarkRemoveModalEvent, LightBookmarkRemoveModalState> {
  LightBookmarkRemoveModalBloc(LightBookmarkRemoveModalState initialState)
      : super(initialState) {
    on<LightBookmarkRemoveModalInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightBookmarkRemoveModalInitialEvent event,
    Emitter<LightBookmarkRemoveModalState> emit,
  ) async {}
}
