// ignore_for_file: must_be_immutable

part of 'light_bookmark_remove_modal_bloc.dart';

/// Represents the state of LightBookmarkRemoveModal in the application.
class LightBookmarkRemoveModalState extends Equatable {
  LightBookmarkRemoveModalState({this.lightBookmarkRemoveModalModelObj});

  LightBookmarkRemoveModalModel? lightBookmarkRemoveModalModelObj;

  @override
  List<Object?> get props => [
        lightBookmarkRemoveModalModelObj,
      ];
  LightBookmarkRemoveModalState copyWith(
      {LightBookmarkRemoveModalModel? lightBookmarkRemoveModalModelObj}) {
    return LightBookmarkRemoveModalState(
      lightBookmarkRemoveModalModelObj: lightBookmarkRemoveModalModelObj ??
          this.lightBookmarkRemoveModalModelObj,
    );
  }
}
