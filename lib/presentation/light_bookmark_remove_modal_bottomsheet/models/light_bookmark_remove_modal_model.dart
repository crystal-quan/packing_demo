// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_bookmark_remove_modal_bottomsheet],
/// and is typically used to hold data that is passed between different parts of the application.
class LightBookmarkRemoveModalModel extends Equatable {LightBookmarkRemoveModalModel copyWith() { return LightBookmarkRemoveModalModel(
); } 
@override List<Object?> get props => [];
 }
