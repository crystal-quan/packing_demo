// ignore_for_file: must_be_immutable

part of 'light_parking_pick_a_spot_tab_container_bloc.dart';

/// Represents the state of LightParkingPickASpotTabContainer in the application.
class LightParkingPickASpotTabContainerState extends Equatable {
  LightParkingPickASpotTabContainerState(
      {this.lightParkingPickASpotTabContainerModelObj});

  LightParkingPickASpotTabContainerModel?
      lightParkingPickASpotTabContainerModelObj;

  @override
  List<Object?> get props => [
        lightParkingPickASpotTabContainerModelObj,
      ];
  LightParkingPickASpotTabContainerState copyWith(
      {LightParkingPickASpotTabContainerModel?
          lightParkingPickASpotTabContainerModelObj}) {
    return LightParkingPickASpotTabContainerState(
      lightParkingPickASpotTabContainerModelObj:
          lightParkingPickASpotTabContainerModelObj ??
              this.lightParkingPickASpotTabContainerModelObj,
    );
  }
}
