// ignore_for_file: must_be_immutable

part of 'light_parking_pick_a_spot_tab_container_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightParkingPickASpotTabContainer widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightParkingPickASpotTabContainerEvent extends Equatable {}

/// Event that is dispatched when the LightParkingPickASpotTabContainer widget is first created.
class LightParkingPickASpotTabContainerInitialEvent
    extends LightParkingPickASpotTabContainerEvent {
  @override
  List<Object?> get props => [];
}
