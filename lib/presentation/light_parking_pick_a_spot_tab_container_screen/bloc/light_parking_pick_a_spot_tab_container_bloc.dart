import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_parking_pick_a_spot_tab_container_screen/models/light_parking_pick_a_spot_tab_container_model.dart';part 'light_parking_pick_a_spot_tab_container_event.dart';part 'light_parking_pick_a_spot_tab_container_state.dart';/// A bloc that manages the state of a LightParkingPickASpotTabContainer according to the event that is dispatched to it.
class LightParkingPickASpotTabContainerBloc extends Bloc<LightParkingPickASpotTabContainerEvent, LightParkingPickASpotTabContainerState> {LightParkingPickASpotTabContainerBloc(LightParkingPickASpotTabContainerState initialState) : super(initialState) { on<LightParkingPickASpotTabContainerInitialEvent>(_onInitialize); }

_onInitialize(LightParkingPickASpotTabContainerInitialEvent event, Emitter<LightParkingPickASpotTabContainerState> emit, ) async  {  } 
 }
