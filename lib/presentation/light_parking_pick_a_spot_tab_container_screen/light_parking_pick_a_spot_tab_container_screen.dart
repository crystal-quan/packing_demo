import 'bloc/light_parking_pick_a_spot_tab_container_bloc.dart';import 'models/light_parking_pick_a_spot_tab_container_model.dart';import 'package:flutter/material.dart';import 'package:parking_app/core/app_export.dart';import 'package:parking_app/presentation/light_parking_pick_a_spot_page/light_parking_pick_a_spot_page.dart';import 'package:parking_app/widgets/app_bar/appbar_image.dart';import 'package:parking_app/widgets/app_bar/appbar_title.dart';import 'package:parking_app/widgets/app_bar/custom_app_bar.dart';class LightParkingPickASpotTabContainerScreen extends StatefulWidget {const LightParkingPickASpotTabContainerScreen({Key? key}) : super(key: key);

@override LightParkingPickASpotTabContainerScreenState createState() =>  LightParkingPickASpotTabContainerScreenState();
static Widget builder(BuildContext context) { return BlocProvider<LightParkingPickASpotTabContainerBloc>(create: (context) => LightParkingPickASpotTabContainerBloc(LightParkingPickASpotTabContainerState(lightParkingPickASpotTabContainerModelObj: LightParkingPickASpotTabContainerModel()))..add(LightParkingPickASpotTabContainerInitialEvent()), child: LightParkingPickASpotTabContainerScreen()); } 
 }

// ignore_for_file: must_be_immutable
class LightParkingPickASpotTabContainerScreenState extends State<LightParkingPickASpotTabContainerScreen> with  TickerProviderStateMixin {late TabController tabviewController;

@override void initState() { super.initState(); tabviewController = TabController(length: 3, vsync: this); } 
@override Widget build(BuildContext context) { mediaQueryData = MediaQuery.of(context); return BlocBuilder<LightParkingPickASpotTabContainerBloc, LightParkingPickASpotTabContainerState>(builder: (context, state) {return SafeArea(child: Scaffold(backgroundColor: appTheme.whiteA70001, appBar: CustomAppBar(height: getVerticalSize(77), leadingWidth: 52, leading: AppbarImage(height: getSize(28), width: getSize(28), svgPath: ImageConstant.imgArrowleft, margin: getMargin(left: 24, top: 12, bottom: 15), onTap: () {onTapArrowleft10(context);}), title: AppbarTitle(text: "msg_pick_parking_spot".tr, margin: getMargin(left: 16))), body: SizedBox(width: double.maxFinite, child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [Container(height: getVerticalSize(38), width: getHorizontalSize(380), margin: getMargin(top: 18), child: TabBar(controller: tabviewController, labelColor: appTheme.whiteA70001, labelStyle: TextStyle(), unselectedLabelColor: appTheme.indigo60001, unselectedLabelStyle: TextStyle(), indicator: BoxDecoration(color: appTheme.indigoA10004, borderRadius: BorderRadius.circular(getHorizontalSize(19))), tabs: [Tab(child: Text("lbl_1st_floor".tr, overflow: TextOverflow.ellipsis)), Tab(child: Text("lbl_2nd_floor".tr, overflow: TextOverflow.ellipsis)), Tab(child: Text("lbl_3rd_floor".tr, overflow: TextOverflow.ellipsis))])), SizedBox(height: getVerticalSize(747), child: TabBarView(controller: tabviewController, children: [LightParkingPickASpotPage.builder(context), LightParkingPickASpotPage.builder(context), LightParkingPickASpotPage.builder(context)]))]))));}); } 


/// Navigates to the previous screen.
///
/// This function takes a [BuildContext] object as a parameter, which is
/// used to build the navigation stack. When the action is triggered, this
/// function uses the [NavigatorService] to navigate to the previous screen
/// in the navigation stack.
onTapArrowleft10(BuildContext context) { NavigatorService.goBack(); } 
 }
