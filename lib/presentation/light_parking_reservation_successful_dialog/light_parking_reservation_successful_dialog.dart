import 'bloc/light_parking_reservation_successful_bloc.dart';
import 'models/light_parking_reservation_successful_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_elevated_button.dart';

// ignore_for_file: must_be_immutable
class LightParkingReservationSuccessfulDialog extends StatelessWidget {
  const LightParkingReservationSuccessfulDialog({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LightParkingReservationSuccessfulBloc>(
      create: (context) => LightParkingReservationSuccessfulBloc(
          LightParkingReservationSuccessfulState(
        lightParkingReservationSuccessfulModelObj:
            LightParkingReservationSuccessfulModel(),
      ))
        ..add(LightParkingReservationSuccessfulInitialEvent()),
      child: LightParkingReservationSuccessfulDialog(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return Container(
      width: getHorizontalSize(
        340,
      ),
      padding: getPadding(
        left: 32,
        top: 12,
        right: 32,
        bottom: 12,
      ),
      decoration: AppDecoration.fill.copyWith(
        borderRadius: BorderRadiusStyle.roundedBorder10,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Padding(
            padding: getPadding(
              top: 239,
            ),
            child: Text(
              "lbl_successful".tr,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.left,
              style: CustomTextStyles.headlineSmallIndigo60001,
            ),
          ),
          Container(
            width: getHorizontalSize(
              263,
            ),
            margin: getMargin(
              left: 6,
              top: 17,
              right: 6,
            ),
            child: Text(
              "msg_successfully_made".tr,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              style: theme.textTheme.bodyLarge!.copyWith(
                letterSpacing: getHorizontalSize(
                  0.2,
                ),
              ),
            ),
          ),
          CustomElevatedButton(
            width: getHorizontalSize(
              276,
            ),
            height: getVerticalSize(
              58,
            ),
            text: "msg_view_parking_ticket".tr,
            margin: getMargin(
              top: 29,
            ),
            buttonStyle:
                CustomButtonStyles.gradientnameprimarynameindigoA100.copyWith(
                    fixedSize: MaterialStateProperty.all<Size>(Size(
              double.maxFinite,
              getVerticalSize(
                58,
              ),
            ))),
            decoration:
                CustomButtonStyles.gradientnameprimarynameindigoA100Decoration,
            buttonTextStyle: CustomTextStyles.titleMediumWhiteA7000116,
          ),
          CustomElevatedButton(
            text: "lbl_cancel".tr,
            margin: getMargin(
              top: 12,
            ),
            buttonStyle: CustomButtonStyles.fillGray5003.copyWith(
                fixedSize: MaterialStateProperty.all<Size>(Size(
              double.maxFinite,
              getVerticalSize(
                58,
              ),
            ))),
            buttonTextStyle: CustomTextStyles.titleMediumIndigo60001,
          ),
        ],
      ),
    );
  }
}
