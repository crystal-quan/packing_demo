// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_parking_reservation_successful_dialog],
/// and is typically used to hold data that is passed between different parts of the application.
class LightParkingReservationSuccessfulModel extends Equatable {LightParkingReservationSuccessfulModel copyWith() { return LightParkingReservationSuccessfulModel(
); } 
@override List<Object?> get props => [];
 }
