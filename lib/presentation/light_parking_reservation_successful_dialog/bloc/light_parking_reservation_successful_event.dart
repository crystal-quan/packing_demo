// ignore_for_file: must_be_immutable

part of 'light_parking_reservation_successful_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightParkingReservationSuccessful widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightParkingReservationSuccessfulEvent extends Equatable {}

/// Event that is dispatched when the LightParkingReservationSuccessful widget is first created.
class LightParkingReservationSuccessfulInitialEvent
    extends LightParkingReservationSuccessfulEvent {
  @override
  List<Object?> get props => [];
}
