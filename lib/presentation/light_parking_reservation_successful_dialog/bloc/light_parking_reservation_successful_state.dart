// ignore_for_file: must_be_immutable

part of 'light_parking_reservation_successful_bloc.dart';

/// Represents the state of LightParkingReservationSuccessful in the application.
class LightParkingReservationSuccessfulState extends Equatable {
  LightParkingReservationSuccessfulState(
      {this.lightParkingReservationSuccessfulModelObj});

  LightParkingReservationSuccessfulModel?
      lightParkingReservationSuccessfulModelObj;

  @override
  List<Object?> get props => [
        lightParkingReservationSuccessfulModelObj,
      ];
  LightParkingReservationSuccessfulState copyWith(
      {LightParkingReservationSuccessfulModel?
          lightParkingReservationSuccessfulModelObj}) {
    return LightParkingReservationSuccessfulState(
      lightParkingReservationSuccessfulModelObj:
          lightParkingReservationSuccessfulModelObj ??
              this.lightParkingReservationSuccessfulModelObj,
    );
  }
}
