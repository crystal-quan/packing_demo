import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:parking_app/presentation/light_parking_reservation_successful_dialog/models/light_parking_reservation_successful_model.dart';
part 'light_parking_reservation_successful_event.dart';
part 'light_parking_reservation_successful_state.dart';

/// A bloc that manages the state of a LightParkingReservationSuccessful according to the event that is dispatched to it.
class LightParkingReservationSuccessfulBloc extends Bloc<
    LightParkingReservationSuccessfulEvent,
    LightParkingReservationSuccessfulState> {
  LightParkingReservationSuccessfulBloc(
      LightParkingReservationSuccessfulState initialState)
      : super(initialState) {
    on<LightParkingReservationSuccessfulInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightParkingReservationSuccessfulInitialEvent event,
    Emitter<LightParkingReservationSuccessfulState> emit,
  ) async {}
}
