import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_sign_in_blank_form_screen/models/light_sign_in_blank_form_model.dart';part 'light_sign_in_blank_form_event.dart';part 'light_sign_in_blank_form_state.dart';/// A bloc that manages the state of a LightSignInBlankForm according to the event that is dispatched to it.
class LightSignInBlankFormBloc extends Bloc<LightSignInBlankFormEvent, LightSignInBlankFormState> {LightSignInBlankFormBloc(LightSignInBlankFormState initialState) : super(initialState) { on<LightSignInBlankFormInitialEvent>(_onInitialize); on<ChangePasswordVisibilityEvent>(_changePasswordVisibility); on<ChangeCheckBoxEvent>(_changeCheckBox); }

_changePasswordVisibility(ChangePasswordVisibilityEvent event, Emitter<LightSignInBlankFormState> emit, ) { emit(state.copyWith(isShowPassword: event.value)); } 
_changeCheckBox(ChangeCheckBoxEvent event, Emitter<LightSignInBlankFormState> emit, ) { emit(state.copyWith(isCheckbox: event.value)); } 
_onInitialize(LightSignInBlankFormInitialEvent event, Emitter<LightSignInBlankFormState> emit, ) async  { emit(state.copyWith(emailController: TextEditingController(), passwordController: TextEditingController(), isShowPassword: true, isCheckbox: false)); } 
 }
