// ignore_for_file: must_be_immutable

part of 'light_sign_in_blank_form_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightSignInBlankForm widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightSignInBlankFormEvent extends Equatable {}

/// Event that is dispatched when the LightSignInBlankForm widget is first created.
class LightSignInBlankFormInitialEvent extends LightSignInBlankFormEvent {
  @override
  List<Object?> get props => [];
}

///Event for changing password visibility
class ChangePasswordVisibilityEvent extends LightSignInBlankFormEvent {
  ChangePasswordVisibilityEvent({required this.value});

  bool value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing checkbox
class ChangeCheckBoxEvent extends LightSignInBlankFormEvent {
  ChangeCheckBoxEvent({required this.value});

  bool value;

  @override
  List<Object?> get props => [
        value,
      ];
}
