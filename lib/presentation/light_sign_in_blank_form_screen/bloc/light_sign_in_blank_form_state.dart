// ignore_for_file: must_be_immutable

part of 'light_sign_in_blank_form_bloc.dart';

/// Represents the state of LightSignInBlankForm in the application.
class LightSignInBlankFormState extends Equatable {
  LightSignInBlankFormState({
    this.emailController,
    this.passwordController,
    this.isShowPassword = true,
    this.isCheckbox = false,
    this.lightSignInBlankFormModelObj,
  });

  TextEditingController? emailController;

  TextEditingController? passwordController;

  LightSignInBlankFormModel? lightSignInBlankFormModelObj;

  bool isShowPassword;

  bool isCheckbox;

  @override
  List<Object?> get props => [
        emailController,
        passwordController,
        isShowPassword,
        isCheckbox,
        lightSignInBlankFormModelObj,
      ];
  LightSignInBlankFormState copyWith({
    TextEditingController? emailController,
    TextEditingController? passwordController,
    bool? isShowPassword,
    bool? isCheckbox,
    LightSignInBlankFormModel? lightSignInBlankFormModelObj,
  }) {
    return LightSignInBlankFormState(
      emailController: emailController ?? this.emailController,
      passwordController: passwordController ?? this.passwordController,
      isShowPassword: isShowPassword ?? this.isShowPassword,
      isCheckbox: isCheckbox ?? this.isCheckbox,
      lightSignInBlankFormModelObj:
          lightSignInBlankFormModelObj ?? this.lightSignInBlankFormModelObj,
    );
  }
}
