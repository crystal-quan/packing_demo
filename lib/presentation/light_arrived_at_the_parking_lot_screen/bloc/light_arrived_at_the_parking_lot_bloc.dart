import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:parking_app/presentation/light_arrived_at_the_parking_lot_screen/models/light_arrived_at_the_parking_lot_model.dart';
part 'light_arrived_at_the_parking_lot_event.dart';
part 'light_arrived_at_the_parking_lot_state.dart';

/// A bloc that manages the state of a LightArrivedAtTheParkingLot according to the event that is dispatched to it.
class LightArrivedAtTheParkingLotBloc extends Bloc<
    LightArrivedAtTheParkingLotEvent, LightArrivedAtTheParkingLotState> {
  LightArrivedAtTheParkingLotBloc(LightArrivedAtTheParkingLotState initialState)
      : super(initialState) {
    on<LightArrivedAtTheParkingLotInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightArrivedAtTheParkingLotInitialEvent event,
    Emitter<LightArrivedAtTheParkingLotState> emit,
  ) async {}
}
