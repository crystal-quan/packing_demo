// ignore_for_file: must_be_immutable

part of 'light_arrived_at_the_parking_lot_bloc.dart';

/// Represents the state of LightArrivedAtTheParkingLot in the application.
class LightArrivedAtTheParkingLotState extends Equatable {
  LightArrivedAtTheParkingLotState({this.lightArrivedAtTheParkingLotModelObj});

  LightArrivedAtTheParkingLotModel? lightArrivedAtTheParkingLotModelObj;

  @override
  List<Object?> get props => [
        lightArrivedAtTheParkingLotModelObj,
      ];
  LightArrivedAtTheParkingLotState copyWith(
      {LightArrivedAtTheParkingLotModel? lightArrivedAtTheParkingLotModelObj}) {
    return LightArrivedAtTheParkingLotState(
      lightArrivedAtTheParkingLotModelObj:
          lightArrivedAtTheParkingLotModelObj ??
              this.lightArrivedAtTheParkingLotModelObj,
    );
  }
}
