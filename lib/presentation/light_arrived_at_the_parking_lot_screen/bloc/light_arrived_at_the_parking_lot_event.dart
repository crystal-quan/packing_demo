// ignore_for_file: must_be_immutable

part of 'light_arrived_at_the_parking_lot_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightArrivedAtTheParkingLot widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightArrivedAtTheParkingLotEvent extends Equatable {}

/// Event that is dispatched when the LightArrivedAtTheParkingLot widget is first created.
class LightArrivedAtTheParkingLotInitialEvent
    extends LightArrivedAtTheParkingLotEvent {
  @override
  List<Object?> get props => [];
}
