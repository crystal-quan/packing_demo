import 'bloc/light_arrived_at_the_parking_lot_bloc.dart';
import 'models/light_arrived_at_the_parking_lot_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_elevated_button.dart';

class LightArrivedAtTheParkingLotScreen extends StatelessWidget {
  const LightArrivedAtTheParkingLotScreen({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LightArrivedAtTheParkingLotBloc>(
      create: (context) =>
          LightArrivedAtTheParkingLotBloc(LightArrivedAtTheParkingLotState(
        lightArrivedAtTheParkingLotModelObj: LightArrivedAtTheParkingLotModel(),
      ))
            ..add(LightArrivedAtTheParkingLotInitialEvent()),
      child: LightArrivedAtTheParkingLotScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return BlocBuilder<LightArrivedAtTheParkingLotBloc,
        LightArrivedAtTheParkingLotState>(
      builder: (context, state) {
        return SafeArea(
          child: Scaffold(
            backgroundColor: appTheme.whiteA70001,
            body: SizedBox(
              height: mediaQueryData.size.height,
              width: double.maxFinite,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: SingleChildScrollView(
                      child: SizedBox(
                        height: mediaQueryData.size.height,
                        width: double.maxFinite,
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                height: getVerticalSize(
                                  550,
                                ),
                                width: double.maxFinite,
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment(
                                      0.5,
                                      0,
                                    ),
                                    end: Alignment(
                                      0.5,
                                      1,
                                    ),
                                    colors: [
                                      appTheme.gray80000,
                                      appTheme.blueGray90002,
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: Container(
                                padding: getPadding(
                                  left: 44,
                                  top: 187,
                                  right: 44,
                                  bottom: 187,
                                ),
                                decoration: AppDecoration.fill14,
                                child: Container(
                                  margin: getMargin(
                                    bottom: 45,
                                  ),
                                  padding: getPadding(
                                    all: 32,
                                  ),
                                  decoration: AppDecoration.fill.copyWith(
                                    borderRadius:
                                        BorderRadiusStyle.roundedBorder10,
                                  ),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      CustomImageView(
                                        imagePath: ImageConstant
                                            .imgCurrentlocationpana,
                                        height: getSize(
                                          220,
                                        ),
                                        width: getSize(
                                          220,
                                        ),
                                      ),
                                      Text(
                                        "msg_you_have_arrived".tr,
                                        overflow: TextOverflow.ellipsis,
                                        textAlign: TextAlign.left,
                                        style: CustomTextStyles
                                            .headlineSmallIndigo60001,
                                      ),
                                      Container(
                                        width: getHorizontalSize(
                                          266,
                                        ),
                                        margin: getMargin(
                                          left: 4,
                                          top: 17,
                                          right: 4,
                                        ),
                                        child: Text(
                                          "msg_please_scan_your".tr,
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.center,
                                          style: theme.textTheme.bodyLarge!
                                              .copyWith(
                                            letterSpacing: getHorizontalSize(
                                              0.2,
                                            ),
                                          ),
                                        ),
                                      ),
                                      CustomElevatedButton(
                                        width: getHorizontalSize(
                                          276,
                                        ),
                                        height: getVerticalSize(
                                          58,
                                        ),
                                        text: "lbl_ok".tr,
                                        margin: getMargin(
                                          top: 32,
                                        ),
                                        buttonStyle: CustomButtonStyles
                                            .gradientnameprimarynameindigoA100
                                            .copyWith(
                                                fixedSize: MaterialStateProperty
                                                    .all<Size>(Size(
                                          double.maxFinite,
                                          getVerticalSize(
                                            58,
                                          ),
                                        ))),
                                        decoration: CustomButtonStyles
                                            .gradientnameprimarynameindigoA100Decoration,
                                        buttonTextStyle: CustomTextStyles
                                            .titleMediumWhiteA7000116,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  CustomImageView(
                    imagePath: ImageConstant.imgImage5,
                    height: getVerticalSize(
                      882,
                    ),
                    width: getHorizontalSize(
                      428,
                    ),
                    alignment: Alignment.center,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
