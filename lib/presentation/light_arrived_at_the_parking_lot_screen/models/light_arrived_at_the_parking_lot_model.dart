// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_arrived_at_the_parking_lot_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightArrivedAtTheParkingLotModel extends Equatable {LightArrivedAtTheParkingLotModel copyWith() { return LightArrivedAtTheParkingLotModel(
); } 
@override List<Object?> get props => [];
 }
