// ignore_for_file: must_be_immutable

part of 'light_settings_security_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightSettingsSecurity widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightSettingsSecurityEvent extends Equatable {}

/// Event that is dispatched when the LightSettingsSecurity widget is first created.
class LightSettingsSecurityInitialEvent extends LightSettingsSecurityEvent {
  @override
  List<Object?> get props => [];
}
