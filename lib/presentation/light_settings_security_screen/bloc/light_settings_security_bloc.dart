import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_settings_security_screen/models/light_settings_security_model.dart';part 'light_settings_security_event.dart';part 'light_settings_security_state.dart';/// A bloc that manages the state of a LightSettingsSecurity according to the event that is dispatched to it.
class LightSettingsSecurityBloc extends Bloc<LightSettingsSecurityEvent, LightSettingsSecurityState> {LightSettingsSecurityBloc(LightSettingsSecurityState initialState) : super(initialState) { on<LightSettingsSecurityInitialEvent>(_onInitialize); }

_onInitialize(LightSettingsSecurityInitialEvent event, Emitter<LightSettingsSecurityState> emit, ) async  {  } 
 }
