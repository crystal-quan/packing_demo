// ignore_for_file: must_be_immutable

part of 'light_settings_security_bloc.dart';

/// Represents the state of LightSettingsSecurity in the application.
class LightSettingsSecurityState extends Equatable {
  LightSettingsSecurityState({this.lightSettingsSecurityModelObj});

  LightSettingsSecurityModel? lightSettingsSecurityModelObj;

  @override
  List<Object?> get props => [
        lightSettingsSecurityModelObj,
      ];
  LightSettingsSecurityState copyWith(
      {LightSettingsSecurityModel? lightSettingsSecurityModelObj}) {
    return LightSettingsSecurityState(
      lightSettingsSecurityModelObj:
          lightSettingsSecurityModelObj ?? this.lightSettingsSecurityModelObj,
    );
  }
}
