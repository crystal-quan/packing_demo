// ignore_for_file: must_be_immutable

part of 'light_parking_spot_details_bloc.dart';

/// Represents the state of LightParkingSpotDetails in the application.
class LightParkingSpotDetailsState extends Equatable {
  LightParkingSpotDetailsState({this.lightParkingSpotDetailsModelObj});

  LightParkingSpotDetailsModel? lightParkingSpotDetailsModelObj;

  @override
  List<Object?> get props => [
        lightParkingSpotDetailsModelObj,
      ];
  LightParkingSpotDetailsState copyWith(
      {LightParkingSpotDetailsModel? lightParkingSpotDetailsModelObj}) {
    return LightParkingSpotDetailsState(
      lightParkingSpotDetailsModelObj: lightParkingSpotDetailsModelObj ??
          this.lightParkingSpotDetailsModelObj,
    );
  }
}
