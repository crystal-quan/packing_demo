import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_parking_spot_details_screen/models/light_parking_spot_details_model.dart';part 'light_parking_spot_details_event.dart';part 'light_parking_spot_details_state.dart';/// A bloc that manages the state of a LightParkingSpotDetails according to the event that is dispatched to it.
class LightParkingSpotDetailsBloc extends Bloc<LightParkingSpotDetailsEvent, LightParkingSpotDetailsState> {LightParkingSpotDetailsBloc(LightParkingSpotDetailsState initialState) : super(initialState) { on<LightParkingSpotDetailsInitialEvent>(_onInitialize); }

_onInitialize(LightParkingSpotDetailsInitialEvent event, Emitter<LightParkingSpotDetailsState> emit, ) async  {  } 
 }
