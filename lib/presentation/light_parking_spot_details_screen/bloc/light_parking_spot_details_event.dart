// ignore_for_file: must_be_immutable

part of 'light_parking_spot_details_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightParkingSpotDetails widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightParkingSpotDetailsEvent extends Equatable {}

/// Event that is dispatched when the LightParkingSpotDetails widget is first created.
class LightParkingSpotDetailsInitialEvent extends LightParkingSpotDetailsEvent {
  @override
  List<Object?> get props => [];
}
