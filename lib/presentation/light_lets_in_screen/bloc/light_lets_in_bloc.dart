import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_lets_in_screen/models/light_lets_in_model.dart';part 'light_lets_in_event.dart';part 'light_lets_in_state.dart';/// A bloc that manages the state of a LightLetsIn according to the event that is dispatched to it.
class LightLetsInBloc extends Bloc<LightLetsInEvent, LightLetsInState> {LightLetsInBloc(LightLetsInState initialState) : super(initialState) { on<LightLetsInInitialEvent>(_onInitialize); }

_onInitialize(LightLetsInInitialEvent event, Emitter<LightLetsInState> emit, ) async  {  } 
 }
