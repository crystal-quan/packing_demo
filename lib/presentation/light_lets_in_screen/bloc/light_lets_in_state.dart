// ignore_for_file: must_be_immutable

part of 'light_lets_in_bloc.dart';

/// Represents the state of LightLetsIn in the application.
class LightLetsInState extends Equatable {
  LightLetsInState({this.lightLetsInModelObj});

  LightLetsInModel? lightLetsInModelObj;

  @override
  List<Object?> get props => [
        lightLetsInModelObj,
      ];
  LightLetsInState copyWith({LightLetsInModel? lightLetsInModelObj}) {
    return LightLetsInState(
      lightLetsInModelObj: lightLetsInModelObj ?? this.lightLetsInModelObj,
    );
  }
}
