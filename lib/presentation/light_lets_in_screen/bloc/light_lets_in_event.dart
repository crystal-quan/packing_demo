// ignore_for_file: must_be_immutable

part of 'light_lets_in_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightLetsIn widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightLetsInEvent extends Equatable {}

/// Event that is dispatched when the LightLetsIn widget is first created.
class LightLetsInInitialEvent extends LightLetsInEvent {
  @override
  List<Object?> get props => [];
}
