import '../models/slidername_item_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

// ignore: must_be_immutable
class SlidernameItemWidget extends StatelessWidget {
  SlidernameItemWidget(
    this.slidernameItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  SlidernameItemModel slidernameItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: getHorizontalSize(
            276,
          ),
          margin: getMargin(
            left: 46,
            right: 45,
          ),
          child: RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: "lbl_find_parking".tr,
                  style: theme.textTheme.headlineLarge,
                ),
                TextSpan(
                  text: "msg_places_around_you".tr,
                  style: CustomTextStyles.headlineLargeIndigo60001,
                ),
              ],
            ),
            textAlign: TextAlign.center,
          ),
        ),
        Padding(
          padding: getPadding(
            top: 11,
          ),
          child: Text(
            "msg_lorem_ipsum_dolor".tr,
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
            style: CustomTextStyles.bodyLargeGray700.copyWith(
              letterSpacing: getHorizontalSize(
                0.2,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
