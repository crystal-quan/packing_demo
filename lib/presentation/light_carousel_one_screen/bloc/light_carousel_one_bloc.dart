import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/slidername_item_model.dart';
import 'package:parking_app/presentation/light_carousel_one_screen/models/light_carousel_one_model.dart';
part 'light_carousel_one_event.dart';
part 'light_carousel_one_state.dart';

/// A bloc that manages the state of a LightCarouselOne according to the event that is dispatched to it.
class LightCarouselOneBloc
    extends Bloc<LightCarouselOneEvent, LightCarouselOneState> {
  LightCarouselOneBloc(LightCarouselOneState initialState)
      : super(initialState) {
    on<LightCarouselOneInitialEvent>(_onInitialize);
  }

  List<SlidernameItemModel> fillSlidernameItemList() {
    return List.generate(1, (index) => SlidernameItemModel());
  }

  _onInitialize(
    LightCarouselOneInitialEvent event,
    Emitter<LightCarouselOneState> emit,
  ) async {
    emit(state.copyWith(
      sliderIndex: 0,
    ));
    emit(state.copyWith(
        lightCarouselOneModelObj: state.lightCarouselOneModelObj?.copyWith(
      slidernameItemList: fillSlidernameItemList(),
    )));
  }
}
