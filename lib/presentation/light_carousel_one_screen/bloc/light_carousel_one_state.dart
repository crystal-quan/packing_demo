// ignore_for_file: must_be_immutable

part of 'light_carousel_one_bloc.dart';

/// Represents the state of LightCarouselOne in the application.
class LightCarouselOneState extends Equatable {
  LightCarouselOneState({
    this.sliderIndex = 0,
    this.lightCarouselOneModelObj,
  });

  LightCarouselOneModel? lightCarouselOneModelObj;

  int sliderIndex;

  @override
  List<Object?> get props => [
        sliderIndex,
        lightCarouselOneModelObj,
      ];
  LightCarouselOneState copyWith({
    int? sliderIndex,
    LightCarouselOneModel? lightCarouselOneModelObj,
  }) {
    return LightCarouselOneState(
      sliderIndex: sliderIndex ?? this.sliderIndex,
      lightCarouselOneModelObj:
          lightCarouselOneModelObj ?? this.lightCarouselOneModelObj,
    );
  }
}
