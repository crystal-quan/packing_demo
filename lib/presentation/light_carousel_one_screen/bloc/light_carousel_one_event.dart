// ignore_for_file: must_be_immutable

part of 'light_carousel_one_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightCarouselOne widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightCarouselOneEvent extends Equatable {}

/// Event that is dispatched when the LightCarouselOne widget is first created.
class LightCarouselOneInitialEvent extends LightCarouselOneEvent {
  @override
  List<Object?> get props => [];
}
