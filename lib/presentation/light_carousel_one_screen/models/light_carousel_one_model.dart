// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'slidername_item_model.dart';/// This class defines the variables used in the [light_carousel_one_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightCarouselOneModel extends Equatable {LightCarouselOneModel({this.slidernameItemList = const []});

List<SlidernameItemModel> slidernameItemList;

LightCarouselOneModel copyWith({List<SlidernameItemModel>? slidernameItemList}) { return LightCarouselOneModel(
slidernameItemList : slidernameItemList ?? this.slidernameItemList,
); } 
@override List<Object?> get props => [slidernameItemList];
 }
