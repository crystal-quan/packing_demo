// ignore_for_file: must_be_immutable

part of 'light_parking_lot_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightParkingLot widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightParkingLotEvent extends Equatable {}

/// Event that is dispatched when the LightParkingLot widget is first created.
class LightParkingLotInitialEvent extends LightParkingLotEvent {
  @override
  List<Object?> get props => [];
}
