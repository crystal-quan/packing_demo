// ignore_for_file: must_be_immutable

part of 'light_parking_lot_bloc.dart';

/// Represents the state of LightParkingLot in the application.
class LightParkingLotState extends Equatable {
  LightParkingLotState({this.lightParkingLotModelObj});

  LightParkingLotModel? lightParkingLotModelObj;

  @override
  List<Object?> get props => [
        lightParkingLotModelObj,
      ];
  LightParkingLotState copyWith(
      {LightParkingLotModel? lightParkingLotModelObj}) {
    return LightParkingLotState(
      lightParkingLotModelObj:
          lightParkingLotModelObj ?? this.lightParkingLotModelObj,
    );
  }
}
