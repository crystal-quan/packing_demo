import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:parking_app/presentation/light_parking_lot_screen/models/light_parking_lot_model.dart';
part 'light_parking_lot_event.dart';
part 'light_parking_lot_state.dart';

/// A bloc that manages the state of a LightParkingLot according to the event that is dispatched to it.
class LightParkingLotBloc
    extends Bloc<LightParkingLotEvent, LightParkingLotState> {
  LightParkingLotBloc(LightParkingLotState initialState) : super(initialState) {
    on<LightParkingLotInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightParkingLotInitialEvent event,
    Emitter<LightParkingLotState> emit,
  ) async {}
}
