import 'bloc/light_parking_lot_bloc.dart';
import 'models/light_parking_lot_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

class LightParkingLotScreen extends StatelessWidget {
  const LightParkingLotScreen({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LightParkingLotBloc>(
      create: (context) => LightParkingLotBloc(LightParkingLotState(
        lightParkingLotModelObj: LightParkingLotModel(),
      ))
        ..add(LightParkingLotInitialEvent()),
      child: LightParkingLotScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return BlocBuilder<LightParkingLotBloc, LightParkingLotState>(
      builder: (context, state) {
        return SafeArea(
          child: Scaffold(
            extendBody: true,
            extendBodyBehindAppBar: true,
            backgroundColor: appTheme.whiteA70001,
            body: Container(
              width: mediaQueryData.size.width,
              height: mediaQueryData.size.height,
              decoration: BoxDecoration(
                color: appTheme.whiteA70001,
                image: DecorationImage(
                  image: AssetImage(
                    ImageConstant.img43lightparking,
                  ),
                  fit: BoxFit.cover,
                ),
              ),
              child: SizedBox(
                width: double.maxFinite,
                child: Container(
                  height: mediaQueryData.size.height,
                  width: double.maxFinite,
                  decoration: BoxDecoration(
                    color: appTheme.indigoA10004.withOpacity(0.42),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
