// ignore_for_file: must_be_immutable

part of 'light_sign_in_filled_form_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightSignInFilledForm widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightSignInFilledFormEvent extends Equatable {}

/// Event that is dispatched when the LightSignInFilledForm widget is first created.
class LightSignInFilledFormInitialEvent extends LightSignInFilledFormEvent {
  @override
  List<Object?> get props => [];
}

///Event for changing password visibility
class ChangePasswordVisibilityEvent extends LightSignInFilledFormEvent {
  ChangePasswordVisibilityEvent({required this.value});

  bool value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing checkbox
class ChangeCheckBoxEvent extends LightSignInFilledFormEvent {
  ChangeCheckBoxEvent({required this.value});

  bool value;

  @override
  List<Object?> get props => [
        value,
      ];
}
