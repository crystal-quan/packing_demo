// ignore_for_file: must_be_immutable

part of 'light_sign_in_filled_form_bloc.dart';

/// Represents the state of LightSignInFilledForm in the application.
class LightSignInFilledFormState extends Equatable {
  LightSignInFilledFormState({
    this.statusfilltypeeController,
    this.groupfiftysevenController,
    this.isShowPassword = true,
    this.isCheckbox = false,
    this.lightSignInFilledFormModelObj,
  });

  TextEditingController? statusfilltypeeController;

  TextEditingController? groupfiftysevenController;

  LightSignInFilledFormModel? lightSignInFilledFormModelObj;

  bool isShowPassword;

  bool isCheckbox;

  @override
  List<Object?> get props => [
        statusfilltypeeController,
        groupfiftysevenController,
        isShowPassword,
        isCheckbox,
        lightSignInFilledFormModelObj,
      ];
  LightSignInFilledFormState copyWith({
    TextEditingController? statusfilltypeeController,
    TextEditingController? groupfiftysevenController,
    bool? isShowPassword,
    bool? isCheckbox,
    LightSignInFilledFormModel? lightSignInFilledFormModelObj,
  }) {
    return LightSignInFilledFormState(
      statusfilltypeeController:
          statusfilltypeeController ?? this.statusfilltypeeController,
      groupfiftysevenController:
          groupfiftysevenController ?? this.groupfiftysevenController,
      isShowPassword: isShowPassword ?? this.isShowPassword,
      isCheckbox: isCheckbox ?? this.isCheckbox,
      lightSignInFilledFormModelObj:
          lightSignInFilledFormModelObj ?? this.lightSignInFilledFormModelObj,
    );
  }
}
