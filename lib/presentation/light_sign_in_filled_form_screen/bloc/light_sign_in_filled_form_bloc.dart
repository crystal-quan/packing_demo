import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_sign_in_filled_form_screen/models/light_sign_in_filled_form_model.dart';part 'light_sign_in_filled_form_event.dart';part 'light_sign_in_filled_form_state.dart';/// A bloc that manages the state of a LightSignInFilledForm according to the event that is dispatched to it.
class LightSignInFilledFormBloc extends Bloc<LightSignInFilledFormEvent, LightSignInFilledFormState> {LightSignInFilledFormBloc(LightSignInFilledFormState initialState) : super(initialState) { on<LightSignInFilledFormInitialEvent>(_onInitialize); on<ChangePasswordVisibilityEvent>(_changePasswordVisibility); on<ChangeCheckBoxEvent>(_changeCheckBox); }

_changePasswordVisibility(ChangePasswordVisibilityEvent event, Emitter<LightSignInFilledFormState> emit, ) { emit(state.copyWith(isShowPassword: event.value)); } 
_changeCheckBox(ChangeCheckBoxEvent event, Emitter<LightSignInFilledFormState> emit, ) { emit(state.copyWith(isCheckbox: event.value)); } 
_onInitialize(LightSignInFilledFormInitialEvent event, Emitter<LightSignInFilledFormState> emit, ) async  { emit(state.copyWith(statusfilltypeeController: TextEditingController(), groupfiftysevenController: TextEditingController(), isShowPassword: true, isCheckbox: false)); } 
 }
