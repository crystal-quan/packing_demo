import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:parking_app/presentation/light_profile_settings_page/models/light_profile_settings_model.dart';
part 'light_profile_settings_event.dart';
part 'light_profile_settings_state.dart';

/// A bloc that manages the state of a LightProfileSettings according to the event that is dispatched to it.
class LightProfileSettingsBloc
    extends Bloc<LightProfileSettingsEvent, LightProfileSettingsState> {
  LightProfileSettingsBloc(LightProfileSettingsState initialState)
      : super(initialState) {
    on<LightProfileSettingsInitialEvent>(_onInitialize);
    on<ChangeCheckBoxEvent>(_changeCheckBox);
  }

  _changeCheckBox(
    ChangeCheckBoxEvent event,
    Emitter<LightProfileSettingsState> emit,
  ) {
    emit(state.copyWith(
      isCheckbox: event.value,
    ));
  }

  _onInitialize(
    LightProfileSettingsInitialEvent event,
    Emitter<LightProfileSettingsState> emit,
  ) async {
    emit(state.copyWith(
      isCheckbox: false,
    ));
  }
}
