// ignore_for_file: must_be_immutable

part of 'light_profile_settings_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightProfileSettings widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightProfileSettingsEvent extends Equatable {}

/// Event that is dispatched when the LightProfileSettings widget is first created.
class LightProfileSettingsInitialEvent extends LightProfileSettingsEvent {
  @override
  List<Object?> get props => [];
}

///Event for changing checkbox
class ChangeCheckBoxEvent extends LightProfileSettingsEvent {
  ChangeCheckBoxEvent({required this.value});

  bool value;

  @override
  List<Object?> get props => [
        value,
      ];
}
