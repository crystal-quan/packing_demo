// ignore_for_file: must_be_immutable

part of 'light_profile_settings_bloc.dart';

/// Represents the state of LightProfileSettings in the application.
class LightProfileSettingsState extends Equatable {
  LightProfileSettingsState({
    this.isCheckbox = false,
    this.lightProfileSettingsModelObj,
  });

  LightProfileSettingsModel? lightProfileSettingsModelObj;

  bool isCheckbox;

  @override
  List<Object?> get props => [
        isCheckbox,
        lightProfileSettingsModelObj,
      ];
  LightProfileSettingsState copyWith({
    bool? isCheckbox,
    LightProfileSettingsModel? lightProfileSettingsModelObj,
  }) {
    return LightProfileSettingsState(
      isCheckbox: isCheckbox ?? this.isCheckbox,
      lightProfileSettingsModelObj:
          lightProfileSettingsModelObj ?? this.lightProfileSettingsModelObj,
    );
  }
}
