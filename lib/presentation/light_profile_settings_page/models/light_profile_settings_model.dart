// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_profile_settings_page],
/// and is typically used to hold data that is passed between different parts of the application.
class LightProfileSettingsModel extends Equatable {LightProfileSettingsModel copyWith() { return LightProfileSettingsModel(
); } 
@override List<Object?> get props => [];
 }
