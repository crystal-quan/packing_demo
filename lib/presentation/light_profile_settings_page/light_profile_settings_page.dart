import 'bloc/light_profile_settings_bloc.dart';
import 'models/light_profile_settings_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/app_bar/appbar_image.dart';
import 'package:parking_app/widgets/app_bar/appbar_title.dart';
import 'package:parking_app/widgets/app_bar/custom_app_bar.dart';
import 'package:parking_app/widgets/custom_checkbox_button.dart';

// ignore_for_file: must_be_immutable
class LightProfileSettingsPage extends StatelessWidget {
  const LightProfileSettingsPage({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LightProfileSettingsBloc>(
      create: (context) => LightProfileSettingsBloc(LightProfileSettingsState(
        lightProfileSettingsModelObj: LightProfileSettingsModel(),
      ))
        ..add(LightProfileSettingsInitialEvent()),
      child: LightProfileSettingsPage(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.whiteA70001,
        appBar: CustomAppBar(
          height: getVerticalSize(
            75,
          ),
          title: AppbarTitle(
            text: "lbl_profile".tr,
            margin: getMargin(
              left: 24,
            ),
          ),
          actions: [
            AppbarImage(
              height: getSize(
                28,
              ),
              width: getSize(
                28,
              ),
              svgPath: ImageConstant.imgClock,
              margin: getMargin(
                left: 24,
                top: 14,
                right: 24,
                bottom: 13,
              ),
            ),
          ],
        ),
        body: Container(
          width: double.maxFinite,
          padding: getPadding(
            left: 24,
            right: 24,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              CustomImageView(
                imagePath: ImageConstant.imgEllipse140x140,
                height: getSize(
                  140,
                ),
                width: getSize(
                  140,
                ),
                radius: BorderRadius.circular(
                  getHorizontalSize(
                    70,
                  ),
                ),
                alignment: Alignment.center,
              ),
              Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: getPadding(
                    top: 11,
                  ),
                  child: Text(
                    "lbl_olivia_lucas2".tr,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.left,
                    style: theme.textTheme.headlineSmall,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: getPadding(
                    top: 11,
                  ),
                  child: Text(
                    "msg_olivia_lucas_yourdomain_com4".tr,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.left,
                    style: theme.textTheme.titleSmall!.copyWith(
                      letterSpacing: getHorizontalSize(
                        0.2,
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: getPadding(
                  top: 24,
                ),
                child: Divider(
                  height: getVerticalSize(
                    1,
                  ),
                  thickness: getVerticalSize(
                    1,
                  ),
                  color: appTheme.gray200,
                ),
              ),
              Padding(
                padding: getPadding(
                  top: 23,
                ),
                child: Row(
                  children: [
                    CustomImageView(
                      svgPath: ImageConstant.imgUserGray800,
                      height: getSize(
                        28,
                      ),
                      width: getSize(
                        28,
                      ),
                    ),
                    Padding(
                      padding: getPadding(
                        left: 20,
                        top: 2,
                        bottom: 3,
                      ),
                      child: Text(
                        "lbl_edit_profile".tr,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: CustomTextStyles.titleMediumGray800SemiBold
                            .copyWith(
                          letterSpacing: getHorizontalSize(
                            0.2,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: getPadding(
                  top: 28,
                ),
                child: Row(
                  children: [
                    CustomImageView(
                      svgPath: ImageConstant.imgIconlycurvedwallet,
                      height: getSize(
                        28,
                      ),
                      width: getSize(
                        28,
                      ),
                    ),
                    Padding(
                      padding: getPadding(
                        left: 20,
                        top: 5,
                      ),
                      child: Text(
                        "lbl_payment".tr,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: CustomTextStyles.titleMediumGray800SemiBold
                            .copyWith(
                          letterSpacing: getHorizontalSize(
                            0.2,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: getPadding(
                  top: 28,
                ),
                child: Row(
                  children: [
                    CustomImageView(
                      svgPath: ImageConstant.imgIconlycurvednotificationGray800,
                      height: getSize(
                        28,
                      ),
                      width: getSize(
                        28,
                      ),
                    ),
                    Padding(
                      padding: getPadding(
                        left: 20,
                        top: 2,
                        bottom: 3,
                      ),
                      child: Text(
                        "lbl_notifications".tr,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: CustomTextStyles.titleMediumGray800SemiBold
                            .copyWith(
                          letterSpacing: getHorizontalSize(
                            0.2,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              BlocSelector<LightProfileSettingsBloc, LightProfileSettingsState,
                  bool?>(
                selector: (state) => state.isCheckbox,
                builder: (context, isCheckbox) {
                  return CustomCheckboxButton(
                    text: "lbl_security".tr,
                    value: isCheckbox,
                    margin: getMargin(
                      top: 28,
                    ),
                    padding: getPadding(
                      top: 1,
                      bottom: 1,
                    ),
                    textStyle: CustomTextStyles.titleMediumGray800SemiBold,
                    onChange: (value) {
                      context
                          .read<LightProfileSettingsBloc>()
                          .add(ChangeCheckBoxEvent(value: value));
                    },
                  );
                },
              ),
              Padding(
                padding: getPadding(
                  top: 28,
                ),
                child: Row(
                  children: [
                    CustomImageView(
                      svgPath: ImageConstant.imgIconlycurvedinfo,
                      height: getSize(
                        28,
                      ),
                      width: getSize(
                        28,
                      ),
                    ),
                    Padding(
                      padding: getPadding(
                        left: 20,
                        top: 5,
                        bottom: 1,
                      ),
                      child: Text(
                        "lbl_help".tr,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: CustomTextStyles.titleMediumGray800SemiBold
                            .copyWith(
                          letterSpacing: getHorizontalSize(
                            0.2,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: getPadding(
                  top: 28,
                ),
                child: Row(
                  children: [
                    CustomImageView(
                      svgPath: ImageConstant.imgIconlycurvedshow,
                      height: getSize(
                        28,
                      ),
                      width: getSize(
                        28,
                      ),
                    ),
                    Padding(
                      padding: getPadding(
                        left: 20,
                        top: 2,
                        bottom: 3,
                      ),
                      child: Text(
                        "lbl_dark_theme".tr,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: CustomTextStyles.titleMediumGray800SemiBold
                            .copyWith(
                          letterSpacing: getHorizontalSize(
                            0.2,
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    Container(
                      margin: getMargin(
                        top: 2,
                        bottom: 2,
                      ),
                      decoration: AppDecoration.fill15.copyWith(
                        borderRadius: BorderRadiusStyle.roundedBorder10,
                      ),
                      child: Container(
                        height: getSize(
                          24,
                        ),
                        width: getSize(
                          24,
                        ),
                        decoration: BoxDecoration(
                          color: appTheme.whiteA70001,
                          borderRadius: BorderRadius.circular(
                            getHorizontalSize(
                              12,
                            ),
                          ),
                          border: Border.all(
                            color: appTheme.gray200,
                            width: getHorizontalSize(
                              2,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: getPadding(
                  top: 28,
                  bottom: 5,
                ),
                child: Row(
                  children: [
                    CustomImageView(
                      svgPath: ImageConstant.imgRefreshRedA200,
                      height: getSize(
                        28,
                      ),
                      width: getSize(
                        28,
                      ),
                    ),
                    Padding(
                      padding: getPadding(
                        left: 20,
                        top: 5,
                      ),
                      child: Text(
                        "lbl_logout".tr,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: CustomTextStyles.titleMediumRedA200.copyWith(
                          letterSpacing: getHorizontalSize(
                            0.2,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
