import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import '../models/listname_item_model.dart';import 'package:parking_app/presentation/light_parking_view_ticket_screen/models/light_parking_view_ticket_model.dart';part 'light_parking_view_ticket_event.dart';part 'light_parking_view_ticket_state.dart';/// A bloc that manages the state of a LightParkingViewTicket according to the event that is dispatched to it.
class LightParkingViewTicketBloc extends Bloc<LightParkingViewTicketEvent, LightParkingViewTicketState> {LightParkingViewTicketBloc(LightParkingViewTicketState initialState) : super(initialState) { on<LightParkingViewTicketInitialEvent>(_onInitialize); }

_onInitialize(LightParkingViewTicketInitialEvent event, Emitter<LightParkingViewTicketState> emit, ) async  { emit(state.copyWith(lightParkingViewTicketModelObj: state.lightParkingViewTicketModelObj?.copyWith(listnameItemList: fillListnameItemList()))); } 
List<ListnameItemModel> fillListnameItemList() { return List.generate(4, (index) => ListnameItemModel()); } 
 }
