// ignore_for_file: must_be_immutable

part of 'light_parking_view_ticket_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightParkingViewTicket widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightParkingViewTicketEvent extends Equatable {}

/// Event that is dispatched when the LightParkingViewTicket widget is first created.
class LightParkingViewTicketInitialEvent extends LightParkingViewTicketEvent {
  @override
  List<Object?> get props => [];
}
