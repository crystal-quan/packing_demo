// ignore_for_file: must_be_immutable

part of 'light_parking_view_ticket_bloc.dart';

/// Represents the state of LightParkingViewTicket in the application.
class LightParkingViewTicketState extends Equatable {
  LightParkingViewTicketState({this.lightParkingViewTicketModelObj});

  LightParkingViewTicketModel? lightParkingViewTicketModelObj;

  @override
  List<Object?> get props => [
        lightParkingViewTicketModelObj,
      ];
  LightParkingViewTicketState copyWith(
      {LightParkingViewTicketModel? lightParkingViewTicketModelObj}) {
    return LightParkingViewTicketState(
      lightParkingViewTicketModelObj:
          lightParkingViewTicketModelObj ?? this.lightParkingViewTicketModelObj,
    );
  }
}
