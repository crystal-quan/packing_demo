// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'listname_item_model.dart';/// This class defines the variables used in the [light_parking_view_ticket_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightParkingViewTicketModel extends Equatable {LightParkingViewTicketModel({this.listnameItemList = const []});

List<ListnameItemModel> listnameItemList;

LightParkingViewTicketModel copyWith({List<ListnameItemModel>? listnameItemList}) { return LightParkingViewTicketModel(
listnameItemList : listnameItemList ?? this.listnameItemList,
); } 
@override List<Object?> get props => [listnameItemList];
 }
