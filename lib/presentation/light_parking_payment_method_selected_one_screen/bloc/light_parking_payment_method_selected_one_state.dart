// ignore_for_file: must_be_immutable

part of 'light_parking_payment_method_selected_one_bloc.dart';

/// Represents the state of LightParkingPaymentMethodSelectedOne in the application.
class LightParkingPaymentMethodSelectedOneState extends Equatable {
  LightParkingPaymentMethodSelectedOneState({
    this.radioGroup = "",
    this.radioGroup1 = "",
    this.lightParkingPaymentMethodSelectedOneModelObj,
  });

  LightParkingPaymentMethodSelectedOneModel?
      lightParkingPaymentMethodSelectedOneModelObj;

  String radioGroup;

  String radioGroup1;

  @override
  List<Object?> get props => [
        radioGroup,
        radioGroup1,
        lightParkingPaymentMethodSelectedOneModelObj,
      ];
  LightParkingPaymentMethodSelectedOneState copyWith({
    String? radioGroup,
    String? radioGroup1,
    LightParkingPaymentMethodSelectedOneModel?
        lightParkingPaymentMethodSelectedOneModelObj,
  }) {
    return LightParkingPaymentMethodSelectedOneState(
      radioGroup: radioGroup ?? this.radioGroup,
      radioGroup1: radioGroup1 ?? this.radioGroup1,
      lightParkingPaymentMethodSelectedOneModelObj:
          lightParkingPaymentMethodSelectedOneModelObj ??
              this.lightParkingPaymentMethodSelectedOneModelObj,
    );
  }
}
