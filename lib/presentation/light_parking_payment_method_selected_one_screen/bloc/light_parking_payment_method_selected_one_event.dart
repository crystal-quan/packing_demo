// ignore_for_file: must_be_immutable

part of 'light_parking_payment_method_selected_one_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightParkingPaymentMethodSelectedOne widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightParkingPaymentMethodSelectedOneEvent extends Equatable {}

/// Event that is dispatched when the LightParkingPaymentMethodSelectedOne widget is first created.
class LightParkingPaymentMethodSelectedOneInitialEvent
    extends LightParkingPaymentMethodSelectedOneEvent {
  @override
  List<Object?> get props => [];
}

///Event for changing radio button
class ChangeRadioButtonEvent extends LightParkingPaymentMethodSelectedOneEvent {
  ChangeRadioButtonEvent({required this.value});

  String value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing radio button
class ChangeRadioButton1Event
    extends LightParkingPaymentMethodSelectedOneEvent {
  ChangeRadioButton1Event({required this.value});

  String value;

  @override
  List<Object?> get props => [
        value,
      ];
}
