// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_parking_payment_method_selected_one_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightParkingPaymentMethodSelectedOneModel extends Equatable {LightParkingPaymentMethodSelectedOneModel copyWith() { return LightParkingPaymentMethodSelectedOneModel(
); } 
@override List<Object?> get props => [];
 }
