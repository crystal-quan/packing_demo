import '../models/listmo_item_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

// ignore: must_be_immutable
class ListmoItemWidget extends StatelessWidget {
  ListmoItemWidget(
    this.listmoItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  ListmoItemModel listmoItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          listmoItemModelObj.moTxt,
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.left,
          style: CustomTextStyles.titleSmallMedium.copyWith(
            letterSpacing: getHorizontalSize(
              0.2,
            ),
          ),
        ),
        Text(
          listmoItemModelObj.tuTxt,
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.left,
          style: CustomTextStyles.titleSmallMedium.copyWith(
            letterSpacing: getHorizontalSize(
              0.2,
            ),
          ),
        ),
        Text(
          listmoItemModelObj.weTxt,
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.left,
          style: CustomTextStyles.titleSmallMedium.copyWith(
            letterSpacing: getHorizontalSize(
              0.2,
            ),
          ),
        ),
        Text(
          listmoItemModelObj.thTxt,
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.left,
          style: CustomTextStyles.titleSmallMedium.copyWith(
            letterSpacing: getHorizontalSize(
              0.2,
            ),
          ),
        ),
        Text(
          listmoItemModelObj.frTxt,
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.left,
          style: CustomTextStyles.titleSmallMedium.copyWith(
            letterSpacing: getHorizontalSize(
              0.2,
            ),
          ),
        ),
        Text(
          listmoItemModelObj.saTxt,
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.left,
          style: CustomTextStyles.titleSmallMedium.copyWith(
            letterSpacing: getHorizontalSize(
              0.2,
            ),
          ),
        ),
        Text(
          listmoItemModelObj.suTxt,
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.left,
          style: CustomTextStyles.titleSmallMedium.copyWith(
            letterSpacing: getHorizontalSize(
              0.2,
            ),
          ),
        ),
      ],
    );
  }
}
