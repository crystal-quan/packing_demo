// ignore_for_file: must_be_immutable

part of 'light_parking_book_details_bloc.dart';

/// Represents the state of LightParkingBookDetails in the application.
class LightParkingBookDetailsState extends Equatable {
  LightParkingBookDetailsState({
    this.timeController,
    this.timeoneController,
    this.lightParkingBookDetailsModelObj,
  });

  TextEditingController? timeController;

  TextEditingController? timeoneController;

  LightParkingBookDetailsModel? lightParkingBookDetailsModelObj;

  @override
  List<Object?> get props => [
        timeController,
        timeoneController,
        lightParkingBookDetailsModelObj,
      ];
  LightParkingBookDetailsState copyWith({
    TextEditingController? timeController,
    TextEditingController? timeoneController,
    LightParkingBookDetailsModel? lightParkingBookDetailsModelObj,
  }) {
    return LightParkingBookDetailsState(
      timeController: timeController ?? this.timeController,
      timeoneController: timeoneController ?? this.timeoneController,
      lightParkingBookDetailsModelObj: lightParkingBookDetailsModelObj ??
          this.lightParkingBookDetailsModelObj,
    );
  }
}
