// ignore_for_file: must_be_immutable

part of 'light_parking_book_details_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightParkingBookDetails widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightParkingBookDetailsEvent extends Equatable {}

/// Event that is dispatched when the LightParkingBookDetails widget is first created.
class LightParkingBookDetailsInitialEvent extends LightParkingBookDetailsEvent {
  @override
  List<Object?> get props => [];
}
