// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'listmo_item_model.dart';/// This class defines the variables used in the [light_parking_book_details_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightParkingBookDetailsModel extends Equatable {LightParkingBookDetailsModel({this.listmoItemList = const []});

List<ListmoItemModel> listmoItemList;

LightParkingBookDetailsModel copyWith({List<ListmoItemModel>? listmoItemList}) { return LightParkingBookDetailsModel(
listmoItemList : listmoItemList ?? this.listmoItemList,
); } 
@override List<Object?> get props => [listmoItemList];
 }
