import '../light_carousel_two_screen/widgets/slidertitle_item_widget.dart';
import 'bloc/light_carousel_two_bloc.dart';
import 'models/light_carousel_two_model.dart';
import 'models/slidertitle_item_model.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_elevated_button.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class LightCarouselTwoScreen extends StatelessWidget {
  const LightCarouselTwoScreen({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LightCarouselTwoBloc>(
      create: (context) => LightCarouselTwoBloc(LightCarouselTwoState(
        lightCarouselTwoModelObj: LightCarouselTwoModel(),
      ))
        ..add(LightCarouselTwoInitialEvent()),
      child: LightCarouselTwoScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.whiteA70001,
        body: Container(
          width: double.maxFinite,
          padding: getPadding(
            left: 24,
            top: 48,
            right: 24,
            bottom: 48,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Spacer(),
              CustomImageView(
                svgPath: ImageConstant.imgCreditcardpana,
                height: getSize(
                  270,
                ),
                width: getSize(
                  270,
                ),
              ),
              Padding(
                padding: getPadding(
                  left: 5,
                  top: 22,
                  right: 5,
                ),
                child: BlocBuilder<LightCarouselTwoBloc, LightCarouselTwoState>(
                  builder: (context, state) {
                    return CarouselSlider.builder(
                      options: CarouselOptions(
                        height: getVerticalSize(
                          161,
                        ),
                        initialPage: 0,
                        autoPlay: true,
                        viewportFraction: 1.0,
                        enableInfiniteScroll: false,
                        scrollDirection: Axis.horizontal,
                        onPageChanged: (
                          index,
                          reason,
                        ) {
                          state.sliderIndex = index;
                        },
                      ),
                      itemCount: state.lightCarouselTwoModelObj
                              ?.slidertitleItemList.length ??
                          0,
                      itemBuilder: (context, index, realIndex) {
                        SlidertitleItemModel model = state
                                .lightCarouselTwoModelObj
                                ?.slidertitleItemList[index] ??
                            SlidertitleItemModel();
                        return SlidertitleItemWidget(
                          model,
                        );
                      },
                    );
                  },
                ),
              ),
              BlocBuilder<LightCarouselTwoBloc, LightCarouselTwoState>(
                builder: (context, state) {
                  return Container(
                    height: getVerticalSize(
                      8,
                    ),
                    margin: getMargin(
                      top: 37,
                    ),
                    child: AnimatedSmoothIndicator(
                      activeIndex: state.sliderIndex,
                      count: state.lightCarouselTwoModelObj?.slidertitleItemList
                              .length ??
                          0,
                      axisDirection: Axis.horizontal,
                      effect: ScrollingDotsEffect(
                        activeDotColor: Color(0X1212121D),
                      ),
                    ),
                  );
                },
              ),
              CustomElevatedButton(
                width: getHorizontalSize(
                  380,
                ),
                height: getVerticalSize(
                  58,
                ),
                text: "lbl_next".tr,
                margin: getMargin(
                  top: 40,
                ),
                buttonStyle: CustomButtonStyles
                    .gradientnameprimarynameindigoA100
                    .copyWith(
                        fixedSize: MaterialStateProperty.all<Size>(Size(
                  double.maxFinite,
                  getVerticalSize(
                    58,
                  ),
                ))),
                decoration: CustomButtonStyles
                    .gradientnameprimarynameindigoA100Decoration,
                buttonTextStyle: CustomTextStyles.titleMediumWhiteA7000116,
              ),
              CustomElevatedButton(
                text: "lbl_skip".tr,
                margin: getMargin(
                  top: 12,
                ),
                buttonStyle: CustomButtonStyles.fillIndigo5002.copyWith(
                    fixedSize: MaterialStateProperty.all<Size>(Size(
                  double.maxFinite,
                  getVerticalSize(
                    58,
                  ),
                ))),
                buttonTextStyle: CustomTextStyles.titleMediumIndigoA400,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
