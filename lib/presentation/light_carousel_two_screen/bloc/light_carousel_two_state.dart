// ignore_for_file: must_be_immutable

part of 'light_carousel_two_bloc.dart';

/// Represents the state of LightCarouselTwo in the application.
class LightCarouselTwoState extends Equatable {
  LightCarouselTwoState({
    this.sliderIndex = 0,
    this.lightCarouselTwoModelObj,
  });

  LightCarouselTwoModel? lightCarouselTwoModelObj;

  int sliderIndex;

  @override
  List<Object?> get props => [
        sliderIndex,
        lightCarouselTwoModelObj,
      ];
  LightCarouselTwoState copyWith({
    int? sliderIndex,
    LightCarouselTwoModel? lightCarouselTwoModelObj,
  }) {
    return LightCarouselTwoState(
      sliderIndex: sliderIndex ?? this.sliderIndex,
      lightCarouselTwoModelObj:
          lightCarouselTwoModelObj ?? this.lightCarouselTwoModelObj,
    );
  }
}
