import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/slidertitle_item_model.dart';
import 'package:parking_app/presentation/light_carousel_two_screen/models/light_carousel_two_model.dart';
part 'light_carousel_two_event.dart';
part 'light_carousel_two_state.dart';

/// A bloc that manages the state of a LightCarouselTwo according to the event that is dispatched to it.
class LightCarouselTwoBloc
    extends Bloc<LightCarouselTwoEvent, LightCarouselTwoState> {
  LightCarouselTwoBloc(LightCarouselTwoState initialState)
      : super(initialState) {
    on<LightCarouselTwoInitialEvent>(_onInitialize);
  }

  List<SlidertitleItemModel> fillSlidertitleItemList() {
    return List.generate(1, (index) => SlidertitleItemModel());
  }

  _onInitialize(
    LightCarouselTwoInitialEvent event,
    Emitter<LightCarouselTwoState> emit,
  ) async {
    emit(state.copyWith(
      sliderIndex: 0,
    ));
    emit(state.copyWith(
        lightCarouselTwoModelObj: state.lightCarouselTwoModelObj?.copyWith(
      slidertitleItemList: fillSlidertitleItemList(),
    )));
  }
}
