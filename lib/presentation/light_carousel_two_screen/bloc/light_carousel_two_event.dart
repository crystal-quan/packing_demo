// ignore_for_file: must_be_immutable

part of 'light_carousel_two_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightCarouselTwo widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightCarouselTwoEvent extends Equatable {}

/// Event that is dispatched when the LightCarouselTwo widget is first created.
class LightCarouselTwoInitialEvent extends LightCarouselTwoEvent {
  @override
  List<Object?> get props => [];
}
