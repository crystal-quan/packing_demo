import '../models/slidertitle_item_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

// ignore: must_be_immutable
class SlidertitleItemWidget extends StatelessWidget {
  SlidertitleItemWidget(
    this.slidertitleItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  SlidertitleItemModel slidertitleItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: getHorizontalSize(
            311,
          ),
          margin: getMargin(
            left: 28,
            right: 28,
          ),
          child: RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: "lbl_book_and_pay".tr,
                  style: theme.textTheme.headlineLarge,
                ),
                TextSpan(
                  text: "msg_parking_quickly".tr,
                  style: CustomTextStyles.headlineLargeIndigo600,
                ),
              ],
            ),
            textAlign: TextAlign.center,
          ),
        ),
        Padding(
          padding: getPadding(
            top: 11,
          ),
          child: Text(
            "msg_lorem_ipsum_dolor".tr,
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
            style: CustomTextStyles.bodyLargeGray700.copyWith(
              letterSpacing: getHorizontalSize(
                0.2,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
