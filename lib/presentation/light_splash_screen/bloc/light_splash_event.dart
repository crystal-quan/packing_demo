// ignore_for_file: must_be_immutable

part of 'light_splash_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightSplash widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightSplashEvent extends Equatable {}

/// Event that is dispatched when the LightSplash widget is first created.
class LightSplashInitialEvent extends LightSplashEvent {
  @override
  List<Object?> get props => [];
}
