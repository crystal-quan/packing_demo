import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_splash_screen/models/light_splash_model.dart';part 'light_splash_event.dart';part 'light_splash_state.dart';/// A bloc that manages the state of a LightSplash according to the event that is dispatched to it.
class LightSplashBloc extends Bloc<LightSplashEvent, LightSplashState> {LightSplashBloc(LightSplashState initialState) : super(initialState) { on<LightSplashInitialEvent>(_onInitialize); }

_onInitialize(LightSplashInitialEvent event, Emitter<LightSplashState> emit, ) async  { Future.delayed(const Duration(milliseconds: 3000), (){
NavigatorService.popAndPushNamed(AppRoutes.lightWelcomeScreen, );}); } 
 }
