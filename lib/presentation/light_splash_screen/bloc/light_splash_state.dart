// ignore_for_file: must_be_immutable

part of 'light_splash_bloc.dart';

/// Represents the state of LightSplash in the application.
class LightSplashState extends Equatable {
  LightSplashState({this.lightSplashModelObj});

  LightSplashModel? lightSplashModelObj;

  @override
  List<Object?> get props => [
        lightSplashModelObj,
      ];
  LightSplashState copyWith({LightSplashModel? lightSplashModelObj}) {
    return LightSplashState(
      lightSplashModelObj: lightSplashModelObj ?? this.lightSplashModelObj,
    );
  }
}
