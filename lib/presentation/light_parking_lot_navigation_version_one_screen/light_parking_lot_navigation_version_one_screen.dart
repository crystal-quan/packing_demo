import 'bloc/light_parking_lot_navigation_version_one_bloc.dart';
import 'models/light_parking_lot_navigation_version_one_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_floating_button.dart';
import 'package:parking_app/widgets/custom_icon_button.dart';

class LightParkingLotNavigationVersionOneScreen extends StatelessWidget {
  const LightParkingLotNavigationVersionOneScreen({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LightParkingLotNavigationVersionOneBloc>(
      create: (context) => LightParkingLotNavigationVersionOneBloc(
          LightParkingLotNavigationVersionOneState(
        lightParkingLotNavigationVersionOneModelObj:
            LightParkingLotNavigationVersionOneModel(),
      ))
        ..add(LightParkingLotNavigationVersionOneInitialEvent()),
      child: LightParkingLotNavigationVersionOneScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return BlocBuilder<LightParkingLotNavigationVersionOneBloc,
        LightParkingLotNavigationVersionOneState>(
      builder: (context, state) {
        return SafeArea(
          child: Scaffold(
            backgroundColor: appTheme.whiteA70001,
            body: SizedBox(
              height: mediaQueryData.size.height,
              width: double.maxFinite,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  CustomImageView(
                    imagePath: ImageConstant.imgVectorIndigo5001,
                    height: getVerticalSize(
                      882,
                    ),
                    width: getHorizontalSize(
                      428,
                    ),
                    alignment: Alignment.center,
                  ),
                  SizedBox(
                    height: mediaQueryData.size.height,
                    width: double.maxFinite,
                    child: Stack(
                      alignment: Alignment.bottomRight,
                      children: [
                        Align(
                          alignment: Alignment.center,
                          child: Container(
                            height: mediaQueryData.size.height,
                            width: double.maxFinite,
                            decoration: BoxDecoration(
                              color: appTheme.gray5002,
                            ),
                          ),
                        ),
                        CustomIconButton(
                          height: 52,
                          width: 52,
                          margin: getMargin(
                            right: 24,
                            bottom: 120,
                          ),
                          padding: getPadding(
                            all: 12,
                          ),
                          decoration: IconButtonStyleHelper.fillIndigo60001TL26,
                          alignment: Alignment.bottomRight,
                          child: CustomImageView(
                            svgPath: ImageConstant.imgFrameWhiteA7000152x52,
                          ),
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Padding(
                            padding: getPadding(
                              top: 46,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: Card(
                                        clipBehavior: Clip.antiAlias,
                                        elevation: 0,
                                        margin: EdgeInsets.all(0),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadiusStyle.circleBorder19,
                                        ),
                                        child: Container(
                                          height: getSize(
                                            40,
                                          ),
                                          width: getSize(
                                            40,
                                          ),
                                          padding: getPadding(
                                            all: 6,
                                          ),
                                          decoration: AppDecoration
                                              .gradientnameredA20001opacity025nameredA100opacity025
                                              .copyWith(
                                            borderRadius: BorderRadiusStyle
                                                .circleBorder19,
                                          ),
                                          child: Stack(
                                            children: [
                                              CustomIconButton(
                                                height: 28,
                                                width: 28,
                                                padding: getPadding(
                                                  all: 6,
                                                ),
                                                alignment: Alignment.center,
                                                child: CustomImageView(
                                                  svgPath: ImageConstant
                                                      .imgFrameWhiteA70001,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    CustomImageView(
                                      svgPath: ImageConstant
                                          .imgTypemuseumcomponentmaps,
                                      height: getSize(
                                        28,
                                      ),
                                      width: getSize(
                                        28,
                                      ),
                                      alignment: Alignment.center,
                                      margin: getMargin(
                                        top: 72,
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: Padding(
                                        padding: getPadding(
                                          top: 49,
                                          right: 13,
                                        ),
                                        child: Text(
                                          "lbl_portley_lane".tr,
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.left,
                                          style: CustomTextStyles
                                              .bodySmallGray700_1
                                              .copyWith(
                                            letterSpacing: getHorizontalSize(
                                              0.2,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    CustomImageView(
                                      svgPath: ImageConstant
                                          .imgTypehospitalcomponentmaps,
                                      height: getSize(
                                        28,
                                      ),
                                      width: getSize(
                                        28,
                                      ),
                                      margin: getMargin(
                                        top: 111,
                                      ),
                                    ),
                                    Spacer(),
                                    Card(
                                      clipBehavior: Clip.antiAlias,
                                      elevation: 0,
                                      margin: getMargin(
                                        left: 20,
                                      ),
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadiusStyle.circleBorder19,
                                      ),
                                      child: Container(
                                        height: getSize(
                                          40,
                                        ),
                                        width: getSize(
                                          40,
                                        ),
                                        padding: getPadding(
                                          all: 6,
                                        ),
                                        decoration: AppDecoration
                                            .gradientnameredA20001opacity025nameredA100opacity025
                                            .copyWith(
                                          borderRadius:
                                              BorderRadiusStyle.circleBorder19,
                                        ),
                                        child: Stack(
                                          children: [
                                            CustomIconButton(
                                              height: 28,
                                              width: 28,
                                              padding: getPadding(
                                                all: 6,
                                              ),
                                              alignment: Alignment.center,
                                              child: CustomImageView(
                                                svgPath: ImageConstant
                                                    .imgFrameWhiteA70001,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  height: getVerticalSize(
                                    520,
                                  ),
                                  width: getHorizontalSize(
                                    185,
                                  ),
                                  margin: getMargin(
                                    left: 7,
                                    top: 89,
                                    bottom: 5,
                                  ),
                                  child: Stack(
                                    alignment: Alignment.topLeft,
                                    children: [
                                      CustomImageView(
                                        svgPath: ImageConstant
                                            .imgTypeofficecomponentmaps,
                                        height: getSize(
                                          28,
                                        ),
                                        width: getSize(
                                          28,
                                        ),
                                        alignment: Alignment.topRight,
                                        margin: getMargin(
                                          top: 237,
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Card(
                                          clipBehavior: Clip.antiAlias,
                                          elevation: 0,
                                          margin: getMargin(
                                            left: 8,
                                            top: 143,
                                          ),
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadiusStyle
                                                .circleBorder19,
                                          ),
                                          child: Container(
                                            height: getSize(
                                              40,
                                            ),
                                            width: getSize(
                                              40,
                                            ),
                                            padding: getPadding(
                                              all: 6,
                                            ),
                                            decoration: AppDecoration
                                                .gradientnameredA20001opacity025nameredA100opacity025
                                                .copyWith(
                                              borderRadius: BorderRadiusStyle
                                                  .circleBorder19,
                                            ),
                                            child: Stack(
                                              children: [
                                                CustomIconButton(
                                                  height: 28,
                                                  width: 28,
                                                  padding: getPadding(
                                                    all: 6,
                                                  ),
                                                  alignment: Alignment.center,
                                                  child: CustomImageView(
                                                    svgPath: ImageConstant
                                                        .imgFrameWhiteA70001,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      CustomImageView(
                                        svgPath:
                                            ImageConstant.imgVectorIndigo60001,
                                        height: getVerticalSize(
                                          494,
                                        ),
                                        width: getHorizontalSize(
                                          172,
                                        ),
                                        alignment: Alignment.centerLeft,
                                      ),
                                      Align(
                                        alignment: Alignment.topRight,
                                        child: Card(
                                          clipBehavior: Clip.antiAlias,
                                          elevation: 0,
                                          margin: getMargin(
                                            right: 63,
                                          ),
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadiusStyle
                                                .circleBorder19,
                                          ),
                                          child: Container(
                                            height: getSize(
                                              40,
                                            ),
                                            width: getSize(
                                              40,
                                            ),
                                            padding: getPadding(
                                              all: 6,
                                            ),
                                            decoration: AppDecoration
                                                .gradientnameindigoA400opacity025nameindigoA10002opacity025
                                                .copyWith(
                                              borderRadius: BorderRadiusStyle
                                                  .circleBorder19,
                                            ),
                                            child: Stack(
                                              children: [
                                                CustomIconButton(
                                                  height: 28,
                                                  width: 28,
                                                  padding: getPadding(
                                                    all: 6,
                                                  ),
                                                  decoration:
                                                      IconButtonStyleHelper
                                                          .fillIndigo60001,
                                                  alignment: Alignment.center,
                                                  child: CustomImageView(
                                                    svgPath: ImageConstant
                                                        .imgLocationWhiteA70001,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: getPadding(
                                    left: 13,
                                    top: 64,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "lbl_ffordd_owain".tr,
                                        overflow: TextOverflow.ellipsis,
                                        textAlign: TextAlign.left,
                                        style: CustomTextStyles
                                            .bodySmallGray700_1
                                            .copyWith(
                                          letterSpacing: getHorizontalSize(
                                            0.2,
                                          ),
                                        ),
                                      ),
                                      CustomImageView(
                                        svgPath: ImageConstant
                                            .imgTypefitnesscomponentmaps,
                                        height: getSize(
                                          28,
                                        ),
                                        width: getSize(
                                          28,
                                        ),
                                        alignment: Alignment.centerRight,
                                        margin: getMargin(
                                          top: 122,
                                          right: 6,
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.centerRight,
                                        child: Card(
                                          clipBehavior: Clip.antiAlias,
                                          elevation: 0,
                                          margin: getMargin(
                                            top: 198,
                                          ),
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadiusStyle
                                                .circleBorder19,
                                          ),
                                          child: Container(
                                            height: getSize(
                                              40,
                                            ),
                                            width: getSize(
                                              40,
                                            ),
                                            padding: getPadding(
                                              all: 6,
                                            ),
                                            decoration: AppDecoration
                                                .gradientnameredA20001opacity025nameredA100opacity025
                                                .copyWith(
                                              borderRadius: BorderRadiusStyle
                                                  .circleBorder19,
                                            ),
                                            child: Stack(
                                              children: [
                                                CustomIconButton(
                                                  height: 28,
                                                  width: 28,
                                                  padding: getPadding(
                                                    all: 6,
                                                  ),
                                                  alignment: Alignment.center,
                                                  child: CustomImageView(
                                                    svgPath: ImageConstant
                                                        .imgFrameWhiteA70001,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: getPadding(
                                          left: 8,
                                          top: 40,
                                        ),
                                        child: Text(
                                          "lbl_bondhouse_lane".tr,
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.left,
                                          style: CustomTextStyles
                                              .bodySmallGray700_1
                                              .copyWith(
                                            letterSpacing: getHorizontalSize(
                                              0.2,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            margin: getMargin(
                              left: 60,
                              right: 68,
                              bottom: 108,
                            ),
                            padding: getPadding(
                              all: 80,
                            ),
                            decoration: AppDecoration.fill13.copyWith(
                              borderRadius: BorderRadiusStyle.circleBorder150,
                            ),
                            child: Card(
                              clipBehavior: Clip.antiAlias,
                              elevation: 0,
                              margin: EdgeInsets.all(0),
                              color: appTheme.indigo60001.withOpacity(0.39),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadiusStyle.circleBorder70,
                              ),
                              child: Container(
                                height: getSize(
                                  140,
                                ),
                                width: getSize(
                                  140,
                                ),
                                padding: getPadding(
                                  left: 35,
                                  top: 23,
                                  right: 35,
                                  bottom: 23,
                                ),
                                decoration: AppDecoration.fill13.copyWith(
                                  borderRadius:
                                      BorderRadiusStyle.circleBorder70,
                                ),
                                child: Stack(
                                  children: [
                                    CustomImageView(
                                      imagePath: ImageConstant.imgImage,
                                      height: getVerticalSize(
                                        92,
                                      ),
                                      width: getHorizontalSize(
                                        67,
                                      ),
                                      alignment: Alignment.center,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            margin: getMargin(
                              left: 24,
                              top: 22,
                              right: 24,
                              bottom: 784,
                            ),
                            padding: getPadding(
                              left: 16,
                              top: 12,
                              right: 16,
                              bottom: 12,
                            ),
                            decoration: AppDecoration
                                .gradientnameprimarynameindigoA100
                                .copyWith(
                              borderRadius: BorderRadiusStyle.roundedBorder10,
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                CustomIconButton(
                                  height: 52,
                                  width: 52,
                                  padding: getPadding(
                                    all: 12,
                                  ),
                                  decoration:
                                      IconButtonStyleHelper.fillWhiteA70001,
                                  child: CustomImageView(
                                    svgPath: ImageConstant.imgPlayIndigo60001,
                                  ),
                                ),
                                Padding(
                                  padding: getPadding(
                                    left: 16,
                                    top: 2,
                                    bottom: 4,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "msg_to_trantow_courts".tr,
                                        overflow: TextOverflow.ellipsis,
                                        textAlign: TextAlign.left,
                                        style: CustomTextStyles
                                            .titleMediumWhiteA70001,
                                      ),
                                      Padding(
                                        padding: getPadding(
                                          top: 5,
                                        ),
                                        child: Text(
                                          "lbl_500_m".tr,
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.left,
                                          style: CustomTextStyles
                                              .titleSmallGray10001
                                              .copyWith(
                                            letterSpacing: getHorizontalSize(
                                              0.2,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            bottomNavigationBar: Container(
              margin: getMargin(
                left: 24,
                right: 24,
                bottom: 16,
              ),
              decoration: AppDecoration.fill7,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CustomIconButton(
                    height: 64,
                    width: 64,
                    padding: getPadding(
                      all: 16,
                    ),
                    decoration: IconButtonStyleHelper.fillWhiteA70001TL32,
                    child: CustomImageView(
                      svgPath: ImageConstant.imgLocation,
                    ),
                  ),
                  Padding(
                    padding: getPadding(
                      left: 16,
                      top: 8,
                      bottom: 7,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "msg_san_manolia_parking".tr,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.left,
                          style: CustomTextStyles.titleLargeWhiteA70001,
                        ),
                        Padding(
                          padding: getPadding(
                            top: 7,
                          ),
                          child: Text(
                            "msg_9569_trantow_courts3".tr,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.left,
                            style:
                                CustomTextStyles.titleSmallGray10001.copyWith(
                              letterSpacing: getHorizontalSize(
                                0.2,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            floatingActionButton: CustomFloatingButton(
              height: 52,
              width: 52,
              decoration:
                  FloatingButtonStyleHelper.gradientnameredA20001nameredA100,
              child: CustomImageView(
                svgPath: ImageConstant.imgFrame52x52,
                height: getVerticalSize(
                  26.0,
                ),
                width: getHorizontalSize(
                  26.0,
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
