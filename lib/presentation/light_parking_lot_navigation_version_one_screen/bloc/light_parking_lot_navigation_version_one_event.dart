// ignore_for_file: must_be_immutable

part of 'light_parking_lot_navigation_version_one_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightParkingLotNavigationVersionOne widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightParkingLotNavigationVersionOneEvent extends Equatable {}

/// Event that is dispatched when the LightParkingLotNavigationVersionOne widget is first created.
class LightParkingLotNavigationVersionOneInitialEvent
    extends LightParkingLotNavigationVersionOneEvent {
  @override
  List<Object?> get props => [];
}
