import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:parking_app/presentation/light_parking_lot_navigation_version_one_screen/models/light_parking_lot_navigation_version_one_model.dart';
part 'light_parking_lot_navigation_version_one_event.dart';
part 'light_parking_lot_navigation_version_one_state.dart';

/// A bloc that manages the state of a LightParkingLotNavigationVersionOne according to the event that is dispatched to it.
class LightParkingLotNavigationVersionOneBloc extends Bloc<
    LightParkingLotNavigationVersionOneEvent,
    LightParkingLotNavigationVersionOneState> {
  LightParkingLotNavigationVersionOneBloc(
      LightParkingLotNavigationVersionOneState initialState)
      : super(initialState) {
    on<LightParkingLotNavigationVersionOneInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightParkingLotNavigationVersionOneInitialEvent event,
    Emitter<LightParkingLotNavigationVersionOneState> emit,
  ) async {}
}
