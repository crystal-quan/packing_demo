// ignore_for_file: must_be_immutable

part of 'light_parking_lot_navigation_version_one_bloc.dart';

/// Represents the state of LightParkingLotNavigationVersionOne in the application.
class LightParkingLotNavigationVersionOneState extends Equatable {
  LightParkingLotNavigationVersionOneState(
      {this.lightParkingLotNavigationVersionOneModelObj});

  LightParkingLotNavigationVersionOneModel?
      lightParkingLotNavigationVersionOneModelObj;

  @override
  List<Object?> get props => [
        lightParkingLotNavigationVersionOneModelObj,
      ];
  LightParkingLotNavigationVersionOneState copyWith(
      {LightParkingLotNavigationVersionOneModel?
          lightParkingLotNavigationVersionOneModelObj}) {
    return LightParkingLotNavigationVersionOneState(
      lightParkingLotNavigationVersionOneModelObj:
          lightParkingLotNavigationVersionOneModelObj ??
              this.lightParkingLotNavigationVersionOneModelObj,
    );
  }
}
