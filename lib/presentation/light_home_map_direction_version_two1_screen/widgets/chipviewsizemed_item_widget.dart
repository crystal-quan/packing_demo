import '../models/chipviewsizemed_item_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

// ignore: must_be_immutable
class ChipviewsizemedItemWidget extends StatelessWidget {
  ChipviewsizemedItemWidget(
    this.chipviewsizemedItemModelObj, {
    Key? key,
    this.onSelectedChipView,
  }) : super(
          key: key,
        );

  ChipviewsizemedItemModel chipviewsizemedItemModelObj;

  Function(bool)? onSelectedChipView;

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        canvasColor: Colors.transparent,
      ),
      child: RawChip(
        padding: getPadding(
          right: 20,
        ),
        showCheckmark: false,
        labelPadding: EdgeInsets.zero,
        label: Text(
          chipviewsizemedItemModelObj.sizemediumtypebTxt,
          textAlign: TextAlign.left,
          style: TextStyle(
            color: appTheme.indigo60001,
            fontSize: getFontSize(
              16,
            ),
            fontFamily: 'Urbanist',
            fontWeight: FontWeight.w600,
          ),
        ),
        avatar: CustomImageView(
          svgPath: ImageConstant.imgLocation,
          height: getSize(
            16,
          ),
          width: getSize(
            16,
          ),
          margin: getMargin(
            right: 8,
          ),
        ),
        selected: chipviewsizemedItemModelObj.isSelected,
        backgroundColor: Colors.transparent,
        selectedColor: appTheme.indigo60001.withOpacity(0.2),
        shape: chipviewsizemedItemModelObj.isSelected
            ? RoundedRectangleBorder(
                side: BorderSide(
                  color: appTheme.indigo60001,
                  width: getHorizontalSize(
                    1,
                  ),
                ),
                borderRadius: BorderRadius.circular(
                  getHorizontalSize(
                    19,
                  ),
                ),
              )
            : RoundedRectangleBorder(
                side: BorderSide(
                  color: appTheme.indigo60001,
                  width: getHorizontalSize(
                    2,
                  ),
                ),
                borderRadius: BorderRadius.circular(
                  getHorizontalSize(
                    19,
                  ),
                ),
              ),
        onSelected: (value) {
          onSelectedChipView?.call(value);
        },
      ),
    );
  }
}
