import '../light_home_map_direction_version_two1_screen/widgets/chipviewsizemed_item_widget.dart';
import 'bloc/light_home_map_direction_version_two1_bloc.dart';
import 'models/chipviewsizemed_item_model.dart';
import 'models/light_home_map_direction_version_two1_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/presentation/light_home_map_direction_version_two_page/light_home_map_direction_version_two_page.dart';
import 'package:parking_app/presentation/light_my_bookmark_page/light_my_bookmark_page.dart';
import 'package:parking_app/presentation/light_my_parking_reservation_completed_tab_container_page/light_my_parking_reservation_completed_tab_container_page.dart';
import 'package:parking_app/presentation/light_profile_settings_page/light_profile_settings_page.dart';
import 'package:parking_app/widgets/custom_bottom_bar.dart';
import 'package:parking_app/widgets/custom_floating_button.dart';
import 'package:parking_app/widgets/custom_icon_button.dart';
import 'package:parking_app/widgets/custom_outlined_button.dart';

class LightHomeMapDirectionVersionTwo1Screen extends StatelessWidget {
  LightHomeMapDirectionVersionTwo1Screen({Key? key})
      : super(
          key: key,
        );

  GlobalKey<NavigatorState> navigatorKey = GlobalKey();

  static Widget builder(BuildContext context) {
    return BlocProvider<LightHomeMapDirectionVersionTwo1Bloc>(
      create: (context) => LightHomeMapDirectionVersionTwo1Bloc(
          LightHomeMapDirectionVersionTwo1State(
        lightHomeMapDirectionVersionTwo1ModelObj:
            LightHomeMapDirectionVersionTwo1Model(),
      ))
        ..add(LightHomeMapDirectionVersionTwo1InitialEvent()),
      child: LightHomeMapDirectionVersionTwo1Screen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.whiteA70001,
        body: SizedBox(
          height: mediaQueryData.size.height,
          width: double.maxFinite,
          child: Stack(
            alignment: Alignment.center,
            children: [
              CustomImageView(
                imagePath: ImageConstant.imgVectorDeepPurple100882x428,
                height: getVerticalSize(
                  882,
                ),
                width: getHorizontalSize(
                  428,
                ),
                alignment: Alignment.center,
              ),
              SizedBox(
                height: mediaQueryData.size.height,
                width: double.maxFinite,
                child: Stack(
                  alignment: Alignment.bottomRight,
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        height: mediaQueryData.size.height,
                        width: double.maxFinite,
                        decoration: BoxDecoration(
                          color: appTheme.gray5002,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        padding: getPadding(
                          left: 24,
                          top: 726,
                          bottom: 118,
                        ),
                        child: IntrinsicWidth(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              BlocSelector<
                                  LightHomeMapDirectionVersionTwo1Bloc,
                                  LightHomeMapDirectionVersionTwo1State,
                                  LightHomeMapDirectionVersionTwo1Model?>(
                                selector: (state) => state
                                    .lightHomeMapDirectionVersionTwo1ModelObj,
                                builder: (context,
                                    lightHomeMapDirectionVersionTwo1ModelObj) {
                                  return Wrap(
                                    runSpacing: getVerticalSize(
                                      5,
                                    ),
                                    spacing: getHorizontalSize(
                                      5,
                                    ),
                                    children: List<Widget>.generate(
                                      lightHomeMapDirectionVersionTwo1ModelObj
                                              ?.chipviewsizemedItemList
                                              .length ??
                                          0,
                                      (index) {
                                        ChipviewsizemedItemModel model =
                                            lightHomeMapDirectionVersionTwo1ModelObj
                                                        ?.chipviewsizemedItemList[
                                                    index] ??
                                                ChipviewsizemedItemModel();

                                        return ChipviewsizemedItemWidget(
                                          model,
                                          onSelectedChipView: (value) {
                                            context
                                                .read<
                                                    LightHomeMapDirectionVersionTwo1Bloc>()
                                                .add(UpdateChipViewEvent(
                                                    index: index,
                                                    isSelected: value));
                                          },
                                        );
                                      },
                                    ),
                                  );
                                },
                              ),
                              CustomOutlinedButton(
                                text: "lbl_fitness".tr,
                                margin: getMargin(
                                  left: 12,
                                ),
                                leftIcon: Container(
                                  margin: getMargin(
                                    right: 8,
                                  ),
                                  child: CustomImageView(
                                    svgPath: ImageConstant.imgLocation,
                                  ),
                                ),
                                buttonStyle: CustomButtonStyles
                                    .outlineIndigo60001
                                    .copyWith(
                                        fixedSize:
                                            MaterialStateProperty.all<Size>(
                                                Size(
                                  getHorizontalSize(
                                    114,
                                  ),
                                  getVerticalSize(
                                    38,
                                  ),
                                ))),
                                buttonTextStyle: CustomTextStyles
                                    .titleMediumIndigo60001SemiBold16,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    CustomImageView(
                      imagePath: ImageConstant.imgVector416x220,
                      height: getVerticalSize(
                        416,
                      ),
                      width: getHorizontalSize(
                        220,
                      ),
                      alignment: Alignment.topCenter,
                      margin: getMargin(
                        top: 112,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Container(
                        margin: getMargin(
                          right: 30,
                          bottom: 180,
                        ),
                        padding: getPadding(
                          all: 80,
                        ),
                        decoration: AppDecoration.fill10.copyWith(
                          borderRadius: BorderRadiusStyle.circleBorder150,
                        ),
                        child: Card(
                          clipBehavior: Clip.antiAlias,
                          elevation: 0,
                          margin: EdgeInsets.all(0),
                          color: appTheme.indigoA70063,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadiusStyle.circleBorder70,
                          ),
                          child: Container(
                            height: getSize(
                              140,
                            ),
                            width: getSize(
                              140,
                            ),
                            padding: getPadding(
                              left: 38,
                              top: 24,
                              right: 38,
                              bottom: 24,
                            ),
                            decoration: AppDecoration.fill10.copyWith(
                              borderRadius: BorderRadiusStyle.circleBorder70,
                            ),
                            child: Stack(
                              children: [
                                CustomImageView(
                                  imagePath: ImageConstant.imgImage,
                                  height: getVerticalSize(
                                    91,
                                  ),
                                  width: getHorizontalSize(
                                    61,
                                  ),
                                  alignment: Alignment.center,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: Padding(
                        padding: getPadding(
                          top: 22,
                          right: 24,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            CustomIconButton(
                              height: 52,
                              width: 52,
                              padding: getPadding(
                                all: 12,
                              ),
                              decoration: IconButtonStyleHelper.fillWhiteA70001,
                              child: CustomImageView(
                                svgPath: ImageConstant.imgSearchIndigo60001,
                              ),
                            ),
                            CustomIconButton(
                              height: 52,
                              width: 52,
                              margin: getMargin(
                                left: 20,
                              ),
                              padding: getPadding(
                                all: 12,
                              ),
                              decoration: IconButtonStyleHelper.fillWhiteA70001,
                              child: CustomImageView(
                                svgPath:
                                    ImageConstant.imgIconlycurvednotification,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Padding(
                        padding: getPadding(
                          left: 19,
                          top: 35,
                          right: 23,
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Card(
                              clipBehavior: Clip.antiAlias,
                              elevation: 0,
                              margin: getMargin(
                                left: 19,
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadiusStyle.circleBorder19,
                              ),
                              child: Container(
                                height: getSize(
                                  40,
                                ),
                                width: getSize(
                                  40,
                                ),
                                padding: getPadding(
                                  all: 6,
                                ),
                                decoration: AppDecoration
                                    .gradientnameredA20001opacity025nameredA100opacity025
                                    .copyWith(
                                  borderRadius:
                                      BorderRadiusStyle.circleBorder19,
                                ),
                                child: Stack(
                                  children: [
                                    CustomIconButton(
                                      height: 28,
                                      width: 28,
                                      padding: getPadding(
                                        all: 6,
                                      ),
                                      alignment: Alignment.center,
                                      child: CustomImageView(
                                        svgPath:
                                            ImageConstant.imgFrameWhiteA70001,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.centerRight,
                              child: Padding(
                                padding: getPadding(
                                  top: 18,
                                  right: 90,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Card(
                                      clipBehavior: Clip.antiAlias,
                                      elevation: 0,
                                      margin: getMargin(
                                        bottom: 16,
                                      ),
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadiusStyle.circleBorder19,
                                      ),
                                      child: Container(
                                        height: getSize(
                                          40,
                                        ),
                                        width: getSize(
                                          40,
                                        ),
                                        padding: getPadding(
                                          all: 6,
                                        ),
                                        decoration: AppDecoration
                                            .gradientnameindigoA400opacity025nameindigoA10002opacity025
                                            .copyWith(
                                          borderRadius:
                                              BorderRadiusStyle.circleBorder19,
                                        ),
                                        child: Stack(
                                          children: [
                                            CustomIconButton(
                                              height: 28,
                                              width: 28,
                                              padding: getPadding(
                                                all: 6,
                                              ),
                                              decoration: IconButtonStyleHelper
                                                  .fillIndigo60001,
                                              alignment: Alignment.center,
                                              child: CustomImageView(
                                                svgPath: ImageConstant
                                                    .imgLocationWhiteA70001,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    CustomImageView(
                                      svgPath: ImageConstant
                                          .imgTypeofficecomponentmaps,
                                      height: getSize(
                                        28,
                                      ),
                                      width: getSize(
                                        28,
                                      ),
                                      margin: getMargin(
                                        left: 75,
                                        top: 28,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            CustomImageView(
                              svgPath: ImageConstant.imgTypemuseumcomponentmaps,
                              height: getSize(
                                28,
                              ),
                              width: getSize(
                                28,
                              ),
                              margin: getMargin(
                                left: 11,
                                top: 7,
                              ),
                            ),
                            Padding(
                              padding: getPadding(
                                top: 1,
                                right: 47,
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: getPadding(
                                      top: 44,
                                    ),
                                    child: Text(
                                      "lbl_bondhouse_lane".tr,
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.left,
                                      style: CustomTextStyles.bodySmallGray700_1
                                          .copyWith(
                                        letterSpacing: getHorizontalSize(
                                          0.2,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: getPadding(
                                      bottom: 20,
                                    ),
                                    child: Text(
                                      "lbl_portley_lane".tr,
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.left,
                                      style: CustomTextStyles.bodySmallGray700_1
                                          .copyWith(
                                        letterSpacing: getHorizontalSize(
                                          0.2,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Card(
                              clipBehavior: Clip.antiAlias,
                              elevation: 0,
                              margin: getMargin(
                                top: 22,
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadiusStyle.circleBorder19,
                              ),
                              child: Container(
                                height: getSize(
                                  40,
                                ),
                                width: getSize(
                                  40,
                                ),
                                padding: getPadding(
                                  all: 6,
                                ),
                                decoration: AppDecoration
                                    .gradientnameredA20001opacity025nameredA100opacity025
                                    .copyWith(
                                  borderRadius:
                                      BorderRadiusStyle.circleBorder19,
                                ),
                                child: Stack(
                                  children: [
                                    CustomIconButton(
                                      height: 28,
                                      width: 28,
                                      padding: getPadding(
                                        all: 6,
                                      ),
                                      alignment: Alignment.center,
                                      child: CustomImageView(
                                        svgPath:
                                            ImageConstant.imgFrameWhiteA70001,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            CustomImageView(
                              svgPath:
                                  ImageConstant.imgTypefitnesscomponentmaps,
                              height: getSize(
                                28,
                              ),
                              width: getSize(
                                28,
                              ),
                              alignment: Alignment.centerRight,
                              margin: getMargin(
                                top: 20,
                              ),
                            ),
                            Padding(
                              padding: getPadding(
                                left: 5,
                                top: 30,
                                right: 33,
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  CustomImageView(
                                    svgPath: ImageConstant
                                        .imgTypehospitalcomponentmaps,
                                    height: getSize(
                                      28,
                                    ),
                                    width: getSize(
                                      28,
                                    ),
                                    margin: getMargin(
                                      bottom: 38,
                                    ),
                                  ),
                                  Card(
                                    clipBehavior: Clip.antiAlias,
                                    elevation: 0,
                                    margin: getMargin(
                                      top: 26,
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadiusStyle.circleBorder19,
                                    ),
                                    child: Container(
                                      height: getSize(
                                        40,
                                      ),
                                      width: getSize(
                                        40,
                                      ),
                                      padding: getPadding(
                                        all: 6,
                                      ),
                                      decoration: AppDecoration
                                          .gradientnameredA20001opacity025nameredA100opacity025
                                          .copyWith(
                                        borderRadius:
                                            BorderRadiusStyle.circleBorder19,
                                      ),
                                      child: Stack(
                                        children: [
                                          CustomIconButton(
                                            height: 28,
                                            width: 28,
                                            padding: getPadding(
                                              all: 6,
                                            ),
                                            alignment: Alignment.center,
                                            child: CustomImageView(
                                              svgPath: ImageConstant
                                                  .imgFrameWhiteA70001,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: getPadding(
                                left: 22,
                                top: 80,
                              ),
                              child: Text(
                                "lbl_ffordd_owain".tr,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.left,
                                style: CustomTextStyles.bodySmallGray700_1
                                    .copyWith(
                                  letterSpacing: getHorizontalSize(
                                    0.2,
                                  ),
                                ),
                              ),
                            ),
                            Card(
                              clipBehavior: Clip.antiAlias,
                              elevation: 0,
                              margin: getMargin(
                                left: 25,
                                top: 39,
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadiusStyle.circleBorder19,
                              ),
                              child: Container(
                                height: getSize(
                                  40,
                                ),
                                width: getSize(
                                  40,
                                ),
                                padding: getPadding(
                                  all: 6,
                                ),
                                decoration: AppDecoration
                                    .gradientnameredA20001opacity025nameredA100opacity025
                                    .copyWith(
                                  borderRadius:
                                      BorderRadiusStyle.circleBorder19,
                                ),
                                child: Stack(
                                  children: [
                                    CustomIconButton(
                                      height: 28,
                                      width: 28,
                                      padding: getPadding(
                                        all: 6,
                                      ),
                                      alignment: Alignment.center,
                                      child: CustomImageView(
                                        svgPath:
                                            ImageConstant.imgFrameWhiteA70001,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: CustomBottomBar(
          onChanged: (BottomBarEnum type) {
            Navigator.pushNamed(
                navigatorKey.currentContext!, getCurrentRoute(type));
          },
        ),
        floatingActionButton: CustomFloatingButton(
          height: 52,
          width: 52,
          backgroundColor: appTheme.indigo60001,
          child: CustomImageView(
            svgPath: ImageConstant.imgFrameWhiteA7000152x52,
            height: getVerticalSize(
              26.0,
            ),
            width: getHorizontalSize(
              26.0,
            ),
          ),
        ),
      ),
    );
  }

  ///Handling route based on bottom click actions
  String getCurrentRoute(BottomBarEnum type) {
    switch (type) {
      case BottomBarEnum.Home:
        return AppRoutes.lightHomeMapDirectionVersionTwoPage;
      case BottomBarEnum.Saved:
        return AppRoutes.lightMyBookmarkPage;
      case BottomBarEnum.Booking:
        return AppRoutes.lightMyParkingReservationCompletedTabContainerPage;
      case BottomBarEnum.Profile:
        return AppRoutes.lightProfileSettingsPage;
      default:
        return "/";
    }
  }

  ///Handling page based on route
  Widget getCurrentPage(
    BuildContext context,
    String currentRoute,
  ) {
    switch (currentRoute) {
      case AppRoutes.lightHomeMapDirectionVersionTwoPage:
        return LightHomeMapDirectionVersionTwoPage.builder(context);
      case AppRoutes.lightMyBookmarkPage:
        return LightMyBookmarkPage.builder(context);
      case AppRoutes.lightMyParkingReservationCompletedTabContainerPage:
        return LightMyParkingReservationCompletedTabContainerPage.builder(
            context);
      case AppRoutes.lightProfileSettingsPage:
        return LightProfileSettingsPage.builder(context);
      default:
        return DefaultWidget();
    }
  }
}
