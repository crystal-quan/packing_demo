import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/chipviewsizemed_item_model.dart';
import 'package:parking_app/presentation/light_home_map_direction_version_two1_screen/models/light_home_map_direction_version_two1_model.dart';
part 'light_home_map_direction_version_two1_event.dart';
part 'light_home_map_direction_version_two1_state.dart';

/// A bloc that manages the state of a LightHomeMapDirectionVersionTwo1 according to the event that is dispatched to it.
class LightHomeMapDirectionVersionTwo1Bloc extends Bloc<
    LightHomeMapDirectionVersionTwo1Event,
    LightHomeMapDirectionVersionTwo1State> {
  LightHomeMapDirectionVersionTwo1Bloc(
      LightHomeMapDirectionVersionTwo1State initialState)
      : super(initialState) {
    on<LightHomeMapDirectionVersionTwo1InitialEvent>(_onInitialize);
    on<UpdateChipViewEvent>(_updateChipView);
  }

  _onInitialize(
    LightHomeMapDirectionVersionTwo1InitialEvent event,
    Emitter<LightHomeMapDirectionVersionTwo1State> emit,
  ) async {
    emit(state.copyWith(
        lightHomeMapDirectionVersionTwo1ModelObj:
            state.lightHomeMapDirectionVersionTwo1ModelObj?.copyWith(
      chipviewsizemedItemList: fillChipviewsizemedItemList(),
    )));
  }

  _updateChipView(
    UpdateChipViewEvent event,
    Emitter<LightHomeMapDirectionVersionTwo1State> emit,
  ) {
    List<ChipviewsizemedItemModel> newList =
        List<ChipviewsizemedItemModel>.from(state
            .lightHomeMapDirectionVersionTwo1ModelObj!.chipviewsizemedItemList);
    newList[event.index] = newList[event.index].copyWith(
      isSelected: event.isSelected,
    );
    emit(state.copyWith(
        lightHomeMapDirectionVersionTwo1ModelObj: state
            .lightHomeMapDirectionVersionTwo1ModelObj
            ?.copyWith(chipviewsizemedItemList: newList)));
  }

  List<ChipviewsizemedItemModel> fillChipviewsizemedItemList() {
    return List.generate(3, (index) => ChipviewsizemedItemModel());
  }
}
