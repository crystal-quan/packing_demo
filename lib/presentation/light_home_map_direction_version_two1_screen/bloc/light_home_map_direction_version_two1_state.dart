// ignore_for_file: must_be_immutable

part of 'light_home_map_direction_version_two1_bloc.dart';

/// Represents the state of LightHomeMapDirectionVersionTwo1 in the application.
class LightHomeMapDirectionVersionTwo1State extends Equatable {
  LightHomeMapDirectionVersionTwo1State(
      {this.lightHomeMapDirectionVersionTwo1ModelObj});

  LightHomeMapDirectionVersionTwo1Model?
      lightHomeMapDirectionVersionTwo1ModelObj;

  @override
  List<Object?> get props => [
        lightHomeMapDirectionVersionTwo1ModelObj,
      ];
  LightHomeMapDirectionVersionTwo1State copyWith(
      {LightHomeMapDirectionVersionTwo1Model?
          lightHomeMapDirectionVersionTwo1ModelObj}) {
    return LightHomeMapDirectionVersionTwo1State(
      lightHomeMapDirectionVersionTwo1ModelObj:
          lightHomeMapDirectionVersionTwo1ModelObj ??
              this.lightHomeMapDirectionVersionTwo1ModelObj,
    );
  }
}
