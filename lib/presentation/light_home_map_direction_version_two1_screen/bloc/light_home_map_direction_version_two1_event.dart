// ignore_for_file: must_be_immutable

part of 'light_home_map_direction_version_two1_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightHomeMapDirectionVersionTwo1 widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightHomeMapDirectionVersionTwo1Event extends Equatable {}

/// Event that is dispatched when the LightHomeMapDirectionVersionTwo1 widget is first created.
class LightHomeMapDirectionVersionTwo1InitialEvent
    extends LightHomeMapDirectionVersionTwo1Event {
  @override
  List<Object?> get props => [];
}

///Event for changing ChipView selection
class UpdateChipViewEvent extends LightHomeMapDirectionVersionTwo1Event {
  UpdateChipViewEvent({
    required this.index,
    this.isSelected,
  });

  int index;

  bool? isSelected;

  @override
  List<Object?> get props => [
        index,
        isSelected,
      ];
}
