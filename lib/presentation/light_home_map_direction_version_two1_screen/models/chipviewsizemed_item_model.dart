// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class is used in the [chipviewsizemed_item_widget] screen.
class ChipviewsizemedItemModel extends Equatable {ChipviewsizemedItemModel({this.sizemediumtypebTxt = "Home", this.isSelected = false, });

String sizemediumtypebTxt;

bool isSelected;

ChipviewsizemedItemModel copyWith({String? sizemediumtypebTxt, bool? isSelected, }) { return ChipviewsizemedItemModel(
sizemediumtypebTxt : sizemediumtypebTxt ?? this.sizemediumtypebTxt,
isSelected : isSelected ?? this.isSelected,
); } 
@override List<Object?> get props => [sizemediumtypebTxt,isSelected];
 }
