// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'chipviewsizemed_item_model.dart';/// This class defines the variables used in the [light_home_map_direction_version_two1_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightHomeMapDirectionVersionTwo1Model extends Equatable {LightHomeMapDirectionVersionTwo1Model({this.chipviewsizemedItemList = const []});

List<ChipviewsizemedItemModel> chipviewsizemedItemList;

LightHomeMapDirectionVersionTwo1Model copyWith({List<ChipviewsizemedItemModel>? chipviewsizemedItemList}) { return LightHomeMapDirectionVersionTwo1Model(
chipviewsizemedItemList : chipviewsizemedItemList ?? this.chipviewsizemedItemList,
); } 
@override List<Object?> get props => [chipviewsizemedItemList];
 }
