// ignore_for_file: must_be_immutable

part of 'mobile_login_bro_two_bloc.dart';

/// Represents the state of MobileLoginBroTwo in the application.
class MobileLoginBroTwoState extends Equatable {
  MobileLoginBroTwoState({this.mobileLoginBroTwoModelObj});

  MobileLoginBroTwoModel? mobileLoginBroTwoModelObj;

  @override
  List<Object?> get props => [
        mobileLoginBroTwoModelObj,
      ];
  MobileLoginBroTwoState copyWith(
      {MobileLoginBroTwoModel? mobileLoginBroTwoModelObj}) {
    return MobileLoginBroTwoState(
      mobileLoginBroTwoModelObj:
          mobileLoginBroTwoModelObj ?? this.mobileLoginBroTwoModelObj,
    );
  }
}
