// ignore_for_file: must_be_immutable

part of 'mobile_login_bro_two_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///MobileLoginBroTwo widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class MobileLoginBroTwoEvent extends Equatable {}

/// Event that is dispatched when the MobileLoginBroTwo widget is first created.
class MobileLoginBroTwoInitialEvent extends MobileLoginBroTwoEvent {
  @override
  List<Object?> get props => [];
}
