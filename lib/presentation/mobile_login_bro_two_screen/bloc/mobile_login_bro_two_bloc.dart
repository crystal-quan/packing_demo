import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:parking_app/presentation/mobile_login_bro_two_screen/models/mobile_login_bro_two_model.dart';
part 'mobile_login_bro_two_event.dart';
part 'mobile_login_bro_two_state.dart';

/// A bloc that manages the state of a MobileLoginBroTwo according to the event that is dispatched to it.
class MobileLoginBroTwoBloc
    extends Bloc<MobileLoginBroTwoEvent, MobileLoginBroTwoState> {
  MobileLoginBroTwoBloc(MobileLoginBroTwoState initialState)
      : super(initialState) {
    on<MobileLoginBroTwoInitialEvent>(_onInitialize);
  }

  _onInitialize(
    MobileLoginBroTwoInitialEvent event,
    Emitter<MobileLoginBroTwoState> emit,
  ) async {}
}
