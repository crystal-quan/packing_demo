import 'bloc/mobile_login_bro_two_bloc.dart';
import 'models/mobile_login_bro_two_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

class MobileLoginBroTwoScreen extends StatelessWidget {
  const MobileLoginBroTwoScreen({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<MobileLoginBroTwoBloc>(
      create: (context) => MobileLoginBroTwoBloc(MobileLoginBroTwoState(
        mobileLoginBroTwoModelObj: MobileLoginBroTwoModel(),
      ))
        ..add(MobileLoginBroTwoInitialEvent()),
      child: MobileLoginBroTwoScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return BlocBuilder<MobileLoginBroTwoBloc, MobileLoginBroTwoState>(
      builder: (context, state) {
        return SafeArea(
          child: Scaffold(
            body: SizedBox(
              width: getHorizontalSize(
                256,
              ),
              child: CustomImageView(
                imagePath: ImageConstant.imgCardrivingbro,
                height: getSize(
                  256,
                ),
                width: getSize(
                  256,
                ),
                alignment: Alignment.center,
              ),
            ),
          ),
        );
      },
    );
  }
}
