// ignore_for_file: must_be_immutable

part of 'light_extend_parking_time_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightExtendParkingTime widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightExtendParkingTimeEvent extends Equatable {}

/// Event that is dispatched when the LightExtendParkingTime widget is first created.
class LightExtendParkingTimeInitialEvent extends LightExtendParkingTimeEvent {
  @override
  List<Object?> get props => [];
}

///Event for changing radio button
class ChangeRadioButtonEvent extends LightExtendParkingTimeEvent {
  ChangeRadioButtonEvent({required this.value});

  String value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing radio button
class ChangeRadioButton1Event extends LightExtendParkingTimeEvent {
  ChangeRadioButton1Event({required this.value});

  String value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing radio button
class ChangeRadioButton2Event extends LightExtendParkingTimeEvent {
  ChangeRadioButton2Event({required this.value});

  String value;

  @override
  List<Object?> get props => [
        value,
      ];
}
