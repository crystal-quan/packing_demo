import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_extend_parking_time_screen/models/light_extend_parking_time_model.dart';part 'light_extend_parking_time_event.dart';part 'light_extend_parking_time_state.dart';/// A bloc that manages the state of a LightExtendParkingTime according to the event that is dispatched to it.
class LightExtendParkingTimeBloc extends Bloc<LightExtendParkingTimeEvent, LightExtendParkingTimeState> {LightExtendParkingTimeBloc(LightExtendParkingTimeState initialState) : super(initialState) { on<LightExtendParkingTimeInitialEvent>(_onInitialize); on<ChangeRadioButtonEvent>(_changeRadioButton); on<ChangeRadioButton1Event>(_changeRadioButton1); on<ChangeRadioButton2Event>(_changeRadioButton2); }

_changeRadioButton(ChangeRadioButtonEvent event, Emitter<LightExtendParkingTimeState> emit, ) { emit(state.copyWith(radioGroup: event.value)); } 
_changeRadioButton1(ChangeRadioButton1Event event, Emitter<LightExtendParkingTimeState> emit, ) { emit(state.copyWith(radioGroup1: event.value)); } 
_changeRadioButton2(ChangeRadioButton2Event event, Emitter<LightExtendParkingTimeState> emit, ) { emit(state.copyWith(radioGroup2: event.value)); } 
_onInitialize(LightExtendParkingTimeInitialEvent event, Emitter<LightExtendParkingTimeState> emit, ) async  { emit(state.copyWith(radioGroup: "", radioGroup1: "", radioGroup2: "")); } 
 }
