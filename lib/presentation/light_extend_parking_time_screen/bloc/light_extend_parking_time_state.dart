// ignore_for_file: must_be_immutable

part of 'light_extend_parking_time_bloc.dart';

/// Represents the state of LightExtendParkingTime in the application.
class LightExtendParkingTimeState extends Equatable {
  LightExtendParkingTimeState({
    this.radioGroup = "",
    this.radioGroup1 = "",
    this.radioGroup2 = "",
    this.lightExtendParkingTimeModelObj,
  });

  LightExtendParkingTimeModel? lightExtendParkingTimeModelObj;

  String radioGroup;

  String radioGroup1;

  String radioGroup2;

  @override
  List<Object?> get props => [
        radioGroup,
        radioGroup1,
        radioGroup2,
        lightExtendParkingTimeModelObj,
      ];
  LightExtendParkingTimeState copyWith({
    String? radioGroup,
    String? radioGroup1,
    String? radioGroup2,
    LightExtendParkingTimeModel? lightExtendParkingTimeModelObj,
  }) {
    return LightExtendParkingTimeState(
      radioGroup: radioGroup ?? this.radioGroup,
      radioGroup1: radioGroup1 ?? this.radioGroup1,
      radioGroup2: radioGroup2 ?? this.radioGroup2,
      lightExtendParkingTimeModelObj:
          lightExtendParkingTimeModelObj ?? this.lightExtendParkingTimeModelObj,
    );
  }
}
