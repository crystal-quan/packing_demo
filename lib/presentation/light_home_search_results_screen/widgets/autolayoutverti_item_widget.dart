import '../models/autolayoutverti_item_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_icon_button.dart';

// ignore: must_be_immutable
class AutolayoutvertiItemWidget extends StatelessWidget {
  AutolayoutvertiItemWidget(
    this.autolayoutvertiItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  AutolayoutvertiItemModel autolayoutvertiItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Card(
          clipBehavior: Clip.antiAlias,
          elevation: 0,
          margin: getMargin(
            top: 2,
            bottom: 1,
          ),
          color: appTheme.deepPurple100,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadiusStyle.circleBorder19,
          ),
          child: Container(
            height: getSize(
              40,
            ),
            width: getSize(
              40,
            ),
            padding: getPadding(
              all: 6,
            ),
            decoration: AppDecoration.fill9.copyWith(
              borderRadius: BorderRadiusStyle.circleBorder19,
            ),
            child: Stack(
              children: [
                CustomIconButton(
                  height: 28,
                  width: 28,
                  padding: getPadding(
                    all: 6,
                  ),
                  decoration: IconButtonStyleHelper.fillIndigo60001,
                  alignment: Alignment.center,
                  child: CustomImageView(
                    svgPath: ImageConstant.imgFrameWhiteA70001,
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: getPadding(
            left: 20,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                autolayoutvertiItemModelObj.nameTxt,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.left,
                style: theme.textTheme.titleMedium,
              ),
              Padding(
                padding: getPadding(
                  top: 3,
                ),
                child: Text(
                  autolayoutvertiItemModelObj.timezoneTxt,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.left,
                  style: CustomTextStyles.titleSmallGray700.copyWith(
                    letterSpacing: getHorizontalSize(
                      0.2,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Spacer(),
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              autolayoutvertiItemModelObj.distanceTxt,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.left,
              style: CustomTextStyles.titleMedium16.copyWith(
                letterSpacing: getHorizontalSize(
                  0.2,
                ),
              ),
            ),
            Padding(
              padding: getPadding(
                top: 6,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    autolayoutvertiItemModelObj.priceTxt,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.left,
                    style: CustomTextStyles.titleSmallIndigo60001.copyWith(
                      letterSpacing: getHorizontalSize(
                        0.2,
                      ),
                    ),
                  ),
                  Padding(
                    padding: getPadding(
                      left: 4,
                      top: 2,
                      bottom: 2,
                    ),
                    child: Text(
                      "lbl_hour".tr,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                      style: CustomTextStyles.bodySmallGray700.copyWith(
                        letterSpacing: getHorizontalSize(
                          0.2,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
