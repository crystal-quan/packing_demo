// ignore_for_file: must_be_immutable

part of 'light_home_search_results_bloc.dart';

/// Represents the state of LightHomeSearchResults in the application.
class LightHomeSearchResultsState extends Equatable {
  LightHomeSearchResultsState({
    this.statefilledsearController,
    this.lightHomeSearchResultsModelObj,
  });

  TextEditingController? statefilledsearController;

  LightHomeSearchResultsModel? lightHomeSearchResultsModelObj;

  @override
  List<Object?> get props => [
        statefilledsearController,
        lightHomeSearchResultsModelObj,
      ];
  LightHomeSearchResultsState copyWith({
    TextEditingController? statefilledsearController,
    LightHomeSearchResultsModel? lightHomeSearchResultsModelObj,
  }) {
    return LightHomeSearchResultsState(
      statefilledsearController:
          statefilledsearController ?? this.statefilledsearController,
      lightHomeSearchResultsModelObj:
          lightHomeSearchResultsModelObj ?? this.lightHomeSearchResultsModelObj,
    );
  }
}
