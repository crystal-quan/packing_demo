// ignore_for_file: must_be_immutable

part of 'light_home_search_results_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightHomeSearchResults widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightHomeSearchResultsEvent extends Equatable {}

/// Event that is dispatched when the LightHomeSearchResults widget is first created.
class LightHomeSearchResultsInitialEvent extends LightHomeSearchResultsEvent {
  @override
  List<Object?> get props => [];
}
