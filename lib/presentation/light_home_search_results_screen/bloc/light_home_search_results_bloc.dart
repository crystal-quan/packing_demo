import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/autolayoutverti_item_model.dart';
import 'package:parking_app/presentation/light_home_search_results_screen/models/light_home_search_results_model.dart';
part 'light_home_search_results_event.dart';
part 'light_home_search_results_state.dart';

/// A bloc that manages the state of a LightHomeSearchResults according to the event that is dispatched to it.
class LightHomeSearchResultsBloc
    extends Bloc<LightHomeSearchResultsEvent, LightHomeSearchResultsState> {
  LightHomeSearchResultsBloc(LightHomeSearchResultsState initialState)
      : super(initialState) {
    on<LightHomeSearchResultsInitialEvent>(_onInitialize);
  }

  List<AutolayoutvertiItemModel> fillAutolayoutvertiItemList() {
    return List.generate(10, (index) => AutolayoutvertiItemModel());
  }

  _onInitialize(
    LightHomeSearchResultsInitialEvent event,
    Emitter<LightHomeSearchResultsState> emit,
  ) async {
    emit(state.copyWith(
      statefilledsearController: TextEditingController(),
    ));
    emit(state.copyWith(
        lightHomeSearchResultsModelObj:
            state.lightHomeSearchResultsModelObj?.copyWith(
      autolayoutvertiItemList: fillAutolayoutvertiItemList(),
    )));
  }
}
