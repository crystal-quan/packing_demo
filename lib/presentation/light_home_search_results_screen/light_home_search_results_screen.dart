import '../light_home_search_results_screen/widgets/autolayoutverti_item_widget.dart';
import 'bloc/light_home_search_results_bloc.dart';
import 'models/autolayoutverti_item_model.dart';
import 'models/light_home_search_results_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_text_form_field.dart';

class LightHomeSearchResultsScreen extends StatelessWidget {
  const LightHomeSearchResultsScreen({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LightHomeSearchResultsBloc>(
      create: (context) =>
          LightHomeSearchResultsBloc(LightHomeSearchResultsState(
        lightHomeSearchResultsModelObj: LightHomeSearchResultsModel(),
      ))
            ..add(LightHomeSearchResultsInitialEvent()),
      child: LightHomeSearchResultsScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.whiteA70001,
        resizeToAvoidBottomInset: false,
        body: Container(
          width: double.maxFinite,
          padding: getPadding(
            left: 24,
            top: 19,
            right: 24,
            bottom: 19,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              BlocSelector<LightHomeSearchResultsBloc,
                  LightHomeSearchResultsState, TextEditingController?>(
                selector: (state) => state.statefilledsearController,
                builder: (context, statefilledsearController) {
                  return CustomTextFormField(
                    controller: statefilledsearController,
                    margin: getMargin(
                      top: 4,
                    ),
                    contentPadding: getPadding(
                      top: 19,
                      bottom: 19,
                    ),
                    textStyle: theme.textTheme.titleSmall!,
                    hintText: "lbl_parking".tr,
                    hintStyle: theme.textTheme.titleSmall!,
                    prefix: Container(
                      margin: getMargin(
                        left: 20,
                        top: 18,
                        right: 12,
                        bottom: 18,
                      ),
                      child: CustomImageView(
                        svgPath: ImageConstant.imgSearchIndigo60001,
                      ),
                    ),
                    prefixConstraints: BoxConstraints(
                      maxHeight: getVerticalSize(
                        56,
                      ),
                    ),
                    suffix: Container(
                      margin: getMargin(
                        left: 30,
                        top: 18,
                        right: 20,
                        bottom: 18,
                      ),
                      child: CustomImageView(
                        svgPath: ImageConstant.imgMenuIndigo60001,
                      ),
                    ),
                    suffixConstraints: BoxConstraints(
                      maxHeight: getVerticalSize(
                        56,
                      ),
                    ),
                    filled: true,
                    fillColor: appTheme.gray5003,
                  );
                },
              ),
              Padding(
                padding: getPadding(
                  top: 24,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: getPadding(
                        bottom: 1,
                      ),
                      child: Text(
                        "lbl_results_1_274".tr,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: theme.textTheme.titleMedium,
                      ),
                    ),
                    CustomImageView(
                      svgPath: ImageConstant.imgArrowup,
                      height: getSize(
                        24,
                      ),
                      width: getSize(
                        24,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Padding(
                  padding: getPadding(
                    top: 25,
                  ),
                  child: BlocSelector<
                      LightHomeSearchResultsBloc,
                      LightHomeSearchResultsState,
                      LightHomeSearchResultsModel?>(
                    selector: (state) => state.lightHomeSearchResultsModelObj,
                    builder: (context, lightHomeSearchResultsModelObj) {
                      return ListView.separated(
                        physics: BouncingScrollPhysics(),
                        shrinkWrap: true,
                        separatorBuilder: (
                          context,
                          index,
                        ) {
                          return SizedBox(
                            height: getVerticalSize(
                              30,
                            ),
                          );
                        },
                        itemCount: lightHomeSearchResultsModelObj
                                ?.autolayoutvertiItemList.length ??
                            0,
                        itemBuilder: (context, index) {
                          AutolayoutvertiItemModel model =
                              lightHomeSearchResultsModelObj
                                      ?.autolayoutvertiItemList[index] ??
                                  AutolayoutvertiItemModel();
                          return AutolayoutvertiItemWidget(
                            model,
                          );
                        },
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
