// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'autolayoutverti_item_model.dart';/// This class defines the variables used in the [light_home_search_results_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightHomeSearchResultsModel extends Equatable {LightHomeSearchResultsModel({this.autolayoutvertiItemList = const []});

List<AutolayoutvertiItemModel> autolayoutvertiItemList;

LightHomeSearchResultsModel copyWith({List<AutolayoutvertiItemModel>? autolayoutvertiItemList}) { return LightHomeSearchResultsModel(
autolayoutvertiItemList : autolayoutvertiItemList ?? this.autolayoutvertiItemList,
); } 
@override List<Object?> get props => [autolayoutvertiItemList];
 }
