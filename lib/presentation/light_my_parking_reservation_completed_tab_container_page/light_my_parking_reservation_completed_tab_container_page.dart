import 'bloc/light_my_parking_reservation_completed_tab_container_bloc.dart';
import 'models/light_my_parking_reservation_completed_tab_container_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/presentation/light_my_parking_reservation_canceled_page/light_my_parking_reservation_canceled_page.dart';
import 'package:parking_app/presentation/light_my_parking_reservation_completed_page/light_my_parking_reservation_completed_page.dart';
import 'package:parking_app/presentation/light_my_parking_reservation_ongoing_page/light_my_parking_reservation_ongoing_page.dart';
import 'package:parking_app/widgets/app_bar/appbar_image.dart';
import 'package:parking_app/widgets/app_bar/appbar_title.dart';
import 'package:parking_app/widgets/app_bar/custom_app_bar.dart';

// ignore_for_file: must_be_immutable
class LightMyParkingReservationCompletedTabContainerPage
    extends StatefulWidget {
  const LightMyParkingReservationCompletedTabContainerPage({Key? key})
      : super(
          key: key,
        );

  @override
  LightMyParkingReservationCompletedTabContainerPageState createState() =>
      LightMyParkingReservationCompletedTabContainerPageState();
  static Widget builder(BuildContext context) {
    return BlocProvider<LightMyParkingReservationCompletedTabContainerBloc>(
      create: (context) => LightMyParkingReservationCompletedTabContainerBloc(
          LightMyParkingReservationCompletedTabContainerState(
        lightMyParkingReservationCompletedTabContainerModelObj:
            LightMyParkingReservationCompletedTabContainerModel(),
      ))
        ..add(LightMyParkingReservationCompletedTabContainerInitialEvent()),
      child: LightMyParkingReservationCompletedTabContainerPage(),
    );
  }
}

class LightMyParkingReservationCompletedTabContainerPageState
    extends State<LightMyParkingReservationCompletedTabContainerPage>
    with TickerProviderStateMixin {
  late TabController tabviewController;

  @override
  void initState() {
    super.initState();
    tabviewController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return BlocBuilder<LightMyParkingReservationCompletedTabContainerBloc,
        LightMyParkingReservationCompletedTabContainerState>(
      builder: (context, state) {
        return SafeArea(
          child: Scaffold(
            backgroundColor: appTheme.whiteA70001,
            appBar: CustomAppBar(
              height: getVerticalSize(
                77,
              ),
              title: AppbarTitle(
                text: "lbl_my_parking".tr,
                margin: getMargin(
                  left: 72,
                ),
              ),
              actions: [
                AppbarImage(
                  height: getSize(
                    28,
                  ),
                  width: getSize(
                    28,
                  ),
                  svgPath: ImageConstant.imgSearchGray900,
                  margin: getMargin(
                    left: 24,
                    top: 12,
                    right: 24,
                    bottom: 15,
                  ),
                ),
              ],
            ),
            body: SizedBox(
              width: double.maxFinite,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    height: getVerticalSize(
                      45,
                    ),
                    width: getHorizontalSize(
                      380,
                    ),
                    margin: getMargin(
                      top: 18,
                    ),
                    child: TabBar(
                      controller: tabviewController,
                      labelColor: appTheme.whiteA70001,
                      labelStyle: TextStyle(),
                      unselectedLabelColor: appTheme.indigo60001,
                      unselectedLabelStyle: TextStyle(),
                      indicator: BoxDecoration(
                        borderRadius: BorderRadius.circular(
                          getHorizontalSize(
                            22,
                          ),
                        ),
                        gradient: LinearGradient(
                          begin: Alignment(
                            0.5,
                            0,
                          ),
                          end: Alignment(
                            0.5,
                            1,
                          ),
                          colors: [
                            theme.colorScheme.primary,
                            appTheme.indigoA100,
                          ],
                        ),
                      ),
                      tabs: [
                        Tab(
                          child: Text(
                            "lbl_ongoing".tr,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Tab(
                          child: Text(
                            "lbl_completed".tr,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Tab(
                          child: Text(
                            "lbl_canceled".tr,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: SizedBox(
                      height: getVerticalSize(
                        650,
                      ),
                      child: TabBarView(
                        controller: tabviewController,
                        children: [
                          LightMyParkingReservationOngoingPage.builder(context),
                          LightMyParkingReservationCompletedPage.builder(
                              context),
                          LightMyParkingReservationCanceledPage.builder(
                              context),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
