// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_my_parking_reservation_completed_tab_container_page],
/// and is typically used to hold data that is passed between different parts of the application.
class LightMyParkingReservationCompletedTabContainerModel extends Equatable {LightMyParkingReservationCompletedTabContainerModel copyWith() { return LightMyParkingReservationCompletedTabContainerModel(
); } 
@override List<Object?> get props => [];
 }
