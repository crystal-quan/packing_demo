// ignore_for_file: must_be_immutable

part of 'light_my_parking_reservation_completed_tab_container_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightMyParkingReservationCompletedTabContainer widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightMyParkingReservationCompletedTabContainerEvent
    extends Equatable {}

/// Event that is dispatched when the LightMyParkingReservationCompletedTabContainer widget is first created.
class LightMyParkingReservationCompletedTabContainerInitialEvent
    extends LightMyParkingReservationCompletedTabContainerEvent {
  @override
  List<Object?> get props => [];
}
