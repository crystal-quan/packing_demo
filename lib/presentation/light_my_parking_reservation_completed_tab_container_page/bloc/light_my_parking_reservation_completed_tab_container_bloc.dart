import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:parking_app/presentation/light_my_parking_reservation_completed_tab_container_page/models/light_my_parking_reservation_completed_tab_container_model.dart';
part 'light_my_parking_reservation_completed_tab_container_event.dart';
part 'light_my_parking_reservation_completed_tab_container_state.dart';

/// A bloc that manages the state of a LightMyParkingReservationCompletedTabContainer according to the event that is dispatched to it.
class LightMyParkingReservationCompletedTabContainerBloc extends Bloc<
    LightMyParkingReservationCompletedTabContainerEvent,
    LightMyParkingReservationCompletedTabContainerState> {
  LightMyParkingReservationCompletedTabContainerBloc(
      LightMyParkingReservationCompletedTabContainerState initialState)
      : super(initialState) {
    on<LightMyParkingReservationCompletedTabContainerInitialEvent>(
        _onInitialize);
  }

  _onInitialize(
    LightMyParkingReservationCompletedTabContainerInitialEvent event,
    Emitter<LightMyParkingReservationCompletedTabContainerState> emit,
  ) async {}
}
