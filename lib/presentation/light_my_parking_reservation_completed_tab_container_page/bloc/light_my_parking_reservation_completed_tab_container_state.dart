// ignore_for_file: must_be_immutable

part of 'light_my_parking_reservation_completed_tab_container_bloc.dart';

/// Represents the state of LightMyParkingReservationCompletedTabContainer in the application.
class LightMyParkingReservationCompletedTabContainerState extends Equatable {
  LightMyParkingReservationCompletedTabContainerState(
      {this.lightMyParkingReservationCompletedTabContainerModelObj});

  LightMyParkingReservationCompletedTabContainerModel?
      lightMyParkingReservationCompletedTabContainerModelObj;

  @override
  List<Object?> get props => [
        lightMyParkingReservationCompletedTabContainerModelObj,
      ];
  LightMyParkingReservationCompletedTabContainerState copyWith(
      {LightMyParkingReservationCompletedTabContainerModel?
          lightMyParkingReservationCompletedTabContainerModelObj}) {
    return LightMyParkingReservationCompletedTabContainerState(
      lightMyParkingReservationCompletedTabContainerModelObj:
          lightMyParkingReservationCompletedTabContainerModelObj ??
              this.lightMyParkingReservationCompletedTabContainerModelObj,
    );
  }
}
