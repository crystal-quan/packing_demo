// ignore_for_file: must_be_immutable

part of 'light_home_map_direction_version_two_container_bloc.dart';

/// Represents the state of LightHomeMapDirectionVersionTwoContainer in the application.
class LightHomeMapDirectionVersionTwoContainerState extends Equatable {
  LightHomeMapDirectionVersionTwoContainerState(
      {this.lightHomeMapDirectionVersionTwoContainerModelObj});

  LightHomeMapDirectionVersionTwoContainerModel?
      lightHomeMapDirectionVersionTwoContainerModelObj;

  @override
  List<Object?> get props => [
        lightHomeMapDirectionVersionTwoContainerModelObj,
      ];
  LightHomeMapDirectionVersionTwoContainerState copyWith(
      {LightHomeMapDirectionVersionTwoContainerModel?
          lightHomeMapDirectionVersionTwoContainerModelObj}) {
    return LightHomeMapDirectionVersionTwoContainerState(
      lightHomeMapDirectionVersionTwoContainerModelObj:
          lightHomeMapDirectionVersionTwoContainerModelObj ??
              this.lightHomeMapDirectionVersionTwoContainerModelObj,
    );
  }
}
