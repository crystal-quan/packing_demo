// ignore_for_file: must_be_immutable

part of 'light_home_map_direction_version_two_container_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightHomeMapDirectionVersionTwoContainer widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightHomeMapDirectionVersionTwoContainerEvent
    extends Equatable {}

/// Event that is dispatched when the LightHomeMapDirectionVersionTwoContainer widget is first created.
class LightHomeMapDirectionVersionTwoContainerInitialEvent
    extends LightHomeMapDirectionVersionTwoContainerEvent {
  @override
  List<Object?> get props => [];
}
