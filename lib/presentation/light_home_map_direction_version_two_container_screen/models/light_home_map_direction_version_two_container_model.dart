// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_home_map_direction_version_two_container_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightHomeMapDirectionVersionTwoContainerModel extends Equatable {LightHomeMapDirectionVersionTwoContainerModel copyWith() { return LightHomeMapDirectionVersionTwoContainerModel(
); } 
@override List<Object?> get props => [];
 }
