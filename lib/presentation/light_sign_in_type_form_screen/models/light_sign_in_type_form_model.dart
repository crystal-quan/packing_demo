// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_sign_in_type_form_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightSignInTypeFormModel extends Equatable {LightSignInTypeFormModel copyWith() { return LightSignInTypeFormModel(
); } 
@override List<Object?> get props => [];
 }
