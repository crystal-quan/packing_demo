// ignore_for_file: must_be_immutable

part of 'light_sign_in_type_form_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightSignInTypeForm widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightSignInTypeFormEvent extends Equatable {}

/// Event that is dispatched when the LightSignInTypeForm widget is first created.
class LightSignInTypeFormInitialEvent extends LightSignInTypeFormEvent {
  @override
  List<Object?> get props => [];
}

///Event for changing password visibility
class ChangePasswordVisibilityEvent extends LightSignInTypeFormEvent {
  ChangePasswordVisibilityEvent({required this.value});

  bool value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing checkbox
class ChangeCheckBoxEvent extends LightSignInTypeFormEvent {
  ChangeCheckBoxEvent({required this.value});

  bool value;

  @override
  List<Object?> get props => [
        value,
      ];
}
