import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_sign_in_type_form_screen/models/light_sign_in_type_form_model.dart';part 'light_sign_in_type_form_event.dart';part 'light_sign_in_type_form_state.dart';/// A bloc that manages the state of a LightSignInTypeForm according to the event that is dispatched to it.
class LightSignInTypeFormBloc extends Bloc<LightSignInTypeFormEvent, LightSignInTypeFormState> {LightSignInTypeFormBloc(LightSignInTypeFormState initialState) : super(initialState) { on<LightSignInTypeFormInitialEvent>(_onInitialize); on<ChangePasswordVisibilityEvent>(_changePasswordVisibility); on<ChangeCheckBoxEvent>(_changeCheckBox); }

_changePasswordVisibility(ChangePasswordVisibilityEvent event, Emitter<LightSignInTypeFormState> emit, ) { emit(state.copyWith(isShowPassword: event.value)); } 
_changeCheckBox(ChangeCheckBoxEvent event, Emitter<LightSignInTypeFormState> emit, ) { emit(state.copyWith(isCheckbox: event.value)); } 
_onInitialize(LightSignInTypeFormInitialEvent event, Emitter<LightSignInTypeFormState> emit, ) async  { emit(state.copyWith(emailController: TextEditingController(), languageController: TextEditingController(), isShowPassword: true, isCheckbox: false)); } 
 }
