// ignore_for_file: must_be_immutable

part of 'light_sign_in_type_form_bloc.dart';

/// Represents the state of LightSignInTypeForm in the application.
class LightSignInTypeFormState extends Equatable {
  LightSignInTypeFormState({
    this.emailController,
    this.languageController,
    this.isShowPassword = true,
    this.isCheckbox = false,
    this.lightSignInTypeFormModelObj,
  });

  TextEditingController? emailController;

  TextEditingController? languageController;

  LightSignInTypeFormModel? lightSignInTypeFormModelObj;

  bool isShowPassword;

  bool isCheckbox;

  @override
  List<Object?> get props => [
        emailController,
        languageController,
        isShowPassword,
        isCheckbox,
        lightSignInTypeFormModelObj,
      ];
  LightSignInTypeFormState copyWith({
    TextEditingController? emailController,
    TextEditingController? languageController,
    bool? isShowPassword,
    bool? isCheckbox,
    LightSignInTypeFormModel? lightSignInTypeFormModelObj,
  }) {
    return LightSignInTypeFormState(
      emailController: emailController ?? this.emailController,
      languageController: languageController ?? this.languageController,
      isShowPassword: isShowPassword ?? this.isShowPassword,
      isCheckbox: isCheckbox ?? this.isCheckbox,
      lightSignInTypeFormModelObj:
          lightSignInTypeFormModelObj ?? this.lightSignInTypeFormModelObj,
    );
  }
}
