// ignore_for_file: must_be_immutable

part of 'light_sign_up_filled_form_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightSignUpFilledForm widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightSignUpFilledFormEvent extends Equatable {}

/// Event that is dispatched when the LightSignUpFilledForm widget is first created.
class LightSignUpFilledFormInitialEvent extends LightSignUpFilledFormEvent {
  @override
  List<Object?> get props => [];
}

///Event for changing password visibility
class ChangePasswordVisibilityEvent extends LightSignUpFilledFormEvent {
  ChangePasswordVisibilityEvent({required this.value});

  bool value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing checkbox
class ChangeCheckBoxEvent extends LightSignUpFilledFormEvent {
  ChangeCheckBoxEvent({required this.value});

  bool value;

  @override
  List<Object?> get props => [
        value,
      ];
}
