import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_sign_up_filled_form_screen/models/light_sign_up_filled_form_model.dart';part 'light_sign_up_filled_form_event.dart';part 'light_sign_up_filled_form_state.dart';/// A bloc that manages the state of a LightSignUpFilledForm according to the event that is dispatched to it.
class LightSignUpFilledFormBloc extends Bloc<LightSignUpFilledFormEvent, LightSignUpFilledFormState> {LightSignUpFilledFormBloc(LightSignUpFilledFormState initialState) : super(initialState) { on<LightSignUpFilledFormInitialEvent>(_onInitialize); on<ChangePasswordVisibilityEvent>(_changePasswordVisibility); on<ChangeCheckBoxEvent>(_changeCheckBox); }

_changePasswordVisibility(ChangePasswordVisibilityEvent event, Emitter<LightSignUpFilledFormState> emit, ) { emit(state.copyWith(isShowPassword: event.value)); } 
_changeCheckBox(ChangeCheckBoxEvent event, Emitter<LightSignUpFilledFormState> emit, ) { emit(state.copyWith(isCheckbox: event.value)); } 
_onInitialize(LightSignUpFilledFormInitialEvent event, Emitter<LightSignUpFilledFormState> emit, ) async  { emit(state.copyWith(statusfilltypeeController: TextEditingController(), groupseventyController: TextEditingController(), isShowPassword: true, isCheckbox: false)); } 
 }
