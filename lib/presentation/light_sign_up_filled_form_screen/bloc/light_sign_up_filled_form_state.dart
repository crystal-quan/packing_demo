// ignore_for_file: must_be_immutable

part of 'light_sign_up_filled_form_bloc.dart';

/// Represents the state of LightSignUpFilledForm in the application.
class LightSignUpFilledFormState extends Equatable {
  LightSignUpFilledFormState({
    this.statusfilltypeeController,
    this.groupseventyController,
    this.isShowPassword = true,
    this.isCheckbox = false,
    this.lightSignUpFilledFormModelObj,
  });

  TextEditingController? statusfilltypeeController;

  TextEditingController? groupseventyController;

  LightSignUpFilledFormModel? lightSignUpFilledFormModelObj;

  bool isShowPassword;

  bool isCheckbox;

  @override
  List<Object?> get props => [
        statusfilltypeeController,
        groupseventyController,
        isShowPassword,
        isCheckbox,
        lightSignUpFilledFormModelObj,
      ];
  LightSignUpFilledFormState copyWith({
    TextEditingController? statusfilltypeeController,
    TextEditingController? groupseventyController,
    bool? isShowPassword,
    bool? isCheckbox,
    LightSignUpFilledFormModel? lightSignUpFilledFormModelObj,
  }) {
    return LightSignUpFilledFormState(
      statusfilltypeeController:
          statusfilltypeeController ?? this.statusfilltypeeController,
      groupseventyController:
          groupseventyController ?? this.groupseventyController,
      isShowPassword: isShowPassword ?? this.isShowPassword,
      isCheckbox: isCheckbox ?? this.isCheckbox,
      lightSignUpFilledFormModelObj:
          lightSignUpFilledFormModelObj ?? this.lightSignUpFilledFormModelObj,
    );
  }
}
