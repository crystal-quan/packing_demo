import '../models/sliderframe_item_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_elevated_button.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

// ignore: must_be_immutable
class SliderframeItemWidget extends StatelessWidget {
  SliderframeItemWidget(
    this.sliderframeItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  SliderframeItemModel sliderframeItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        margin: getMargin(
          right: 93,
        ),
        padding: getPadding(
          left: 21,
          top: 8,
          right: 21,
          bottom: 8,
        ),
        decoration: AppDecoration.outline5.copyWith(
          borderRadius: BorderRadiusStyle.customBorderTL40,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            CustomImageView(
              svgPath: ImageConstant.imgFrameGray30001,
              height: getVerticalSize(
                3,
              ),
              width: getHorizontalSize(
                38,
              ),
            ),
            Padding(
              padding: getPadding(
                top: 23,
              ),
              child: Text(
                "lbl_details".tr,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.left,
                style: theme.textTheme.headlineSmall,
              ),
            ),
            Padding(
              padding: getPadding(
                top: 24,
              ),
              child: Divider(
                height: getVerticalSize(
                  1,
                ),
                thickness: getVerticalSize(
                  1,
                ),
                color: appTheme.gray200,
                indent: getHorizontalSize(
                  3,
                ),
              ),
            ),
            Container(
              height: getVerticalSize(
                182,
              ),
              width: getHorizontalSize(
                367,
              ),
              margin: getMargin(
                top: 10,
              ),
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      height: getVerticalSize(
                        182,
                      ),
                      width: getHorizontalSize(
                        363,
                      ),
                      decoration: BoxDecoration(
                        color: appTheme.indigo60001.withOpacity(0.39),
                        borderRadius: BorderRadius.circular(
                          getHorizontalSize(
                            10,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: SizedBox(
                      height: getVerticalSize(
                        182,
                      ),
                      width: getHorizontalSize(
                        363,
                      ),
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          CustomImageView(
                            imagePath: ImageConstant.imgImage4,
                            height: getVerticalSize(
                              182,
                            ),
                            width: getHorizontalSize(
                              363,
                            ),
                            radius: BorderRadius.circular(
                              getHorizontalSize(
                                10,
                              ),
                            ),
                            alignment: Alignment.center,
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              height: getVerticalSize(
                                182,
                              ),
                              width: getHorizontalSize(
                                363,
                              ),
                              decoration: BoxDecoration(
                                color: appTheme.indigo60001.withOpacity(0.39),
                                borderRadius: BorderRadius.circular(
                                  getHorizontalSize(
                                    10,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: getVerticalSize(
                6,
              ),
              margin: getMargin(
                top: 13,
              ),
              child: AnimatedSmoothIndicator(
                activeIndex: 0,
                count: 5,
                effect: ScrollingDotsEffect(
                  spacing: 11,
                  activeDotColor: appTheme.indigo60001,
                  dotColor: appTheme.gray30001,
                  dotHeight: getVerticalSize(
                    6,
                  ),
                  dotWidth: getHorizontalSize(
                    6,
                  ),
                ),
              ),
            ),
            Padding(
              padding: getPadding(
                left: 15,
                top: 34,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "msg_parking_lot_of_san".tr,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.left,
                          style: theme.textTheme.headlineSmall,
                        ),
                        Padding(
                          padding: getPadding(
                            top: 7,
                          ),
                          child: Text(
                            "msg_9569_trantow_courts2".tr,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.left,
                            style: CustomTextStyles.titleMediumGray700Medium
                                .copyWith(
                              letterSpacing: getHorizontalSize(
                                0.2,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  CustomImageView(
                    svgPath: ImageConstant.imgBookmarkIndigo60001,
                    height: getSize(
                      28,
                    ),
                    width: getSize(
                      28,
                    ),
                    margin: getMargin(
                      left: 37,
                      top: 13,
                      bottom: 14,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: getPadding(
                left: 3,
                top: 20,
                bottom: 40,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: CustomElevatedButton(
                      text: "lbl_cancel".tr,
                      margin: getMargin(
                        right: 6,
                      ),
                      buttonStyle: CustomButtonStyles.fillGray100.copyWith(
                          fixedSize: MaterialStateProperty.all<Size>(Size(
                        double.maxFinite,
                        getVerticalSize(
                          58,
                        ),
                      ))),
                      buttonTextStyle: CustomTextStyles.titleMediumIndigo60001,
                    ),
                  ),
                  Expanded(
                    child: CustomElevatedButton(
                      width: getHorizontalSize(
                        184,
                      ),
                      height: getVerticalSize(
                        58,
                      ),
                      text: "lbl_details".tr,
                      margin: getMargin(
                        left: 6,
                      ),
                      rightIcon: Container(
                        margin: getMargin(
                          left: 16,
                        ),
                        child: CustomImageView(
                          svgPath: ImageConstant.imgMenuWhiteA70001,
                        ),
                      ),
                      buttonStyle:
                          CustomButtonStyles.outlineDeeppurple100TL10.copyWith(
                              fixedSize: MaterialStateProperty.all<Size>(Size(
                        double.maxFinite,
                        getVerticalSize(
                          58,
                        ),
                      ))),
                      decoration:
                          CustomButtonStyles.outlineDeeppurple100TL10Decoration,
                      buttonTextStyle:
                          CustomTextStyles.titleMediumWhiteA7000116,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
