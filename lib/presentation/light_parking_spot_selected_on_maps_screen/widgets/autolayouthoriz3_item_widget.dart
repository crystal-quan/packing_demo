import '../models/autolayouthoriz3_item_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

// ignore: must_be_immutable
class Autolayouthoriz3ItemWidget extends StatelessWidget {
  Autolayouthoriz3ItemWidget(
    this.autolayouthoriz3ItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  Autolayouthoriz3ItemModel autolayouthoriz3ItemModelObj;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: getHorizontalSize(
        107,
      ),
      child: Align(
        alignment: Alignment.bottomRight,
        child: Container(
          padding: getPadding(
            left: 20,
            top: 8,
            right: 20,
            bottom: 8,
          ),
          decoration: AppDecoration.outline6.copyWith(
            borderRadius: BorderRadiusStyle.circleBorder19,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomImageView(
                svgPath: ImageConstant.imgLocationIndigoA400,
                height: getSize(
                  16,
                ),
                width: getSize(
                  16,
                ),
                margin: getMargin(
                  top: 2,
                  bottom: 2,
                ),
              ),
              Padding(
                padding: getPadding(
                  left: 8,
                  top: 1,
                ),
                child: Text(
                  autolayouthoriz3ItemModelObj.typeTxt,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.left,
                  style:
                      CustomTextStyles.titleMediumIndigoA400SemiBold.copyWith(
                    letterSpacing: getHorizontalSize(
                      0.2,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
