// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'autolayouthoriz3_item_model.dart';import 'sliderframe_item_model.dart';/// This class defines the variables used in the [light_parking_spot_selected_on_maps_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightParkingSpotSelectedOnMapsModel extends Equatable {LightParkingSpotSelectedOnMapsModel({this.autolayouthoriz3ItemList = const [], this.sliderframeItemList = const [], });

List<Autolayouthoriz3ItemModel> autolayouthoriz3ItemList;

List<SliderframeItemModel> sliderframeItemList;

LightParkingSpotSelectedOnMapsModel copyWith({List<Autolayouthoriz3ItemModel>? autolayouthoriz3ItemList, List<SliderframeItemModel>? sliderframeItemList, }) { return LightParkingSpotSelectedOnMapsModel(
autolayouthoriz3ItemList : autolayouthoriz3ItemList ?? this.autolayouthoriz3ItemList,
sliderframeItemList : sliderframeItemList ?? this.sliderframeItemList,
); } 
@override List<Object?> get props => [autolayouthoriz3ItemList,sliderframeItemList];
 }
