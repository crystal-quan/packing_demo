// ignore_for_file: must_be_immutable

part of 'light_parking_spot_selected_on_maps_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightParkingSpotSelectedOnMaps widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightParkingSpotSelectedOnMapsEvent extends Equatable {}

/// Event that is dispatched when the LightParkingSpotSelectedOnMaps widget is first created.
class LightParkingSpotSelectedOnMapsInitialEvent
    extends LightParkingSpotSelectedOnMapsEvent {
  @override
  List<Object?> get props => [];
}
