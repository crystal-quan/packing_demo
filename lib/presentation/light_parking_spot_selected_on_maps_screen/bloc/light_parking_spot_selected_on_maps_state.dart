// ignore_for_file: must_be_immutable

part of 'light_parking_spot_selected_on_maps_bloc.dart';

/// Represents the state of LightParkingSpotSelectedOnMaps in the application.
class LightParkingSpotSelectedOnMapsState extends Equatable {
  LightParkingSpotSelectedOnMapsState({
    this.sliderIndex = 0,
    this.lightParkingSpotSelectedOnMapsModelObj,
  });

  LightParkingSpotSelectedOnMapsModel? lightParkingSpotSelectedOnMapsModelObj;

  int sliderIndex;

  @override
  List<Object?> get props => [
        sliderIndex,
        lightParkingSpotSelectedOnMapsModelObj,
      ];
  LightParkingSpotSelectedOnMapsState copyWith({
    int? sliderIndex,
    LightParkingSpotSelectedOnMapsModel? lightParkingSpotSelectedOnMapsModelObj,
  }) {
    return LightParkingSpotSelectedOnMapsState(
      sliderIndex: sliderIndex ?? this.sliderIndex,
      lightParkingSpotSelectedOnMapsModelObj:
          lightParkingSpotSelectedOnMapsModelObj ??
              this.lightParkingSpotSelectedOnMapsModelObj,
    );
  }
}
