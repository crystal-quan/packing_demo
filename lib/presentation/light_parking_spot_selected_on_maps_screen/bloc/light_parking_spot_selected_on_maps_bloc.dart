import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/autolayouthoriz3_item_model.dart';
import '../models/sliderframe_item_model.dart';
import 'package:parking_app/presentation/light_parking_spot_selected_on_maps_screen/models/light_parking_spot_selected_on_maps_model.dart';
part 'light_parking_spot_selected_on_maps_event.dart';
part 'light_parking_spot_selected_on_maps_state.dart';

/// A bloc that manages the state of a LightParkingSpotSelectedOnMaps according to the event that is dispatched to it.
class LightParkingSpotSelectedOnMapsBloc extends Bloc<
    LightParkingSpotSelectedOnMapsEvent, LightParkingSpotSelectedOnMapsState> {
  LightParkingSpotSelectedOnMapsBloc(
      LightParkingSpotSelectedOnMapsState initialState)
      : super(initialState) {
    on<LightParkingSpotSelectedOnMapsInitialEvent>(_onInitialize);
  }

  List<Autolayouthoriz3ItemModel> fillAutolayouthoriz3ItemList() {
    return List.generate(3, (index) => Autolayouthoriz3ItemModel());
  }

  List<SliderframeItemModel> fillSliderframeItemList() {
    return List.generate(2, (index) => SliderframeItemModel());
  }

  _onInitialize(
    LightParkingSpotSelectedOnMapsInitialEvent event,
    Emitter<LightParkingSpotSelectedOnMapsState> emit,
  ) async {
    emit(state.copyWith(
      sliderIndex: 0,
    ));
    emit(state.copyWith(
        lightParkingSpotSelectedOnMapsModelObj:
            state.lightParkingSpotSelectedOnMapsModelObj?.copyWith(
      autolayouthoriz3ItemList: fillAutolayouthoriz3ItemList(),
      sliderframeItemList: fillSliderframeItemList(),
    )));
  }
}
