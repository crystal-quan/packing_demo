import '../light_parking_spot_selected_on_maps_screen/widgets/autolayouthoriz3_item_widget.dart';
import '../light_parking_spot_selected_on_maps_screen/widgets/sliderframe_item_widget.dart';
import 'bloc/light_parking_spot_selected_on_maps_bloc.dart';
import 'models/autolayouthoriz3_item_model.dart';
import 'models/light_parking_spot_selected_on_maps_model.dart';
import 'models/sliderframe_item_model.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/presentation/light_home_map_direction_version_two_page/light_home_map_direction_version_two_page.dart';
import 'package:parking_app/presentation/light_my_bookmark_page/light_my_bookmark_page.dart';
import 'package:parking_app/presentation/light_my_parking_reservation_completed_tab_container_page/light_my_parking_reservation_completed_tab_container_page.dart';
import 'package:parking_app/presentation/light_profile_settings_page/light_profile_settings_page.dart';
import 'package:parking_app/widgets/custom_bottom_bar.dart';
import 'package:parking_app/widgets/custom_elevated_button.dart';
import 'package:parking_app/widgets/custom_icon_button.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class LightParkingSpotSelectedOnMapsScreen extends StatelessWidget {
  LightParkingSpotSelectedOnMapsScreen({Key? key})
      : super(
          key: key,
        );

  GlobalKey<NavigatorState> navigatorKey = GlobalKey();

  static Widget builder(BuildContext context) {
    return BlocProvider<LightParkingSpotSelectedOnMapsBloc>(
      create: (context) => LightParkingSpotSelectedOnMapsBloc(
          LightParkingSpotSelectedOnMapsState(
        lightParkingSpotSelectedOnMapsModelObj:
            LightParkingSpotSelectedOnMapsModel(),
      ))
        ..add(LightParkingSpotSelectedOnMapsInitialEvent()),
      child: LightParkingSpotSelectedOnMapsScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.whiteA70001,
        body: SizedBox(
          height: mediaQueryData.size.height,
          width: double.maxFinite,
          child: Stack(
            alignment: Alignment.center,
            children: [
              CustomImageView(
                imagePath: ImageConstant.imgVectorIndigo50,
                height: getVerticalSize(
                  882,
                ),
                width: getHorizontalSize(
                  428,
                ),
                alignment: Alignment.center,
              ),
              SizedBox(
                height: mediaQueryData.size.height,
                width: double.maxFinite,
                child: Stack(
                  alignment: Alignment.bottomRight,
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        height: mediaQueryData.size.height,
                        width: double.maxFinite,
                        decoration: BoxDecoration(
                          color: appTheme.gray5002,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: SizedBox(
                        height: mediaQueryData.size.height,
                        child: BlocSelector<
                            LightParkingSpotSelectedOnMapsBloc,
                            LightParkingSpotSelectedOnMapsState,
                            LightParkingSpotSelectedOnMapsModel?>(
                          selector: (state) =>
                              state.lightParkingSpotSelectedOnMapsModelObj,
                          builder: (context,
                              lightParkingSpotSelectedOnMapsModelObj) {
                            return ListView.separated(
                              padding: getPadding(
                                left: 24,
                                top: 726,
                                bottom: 118,
                              ),
                              scrollDirection: Axis.horizontal,
                              separatorBuilder: (
                                context,
                                index,
                              ) {
                                return SizedBox(
                                  width: getHorizontalSize(
                                    12,
                                  ),
                                );
                              },
                              itemCount: lightParkingSpotSelectedOnMapsModelObj
                                      ?.autolayouthoriz3ItemList.length ??
                                  0,
                              itemBuilder: (context, index) {
                                Autolayouthoriz3ItemModel model =
                                    lightParkingSpotSelectedOnMapsModelObj
                                            ?.autolayouthoriz3ItemList[index] ??
                                        Autolayouthoriz3ItemModel();
                                return Autolayouthoriz3ItemWidget(
                                  model,
                                );
                              },
                            );
                          },
                        ),
                      ),
                    ),
                    CustomIconButton(
                      height: 52,
                      width: 52,
                      margin: getMargin(
                        right: 24,
                        bottom: 180,
                      ),
                      padding: getPadding(
                        all: 12,
                      ),
                      decoration: IconButtonStyleHelper
                          .gradientnameindigoA400nameindigoA10002,
                      alignment: Alignment.bottomRight,
                      child: CustomImageView(
                        svgPath: ImageConstant.imgFrameWhiteA7000152x52,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Container(
                        margin: getMargin(
                          right: 51,
                          bottom: 249,
                        ),
                        padding: getPadding(
                          all: 80,
                        ),
                        decoration: AppDecoration.fill11.copyWith(
                          borderRadius: BorderRadiusStyle.circleBorder150,
                        ),
                        child: Card(
                          clipBehavior: Clip.antiAlias,
                          elevation: 0,
                          margin: EdgeInsets.all(0),
                          color: appTheme.indigoA400.withOpacity(0.39),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadiusStyle.circleBorder70,
                          ),
                          child: Container(
                            height: getSize(
                              140,
                            ),
                            width: getSize(
                              140,
                            ),
                            padding: getPadding(
                              left: 39,
                              top: 24,
                              right: 39,
                              bottom: 24,
                            ),
                            decoration: AppDecoration.fill11.copyWith(
                              borderRadius: BorderRadiusStyle.circleBorder70,
                            ),
                            child: Stack(
                              children: [
                                CustomImageView(
                                  imagePath: ImageConstant.imgImage,
                                  height: getVerticalSize(
                                    91,
                                  ),
                                  width: getHorizontalSize(
                                    60,
                                  ),
                                  alignment: Alignment.center,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: Padding(
                        padding: getPadding(
                          top: 22,
                          right: 24,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            CustomIconButton(
                              height: 52,
                              width: 52,
                              padding: getPadding(
                                all: 12,
                              ),
                              decoration: IconButtonStyleHelper.fillWhiteA70001,
                              child: CustomImageView(
                                svgPath: ImageConstant.imgSearchIndigo60001,
                              ),
                            ),
                            CustomIconButton(
                              height: 52,
                              width: 52,
                              margin: getMargin(
                                left: 20,
                              ),
                              padding: getPadding(
                                all: 12,
                              ),
                              decoration: IconButtonStyleHelper.fillWhiteA70001,
                              child: CustomImageView(
                                svgPath:
                                    ImageConstant.imgIconlycurvednotification,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Padding(
                        padding: getPadding(
                          left: 26,
                          top: 34,
                          right: 36,
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding: getPadding(
                                left: 4,
                              ),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: getPadding(
                                      top: 119,
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        CustomImageView(
                                          imagePath: ImageConstant
                                              .imgTypemuseumcomponentmaps28x28,
                                          height: getSize(
                                            28,
                                          ),
                                          width: getSize(
                                            28,
                                          ),
                                          margin: getMargin(
                                            left: 17,
                                          ),
                                        ),
                                        Padding(
                                          padding: getPadding(
                                            top: 35,
                                          ),
                                          child: Text(
                                            "lbl_ffordd_owain".tr,
                                            overflow: TextOverflow.ellipsis,
                                            textAlign: TextAlign.left,
                                            style: CustomTextStyles
                                                .bodySmallGray700_1
                                                .copyWith(
                                              letterSpacing: getHorizontalSize(
                                                0.2,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: getPadding(
                                        left: 5,
                                        bottom: 21,
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Card(
                                                clipBehavior: Clip.antiAlias,
                                                elevation: 0,
                                                margin: getMargin(
                                                  bottom: 93,
                                                ),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadiusStyle
                                                          .circleBorder19,
                                                ),
                                                child: Container(
                                                  height: getSize(
                                                    40,
                                                  ),
                                                  width: getSize(
                                                    40,
                                                  ),
                                                  padding: getPadding(
                                                    all: 6,
                                                  ),
                                                  decoration: AppDecoration
                                                      .gradientnameredA20001opacity025nameredA100opacity025
                                                      .copyWith(
                                                    borderRadius:
                                                        BorderRadiusStyle
                                                            .circleBorder19,
                                                  ),
                                                  child: Stack(
                                                    children: [
                                                      CustomIconButton(
                                                        height: 28,
                                                        width: 28,
                                                        padding: getPadding(
                                                          all: 6,
                                                        ),
                                                        alignment:
                                                            Alignment.center,
                                                        child: CustomImageView(
                                                          svgPath: ImageConstant
                                                              .imgFrameWhiteA70001,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                height: getVerticalSize(
                                                  119,
                                                ),
                                                width: getHorizontalSize(
                                                  207,
                                                ),
                                                margin: getMargin(
                                                  left: 33,
                                                  top: 13,
                                                ),
                                                child: Stack(
                                                  alignment:
                                                      Alignment.bottomRight,
                                                  children: [
                                                    Align(
                                                      alignment:
                                                          Alignment.topLeft,
                                                      child: Text(
                                                        "lbl_bondhouse_lane".tr,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        textAlign:
                                                            TextAlign.left,
                                                        style: CustomTextStyles
                                                            .bodySmallGray700_1
                                                            .copyWith(
                                                          letterSpacing:
                                                              getHorizontalSize(
                                                            0.2,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    CustomElevatedButton(
                                                      text:
                                                          "lbl_san_manolia".tr,
                                                      leftIcon: Container(
                                                        margin: getMargin(
                                                          right: 8,
                                                        ),
                                                        child: CustomImageView(
                                                          svgPath: ImageConstant
                                                              .imgLocationWhiteA70001,
                                                        ),
                                                      ),
                                                      buttonStyle: CustomButtonStyles
                                                          .fillIndigo60001
                                                          .copyWith(
                                                              fixedSize:
                                                                  MaterialStateProperty
                                                                      .all<Size>(
                                                                          Size(
                                                        getHorizontalSize(
                                                          180,
                                                        ),
                                                        getVerticalSize(
                                                          45,
                                                        ),
                                                      ))),
                                                      buttonTextStyle:
                                                          CustomTextStyles
                                                              .titleMediumWhiteA70001,
                                                      alignment:
                                                          Alignment.bottomRight,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          Padding(
                                            padding: getPadding(
                                              top: 4,
                                              right: 56,
                                            ),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Card(
                                                  clipBehavior: Clip.antiAlias,
                                                  elevation: 0,
                                                  margin: getMargin(
                                                    bottom: 6,
                                                  ),
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadiusStyle
                                                            .circleBorder19,
                                                  ),
                                                  child: Container(
                                                    height: getSize(
                                                      40,
                                                    ),
                                                    width: getSize(
                                                      40,
                                                    ),
                                                    padding: getPadding(
                                                      all: 6,
                                                    ),
                                                    decoration: AppDecoration
                                                        .gradientnameredA20001opacity025nameredA100opacity025
                                                        .copyWith(
                                                      borderRadius:
                                                          BorderRadiusStyle
                                                              .circleBorder19,
                                                    ),
                                                    child: Stack(
                                                      children: [
                                                        CustomIconButton(
                                                          height: 28,
                                                          width: 28,
                                                          padding: getPadding(
                                                            all: 6,
                                                          ),
                                                          alignment:
                                                              Alignment.center,
                                                          child:
                                                              CustomImageView(
                                                            svgPath: ImageConstant
                                                                .imgFrameWhiteA70001,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Padding(
                                                  padding: getPadding(
                                                    left: 34,
                                                    top: 21,
                                                  ),
                                                  child: Text(
                                                    "lbl_portley_lane".tr,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    textAlign: TextAlign.left,
                                                    style: CustomTextStyles
                                                        .bodySmallGray700_1
                                                        .copyWith(
                                                      letterSpacing:
                                                          getHorizontalSize(
                                                        0.2,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Align(
                              alignment: Alignment.centerRight,
                              child: Padding(
                                padding: getPadding(
                                  left: 54,
                                  top: 51,
                                  right: 34,
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Card(
                                      clipBehavior: Clip.antiAlias,
                                      elevation: 0,
                                      margin: getMargin(
                                        top: 8,
                                      ),
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadiusStyle.circleBorder19,
                                      ),
                                      child: Container(
                                        height: getSize(
                                          40,
                                        ),
                                        width: getSize(
                                          40,
                                        ),
                                        padding: getPadding(
                                          all: 6,
                                        ),
                                        decoration: AppDecoration
                                            .gradientnameredA20001opacity025nameredA100opacity025
                                            .copyWith(
                                          borderRadius:
                                              BorderRadiusStyle.circleBorder19,
                                        ),
                                        child: Stack(
                                          children: [
                                            CustomIconButton(
                                              height: 28,
                                              width: 28,
                                              padding: getPadding(
                                                all: 6,
                                              ),
                                              alignment: Alignment.center,
                                              child: CustomImageView(
                                                svgPath: ImageConstant
                                                    .imgFrameWhiteA70001,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    CustomImageView(
                                      imagePath: ImageConstant
                                          .imgTypetrainstation28x28,
                                      height: getSize(
                                        28,
                                      ),
                                      width: getSize(
                                        28,
                                      ),
                                      margin: getMargin(
                                        bottom: 20,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: getPadding(
                                left: 4,
                                top: 16,
                              ),
                              child: Text(
                                "lbl_hopton_hollies".tr,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.left,
                                style: CustomTextStyles.bodySmallGray700_1
                                    .copyWith(
                                  letterSpacing: getHorizontalSize(
                                    0.2,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: getPadding(
                                top: 59,
                                right: 20,
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  CustomImageView(
                                    imagePath: ImageConstant
                                        .imgTypefitnesscomponentmaps28x28,
                                    height: getSize(
                                      28,
                                    ),
                                    width: getSize(
                                      28,
                                    ),
                                    margin: getMargin(
                                      bottom: 37,
                                    ),
                                  ),
                                  Card(
                                    clipBehavior: Clip.antiAlias,
                                    elevation: 0,
                                    margin: getMargin(
                                      top: 25,
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadiusStyle.circleBorder19,
                                    ),
                                    child: Container(
                                      height: getSize(
                                        40,
                                      ),
                                      width: getSize(
                                        40,
                                      ),
                                      padding: getPadding(
                                        all: 6,
                                      ),
                                      decoration: AppDecoration
                                          .gradientnameredA20001opacity025nameredA100opacity025
                                          .copyWith(
                                        borderRadius:
                                            BorderRadiusStyle.circleBorder19,
                                      ),
                                      child: Stack(
                                        children: [
                                          CustomIconButton(
                                            height: 28,
                                            width: 28,
                                            padding: getPadding(
                                              all: 6,
                                            ),
                                            alignment: Alignment.center,
                                            child: CustomImageView(
                                              svgPath: ImageConstant
                                                  .imgFrameWhiteA70001,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Align(
                              alignment: Alignment.centerRight,
                              child: Padding(
                                padding: getPadding(
                                  left: 79,
                                  top: 83,
                                  right: 1,
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Card(
                                      clipBehavior: Clip.antiAlias,
                                      elevation: 0,
                                      margin: EdgeInsets.all(0),
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadiusStyle.circleBorder19,
                                      ),
                                      child: Container(
                                        height: getSize(
                                          40,
                                        ),
                                        width: getSize(
                                          40,
                                        ),
                                        padding: getPadding(
                                          all: 6,
                                        ),
                                        decoration: AppDecoration
                                            .gradientnameredA20001opacity025nameredA100opacity025
                                            .copyWith(
                                          borderRadius:
                                              BorderRadiusStyle.circleBorder19,
                                        ),
                                        child: Stack(
                                          children: [
                                            CustomIconButton(
                                              height: 28,
                                              width: 28,
                                              padding: getPadding(
                                                all: 6,
                                              ),
                                              alignment: Alignment.center,
                                              child: CustomImageView(
                                                svgPath: ImageConstant
                                                    .imgFrameWhiteA70001,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    CustomImageView(
                                      imagePath: ImageConstant
                                          .imgTypebankcomponentmaps28x28,
                                      height: getSize(
                                        28,
                                      ),
                                      width: getSize(
                                        28,
                                      ),
                                      margin: getMargin(
                                        top: 12,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: getPadding(
                                left: 7,
                                top: 22,
                              ),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  CustomImageView(
                                    imagePath: ImageConstant
                                        .imgTypeofficecomponentmaps28x28,
                                    height: getSize(
                                      28,
                                    ),
                                    width: getSize(
                                      28,
                                    ),
                                    margin: getMargin(
                                      top: 2,
                                      bottom: 14,
                                    ),
                                  ),
                                  Text(
                                    "lbl_broughton_woods".tr,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.left,
                                    style: CustomTextStyles.bodySmallGray700_1
                                        .copyWith(
                                      letterSpacing: getHorizontalSize(
                                        0.2,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        padding: getPadding(
                          left: 3,
                          top: 288,
                          bottom: 77,
                        ),
                        child: IntrinsicWidth(
                          child: SizedBox(
                            height: getVerticalSize(
                              517,
                            ),
                            width: getHorizontalSize(
                              518,
                            ),
                            child: Stack(
                              alignment: Alignment.bottomLeft,
                              children: [
                                BlocBuilder<LightParkingSpotSelectedOnMapsBloc,
                                    LightParkingSpotSelectedOnMapsState>(
                                  builder: (context, state) {
                                    return CarouselSlider.builder(
                                      options: CarouselOptions(
                                        height: getVerticalSize(
                                          517,
                                        ),
                                        initialPage: 0,
                                        autoPlay: true,
                                        viewportFraction: 1.0,
                                        enableInfiniteScroll: false,
                                        scrollDirection: Axis.horizontal,
                                        onPageChanged: (
                                          index,
                                          reason,
                                        ) {
                                          state.sliderIndex = index;
                                        },
                                      ),
                                      itemCount: state
                                              .lightParkingSpotSelectedOnMapsModelObj
                                              ?.sliderframeItemList
                                              .length ??
                                          0,
                                      itemBuilder: (context, index, realIndex) {
                                        SliderframeItemModel model = state
                                                .lightParkingSpotSelectedOnMapsModelObj
                                                ?.sliderframeItemList[index] ??
                                            SliderframeItemModel();
                                        return SliderframeItemWidget(
                                          model,
                                        );
                                      },
                                    );
                                  },
                                ),
                                Align(
                                  alignment: Alignment.bottomLeft,
                                  child: BlocBuilder<
                                      LightParkingSpotSelectedOnMapsBloc,
                                      LightParkingSpotSelectedOnMapsState>(
                                    builder: (context, state) {
                                      return Container(
                                        height: getVerticalSize(
                                          6,
                                        ),
                                        margin: getMargin(
                                          left: 164,
                                          bottom: 217,
                                        ),
                                        child: AnimatedSmoothIndicator(
                                          activeIndex: state.sliderIndex,
                                          count: state
                                                  .lightParkingSpotSelectedOnMapsModelObj
                                                  ?.sliderframeItemList
                                                  .length ??
                                              0,
                                          axisDirection: Axis.horizontal,
                                          effect: ScrollingDotsEffect(
                                            spacing: 11,
                                            activeDotColor:
                                                appTheme.indigo60001,
                                            dotColor: appTheme.gray30001,
                                            dotHeight: getVerticalSize(
                                              6,
                                            ),
                                            dotWidth: getHorizontalSize(
                                              6,
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: CustomBottomBar(
          onChanged: (BottomBarEnum type) {
            Navigator.pushNamed(
                navigatorKey.currentContext!, getCurrentRoute(type));
          },
        ),
      ),
    );
  }

  ///Handling route based on bottom click actions
  String getCurrentRoute(BottomBarEnum type) {
    switch (type) {
      case BottomBarEnum.Home:
        return AppRoutes.lightHomeMapDirectionVersionTwoPage;
      case BottomBarEnum.Saved:
        return AppRoutes.lightMyBookmarkPage;
      case BottomBarEnum.Booking:
        return AppRoutes.lightMyParkingReservationCompletedTabContainerPage;
      case BottomBarEnum.Profile:
        return AppRoutes.lightProfileSettingsPage;
      default:
        return "/";
    }
  }

  ///Handling page based on route
  Widget getCurrentPage(
    BuildContext context,
    String currentRoute,
  ) {
    switch (currentRoute) {
      case AppRoutes.lightHomeMapDirectionVersionTwoPage:
        return LightHomeMapDirectionVersionTwoPage.builder(context);
      case AppRoutes.lightMyBookmarkPage:
        return LightMyBookmarkPage.builder(context);
      case AppRoutes.lightMyParkingReservationCompletedTabContainerPage:
        return LightMyParkingReservationCompletedTabContainerPage.builder(
            context);
      case AppRoutes.lightProfileSettingsPage:
        return LightProfileSettingsPage.builder(context);
      default:
        return DefaultWidget();
    }
  }
}
