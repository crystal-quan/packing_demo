// ignore_for_file: must_be_immutable

part of 'light_fill_profile_filled_form_bloc.dart';

/// Represents the state of LightFillProfileFilledForm in the application.
class LightFillProfileFilledFormState extends Equatable {
  LightFillProfileFilledFormState({
    this.statusfilltypedController,
    this.statusfilltypedController1,
    this.emailController,
    this.statusfilltypepController,
    this.selectedDropDownValue,
    this.lightFillProfileFilledFormModelObj,
  });

  TextEditingController? statusfilltypedController;

  TextEditingController? statusfilltypedController1;

  TextEditingController? emailController;

  TextEditingController? statusfilltypepController;

  SelectionPopupModel? selectedDropDownValue;

  LightFillProfileFilledFormModel? lightFillProfileFilledFormModelObj;

  @override
  List<Object?> get props => [
        statusfilltypedController,
        statusfilltypedController1,
        emailController,
        statusfilltypepController,
        selectedDropDownValue,
        lightFillProfileFilledFormModelObj,
      ];
  LightFillProfileFilledFormState copyWith({
    TextEditingController? statusfilltypedController,
    TextEditingController? statusfilltypedController1,
    TextEditingController? emailController,
    TextEditingController? statusfilltypepController,
    SelectionPopupModel? selectedDropDownValue,
    LightFillProfileFilledFormModel? lightFillProfileFilledFormModelObj,
  }) {
    return LightFillProfileFilledFormState(
      statusfilltypedController:
          statusfilltypedController ?? this.statusfilltypedController,
      statusfilltypedController1:
          statusfilltypedController1 ?? this.statusfilltypedController1,
      emailController: emailController ?? this.emailController,
      statusfilltypepController:
          statusfilltypepController ?? this.statusfilltypepController,
      selectedDropDownValue:
          selectedDropDownValue ?? this.selectedDropDownValue,
      lightFillProfileFilledFormModelObj: lightFillProfileFilledFormModelObj ??
          this.lightFillProfileFilledFormModelObj,
    );
  }
}
