// ignore_for_file: must_be_immutable

part of 'light_fill_profile_filled_form_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightFillProfileFilledForm widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightFillProfileFilledFormEvent extends Equatable {}

/// Event that is dispatched when the LightFillProfileFilledForm widget is first created.
class LightFillProfileFilledFormInitialEvent
    extends LightFillProfileFilledFormEvent {
  @override
  List<Object?> get props => [];
}

///event for dropdown selection
class ChangeDropDownEvent extends LightFillProfileFilledFormEvent {
  ChangeDropDownEvent({required this.value});

  SelectionPopupModel value;

  @override
  List<Object?> get props => [
        value,
      ];
}
