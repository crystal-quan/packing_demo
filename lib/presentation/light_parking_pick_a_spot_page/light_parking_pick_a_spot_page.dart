import 'bloc/light_parking_pick_a_spot_bloc.dart';
import 'models/light_parking_pick_a_spot_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_checkbox_button.dart';
import 'package:parking_app/widgets/custom_elevated_button.dart';
import 'package:parking_app/widgets/custom_outlined_button.dart';

// ignore_for_file: must_be_immutable
class LightParkingPickASpotPage extends StatefulWidget {
  const LightParkingPickASpotPage({Key? key})
      : super(
          key: key,
        );

  @override
  LightParkingPickASpotPageState createState() =>
      LightParkingPickASpotPageState();
  static Widget builder(BuildContext context) {
    return BlocProvider<LightParkingPickASpotBloc>(
      create: (context) => LightParkingPickASpotBloc(LightParkingPickASpotState(
        lightParkingPickASpotModelObj: LightParkingPickASpotModel(),
      ))
        ..add(LightParkingPickASpotInitialEvent()),
      child: LightParkingPickASpotPage(),
    );
  }
}

class LightParkingPickASpotPageState extends State<LightParkingPickASpotPage>
    with AutomaticKeepAliveClientMixin<LightParkingPickASpotPage> {
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SizedBox(
          width: mediaQueryData.size.width,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: getPadding(
                    top: 32,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: getPadding(
                          left: 29,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: getPadding(
                                top: 144,
                                bottom: 147,
                              ),
                              child: Text(
                                "lbl_2_way_traffic".tr,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.left,
                                style: CustomTextStyles.titleMediumGray500
                                    .copyWith(
                                  letterSpacing: getHorizontalSize(
                                    12.0,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: getPadding(
                                left: 28,
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    "lbl_entry".tr,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.left,
                                    style: theme.textTheme.titleMedium,
                                  ),
                                  SizedBox(
                                    height: getVerticalSize(
                                      58,
                                    ),
                                    child: VerticalDivider(
                                      width: getHorizontalSize(
                                        2,
                                      ),
                                      thickness: getVerticalSize(
                                        2,
                                      ),
                                      color: appTheme.gray400,
                                      indent: getHorizontalSize(
                                        26,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: getVerticalSize(
                                      60,
                                    ),
                                    child: VerticalDivider(
                                      width: getHorizontalSize(
                                        2,
                                      ),
                                      thickness: getVerticalSize(
                                        2,
                                      ),
                                      color: appTheme.gray400,
                                      indent: getHorizontalSize(
                                        28,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: getVerticalSize(
                                      60,
                                    ),
                                    child: VerticalDivider(
                                      width: getHorizontalSize(
                                        2,
                                      ),
                                      thickness: getVerticalSize(
                                        2,
                                      ),
                                      color: appTheme.gray400,
                                      indent: getHorizontalSize(
                                        28,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: getVerticalSize(
                                      60,
                                    ),
                                    child: VerticalDivider(
                                      width: getHorizontalSize(
                                        2,
                                      ),
                                      thickness: getVerticalSize(
                                        2,
                                      ),
                                      color: appTheme.gray400,
                                      indent: getHorizontalSize(
                                        28,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: getVerticalSize(
                                      60,
                                    ),
                                    child: VerticalDivider(
                                      width: getHorizontalSize(
                                        2,
                                      ),
                                      thickness: getVerticalSize(
                                        2,
                                      ),
                                      color: appTheme.gray400,
                                      indent: getHorizontalSize(
                                        28,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: getVerticalSize(
                                      60,
                                    ),
                                    child: VerticalDivider(
                                      width: getHorizontalSize(
                                        2,
                                      ),
                                      thickness: getVerticalSize(
                                        2,
                                      ),
                                      color: appTheme.gray400,
                                      indent: getHorizontalSize(
                                        28,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: getVerticalSize(
                                      60,
                                    ),
                                    child: VerticalDivider(
                                      width: getHorizontalSize(
                                        2,
                                      ),
                                      thickness: getVerticalSize(
                                        2,
                                      ),
                                      color: appTheme.gray400,
                                      indent: getHorizontalSize(
                                        28,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: getVerticalSize(
                                      60,
                                    ),
                                    child: VerticalDivider(
                                      width: getHorizontalSize(
                                        2,
                                      ),
                                      thickness: getVerticalSize(
                                        2,
                                      ),
                                      color: appTheme.gray400,
                                      indent: getHorizontalSize(
                                        28,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: getVerticalSize(
                                      60,
                                    ),
                                    child: VerticalDivider(
                                      width: getHorizontalSize(
                                        2,
                                      ),
                                      thickness: getVerticalSize(
                                        2,
                                      ),
                                      color: appTheme.gray400,
                                      indent: getHorizontalSize(
                                        28,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: getPadding(
                                left: 24,
                                top: 16,
                                bottom: 17,
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: getVerticalSize(
                                      210,
                                    ),
                                    width: getHorizontalSize(
                                      260,
                                    ),
                                    child: Stack(
                                      alignment: Alignment.center,
                                      children: [
                                        Align(
                                          alignment: Alignment.topCenter,
                                          child: SizedBox(
                                            width: getHorizontalSize(
                                              260,
                                            ),
                                            child: Divider(
                                              height: getVerticalSize(
                                                1,
                                              ),
                                              thickness: getVerticalSize(
                                                1,
                                              ),
                                              color: appTheme.gray400,
                                            ),
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.center,
                                          child: SizedBox(
                                            height: getVerticalSize(
                                              210,
                                            ),
                                            child: VerticalDivider(
                                              width: getHorizontalSize(
                                                1,
                                              ),
                                              thickness: getVerticalSize(
                                                1,
                                              ),
                                              color: appTheme.gray400,
                                            ),
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.topCenter,
                                          child: Padding(
                                            padding: getPadding(
                                              top: 70,
                                            ),
                                            child: SizedBox(
                                              width: getHorizontalSize(
                                                260,
                                              ),
                                              child: Divider(
                                                height: getVerticalSize(
                                                  1,
                                                ),
                                                thickness: getVerticalSize(
                                                  1,
                                                ),
                                                color: appTheme.gray400,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.bottomCenter,
                                          child: Padding(
                                            padding: getPadding(
                                              bottom: 69,
                                            ),
                                            child: SizedBox(
                                              width: getHorizontalSize(
                                                260,
                                              ),
                                              child: Divider(
                                                height: getVerticalSize(
                                                  1,
                                                ),
                                                thickness: getVerticalSize(
                                                  1,
                                                ),
                                                color: appTheme.gray400,
                                              ),
                                            ),
                                          ),
                                        ),
                                        CustomImageView(
                                          imagePath: ImageConstant.imgImage,
                                          height: getVerticalSize(
                                            40,
                                          ),
                                          width: getHorizontalSize(
                                            84,
                                          ),
                                          alignment: Alignment.topLeft,
                                          margin: getMargin(
                                            left: 23,
                                            top: 15,
                                          ),
                                        ),
                                        CustomImageView(
                                          imagePath: ImageConstant.imgImage,
                                          height: getVerticalSize(
                                            40,
                                          ),
                                          width: getHorizontalSize(
                                            84,
                                          ),
                                          alignment: Alignment.centerRight,
                                          margin: getMargin(
                                            right: 23,
                                          ),
                                        ),
                                        CustomImageView(
                                          imagePath: ImageConstant.imgImage,
                                          height: getVerticalSize(
                                            40,
                                          ),
                                          width: getHorizontalSize(
                                            84,
                                          ),
                                          alignment: Alignment.bottomRight,
                                          margin: getMargin(
                                            right: 23,
                                            bottom: 15,
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.bottomLeft,
                                          child: BlocSelector<
                                              LightParkingPickASpotBloc,
                                              LightParkingPickASpotState,
                                              bool?>(
                                            selector: (state) =>
                                                state.isCheckbox,
                                            builder: (context, isCheckbox) {
                                              return CustomCheckboxButton(
                                                alignment: Alignment.bottomLeft,
                                                width: getHorizontalSize(
                                                  111,
                                                ),
                                                text: "lbl_a05".tr,
                                                value: isCheckbox,
                                                margin: getMargin(
                                                  left: 11,
                                                  bottom: 13,
                                                ),
                                                padding: getPadding(
                                                  left: 24,
                                                  top: 11,
                                                  right: 24,
                                                  bottom: 11,
                                                ),
                                                textStyle: CustomTextStyles
                                                    .titleMediumWhiteA70001,
                                                isRightCheck: true,
                                                onChange: (value) {
                                                  context
                                                      .read<
                                                          LightParkingPickASpotBloc>()
                                                      .add(ChangeCheckBoxEvent(
                                                          value: value));
                                                },
                                              );
                                            },
                                          ),
                                        ),
                                        CustomOutlinedButton(
                                          text: "lbl_a03".tr,
                                          margin: getMargin(
                                            left: 11,
                                          ),
                                          buttonStyle: CustomButtonStyles
                                              .outlineIndigo60001TL12
                                              .copyWith(
                                                  fixedSize:
                                                      MaterialStateProperty.all<
                                                          Size>(Size(
                                            getHorizontalSize(
                                              107,
                                            ),
                                            getVerticalSize(
                                              45,
                                            ),
                                          ))),
                                          buttonTextStyle: CustomTextStyles
                                              .titleMediumIndigo60001_1,
                                          alignment: Alignment.centerLeft,
                                        ),
                                        CustomOutlinedButton(
                                          text: "lbl_a02".tr,
                                          margin: getMargin(
                                            top: 12,
                                            right: 11,
                                          ),
                                          buttonStyle: CustomButtonStyles
                                              .outlineIndigo60001TL12
                                              .copyWith(
                                                  fixedSize:
                                                      MaterialStateProperty.all<
                                                          Size>(Size(
                                            getHorizontalSize(
                                              107,
                                            ),
                                            getVerticalSize(
                                              45,
                                            ),
                                          ))),
                                          buttonTextStyle: CustomTextStyles
                                              .titleMediumIndigo60001_1,
                                          alignment: Alignment.topRight,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Divider(
                                    height: getVerticalSize(
                                      1,
                                    ),
                                    thickness: getVerticalSize(
                                      1,
                                    ),
                                    color: appTheme.gray400,
                                  ),
                                  CustomImageView(
                                    svgPath:
                                        ImageConstant.imgAutolayoutvertical,
                                    height: getVerticalSize(
                                      1,
                                    ),
                                    width: getHorizontalSize(
                                      260,
                                    ),
                                    margin: getMargin(
                                      top: 54,
                                    ),
                                  ),
                                  Container(
                                    height: getVerticalSize(
                                      210,
                                    ),
                                    width: getHorizontalSize(
                                      260,
                                    ),
                                    margin: getMargin(
                                      top: 54,
                                    ),
                                    child: Stack(
                                      alignment: Alignment.topCenter,
                                      children: [
                                        Align(
                                          alignment: Alignment.center,
                                          child: SizedBox(
                                            height: getVerticalSize(
                                              210,
                                            ),
                                            child: VerticalDivider(
                                              width: getHorizontalSize(
                                                1,
                                              ),
                                              thickness: getVerticalSize(
                                                1,
                                              ),
                                              color: appTheme.gray400,
                                            ),
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.topCenter,
                                          child: SizedBox(
                                            width: getHorizontalSize(
                                              260,
                                            ),
                                            child: Divider(
                                              height: getVerticalSize(
                                                1,
                                              ),
                                              thickness: getVerticalSize(
                                                1,
                                              ),
                                              color: appTheme.gray400,
                                            ),
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.topCenter,
                                          child: Padding(
                                            padding: getPadding(
                                              top: 70,
                                            ),
                                            child: SizedBox(
                                              width: getHorizontalSize(
                                                260,
                                              ),
                                              child: Divider(
                                                height: getVerticalSize(
                                                  1,
                                                ),
                                                thickness: getVerticalSize(
                                                  1,
                                                ),
                                                color: appTheme.gray400,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.bottomCenter,
                                          child: Padding(
                                            padding: getPadding(
                                              bottom: 69,
                                            ),
                                            child: SizedBox(
                                              width: getHorizontalSize(
                                                260,
                                              ),
                                              child: Divider(
                                                height: getVerticalSize(
                                                  1,
                                                ),
                                                thickness: getVerticalSize(
                                                  1,
                                                ),
                                                color: appTheme.gray400,
                                              ),
                                            ),
                                          ),
                                        ),
                                        CustomImageView(
                                          imagePath: ImageConstant.imgImage,
                                          height: getVerticalSize(
                                            40,
                                          ),
                                          width: getHorizontalSize(
                                            84,
                                          ),
                                          alignment: Alignment.topLeft,
                                          margin: getMargin(
                                            left: 23,
                                            top: 15,
                                          ),
                                        ),
                                        CustomImageView(
                                          imagePath: ImageConstant.imgImage,
                                          height: getVerticalSize(
                                            40,
                                          ),
                                          width: getHorizontalSize(
                                            84,
                                          ),
                                          alignment: Alignment.centerLeft,
                                          margin: getMargin(
                                            left: 23,
                                          ),
                                        ),
                                        CustomImageView(
                                          imagePath: ImageConstant.imgImage,
                                          height: getVerticalSize(
                                            40,
                                          ),
                                          width: getHorizontalSize(
                                            84,
                                          ),
                                          alignment: Alignment.centerRight,
                                          margin: getMargin(
                                            right: 23,
                                          ),
                                        ),
                                        CustomImageView(
                                          imagePath: ImageConstant.imgImage,
                                          height: getVerticalSize(
                                            40,
                                          ),
                                          width: getHorizontalSize(
                                            84,
                                          ),
                                          alignment: Alignment.bottomRight,
                                          margin: getMargin(
                                            right: 23,
                                            bottom: 15,
                                          ),
                                        ),
                                        CustomOutlinedButton(
                                          text: "lbl_a11".tr,
                                          margin: getMargin(
                                            left: 11,
                                            bottom: 13,
                                          ),
                                          buttonStyle: CustomButtonStyles
                                              .outlineIndigoA400TL12
                                              .copyWith(
                                                  fixedSize:
                                                      MaterialStateProperty.all<
                                                          Size>(Size(
                                            getHorizontalSize(
                                              107,
                                            ),
                                            getVerticalSize(
                                              45,
                                            ),
                                          ))),
                                          buttonTextStyle: CustomTextStyles
                                              .titleMediumIndigo60001_1,
                                          alignment: Alignment.bottomLeft,
                                        ),
                                        CustomOutlinedButton(
                                          text: "lbl_a08".tr,
                                          margin: getMargin(
                                            top: 12,
                                            right: 11,
                                          ),
                                          buttonStyle: CustomButtonStyles
                                              .outlineIndigoA400TL12
                                              .copyWith(
                                                  fixedSize:
                                                      MaterialStateProperty.all<
                                                          Size>(Size(
                                            getHorizontalSize(
                                              107,
                                            ),
                                            getVerticalSize(
                                              45,
                                            ),
                                          ))),
                                          buttonTextStyle: CustomTextStyles
                                              .titleMediumIndigo60001_1,
                                          alignment: Alignment.topRight,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Divider(
                                    height: getVerticalSize(
                                      1,
                                    ),
                                    thickness: getVerticalSize(
                                      1,
                                    ),
                                    color: appTheme.gray400,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: getMargin(
                          top: 32,
                        ),
                        padding: getPadding(
                          all: 24,
                        ),
                        decoration: AppDecoration.outline9.copyWith(
                          borderRadius: BorderRadiusStyle.customBorderTL24,
                        ),
                        child: CustomElevatedButton(
                          width: getHorizontalSize(
                            380,
                          ),
                          height: getVerticalSize(
                            58,
                          ),
                          text: "lbl_continue".tr,
                          margin: getMargin(
                            bottom: 12,
                          ),
                          buttonStyle: CustomButtonStyles
                              .outlineDeeppurple100TL10
                              .copyWith(
                                  fixedSize:
                                      MaterialStateProperty.all<Size>(Size(
                            double.maxFinite,
                            getVerticalSize(
                              58,
                            ),
                          ))),
                          decoration: CustomButtonStyles
                              .outlineDeeppurple100TL10Decoration,
                          buttonTextStyle:
                              CustomTextStyles.titleMediumWhiteA7000116,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
