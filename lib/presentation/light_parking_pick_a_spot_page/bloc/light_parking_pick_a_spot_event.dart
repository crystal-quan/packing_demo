// ignore_for_file: must_be_immutable

part of 'light_parking_pick_a_spot_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightParkingPickASpot widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightParkingPickASpotEvent extends Equatable {}

/// Event that is dispatched when the LightParkingPickASpot widget is first created.
class LightParkingPickASpotInitialEvent extends LightParkingPickASpotEvent {
  @override
  List<Object?> get props => [];
}

///Event for changing checkbox
class ChangeCheckBoxEvent extends LightParkingPickASpotEvent {
  ChangeCheckBoxEvent({required this.value});

  bool value;

  @override
  List<Object?> get props => [
        value,
      ];
}
