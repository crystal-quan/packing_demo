import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:parking_app/presentation/light_parking_pick_a_spot_page/models/light_parking_pick_a_spot_model.dart';
part 'light_parking_pick_a_spot_event.dart';
part 'light_parking_pick_a_spot_state.dart';

/// A bloc that manages the state of a LightParkingPickASpot according to the event that is dispatched to it.
class LightParkingPickASpotBloc
    extends Bloc<LightParkingPickASpotEvent, LightParkingPickASpotState> {
  LightParkingPickASpotBloc(LightParkingPickASpotState initialState)
      : super(initialState) {
    on<LightParkingPickASpotInitialEvent>(_onInitialize);
    on<ChangeCheckBoxEvent>(_changeCheckBox);
  }

  _changeCheckBox(
    ChangeCheckBoxEvent event,
    Emitter<LightParkingPickASpotState> emit,
  ) {
    emit(state.copyWith(
      isCheckbox: event.value,
    ));
  }

  _onInitialize(
    LightParkingPickASpotInitialEvent event,
    Emitter<LightParkingPickASpotState> emit,
  ) async {
    emit(state.copyWith(
      isCheckbox: false,
    ));
  }
}
