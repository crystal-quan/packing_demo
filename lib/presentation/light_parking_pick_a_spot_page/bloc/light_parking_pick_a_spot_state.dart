// ignore_for_file: must_be_immutable

part of 'light_parking_pick_a_spot_bloc.dart';

/// Represents the state of LightParkingPickASpot in the application.
class LightParkingPickASpotState extends Equatable {
  LightParkingPickASpotState({
    this.isCheckbox = false,
    this.lightParkingPickASpotModelObj,
  });

  LightParkingPickASpotModel? lightParkingPickASpotModelObj;

  bool isCheckbox;

  @override
  List<Object?> get props => [
        isCheckbox,
        lightParkingPickASpotModelObj,
      ];
  LightParkingPickASpotState copyWith({
    bool? isCheckbox,
    LightParkingPickASpotModel? lightParkingPickASpotModelObj,
  }) {
    return LightParkingPickASpotState(
      isCheckbox: isCheckbox ?? this.isCheckbox,
      lightParkingPickASpotModelObj:
          lightParkingPickASpotModelObj ?? this.lightParkingPickASpotModelObj,
    );
  }
}
