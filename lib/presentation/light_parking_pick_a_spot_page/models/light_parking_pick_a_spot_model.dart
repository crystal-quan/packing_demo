// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_parking_pick_a_spot_page],
/// and is typically used to hold data that is passed between different parts of the application.
class LightParkingPickASpotModel extends Equatable {LightParkingPickASpotModel copyWith() { return LightParkingPickASpotModel(
); } 
@override List<Object?> get props => [];
 }
