// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_parking_payment_method_selected_two_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightParkingPaymentMethodSelectedTwoModel extends Equatable {LightParkingPaymentMethodSelectedTwoModel copyWith() { return LightParkingPaymentMethodSelectedTwoModel(
); } 
@override List<Object?> get props => [];
 }
