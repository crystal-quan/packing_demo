// ignore_for_file: must_be_immutable

part of 'light_parking_payment_method_selected_two_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightParkingPaymentMethodSelectedTwo widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightParkingPaymentMethodSelectedTwoEvent extends Equatable {}

/// Event that is dispatched when the LightParkingPaymentMethodSelectedTwo widget is first created.
class LightParkingPaymentMethodSelectedTwoInitialEvent
    extends LightParkingPaymentMethodSelectedTwoEvent {
  @override
  List<Object?> get props => [];
}

///Event for changing radio button
class ChangeRadioButtonEvent extends LightParkingPaymentMethodSelectedTwoEvent {
  ChangeRadioButtonEvent({required this.value});

  String value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing radio button
class ChangeRadioButton1Event
    extends LightParkingPaymentMethodSelectedTwoEvent {
  ChangeRadioButton1Event({required this.value});

  String value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing radio button
class ChangeRadioButton2Event
    extends LightParkingPaymentMethodSelectedTwoEvent {
  ChangeRadioButton2Event({required this.value});

  String value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing radio button
class ChangeRadioButton3Event
    extends LightParkingPaymentMethodSelectedTwoEvent {
  ChangeRadioButton3Event({required this.value});

  String value;

  @override
  List<Object?> get props => [
        value,
      ];
}
