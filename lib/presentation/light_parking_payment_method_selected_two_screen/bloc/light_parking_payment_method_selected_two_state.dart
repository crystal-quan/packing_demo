// ignore_for_file: must_be_immutable

part of 'light_parking_payment_method_selected_two_bloc.dart';

/// Represents the state of LightParkingPaymentMethodSelectedTwo in the application.
class LightParkingPaymentMethodSelectedTwoState extends Equatable {
  LightParkingPaymentMethodSelectedTwoState({
    this.radioGroup = "",
    this.radioGroup1 = "",
    this.radioGroup2 = "",
    this.radioGroup3 = "",
    this.lightParkingPaymentMethodSelectedTwoModelObj,
  });

  LightParkingPaymentMethodSelectedTwoModel?
      lightParkingPaymentMethodSelectedTwoModelObj;

  String radioGroup;

  String radioGroup1;

  String radioGroup2;

  String radioGroup3;

  @override
  List<Object?> get props => [
        radioGroup,
        radioGroup1,
        radioGroup2,
        radioGroup3,
        lightParkingPaymentMethodSelectedTwoModelObj,
      ];
  LightParkingPaymentMethodSelectedTwoState copyWith({
    String? radioGroup,
    String? radioGroup1,
    String? radioGroup2,
    String? radioGroup3,
    LightParkingPaymentMethodSelectedTwoModel?
        lightParkingPaymentMethodSelectedTwoModelObj,
  }) {
    return LightParkingPaymentMethodSelectedTwoState(
      radioGroup: radioGroup ?? this.radioGroup,
      radioGroup1: radioGroup1 ?? this.radioGroup1,
      radioGroup2: radioGroup2 ?? this.radioGroup2,
      radioGroup3: radioGroup3 ?? this.radioGroup3,
      lightParkingPaymentMethodSelectedTwoModelObj:
          lightParkingPaymentMethodSelectedTwoModelObj ??
              this.lightParkingPaymentMethodSelectedTwoModelObj,
    );
  }
}
