// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_create_new_password_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightCreateNewPasswordModel extends Equatable {LightCreateNewPasswordModel copyWith() { return LightCreateNewPasswordModel(
); } 
@override List<Object?> get props => [];
 }
