// ignore_for_file: must_be_immutable

part of 'light_create_new_password_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightCreateNewPassword widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightCreateNewPasswordEvent extends Equatable {}

/// Event that is dispatched when the LightCreateNewPassword widget is first created.
class LightCreateNewPasswordInitialEvent extends LightCreateNewPasswordEvent {
  @override
  List<Object?> get props => [];
}

///Event for changing password visibility
class ChangePasswordVisibilityEvent extends LightCreateNewPasswordEvent {
  ChangePasswordVisibilityEvent({required this.value});

  bool value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing password visibility
class ChangePasswordVisibilityEvent1 extends LightCreateNewPasswordEvent {
  ChangePasswordVisibilityEvent1({required this.value});

  bool value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing checkbox
class ChangeCheckBoxEvent extends LightCreateNewPasswordEvent {
  ChangeCheckBoxEvent({required this.value});

  bool value;

  @override
  List<Object?> get props => [
        value,
      ];
}
