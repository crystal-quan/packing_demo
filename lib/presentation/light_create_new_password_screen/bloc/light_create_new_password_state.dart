// ignore_for_file: must_be_immutable

part of 'light_create_new_password_bloc.dart';

/// Represents the state of LightCreateNewPassword in the application.
class LightCreateNewPasswordState extends Equatable {
  LightCreateNewPasswordState({
    this.grouptwentyeighController,
    this.grouptwentynineController,
    this.isShowPassword = true,
    this.isShowPassword1 = true,
    this.isCheckbox = false,
    this.lightCreateNewPasswordModelObj,
  });

  TextEditingController? grouptwentyeighController;

  TextEditingController? grouptwentynineController;

  LightCreateNewPasswordModel? lightCreateNewPasswordModelObj;

  bool isShowPassword;

  bool isShowPassword1;

  bool isCheckbox;

  @override
  List<Object?> get props => [
        grouptwentyeighController,
        grouptwentynineController,
        isShowPassword,
        isShowPassword1,
        isCheckbox,
        lightCreateNewPasswordModelObj,
      ];
  LightCreateNewPasswordState copyWith({
    TextEditingController? grouptwentyeighController,
    TextEditingController? grouptwentynineController,
    bool? isShowPassword,
    bool? isShowPassword1,
    bool? isCheckbox,
    LightCreateNewPasswordModel? lightCreateNewPasswordModelObj,
  }) {
    return LightCreateNewPasswordState(
      grouptwentyeighController:
          grouptwentyeighController ?? this.grouptwentyeighController,
      grouptwentynineController:
          grouptwentynineController ?? this.grouptwentynineController,
      isShowPassword: isShowPassword ?? this.isShowPassword,
      isShowPassword1: isShowPassword1 ?? this.isShowPassword1,
      isCheckbox: isCheckbox ?? this.isCheckbox,
      lightCreateNewPasswordModelObj:
          lightCreateNewPasswordModelObj ?? this.lightCreateNewPasswordModelObj,
    );
  }
}
