import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:parking_app/presentation/light_parking_timer_screen/models/light_parking_timer_model.dart';part 'light_parking_timer_event.dart';part 'light_parking_timer_state.dart';/// A bloc that manages the state of a LightParkingTimer according to the event that is dispatched to it.
class LightParkingTimerBloc extends Bloc<LightParkingTimerEvent, LightParkingTimerState> {LightParkingTimerBloc(LightParkingTimerState initialState) : super(initialState) { on<LightParkingTimerInitialEvent>(_onInitialize); }

_onInitialize(LightParkingTimerInitialEvent event, Emitter<LightParkingTimerState> emit, ) async  {  } 
 }
