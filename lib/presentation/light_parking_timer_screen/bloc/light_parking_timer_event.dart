// ignore_for_file: must_be_immutable

part of 'light_parking_timer_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightParkingTimer widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightParkingTimerEvent extends Equatable {}

/// Event that is dispatched when the LightParkingTimer widget is first created.
class LightParkingTimerInitialEvent extends LightParkingTimerEvent {
  @override
  List<Object?> get props => [];
}
