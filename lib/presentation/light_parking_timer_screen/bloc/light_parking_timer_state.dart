// ignore_for_file: must_be_immutable

part of 'light_parking_timer_bloc.dart';

/// Represents the state of LightParkingTimer in the application.
class LightParkingTimerState extends Equatable {
  LightParkingTimerState({this.lightParkingTimerModelObj});

  LightParkingTimerModel? lightParkingTimerModelObj;

  @override
  List<Object?> get props => [
        lightParkingTimerModelObj,
      ];
  LightParkingTimerState copyWith(
      {LightParkingTimerModel? lightParkingTimerModelObj}) {
    return LightParkingTimerState(
      lightParkingTimerModelObj:
          lightParkingTimerModelObj ?? this.lightParkingTimerModelObj,
    );
  }
}
