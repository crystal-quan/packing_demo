// ignore_for_file: must_be_immutable

part of 'light_parking_lot_navigation_version_two_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightParkingLotNavigationVersionTwo widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightParkingLotNavigationVersionTwoEvent extends Equatable {}

/// Event that is dispatched when the LightParkingLotNavigationVersionTwo widget is first created.
class LightParkingLotNavigationVersionTwoInitialEvent
    extends LightParkingLotNavigationVersionTwoEvent {
  @override
  List<Object?> get props => [];
}
