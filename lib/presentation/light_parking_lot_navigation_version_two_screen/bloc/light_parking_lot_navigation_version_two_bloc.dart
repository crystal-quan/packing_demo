import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:parking_app/presentation/light_parking_lot_navigation_version_two_screen/models/light_parking_lot_navigation_version_two_model.dart';
part 'light_parking_lot_navigation_version_two_event.dart';
part 'light_parking_lot_navigation_version_two_state.dart';

/// A bloc that manages the state of a LightParkingLotNavigationVersionTwo according to the event that is dispatched to it.
class LightParkingLotNavigationVersionTwoBloc extends Bloc<
    LightParkingLotNavigationVersionTwoEvent,
    LightParkingLotNavigationVersionTwoState> {
  LightParkingLotNavigationVersionTwoBloc(
      LightParkingLotNavigationVersionTwoState initialState)
      : super(initialState) {
    on<LightParkingLotNavigationVersionTwoInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightParkingLotNavigationVersionTwoInitialEvent event,
    Emitter<LightParkingLotNavigationVersionTwoState> emit,
  ) async {}
}
