// ignore_for_file: must_be_immutable

part of 'light_parking_lot_navigation_version_two_bloc.dart';

/// Represents the state of LightParkingLotNavigationVersionTwo in the application.
class LightParkingLotNavigationVersionTwoState extends Equatable {
  LightParkingLotNavigationVersionTwoState(
      {this.lightParkingLotNavigationVersionTwoModelObj});

  LightParkingLotNavigationVersionTwoModel?
      lightParkingLotNavigationVersionTwoModelObj;

  @override
  List<Object?> get props => [
        lightParkingLotNavigationVersionTwoModelObj,
      ];
  LightParkingLotNavigationVersionTwoState copyWith(
      {LightParkingLotNavigationVersionTwoModel?
          lightParkingLotNavigationVersionTwoModelObj}) {
    return LightParkingLotNavigationVersionTwoState(
      lightParkingLotNavigationVersionTwoModelObj:
          lightParkingLotNavigationVersionTwoModelObj ??
              this.lightParkingLotNavigationVersionTwoModelObj,
    );
  }
}
