import 'bloc/light_parking_lot_navigation_version_two_bloc.dart';
import 'models/light_parking_lot_navigation_version_two_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_floating_button.dart';
import 'package:parking_app/widgets/custom_icon_button.dart';

class LightParkingLotNavigationVersionTwoScreen extends StatelessWidget {
  const LightParkingLotNavigationVersionTwoScreen({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LightParkingLotNavigationVersionTwoBloc>(
      create: (context) => LightParkingLotNavigationVersionTwoBloc(
          LightParkingLotNavigationVersionTwoState(
        lightParkingLotNavigationVersionTwoModelObj:
            LightParkingLotNavigationVersionTwoModel(),
      ))
        ..add(LightParkingLotNavigationVersionTwoInitialEvent()),
      child: LightParkingLotNavigationVersionTwoScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return BlocBuilder<LightParkingLotNavigationVersionTwoBloc,
        LightParkingLotNavigationVersionTwoState>(
      builder: (context, state) {
        return SafeArea(
          child: Scaffold(
            backgroundColor: appTheme.whiteA70001,
            body: SizedBox(
              height: mediaQueryData.size.height,
              width: double.maxFinite,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  CustomImageView(
                    imagePath: ImageConstant.imgVectorGray5001,
                    height: getVerticalSize(
                      882,
                    ),
                    width: getHorizontalSize(
                      428,
                    ),
                    alignment: Alignment.center,
                  ),
                  SizedBox(
                    height: mediaQueryData.size.height,
                    width: double.maxFinite,
                    child: Stack(
                      alignment: Alignment.bottomRight,
                      children: [
                        Align(
                          alignment: Alignment.center,
                          child: Container(
                            height: mediaQueryData.size.height,
                            width: double.maxFinite,
                            decoration: BoxDecoration(
                              color: appTheme.gray5002,
                            ),
                          ),
                        ),
                        CustomIconButton(
                          height: 52,
                          width: 52,
                          margin: getMargin(
                            right: 24,
                            bottom: 120,
                          ),
                          padding: getPadding(
                            all: 12,
                          ),
                          decoration: IconButtonStyleHelper.fillIndigo60001TL26,
                          alignment: Alignment.bottomRight,
                          child: CustomImageView(
                            svgPath: ImageConstant.imgFrameWhiteA7000152x52,
                          ),
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Padding(
                            padding: getPadding(
                              left: 26,
                              top: 34,
                              right: 37,
                            ),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: getPadding(
                                    left: 4,
                                  ),
                                  child: Row(
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Align(
                                            alignment: Alignment.center,
                                            child: Card(
                                              clipBehavior: Clip.antiAlias,
                                              elevation: 0,
                                              margin: EdgeInsets.all(0),
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadiusStyle
                                                    .circleBorder19,
                                              ),
                                              child: Container(
                                                height: getSize(
                                                  40,
                                                ),
                                                width: getSize(
                                                  40,
                                                ),
                                                padding: getPadding(
                                                  all: 6,
                                                ),
                                                decoration: AppDecoration
                                                    .gradientnameredA20001opacity025nameredA100opacity025
                                                    .copyWith(
                                                  borderRadius:
                                                      BorderRadiusStyle
                                                          .circleBorder19,
                                                ),
                                                child: Stack(
                                                  children: [
                                                    CustomIconButton(
                                                      height: 28,
                                                      width: 28,
                                                      padding: getPadding(
                                                        all: 6,
                                                      ),
                                                      alignment:
                                                          Alignment.center,
                                                      child: CustomImageView(
                                                        svgPath: ImageConstant
                                                            .imgFrameWhiteA70001,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerRight,
                                            child: Padding(
                                              padding: getPadding(
                                                top: 79,
                                              ),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  CustomImageView(
                                                    svgPath: ImageConstant
                                                        .imgTypemuseumcomponentmaps,
                                                    height: getSize(
                                                      28,
                                                    ),
                                                    width: getSize(
                                                      28,
                                                    ),
                                                    margin: getMargin(
                                                      bottom: 30,
                                                    ),
                                                  ),
                                                  Card(
                                                    clipBehavior:
                                                        Clip.antiAlias,
                                                    elevation: 0,
                                                    margin: getMargin(
                                                      top: 18,
                                                    ),
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadiusStyle
                                                              .circleBorder19,
                                                    ),
                                                    child: Container(
                                                      height: getSize(
                                                        40,
                                                      ),
                                                      width: getSize(
                                                        40,
                                                      ),
                                                      padding: getPadding(
                                                        all: 6,
                                                      ),
                                                      decoration: AppDecoration
                                                          .gradientnameredA20001opacity025nameredA100opacity025
                                                          .copyWith(
                                                        borderRadius:
                                                            BorderRadiusStyle
                                                                .circleBorder19,
                                                      ),
                                                      child: Stack(
                                                        children: [
                                                          CustomIconButton(
                                                            height: 28,
                                                            width: 28,
                                                            padding: getPadding(
                                                              all: 6,
                                                            ),
                                                            alignment: Alignment
                                                                .center,
                                                            child:
                                                                CustomImageView(
                                                              svgPath: ImageConstant
                                                                  .imgFrameWhiteA70001,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: getPadding(
                                              top: 5,
                                            ),
                                            child: Text(
                                              "lbl_ffordd_owain".tr,
                                              overflow: TextOverflow.ellipsis,
                                              textAlign: TextAlign.left,
                                              style: CustomTextStyles
                                                  .bodySmallGray700_1
                                                  .copyWith(
                                                letterSpacing:
                                                    getHorizontalSize(
                                                  0.2,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: getPadding(
                                          left: 34,
                                          top: 158,
                                          bottom: 21,
                                        ),
                                        child: Text(
                                          "lbl_portley_lane".tr,
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.left,
                                          style: CustomTextStyles
                                              .bodySmallGray700_1
                                              .copyWith(
                                            letterSpacing: getHorizontalSize(
                                              0.2,
                                            ),
                                          ),
                                        ),
                                      ),
                                      CustomImageView(
                                        svgPath: ImageConstant
                                            .imgTypehospitalcomponentmaps,
                                        height: getSize(
                                          28,
                                        ),
                                        width: getSize(
                                          28,
                                        ),
                                        margin: getMargin(
                                          top: 91,
                                          bottom: 86,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding: getPadding(
                                      left: 54,
                                      top: 51,
                                      right: 33,
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Card(
                                          clipBehavior: Clip.antiAlias,
                                          elevation: 0,
                                          margin: getMargin(
                                            top: 8,
                                          ),
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadiusStyle
                                                .circleBorder19,
                                          ),
                                          child: Container(
                                            height: getSize(
                                              40,
                                            ),
                                            width: getSize(
                                              40,
                                            ),
                                            padding: getPadding(
                                              all: 6,
                                            ),
                                            decoration: AppDecoration
                                                .gradientnameredA20001opacity025nameredA100opacity025
                                                .copyWith(
                                              borderRadius: BorderRadiusStyle
                                                  .circleBorder19,
                                            ),
                                            child: Stack(
                                              children: [
                                                CustomIconButton(
                                                  height: 28,
                                                  width: 28,
                                                  padding: getPadding(
                                                    all: 6,
                                                  ),
                                                  alignment: Alignment.center,
                                                  child: CustomImageView(
                                                    svgPath: ImageConstant
                                                        .imgFrameWhiteA70001,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        CustomImageView(
                                          svgPath:
                                              ImageConstant.imgTypetrainstation,
                                          height: getSize(
                                            28,
                                          ),
                                          width: getSize(
                                            28,
                                          ),
                                          margin: getMargin(
                                            bottom: 20,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: getPadding(
                                    left: 4,
                                    top: 16,
                                  ),
                                  child: Text(
                                    "lbl_hopton_hollies".tr,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.left,
                                    style: CustomTextStyles.bodySmallGray700_1
                                        .copyWith(
                                      letterSpacing: getHorizontalSize(
                                        0.2,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: getPadding(
                                    top: 59,
                                    right: 19,
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      CustomImageView(
                                        svgPath: ImageConstant
                                            .imgTypefitnesscomponentmaps,
                                        height: getSize(
                                          28,
                                        ),
                                        width: getSize(
                                          28,
                                        ),
                                        margin: getMargin(
                                          bottom: 37,
                                        ),
                                      ),
                                      Card(
                                        clipBehavior: Clip.antiAlias,
                                        elevation: 0,
                                        margin: getMargin(
                                          top: 25,
                                        ),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadiusStyle.circleBorder19,
                                        ),
                                        child: Container(
                                          height: getSize(
                                            40,
                                          ),
                                          width: getSize(
                                            40,
                                          ),
                                          padding: getPadding(
                                            all: 6,
                                          ),
                                          decoration: AppDecoration
                                              .gradientnameredA20001opacity025nameredA100opacity025
                                              .copyWith(
                                            borderRadius: BorderRadiusStyle
                                                .circleBorder19,
                                          ),
                                          child: Stack(
                                            children: [
                                              CustomIconButton(
                                                height: 28,
                                                width: 28,
                                                padding: getPadding(
                                                  all: 6,
                                                ),
                                                alignment: Alignment.center,
                                                child: CustomImageView(
                                                  svgPath: ImageConstant
                                                      .imgFrameWhiteA70001,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding: getPadding(
                                      left: 79,
                                      top: 83,
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Card(
                                          clipBehavior: Clip.antiAlias,
                                          elevation: 0,
                                          margin: EdgeInsets.all(0),
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadiusStyle
                                                .circleBorder19,
                                          ),
                                          child: Container(
                                            height: getSize(
                                              40,
                                            ),
                                            width: getSize(
                                              40,
                                            ),
                                            padding: getPadding(
                                              all: 6,
                                            ),
                                            decoration: AppDecoration
                                                .gradientnameredA20001opacity025nameredA100opacity025
                                                .copyWith(
                                              borderRadius: BorderRadiusStyle
                                                  .circleBorder19,
                                            ),
                                            child: Stack(
                                              children: [
                                                CustomIconButton(
                                                  height: 28,
                                                  width: 28,
                                                  padding: getPadding(
                                                    all: 6,
                                                  ),
                                                  alignment: Alignment.center,
                                                  child: CustomImageView(
                                                    svgPath: ImageConstant
                                                        .imgFrameWhiteA70001,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        CustomImageView(
                                          imagePath: ImageConstant
                                              .imgTypebankcomponentmaps28x28,
                                          height: getSize(
                                            28,
                                          ),
                                          width: getSize(
                                            28,
                                          ),
                                          margin: getMargin(
                                            top: 12,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: getPadding(
                                    left: 7,
                                    top: 22,
                                  ),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      CustomImageView(
                                        svgPath: ImageConstant
                                            .imgTypeofficecomponentmaps,
                                        height: getSize(
                                          28,
                                        ),
                                        width: getSize(
                                          28,
                                        ),
                                        margin: getMargin(
                                          top: 2,
                                          bottom: 14,
                                        ),
                                      ),
                                      Text(
                                        "lbl_broughton_woods".tr,
                                        overflow: TextOverflow.ellipsis,
                                        textAlign: TextAlign.left,
                                        style: CustomTextStyles
                                            .bodySmallGray700_1
                                            .copyWith(
                                          letterSpacing: getHorizontalSize(
                                            0.2,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        CustomImageView(
                          imagePath: ImageConstant.imgVector454x132,
                          height: getVerticalSize(
                            600,
                          ),
                          width: getHorizontalSize(
                            139,
                          ),
                          radius: BorderRadius.circular(
                            getHorizontalSize(
                              20,
                            ),
                          ),
                          alignment: Alignment.topCenter,
                        ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Container(
                            margin: getMargin(
                              right: 22,
                              bottom: 146,
                            ),
                            padding: getPadding(
                              all: 80,
                            ),
                            decoration: AppDecoration.fill13.copyWith(
                              borderRadius: BorderRadiusStyle.circleBorder150,
                            ),
                            child: Card(
                              clipBehavior: Clip.antiAlias,
                              elevation: 0,
                              margin: EdgeInsets.all(0),
                              color: appTheme.indigoA400.withOpacity(0.39),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadiusStyle.circleBorder70,
                              ),
                              child: Container(
                                height: getSize(
                                  140,
                                ),
                                width: getSize(
                                  140,
                                ),
                                padding: getPadding(
                                  left: 39,
                                  top: 24,
                                  right: 39,
                                  bottom: 24,
                                ),
                                decoration: AppDecoration.fill11.copyWith(
                                  borderRadius:
                                      BorderRadiusStyle.circleBorder70,
                                ),
                                child: Stack(
                                  children: [
                                    CustomImageView(
                                      imagePath: ImageConstant.imgImage,
                                      height: getVerticalSize(
                                        91,
                                      ),
                                      width: getHorizontalSize(
                                        60,
                                      ),
                                      alignment: Alignment.center,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            margin: getMargin(
                              left: 24,
                              top: 22,
                              right: 24,
                              bottom: 784,
                            ),
                            padding: getPadding(
                              left: 16,
                              top: 12,
                              right: 16,
                              bottom: 12,
                            ),
                            decoration: AppDecoration
                                .gradientnameprimarynameindigoA100
                                .copyWith(
                              borderRadius: BorderRadiusStyle.roundedBorder10,
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                CustomIconButton(
                                  height: 52,
                                  width: 52,
                                  padding: getPadding(
                                    all: 12,
                                  ),
                                  decoration:
                                      IconButtonStyleHelper.fillWhiteA70001,
                                  child: CustomImageView(
                                    svgPath: ImageConstant.imgPlayIndigo60001,
                                  ),
                                ),
                                Padding(
                                  padding: getPadding(
                                    left: 16,
                                    top: 2,
                                    bottom: 4,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        "msg_to_trantow_courts".tr,
                                        overflow: TextOverflow.ellipsis,
                                        textAlign: TextAlign.left,
                                        style: CustomTextStyles
                                            .titleMediumWhiteA70001,
                                      ),
                                      Padding(
                                        padding: getPadding(
                                          top: 5,
                                        ),
                                        child: Text(
                                          "lbl_500_m".tr,
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.left,
                                          style: CustomTextStyles
                                              .titleSmallGray10001
                                              .copyWith(
                                            letterSpacing: getHorizontalSize(
                                              0.2,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            bottomNavigationBar: Container(
              margin: getMargin(
                left: 24,
                right: 24,
                bottom: 16,
              ),
              decoration: AppDecoration.fill7,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CustomIconButton(
                    height: 64,
                    width: 64,
                    padding: getPadding(
                      all: 16,
                    ),
                    decoration: IconButtonStyleHelper.fillWhiteA70001TL32,
                    child: CustomImageView(
                      svgPath: ImageConstant.imgLocation,
                    ),
                  ),
                  Padding(
                    padding: getPadding(
                      left: 16,
                      top: 8,
                      bottom: 7,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "msg_san_manolia_parking".tr,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.left,
                          style: CustomTextStyles.titleLargeWhiteA70001,
                        ),
                        Padding(
                          padding: getPadding(
                            top: 7,
                          ),
                          child: Text(
                            "msg_9569_trantow_courts3".tr,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.left,
                            style:
                                CustomTextStyles.titleSmallGray10001.copyWith(
                              letterSpacing: getHorizontalSize(
                                0.2,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            floatingActionButton: CustomFloatingButton(
              height: 52,
              width: 52,
              decoration:
                  FloatingButtonStyleHelper.gradientnameredA20001nameredA100,
              child: CustomImageView(
                svgPath: ImageConstant.imgFrame52x52,
                height: getVerticalSize(
                  26.0,
                ),
                width: getHorizontalSize(
                  26.0,
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
