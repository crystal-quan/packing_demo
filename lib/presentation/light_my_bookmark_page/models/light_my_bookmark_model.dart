// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'autolayoutverti1_item_model.dart';/// This class defines the variables used in the [light_my_bookmark_page],
/// and is typically used to hold data that is passed between different parts of the application.
class LightMyBookmarkModel extends Equatable {LightMyBookmarkModel({this.autolayoutverti1ItemList = const []});

List<Autolayoutverti1ItemModel> autolayoutverti1ItemList;

LightMyBookmarkModel copyWith({List<Autolayoutverti1ItemModel>? autolayoutverti1ItemList}) { return LightMyBookmarkModel(
autolayoutverti1ItemList : autolayoutverti1ItemList ?? this.autolayoutverti1ItemList,
); } 
@override List<Object?> get props => [autolayoutverti1ItemList];
 }
