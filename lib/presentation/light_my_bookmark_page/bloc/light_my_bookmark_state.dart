// ignore_for_file: must_be_immutable

part of 'light_my_bookmark_bloc.dart';

/// Represents the state of LightMyBookmark in the application.
class LightMyBookmarkState extends Equatable {
  LightMyBookmarkState({
    this.searchController,
    this.lightMyBookmarkModelObj,
  });

  TextEditingController? searchController;

  LightMyBookmarkModel? lightMyBookmarkModelObj;

  @override
  List<Object?> get props => [
        searchController,
        lightMyBookmarkModelObj,
      ];
  LightMyBookmarkState copyWith({
    TextEditingController? searchController,
    LightMyBookmarkModel? lightMyBookmarkModelObj,
  }) {
    return LightMyBookmarkState(
      searchController: searchController ?? this.searchController,
      lightMyBookmarkModelObj:
          lightMyBookmarkModelObj ?? this.lightMyBookmarkModelObj,
    );
  }
}
