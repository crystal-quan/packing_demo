import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/autolayoutverti1_item_model.dart';
import 'package:parking_app/presentation/light_my_bookmark_page/models/light_my_bookmark_model.dart';
part 'light_my_bookmark_event.dart';
part 'light_my_bookmark_state.dart';

/// A bloc that manages the state of a LightMyBookmark according to the event that is dispatched to it.
class LightMyBookmarkBloc
    extends Bloc<LightMyBookmarkEvent, LightMyBookmarkState> {
  LightMyBookmarkBloc(LightMyBookmarkState initialState) : super(initialState) {
    on<LightMyBookmarkInitialEvent>(_onInitialize);
  }

  List<Autolayoutverti1ItemModel> fillAutolayoutverti1ItemList() {
    return List.generate(5, (index) => Autolayoutverti1ItemModel());
  }

  _onInitialize(
    LightMyBookmarkInitialEvent event,
    Emitter<LightMyBookmarkState> emit,
  ) async {
    emit(state.copyWith(
      searchController: TextEditingController(),
    ));
    emit(state.copyWith(
        lightMyBookmarkModelObj: state.lightMyBookmarkModelObj?.copyWith(
      autolayoutverti1ItemList: fillAutolayoutverti1ItemList(),
    )));
  }
}
