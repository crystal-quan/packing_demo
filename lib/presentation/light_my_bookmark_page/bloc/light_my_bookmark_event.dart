// ignore_for_file: must_be_immutable

part of 'light_my_bookmark_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightMyBookmark widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightMyBookmarkEvent extends Equatable {}

/// Event that is dispatched when the LightMyBookmark widget is first created.
class LightMyBookmarkInitialEvent extends LightMyBookmarkEvent {
  @override
  List<Object?> get props => [];
}
