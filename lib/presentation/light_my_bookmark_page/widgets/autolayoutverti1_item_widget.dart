import '../models/autolayoutverti1_item_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

// ignore: must_be_immutable
class Autolayoutverti1ItemWidget extends StatelessWidget {
  Autolayoutverti1ItemWidget(
    this.autolayoutverti1ItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  Autolayoutverti1ItemModel autolayoutverti1ItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: getPadding(
        all: 16,
      ),
      decoration: AppDecoration.outline4.copyWith(
        borderRadius: BorderRadiusStyle.roundedBorder10,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CustomImageView(
            imagePath: ImageConstant.imgRectangle,
            height: getSize(
              70,
            ),
            width: getSize(
              70,
            ),
            radius: BorderRadius.circular(
              getHorizontalSize(
                10,
              ),
            ),
          ),
          Padding(
            padding: getPadding(
              left: 24,
              top: 8,
              bottom: 9,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  autolayoutverti1ItemModelObj.streetnameTxt,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.left,
                  style: theme.textTheme.titleLarge,
                ),
                Padding(
                  padding: getPadding(
                    top: 11,
                  ),
                  child: Text(
                    autolayoutverti1ItemModelObj.streetnumberTxt,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.left,
                    style: CustomTextStyles.titleSmallGray700.copyWith(
                      letterSpacing: getHorizontalSize(
                        0.2,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
          CustomImageView(
            svgPath: ImageConstant.imgBookmarkIndigo6000128x28,
            height: getSize(
              28,
            ),
            width: getSize(
              28,
            ),
            margin: getMargin(
              top: 21,
              right: 4,
              bottom: 21,
            ),
          ),
        ],
      ),
    );
  }
}
