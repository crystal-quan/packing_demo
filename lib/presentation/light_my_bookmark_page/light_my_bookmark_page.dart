import '../light_my_bookmark_page/widgets/autolayoutverti1_item_widget.dart';
import 'bloc/light_my_bookmark_bloc.dart';
import 'models/autolayoutverti1_item_model.dart';
import 'models/light_my_bookmark_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/app_bar/appbar_image.dart';
import 'package:parking_app/widgets/app_bar/appbar_title.dart';
import 'package:parking_app/widgets/app_bar/custom_app_bar.dart';
import 'package:parking_app/widgets/custom_search_view.dart';

// ignore_for_file: must_be_immutable
class LightMyBookmarkPage extends StatelessWidget {
  const LightMyBookmarkPage({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LightMyBookmarkBloc>(
      create: (context) => LightMyBookmarkBloc(LightMyBookmarkState(
        lightMyBookmarkModelObj: LightMyBookmarkModel(),
      ))
        ..add(LightMyBookmarkInitialEvent()),
      child: LightMyBookmarkPage(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.gray5003,
        resizeToAvoidBottomInset: false,
        appBar: CustomAppBar(
          height: getVerticalSize(
            77,
          ),
          title: AppbarTitle(
            text: "lbl_my_bookmark".tr,
            margin: getMargin(
              left: 72,
            ),
          ),
          actions: [
            AppbarImage(
              height: getSize(
                28,
              ),
              width: getSize(
                28,
              ),
              svgPath: ImageConstant.imgClock,
              margin: getMargin(
                left: 24,
                top: 12,
                right: 24,
                bottom: 15,
              ),
            ),
          ],
        ),
        body: Container(
          width: double.maxFinite,
          padding: getPadding(
            left: 24,
            top: 18,
            right: 24,
            bottom: 18,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              BlocSelector<LightMyBookmarkBloc, LightMyBookmarkState,
                  TextEditingController?>(
                selector: (state) => state.searchController,
                builder: (context, searchController) {
                  return CustomSearchView(
                    controller: searchController,
                    hintText: "lbl_search".tr,
                    hintStyle: CustomTextStyles.bodyMediumGray400,
                    textStyle: CustomTextStyles.bodyMediumGray400,
                    prefix: Container(
                      margin: getMargin(
                        left: 20,
                        top: 18,
                        right: 12,
                        bottom: 18,
                      ),
                      child: CustomImageView(
                        svgPath: ImageConstant.imgSearchGray400,
                      ),
                    ),
                    prefixConstraints: BoxConstraints(
                      maxHeight: getVerticalSize(
                        56,
                      ),
                    ),
                    suffix: Padding(
                      padding: EdgeInsets.only(
                        right: getHorizontalSize(
                          15,
                        ),
                      ),
                      child: IconButton(
                        onPressed: () {
                          searchController!.clear();
                        },
                        icon: Icon(
                          Icons.clear,
                          color: Colors.grey.shade600,
                        ),
                      ),
                    ),
                    filled: true,
                    fillColor: appTheme.whiteA70001,
                    contentPadding: getPadding(
                      top: 19,
                      right: 30,
                      bottom: 19,
                    ),
                  );
                },
              ),
              Expanded(
                child: Padding(
                  padding: getPadding(
                    top: 24,
                  ),
                  child: BlocSelector<LightMyBookmarkBloc, LightMyBookmarkState,
                      LightMyBookmarkModel?>(
                    selector: (state) => state.lightMyBookmarkModelObj,
                    builder: (context, lightMyBookmarkModelObj) {
                      return ListView.separated(
                        physics: BouncingScrollPhysics(),
                        shrinkWrap: true,
                        separatorBuilder: (
                          context,
                          index,
                        ) {
                          return SizedBox(
                            height: getVerticalSize(
                              20,
                            ),
                          );
                        },
                        itemCount: lightMyBookmarkModelObj
                                ?.autolayoutverti1ItemList.length ??
                            0,
                        itemBuilder: (context, index) {
                          Autolayoutverti1ItemModel model =
                              lightMyBookmarkModelObj
                                      ?.autolayoutverti1ItemList[index] ??
                                  Autolayoutverti1ItemModel();
                          return Autolayoutverti1ItemWidget(
                            model,
                          );
                        },
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
