import 'bloc/light_settings_logout_modal_bloc.dart';
import 'models/light_settings_logout_modal_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';
import 'package:parking_app/widgets/custom_elevated_button.dart';

// ignore_for_file: must_be_immutable
class LightSettingsLogoutModalBottomsheet extends StatelessWidget {
  const LightSettingsLogoutModalBottomsheet({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LightSettingsLogoutModalBloc>(
      create: (context) =>
          LightSettingsLogoutModalBloc(LightSettingsLogoutModalState(
        lightSettingsLogoutModalModelObj: LightSettingsLogoutModalModel(),
      ))
            ..add(LightSettingsLogoutModalInitialEvent()),
      child: LightSettingsLogoutModalBottomsheet(),
    );
  }

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    return SingleChildScrollView(
      child: SingleChildScrollView(
        child: Container(
          padding: getPadding(
            left: 24,
            top: 8,
            right: 24,
            bottom: 8,
          ),
          decoration: AppDecoration.outline5.copyWith(
            borderRadius: BorderRadiusStyle.customBorderTL40,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              CustomImageView(
                svgPath: ImageConstant.imgFrameGray30001,
                height: getVerticalSize(
                  3,
                ),
                width: getHorizontalSize(
                  38,
                ),
              ),
              Padding(
                padding: getPadding(
                  top: 27,
                ),
                child: Text(
                  "lbl_logout".tr,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.left,
                  style: CustomTextStyles.headlineSmallRedA200,
                ),
              ),
              Padding(
                padding: getPadding(
                  top: 20,
                ),
                child: Divider(
                  height: getVerticalSize(
                    1,
                  ),
                  thickness: getVerticalSize(
                    1,
                  ),
                  color: appTheme.gray200,
                ),
              ),
              Padding(
                padding: getPadding(
                  top: 25,
                ),
                child: Text(
                  "msg_are_you_sure_you2".tr,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.left,
                  style: CustomTextStyles.titleLargeGray800,
                ),
              ),
              CustomElevatedButton(
                width: getHorizontalSize(
                  380,
                ),
                height: getVerticalSize(
                  58,
                ),
                text: "lbl_yes_logout".tr,
                margin: getMargin(
                  top: 22,
                ),
                buttonStyle: CustomButtonStyles
                    .gradientnameprimarynameindigoA100
                    .copyWith(
                        fixedSize: MaterialStateProperty.all<Size>(Size(
                  double.maxFinite,
                  getVerticalSize(
                    58,
                  ),
                ))),
                decoration: CustomButtonStyles
                    .gradientnameprimarynameindigoA100Decoration,
                buttonTextStyle: CustomTextStyles.titleMediumWhiteA7000116,
              ),
              CustomElevatedButton(
                text: "lbl_cancel".tr,
                margin: getMargin(
                  top: 12,
                  bottom: 40,
                ),
                buttonStyle: CustomButtonStyles.fillBlue50TL10.copyWith(
                    fixedSize: MaterialStateProperty.all<Size>(Size(
                  double.maxFinite,
                  getVerticalSize(
                    58,
                  ),
                ))),
                buttonTextStyle: CustomTextStyles.titleMediumIndigo60001,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
