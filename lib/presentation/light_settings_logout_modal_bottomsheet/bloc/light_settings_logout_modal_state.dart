// ignore_for_file: must_be_immutable

part of 'light_settings_logout_modal_bloc.dart';

/// Represents the state of LightSettingsLogoutModal in the application.
class LightSettingsLogoutModalState extends Equatable {
  LightSettingsLogoutModalState({this.lightSettingsLogoutModalModelObj});

  LightSettingsLogoutModalModel? lightSettingsLogoutModalModelObj;

  @override
  List<Object?> get props => [
        lightSettingsLogoutModalModelObj,
      ];
  LightSettingsLogoutModalState copyWith(
      {LightSettingsLogoutModalModel? lightSettingsLogoutModalModelObj}) {
    return LightSettingsLogoutModalState(
      lightSettingsLogoutModalModelObj: lightSettingsLogoutModalModelObj ??
          this.lightSettingsLogoutModalModelObj,
    );
  }
}
