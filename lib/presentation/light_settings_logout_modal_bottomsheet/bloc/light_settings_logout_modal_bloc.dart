import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:parking_app/presentation/light_settings_logout_modal_bottomsheet/models/light_settings_logout_modal_model.dart';
part 'light_settings_logout_modal_event.dart';
part 'light_settings_logout_modal_state.dart';

/// A bloc that manages the state of a LightSettingsLogoutModal according to the event that is dispatched to it.
class LightSettingsLogoutModalBloc
    extends Bloc<LightSettingsLogoutModalEvent, LightSettingsLogoutModalState> {
  LightSettingsLogoutModalBloc(LightSettingsLogoutModalState initialState)
      : super(initialState) {
    on<LightSettingsLogoutModalInitialEvent>(_onInitialize);
  }

  _onInitialize(
    LightSettingsLogoutModalInitialEvent event,
    Emitter<LightSettingsLogoutModalState> emit,
  ) async {}
}
