// ignore_for_file: must_be_immutable

part of 'light_settings_logout_modal_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightSettingsLogoutModal widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightSettingsLogoutModalEvent extends Equatable {}

/// Event that is dispatched when the LightSettingsLogoutModal widget is first created.
class LightSettingsLogoutModalInitialEvent
    extends LightSettingsLogoutModalEvent {
  @override
  List<Object?> get props => [];
}
