// ignore_for_file: must_be_immutable

part of 'light_my_parking_reservation_cancel_parking_refund_metho_bloc.dart';

/// Represents the state of LightMyParkingReservationCancelParkingRefundMetho in the application.
class LightMyParkingReservationCancelParkingRefundMethoState extends Equatable {
  LightMyParkingReservationCancelParkingRefundMethoState({
    this.radioGroup = "",
    this.radioGroup1 = "",
    this.radioGroup2 = "",
    this.radioGroup3 = "",
    this.lightMyParkingReservationCancelParkingRefundMethoModelObj,
  });

  LightMyParkingReservationCancelParkingRefundMethoModel?
      lightMyParkingReservationCancelParkingRefundMethoModelObj;

  String radioGroup;

  String radioGroup1;

  String radioGroup2;

  String radioGroup3;

  @override
  List<Object?> get props => [
        radioGroup,
        radioGroup1,
        radioGroup2,
        radioGroup3,
        lightMyParkingReservationCancelParkingRefundMethoModelObj,
      ];
  LightMyParkingReservationCancelParkingRefundMethoState copyWith({
    String? radioGroup,
    String? radioGroup1,
    String? radioGroup2,
    String? radioGroup3,
    LightMyParkingReservationCancelParkingRefundMethoModel?
        lightMyParkingReservationCancelParkingRefundMethoModelObj,
  }) {
    return LightMyParkingReservationCancelParkingRefundMethoState(
      radioGroup: radioGroup ?? this.radioGroup,
      radioGroup1: radioGroup1 ?? this.radioGroup1,
      radioGroup2: radioGroup2 ?? this.radioGroup2,
      radioGroup3: radioGroup3 ?? this.radioGroup3,
      lightMyParkingReservationCancelParkingRefundMethoModelObj:
          lightMyParkingReservationCancelParkingRefundMethoModelObj ??
              this.lightMyParkingReservationCancelParkingRefundMethoModelObj,
    );
  }
}
