// ignore_for_file: must_be_immutable

part of 'light_my_parking_reservation_cancel_parking_refund_metho_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightMyParkingReservationCancelParkingRefundMetho widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightMyParkingReservationCancelParkingRefundMethoEvent
    extends Equatable {}

/// Event that is dispatched when the LightMyParkingReservationCancelParkingRefundMetho widget is first created.
class LightMyParkingReservationCancelParkingRefundMethoInitialEvent
    extends LightMyParkingReservationCancelParkingRefundMethoEvent {
  @override
  List<Object?> get props => [];
}

///Event for changing radio button
class ChangeRadioButtonEvent
    extends LightMyParkingReservationCancelParkingRefundMethoEvent {
  ChangeRadioButtonEvent({required this.value});

  String value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing radio button
class ChangeRadioButton1Event
    extends LightMyParkingReservationCancelParkingRefundMethoEvent {
  ChangeRadioButton1Event({required this.value});

  String value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing radio button
class ChangeRadioButton2Event
    extends LightMyParkingReservationCancelParkingRefundMethoEvent {
  ChangeRadioButton2Event({required this.value});

  String value;

  @override
  List<Object?> get props => [
        value,
      ];
}

///Event for changing radio button
class ChangeRadioButton3Event
    extends LightMyParkingReservationCancelParkingRefundMethoEvent {
  ChangeRadioButton3Event({required this.value});

  String value;

  @override
  List<Object?> get props => [
        value,
      ];
}
