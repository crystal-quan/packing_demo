// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_my_parking_reservation_cancel_parking_refund_metho_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightMyParkingReservationCancelParkingRefundMethoModel extends Equatable {LightMyParkingReservationCancelParkingRefundMethoModel copyWith() { return LightMyParkingReservationCancelParkingRefundMethoModel(
); } 
@override List<Object?> get props => [];
 }
