import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/slidername1_item_model.dart';
import 'package:parking_app/presentation/light_carousel_three_screen/models/light_carousel_three_model.dart';
part 'light_carousel_three_event.dart';
part 'light_carousel_three_state.dart';

/// A bloc that manages the state of a LightCarouselThree according to the event that is dispatched to it.
class LightCarouselThreeBloc
    extends Bloc<LightCarouselThreeEvent, LightCarouselThreeState> {
  LightCarouselThreeBloc(LightCarouselThreeState initialState)
      : super(initialState) {
    on<LightCarouselThreeInitialEvent>(_onInitialize);
  }

  List<Slidername1ItemModel> fillSlidername1ItemList() {
    return List.generate(1, (index) => Slidername1ItemModel());
  }

  _onInitialize(
    LightCarouselThreeInitialEvent event,
    Emitter<LightCarouselThreeState> emit,
  ) async {
    emit(state.copyWith(
      sliderIndex: 0,
    ));
    emit(state.copyWith(
        lightCarouselThreeModelObj: state.lightCarouselThreeModelObj?.copyWith(
      slidername1ItemList: fillSlidername1ItemList(),
    )));
  }
}
