// ignore_for_file: must_be_immutable

part of 'light_carousel_three_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///LightCarouselThree widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class LightCarouselThreeEvent extends Equatable {}

/// Event that is dispatched when the LightCarouselThree widget is first created.
class LightCarouselThreeInitialEvent extends LightCarouselThreeEvent {
  @override
  List<Object?> get props => [];
}
