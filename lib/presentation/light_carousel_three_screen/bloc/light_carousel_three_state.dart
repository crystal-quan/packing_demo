// ignore_for_file: must_be_immutable

part of 'light_carousel_three_bloc.dart';

/// Represents the state of LightCarouselThree in the application.
class LightCarouselThreeState extends Equatable {
  LightCarouselThreeState({
    this.sliderIndex = 0,
    this.lightCarouselThreeModelObj,
  });

  LightCarouselThreeModel? lightCarouselThreeModelObj;

  int sliderIndex;

  @override
  List<Object?> get props => [
        sliderIndex,
        lightCarouselThreeModelObj,
      ];
  LightCarouselThreeState copyWith({
    int? sliderIndex,
    LightCarouselThreeModel? lightCarouselThreeModelObj,
  }) {
    return LightCarouselThreeState(
      sliderIndex: sliderIndex ?? this.sliderIndex,
      lightCarouselThreeModelObj:
          lightCarouselThreeModelObj ?? this.lightCarouselThreeModelObj,
    );
  }
}
