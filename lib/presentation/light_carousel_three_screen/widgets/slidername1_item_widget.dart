import '../models/slidername1_item_model.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

// ignore: must_be_immutable
class Slidername1ItemWidget extends StatelessWidget {
  Slidername1ItemWidget(
    this.slidername1ItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  Slidername1ItemModel slidername1ItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: getHorizontalSize(
            343,
          ),
          margin: getMargin(
            left: 13,
            right: 11,
          ),
          child: RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: "lbl_extend".tr,
                  style: theme.textTheme.headlineLarge,
                ),
                TextSpan(
                  text: "msg_parking_time_as".tr,
                  style: CustomTextStyles.headlineLargeIndigo60001,
                ),
              ],
            ),
            textAlign: TextAlign.center,
          ),
        ),
        Padding(
          padding: getPadding(
            top: 18,
          ),
          child: Text(
            "msg_lorem_ipsum_dolor".tr,
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
            style: CustomTextStyles.bodyLargeGray700.copyWith(
              letterSpacing: getHorizontalSize(
                0.2,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
