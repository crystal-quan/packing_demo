// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'slidername1_item_model.dart';/// This class defines the variables used in the [light_carousel_three_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LightCarouselThreeModel extends Equatable {LightCarouselThreeModel({this.slidername1ItemList = const []});

List<Slidername1ItemModel> slidername1ItemList;

LightCarouselThreeModel copyWith({List<Slidername1ItemModel>? slidername1ItemList}) { return LightCarouselThreeModel(
slidername1ItemList : slidername1ItemList ?? this.slidername1ItemList,
); } 
@override List<Object?> get props => [slidername1ItemList];
 }
