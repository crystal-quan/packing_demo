import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

class CustomIconButton extends StatelessWidget {
  CustomIconButton({
    Key? key,
    this.alignment,
    this.margin,
    this.width,
    this.height,
    this.padding,
    this.decoration,
    this.child,
    this.onTap,
  }) : super(
          key: key,
        );

  final Alignment? alignment;

  final EdgeInsetsGeometry? margin;

  final double? width;

  final double? height;

  final EdgeInsetsGeometry? padding;

  final BoxDecoration? decoration;

  final Widget? child;

  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return alignment != null
        ? Align(
            alignment: alignment ?? Alignment.center,
            child: iconButtonWidget,
          )
        : iconButtonWidget;
  }

  Widget get iconButtonWidget => Padding(
        padding: margin ?? EdgeInsets.zero,
        child: IconButton(
          visualDensity: VisualDensity(
            vertical: -4,
            horizontal: -4,
          ),
          iconSize: getSize(height ?? 0),
          padding: EdgeInsets.all(0),
          icon: Container(
            alignment: Alignment.center,
            width: getSize(width ?? 0),
            height: getSize(height ?? 0),
            padding: padding ?? EdgeInsets.zero,
            decoration: decoration ??
                BoxDecoration(
                  borderRadius: BorderRadius.circular(
                    getHorizontalSize(
                      14.00,
                    ),
                  ),
                  gradient: LinearGradient(
                    begin: Alignment(
                      1.0,
                      1,
                    ),
                    end: Alignment(
                      0.0,
                      0,
                    ),
                    colors: [
                      appTheme.redA20001,
                      appTheme.redA100,
                    ],
                  ),
                ),
            child: child,
          ),
          onPressed: onTap,
        ),
      );
}

/// Extension on [CustomIconButton] to facilitate inclusion of all types of border style etc
extension IconButtonStyleHelper on CustomIconButton {
  static BoxDecoration get fillWhiteA70001 => BoxDecoration(
        color: appTheme.whiteA70001,
        borderRadius: BorderRadius.circular(
          getHorizontalSize(
            26.00,
          ),
        ),
      );
  static BoxDecoration get fillIndigo60001 => BoxDecoration(
        color: appTheme.indigo60001,
        borderRadius: BorderRadius.circular(
          getHorizontalSize(
            14.00,
          ),
        ),
      );
  static BoxDecoration get gradientnameindigoA400nameindigoA10002 =>
      BoxDecoration(
        borderRadius: BorderRadius.circular(
          getHorizontalSize(
            26.00,
          ),
        ),
        gradient: LinearGradient(
          begin: Alignment(
            1.0,
            1,
          ),
          end: Alignment(
            0.0,
            0,
          ),
          colors: [
            appTheme.indigoA400,
            appTheme.indigoA10002,
          ],
        ),
      );
  static BoxDecoration get fillIndigo60001TL26 => BoxDecoration(
        color: appTheme.indigo60001,
        borderRadius: BorderRadius.circular(
          getHorizontalSize(
            26.00,
          ),
        ),
      );
  static BoxDecoration get fillWhiteA70001TL32 => BoxDecoration(
        color: appTheme.whiteA70001,
        borderRadius: BorderRadius.circular(
          getHorizontalSize(
            32.00,
          ),
        ),
      );
}
