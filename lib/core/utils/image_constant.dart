class ImageConstant {
  static String imgFrameBlack900 = 'assets/images/img_frame_black_900.svg';

  static String imgLockGray900 = 'assets/images/img_lock_gray_900.svg';

  static String imgBookmarkIndigo60001 =
      'assets/images/img_bookmark_indigo_600_01.svg';

  static String imgVectorIndigo5001 =
      'assets/images/img_vector_indigo_50_01.png';

  static String imgImage27x44 = 'assets/images/img_image_27x44.png';

  static String imgRefresh = 'assets/images/img_refresh.svg';

  static String imgVector = 'assets/images/img_vector.svg';

  static String imgArrowleft = 'assets/images/img_arrowleft.svg';

  static String imgRectangle140 = 'assets/images/img_rectangle140.png';

  static String imgRefreshRedA200 = 'assets/images/img_refresh_red_a200.svg';

  static String imgCurrentlocationpana =
      'assets/images/img_currentlocationpana.png';

  static String imgCheckmarkWhiteA70001 =
      'assets/images/img_checkmark_white_a700_01.svg';

  static String imgIconlycurvedwallet =
      'assets/images/img_iconlycurvedwallet.svg';

  static String imgVector416x220 = 'assets/images/img_vector_416x220.png';

  static String imgForgotpasswordrafiki =
      'assets/images/img_forgotpasswordrafiki.svg';

  static String imgRectangle12 = 'assets/images/img_rectangle_12.png';

  static String imgRectangle6 = 'assets/images/img_rectangle_6.png';

  static String imgTypemuseumcomponentmaps28x28 =
      'assets/images/img_typemuseumcomponentmaps_28x28.png';

  static String imgFrameBlack90032x32 =
      'assets/images/img_frame_black_900_32x32.svg';

  static String imgClose = 'assets/images/img_close.svg';

  static String imgCircle = 'assets/images/img_circle.png';

  static String imgCircle417x365 = 'assets/images/img_circle_417x365.png';

  static String imgArrowup = 'assets/images/img_arrowup.svg';

  static String imgIconlyboldchat = 'assets/images/img_iconlyboldchat.png';

  static String imgImage5 = 'assets/images/img_image5.png';

  static String imgLocationWhiteA70001 =
      'assets/images/img_location_white_a700_01.svg';

  static String imgLock = 'assets/images/img_lock.svg';

  static String imgMenu = 'assets/images/img_menu.svg';

  static String imgRectangle1 = 'assets/images/img_rectangle_1.png';

  static String imgRectangle100x100 = 'assets/images/img_rectangle_100x100.png';

  static String imgClock = 'assets/images/img_clock.svg';

  static String imgIconlylightscan = 'assets/images/img_iconlylightscan.svg';

  static String imgFacebook = 'assets/images/img_facebook.svg';

  static String img43lightparking = 'assets/images/img_43lightparking.png';

  static String imgRectangle35 = 'assets/images/img_rectangle35.png';

  static String imgRectangle865x422 = 'assets/images/img_rectangle_865x422.png';

  static String imgArrowrightIndigo60001 =
      'assets/images/img_arrowright_indigo_600_01.svg';

  static String imgGroup7 = 'assets/images/img_group7.png';

  static String imgCardrivingbro = 'assets/images/img_cardrivingbro.png';

  static String imgUserIndigo60001 = 'assets/images/img_user_indigo_600_01.svg';

  static String imgFrameGray30001 = 'assets/images/img_frame_gray_300_01.svg';

  static String imgArrowright = 'assets/images/img_arrowright.svg';

  static String imgEllipse = 'assets/images/img_ellipse.png';

  static String imgRectangle9 = 'assets/images/img_rectangle_9.png';

  static String imgImage4228x428 = 'assets/images/img_image4_228x428.png';

  static String imgUserGray800 = 'assets/images/img_user_gray_800.svg';

  static String imgSearchIndigo60001 =
      'assets/images/img_search_indigo_600_01.svg';

  static String imgSearchGray900 = 'assets/images/img_search_gray_900.svg';

  static String imgFrameErrorcontainer =
      'assets/images/img_frame_errorcontainer.svg';

  static String imgIconlycurvedshow = 'assets/images/img_iconlycurvedshow.svg';

  static String imgParkingbro1 = 'assets/images/img_parkingbro1.svg';

  static String imgFrame = 'assets/images/img_frame.svg';

  static String imgUser = 'assets/images/img_user.svg';

  static String imgTypebankcomponentmaps28x28 =
      'assets/images/img_typebankcomponentmaps_28x28.png';

  static String imgIconlyCurvedPlus =
      'assets/images/img_iconly_curved_plus.svg';

  static String imgVectorDeepPurple100 =
      'assets/images/img_vector_deep_purple_100.png';

  static String imgDashboard = 'assets/images/img_dashboard.svg';

  static String imgClockWhiteA70001 =
      'assets/images/img_clock_white_a700_01.svg';

  static String imgFavoriteGray900 = 'assets/images/img_favorite_gray_900.svg';

  static String imgArrowleftGray900 =
      'assets/images/img_arrowleft_gray_900.svg';

  static String imgMobileloginbroWhiteA70001 =
      'assets/images/img_mobileloginbro_white_a700_01.svg';

  static String imgRectangle2 = 'assets/images/img_rectangle_2.png';

  static String imgLockIndigo60001 = 'assets/images/img_lock_indigo_600_01.svg';

  static String imgRectangle15 = 'assets/images/img_rectangle15.png';

  static String imgMaskgroup131x231 = 'assets/images/img_maskgroup_131x231.png';

  static String imgCheckmarkGray800 =
      'assets/images/img_checkmark_gray_800.svg';

  static String imgIconlyCurvedMessage =
      'assets/images/img_iconly_curved_message.svg';

  static String imgVectorDeepPurple100882x428 =
      'assets/images/img_vector_deep_purple_100_882x428.png';

  static String imgGroup = 'assets/images/img_group.svg';

  static String imgIconlyBoldHide = 'assets/images/img_iconly_bold_hide.svg';

  static String imgRectangle10 = 'assets/images/img_rectangle_10.png';

  static String imgHomeGray500 = 'assets/images/img_home_gray_500.svg';

  static String imgPlayIndigo60001 = 'assets/images/img_play_indigo_600_01.svg';

  static String imgBookmark = 'assets/images/img_bookmark.svg';

  static String imgRectangle34 = 'assets/images/img_rectangle34.png';

  static String imgRectangle7 = 'assets/images/img_rectangle_7.png';

  static String imgTypefitnesscomponentmaps =
      'assets/images/img_typefitnesscomponentmaps.svg';

  static String imgPlay = 'assets/images/img_play.svg';

  static String imgCardrivingbro256x256 =
      'assets/images/img_cardrivingbro_256x256.png';

  static String imgRectangle3 = 'assets/images/img_rectangle3.png';

  static String imgVectorGray5001 = 'assets/images/img_vector_gray_50_01.png';

  static String imgBookmarkIndigo6000128x28 =
      'assets/images/img_bookmark_indigo_600_01_28x28.svg';

  static String imgCheckmarkGray900 =
      'assets/images/img_checkmark_gray_900.svg';

  static String imgMenuWhiteA70001 = 'assets/images/img_menu_white_a700_01.svg';

  static String imgAutolayouthorizontalGray600 =
      'assets/images/img_autolayouthorizontal_gray_600.svg';

  static String imgIconlycurvedcalendarGray900 =
      'assets/images/img_iconlycurvedcalendar_gray_900.svg';

  static String imgTypeofficecomponentmaps =
      'assets/images/img_typeofficecomponentmaps.svg';

  static String imgTypefitnesscomponentmaps28x28 =
      'assets/images/img_typefitnesscomponentmaps_28x28.png';

  static String imgVectorIndigoA10004 =
      'assets/images/img_vector_indigo_a100_04.png';

  static String imgVectorIndigo50 = 'assets/images/img_vector_indigo_50.png';

  static String imgRectangle30 = 'assets/images/img_rectangle30.png';

  static String imgVectorBlack900 = 'assets/images/img_vector_black_900.svg';

  static String imgRectangle11 = 'assets/images/img_rectangle_11.png';

  static String imgVolume = 'assets/images/img_volume.svg';

  static String imgSearch = 'assets/images/img_search.svg';

  static String imgRectangle14 = 'assets/images/img_rectangle_14.png';

  static String imgIconlycurvednotificationGray800 =
      'assets/images/img_iconlycurvednotification_gray_800.svg';

  static String imgLockWhiteA70001 = 'assets/images/img_lock_white_a700_01.svg';

  static String imgVector454x132 = 'assets/images/img_vector_454x132.png';

  static String imgAutolayoutvertical =
      'assets/images/img_autolayoutvertical.svg';

  static String imgEdit = 'assets/images/img_edit.svg';

  static String imgRectangle70x70 = 'assets/images/img_rectangle_70x70.png';

  static String imgRectangle4 = 'assets/images/img_rectangle_4.png';

  static String imgAutolayouthorizontal =
      'assets/images/img_autolayouthorizontal.png';

  static String imgIconlycurvedcalendar =
      'assets/images/img_iconlycurvedcalendar.svg';

  static String imgLocation = 'assets/images/img_location.svg';

  static String imgRectangle13 = 'assets/images/img_rectangle13.png';

  static String imgMenuIndigo6000124x24 =
      'assets/images/img_menu_indigo_600_01_24x24.svg';

  static String imgCreditcardpana = 'assets/images/img_creditcardpana.svg';

  static String imgIconlyboldmessage =
      'assets/images/img_iconlyboldmessage.png';

  static String imgCheckmarkIndigo60001 =
      'assets/images/img_checkmark_indigo_600_01.svg';

  static String imgFrameWhiteA70001 =
      'assets/images/img_frame_white_a700_01.svg';

  static String imgEllipse140x140 = 'assets/images/img_ellipse_140x140.png';

  static String imgFavorite = 'assets/images/img_favorite.svg';

  static String imgCheckmark = 'assets/images/img_checkmark.svg';

  static String imgRectangle8 = 'assets/images/img_rectangle_8.png';

  static String imgLocationIndigoA400 =
      'assets/images/img_location_indigo_a400.svg';

  static String imgTypetrainstation = 'assets/images/img_typetrainstation.svg';

  static String imgRectangle = 'assets/images/img_rectangle.png';

  static String imgVectorIndigo60001 =
      'assets/images/img_vector_indigo_600_01.svg';

  static String imgRectangle31 = 'assets/images/img_rectangle31.png';

  static String imgOffer = 'assets/images/img_offer.svg';

  static String imgWristwatchrafiki = 'assets/images/img_wristwatchrafiki.svg';

  static String imgFrame52x52 = 'assets/images/img_frame_52x52.svg';

  static String imgIconlycurvedinfo = 'assets/images/img_iconlycurvedinfo.svg';

  static String imgTypemuseumcomponentmaps =
      'assets/images/img_typemuseumcomponentmaps.svg';

  static String imgIconlycurvednotification =
      'assets/images/img_iconlycurvednotification.svg';

  static String imgImage = 'assets/images/img_image.png';

  static String imgSearchGray90020x20 =
      'assets/images/img_search_gray_900_20x20.svg';

  static String imgRectangle5 = 'assets/images/img_rectangle_5.png';

  static String imgMobileloginbro = 'assets/images/img_mobileloginbro.svg';

  static String imgFrameWhiteA7000152x52 =
      'assets/images/img_frame_white_a700_01_52x52.svg';

  static String imgIconlyCurvedMessageGray900 =
      'assets/images/img_iconly_curved_message_gray_900.svg';

  static String imgHome = 'assets/images/img_home.svg';

  static String imgMaskGroup = 'assets/images/img_mask_group.svg';

  static String imgHellorafiki1 = 'assets/images/img_hellorafiki1.png';

  static String imgSearchGray400 = 'assets/images/img_search_gray_400.svg';

  static String imgMenuIndigo60001 = 'assets/images/img_menu_indigo_600_01.svg';

  static String imgTypebankcomponentmaps =
      'assets/images/img_typebankcomponentmaps.svg';

  static String imgTypeofficecomponentmaps28x28 =
      'assets/images/img_typeofficecomponentmaps_28x28.png';

  static String imgTypehospitalcomponentmaps =
      'assets/images/img_typehospitalcomponentmaps.svg';

  static String imgImage4 = 'assets/images/img_image4.png';

  static String imgTypetrainstation28x28 =
      'assets/images/img_typetrainstation_28x28.png';

  static String imgClockIndigo60001 =
      'assets/images/img_clock_indigo_600_01.svg';

  static String imageNotFound = 'assets/images/image_not_found.png';
}
