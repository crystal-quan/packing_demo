import 'package:flutter/material.dart';
import 'package:parking_app/presentation/frame_six_screen/frame_six_screen.dart';
import 'package:parking_app/presentation/light_splash_screen/light_splash_screen.dart';
import 'package:parking_app/presentation/light_welcome_screen/light_welcome_screen.dart';
import 'package:parking_app/presentation/light_carousel_one_screen/light_carousel_one_screen.dart';
import 'package:parking_app/presentation/light_carousel_two_screen/light_carousel_two_screen.dart';
import 'package:parking_app/presentation/light_carousel_three_screen/light_carousel_three_screen.dart';
import 'package:parking_app/presentation/light_lets_in_screen/light_lets_in_screen.dart';
import 'package:parking_app/presentation/light_sign_up_blank_form_screen/light_sign_up_blank_form_screen.dart';
import 'package:parking_app/presentation/light_sign_up_type_form_screen/light_sign_up_type_form_screen.dart';
import 'package:parking_app/presentation/light_sign_up_filled_form_screen/light_sign_up_filled_form_screen.dart';
import 'package:parking_app/presentation/light_fill_profile_blank_form_screen/light_fill_profile_blank_form_screen.dart';
import 'package:parking_app/presentation/light_fill_profile_filled_form_screen/light_fill_profile_filled_form_screen.dart';
import 'package:parking_app/presentation/light_sign_in_blank_form_screen/light_sign_in_blank_form_screen.dart';
import 'package:parking_app/presentation/light_sign_in_type_form_screen/light_sign_in_type_form_screen.dart';
import 'package:parking_app/presentation/light_sign_in_filled_form_screen/light_sign_in_filled_form_screen.dart';
import 'package:parking_app/presentation/light_forgot_password_method_screen/light_forgot_password_method_screen.dart';
import 'package:parking_app/presentation/light_forgot_password_type_otp_screen/light_forgot_password_type_otp_screen.dart';
import 'package:parking_app/presentation/light_forgot_password_filled_otp_screen/light_forgot_password_filled_otp_screen.dart';
import 'package:parking_app/presentation/light_create_new_password_screen/light_create_new_password_screen.dart';
import 'package:parking_app/presentation/light_home_map_direction_version_two_container_screen/light_home_map_direction_version_two_container_screen.dart';
import 'package:parking_app/presentation/light_home_notification_screen/light_home_notification_screen.dart';
import 'package:parking_app/presentation/light_home_search_type_keyword_screen/light_home_search_type_keyword_screen.dart';
import 'package:parking_app/presentation/light_home_search_results_screen/light_home_search_results_screen.dart';
import 'package:parking_app/presentation/light_home_map_direction_version_one_screen/light_home_map_direction_version_one_screen.dart';
import 'package:parking_app/presentation/light_home_map_direction_version_two1_screen/light_home_map_direction_version_two1_screen.dart';
import 'package:parking_app/presentation/light_parking_spot_selected_on_maps_screen/light_parking_spot_selected_on_maps_screen.dart';
import 'package:parking_app/presentation/light_parking_spot_details_screen/light_parking_spot_details_screen.dart';
import 'package:parking_app/presentation/light_parking_select_vehicle_screen/light_parking_select_vehicle_screen.dart';
import 'package:parking_app/presentation/light_parking_book_details_screen/light_parking_book_details_screen.dart';
import 'package:parking_app/presentation/light_parking_pick_a_spot_tab_container_screen/light_parking_pick_a_spot_tab_container_screen.dart';
import 'package:parking_app/presentation/light_parking_payment_method_selected_one_screen/light_parking_payment_method_selected_one_screen.dart';
import 'package:parking_app/presentation/light_parking_add_new_card_screen/light_parking_add_new_card_screen.dart';
import 'package:parking_app/presentation/light_parking_payment_method_selected_two_screen/light_parking_payment_method_selected_two_screen.dart';
import 'package:parking_app/presentation/light_parking_confirm_payment_screen/light_parking_confirm_payment_screen.dart';
import 'package:parking_app/presentation/light_parking_view_ticket_screen/light_parking_view_ticket_screen.dart';
import 'package:parking_app/presentation/mobile_login_bro_two_screen/mobile_login_bro_two_screen.dart';
import 'package:parking_app/presentation/light_parking_lot_navigation_version_one_screen/light_parking_lot_navigation_version_one_screen.dart';
import 'package:parking_app/presentation/light_parking_lot_navigation_version_two_screen/light_parking_lot_navigation_version_two_screen.dart';
import 'package:parking_app/presentation/light_arrived_at_the_parking_lot_screen/light_arrived_at_the_parking_lot_screen.dart';
import 'package:parking_app/presentation/light_scan_ticket_parking_lot_screen/light_scan_ticket_parking_lot_screen.dart';
import 'package:parking_app/presentation/light_parking_lot_screen/light_parking_lot_screen.dart';
import 'package:parking_app/presentation/light_parking_timer_screen/light_parking_timer_screen.dart';
import 'package:parking_app/presentation/light_extend_parking_time_screen/light_extend_parking_time_screen.dart';
import 'package:parking_app/presentation/light_my_parking_reservation_cancel_modal_screen/light_my_parking_reservation_cancel_modal_screen.dart';
import 'package:parking_app/presentation/light_my_parking_reservation_cancel_parking_refund_metho_screen/light_my_parking_reservation_cancel_parking_refund_metho_screen.dart';
import 'package:parking_app/presentation/light_settings_edit_profile_screen/light_settings_edit_profile_screen.dart';
import 'package:parking_app/presentation/light_settings_notification_screen/light_settings_notification_screen.dart';
import 'package:parking_app/presentation/light_settings_security_screen/light_settings_security_screen.dart';
import 'package:parking_app/presentation/app_navigation_screen/app_navigation_screen.dart';

class AppRoutes {
  static const String frameSixScreen = '/frame_six_screen';

  static const String lightSplashScreen = '/light_splash_screen';

  static const String lightWelcomeScreen = '/light_welcome_screen';

  static const String lightCarouselOneScreen = '/light_carousel_one_screen';

  static const String lightCarouselTwoScreen = '/light_carousel_two_screen';

  static const String lightCarouselThreeScreen = '/light_carousel_three_screen';

  static const String lightLetsInScreen = '/light_lets_in_screen';

  static const String lightSignUpBlankFormScreen = '/light_sign_up_blank_form_screen';

  static const String lightSignUpTypeFormScreen = '/light_sign_up_type_form_screen';

  static const String lightSignUpFilledFormScreen = '/light_sign_up_filled_form_screen';

  static const String lightFillProfileBlankFormScreen = '/light_fill_profile_blank_form_screen';

  static const String lightFillProfileFilledFormScreen = '/light_fill_profile_filled_form_screen';

  static const String lightSignInBlankFormScreen = '/light_sign_in_blank_form_screen';

  static const String lightSignInTypeFormScreen = '/light_sign_in_type_form_screen';

  static const String lightSignInFilledFormScreen = '/light_sign_in_filled_form_screen';

  static const String lightForgotPasswordMethodScreen = '/light_forgot_password_method_screen';

  static const String lightForgotPasswordTypeOtpScreen = '/light_forgot_password_type_otp_screen';

  static const String lightForgotPasswordFilledOtpScreen = '/light_forgot_password_filled_otp_screen';

  static const String lightCreateNewPasswordScreen = '/light_create_new_password_screen';

  static const String lightHomeMapDirectionVersionTwoPage = '/light_home_map_direction_version_two_page';

  static const String lightHomeMapDirectionVersionTwoContainerScreen =
      '/light_home_map_direction_version_two_container_screen';

  static const String lightHomeNotificationScreen = '/light_home_notification_screen';

  static const String lightHomeSearchTypeKeywordScreen = '/light_home_search_type_keyword_screen';

  static const String lightHomeSearchResultsScreen = '/light_home_search_results_screen';

  static const String lightHomeMapDirectionVersionOneScreen = '/light_home_map_direction_version_one_screen';

  static const String lightHomeMapDirectionVersionTwo1Screen = '/light_home_map_direction_version_two1_screen';

  static const String lightParkingSpotSelectedOnMapsScreen = '/light_parking_spot_selected_on_maps_screen';

  static const String lightParkingSpotDetailsScreen = '/light_parking_spot_details_screen';

  static const String lightParkingSelectVehicleScreen = '/light_parking_select_vehicle_screen';

  static const String lightParkingBookDetailsScreen = '/light_parking_book_details_screen';

  static const String lightParkingPickASpotPage = '/light_parking_pick_a_spot_page';

  static const String lightParkingPickASpotTabContainerScreen = '/light_parking_pick_a_spot_tab_container_screen';

  static const String lightParkingPaymentMethodSelectedOneScreen = '/light_parking_payment_method_selected_one_screen';

  static const String lightParkingAddNewCardScreen = '/light_parking_add_new_card_screen';

  static const String lightParkingPaymentMethodSelectedTwoScreen = '/light_parking_payment_method_selected_two_screen';

  static const String lightParkingConfirmPaymentScreen = '/light_parking_confirm_payment_screen';

  static const String lightParkingViewTicketScreen = '/light_parking_view_ticket_screen';

  static const String mobileLoginBroTwoScreen = '/mobile_login_bro_two_screen';

  static const String lightParkingLotNavigationVersionOneScreen = '/light_parking_lot_navigation_version_one_screen';

  static const String lightParkingLotNavigationVersionTwoScreen = '/light_parking_lot_navigation_version_two_screen';

  static const String lightArrivedAtTheParkingLotScreen = '/light_arrived_at_the_parking_lot_screen';

  static const String lightScanTicketParkingLotScreen = '/light_scan_ticket_parking_lot_screen';

  static const String lightParkingLotScreen = '/light_parking_lot_screen';

  static const String lightParkingTimerScreen = '/light_parking_timer_screen';

  static const String lightExtendParkingTimeScreen = '/light_extend_parking_time_screen';

  static const String lightMyBookmarkPage = '/light_my_bookmark_page';

  static const String lightMyParkingReservationCanceledPage = '/light_my_parking_reservation_canceled_page';

  static const String lightMyParkingReservationCompletedPage = '/light_my_parking_reservation_completed_page';

  static const String lightMyParkingReservationCompletedTabContainerPage =
      '/light_my_parking_reservation_completed_tab_container_page';

  static const String lightMyParkingReservationOngoingPage = '/light_my_parking_reservation_ongoing_page';

  static const String lightMyParkingReservationCancelModalScreen = '/light_my_parking_reservation_cancel_modal_screen';

  static const String lightMyParkingReservationCancelParkingRefundMethoScreen =
      '/light_my_parking_reservation_cancel_parking_refund_metho_screen';

  static const String lightProfileSettingsPage = '/light_profile_settings_page';

  static const String lightSettingsEditProfileScreen = '/light_settings_edit_profile_screen';

  static const String lightSettingsNotificationScreen = '/light_settings_notification_screen';

  static const String lightSettingsSecurityScreen = '/light_settings_security_screen';

  static const String appNavigationScreen = '/app_navigation_screen';

  static const String initialRoute = '/initialRoute';

  static Map<String, WidgetBuilder> get routes => {
        frameSixScreen: FrameSixScreen.builder,
        lightSplashScreen: LightSplashScreen.builder,
        lightWelcomeScreen: LightWelcomeScreen.builder,
        lightCarouselOneScreen: LightCarouselOneScreen.builder,
        lightCarouselTwoScreen: LightCarouselTwoScreen.builder,
        lightCarouselThreeScreen: LightCarouselThreeScreen.builder,
        lightLetsInScreen: LightLetsInScreen.builder,
        lightSignUpBlankFormScreen: LightSignUpBlankFormScreen.builder,
        lightSignUpTypeFormScreen: LightSignUpTypeFormScreen.builder,
        lightSignUpFilledFormScreen: LightSignUpFilledFormScreen.builder,
        lightFillProfileBlankFormScreen: LightFillProfileBlankFormScreen.builder,
        lightFillProfileFilledFormScreen: LightFillProfileFilledFormScreen.builder,
        lightSignInBlankFormScreen: LightSignInBlankFormScreen.builder,
        lightSignInTypeFormScreen: LightSignInTypeFormScreen.builder,
        lightSignInFilledFormScreen: LightSignInFilledFormScreen.builder,
        lightForgotPasswordMethodScreen: LightForgotPasswordMethodScreen.builder,
        lightForgotPasswordTypeOtpScreen: LightForgotPasswordTypeOtpScreen.builder,
        lightForgotPasswordFilledOtpScreen: LightForgotPasswordFilledOtpScreen.builder,
        lightCreateNewPasswordScreen: LightCreateNewPasswordScreen.builder,
        lightHomeMapDirectionVersionTwoContainerScreen: LightHomeMapDirectionVersionTwoContainerScreen.builder,
        lightHomeNotificationScreen: LightHomeNotificationScreen.builder,
        lightHomeSearchTypeKeywordScreen: LightHomeSearchTypeKeywordScreen.builder,
        lightHomeSearchResultsScreen: LightHomeSearchResultsScreen.builder,
        lightHomeMapDirectionVersionOneScreen: LightHomeMapDirectionVersionOneScreen.builder,
        lightHomeMapDirectionVersionTwo1Screen: LightHomeMapDirectionVersionTwo1Screen.builder,
        lightParkingSpotSelectedOnMapsScreen: LightParkingSpotSelectedOnMapsScreen.builder,
        lightParkingSpotDetailsScreen: LightParkingSpotDetailsScreen.builder,
        lightParkingSelectVehicleScreen: LightParkingSelectVehicleScreen.builder,
        lightParkingBookDetailsScreen: LightParkingBookDetailsScreen.builder,
        lightParkingPickASpotTabContainerScreen: LightParkingPickASpotTabContainerScreen.builder,
        lightParkingPaymentMethodSelectedOneScreen: LightParkingPaymentMethodSelectedOneScreen.builder,
        lightParkingAddNewCardScreen: LightParkingAddNewCardScreen.builder,
        lightParkingPaymentMethodSelectedTwoScreen: LightParkingPaymentMethodSelectedTwoScreen.builder,
        lightParkingConfirmPaymentScreen: LightParkingConfirmPaymentScreen.builder,
        lightParkingViewTicketScreen: LightParkingViewTicketScreen.builder,
        mobileLoginBroTwoScreen: MobileLoginBroTwoScreen.builder,
        lightParkingLotNavigationVersionOneScreen: LightParkingLotNavigationVersionOneScreen.builder,
        lightParkingLotNavigationVersionTwoScreen: LightParkingLotNavigationVersionTwoScreen.builder,
        lightArrivedAtTheParkingLotScreen: LightArrivedAtTheParkingLotScreen.builder,
        lightScanTicketParkingLotScreen: LightScanTicketParkingLotScreen.builder,
        lightParkingLotScreen: LightParkingLotScreen.builder,
        lightParkingTimerScreen: LightParkingTimerScreen.builder,
        lightExtendParkingTimeScreen: LightExtendParkingTimeScreen.builder,
        lightMyParkingReservationCancelModalScreen: LightMyParkingReservationCancelModalScreen.builder,
        lightMyParkingReservationCancelParkingRefundMethoScreen:
            LightMyParkingReservationCancelParkingRefundMethoScreen.builder,
        lightSettingsEditProfileScreen: LightSettingsEditProfileScreen.builder,
        lightSettingsNotificationScreen: LightSettingsNotificationScreen.builder,
        lightSettingsSecurityScreen: LightSettingsSecurityScreen.builder,
        appNavigationScreen: AppNavigationScreen.builder,
        initialRoute: LightSplashScreen.builder
      };
}
