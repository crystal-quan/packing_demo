import 'package:flutter/material.dart';
import '../core/app_export.dart';

/// A collection of pre-defined text styles for customizing text appearance,
/// categorized by different font families and weights.
/// Additionally, this class includes extensions on [TextStyle] to easily apply specific font families to text.

class CustomTextStyles {
  // Title text style
  static get titleMediumIndigo60001SemiBold =>
      theme.textTheme.titleMedium!.copyWith(
        color: appTheme.indigo60001,
        fontWeight: FontWeight.w600,
      );
  static get titleMediumSemiBold => theme.textTheme.titleMedium!.copyWith(
        fontSize: getFontSize(
          16,
        ),
        fontWeight: FontWeight.w600,
      );
  static get titleMediumIndigo60001 => theme.textTheme.titleMedium!.copyWith(
        color: appTheme.indigo60001,
        fontSize: getFontSize(
          16,
        ),
      );
  static get titleMediumIndigo60001_1 => theme.textTheme.titleMedium!.copyWith(
        color: appTheme.indigo60001,
      );
  static get titleSmallPoppinsWhiteA70001 =>
      theme.textTheme.titleSmall!.poppins.copyWith(
        color: appTheme.whiteA70001,
        fontWeight: FontWeight.w500,
      );
  static get titleMediumGray700Medium => theme.textTheme.titleMedium!.copyWith(
        color: appTheme.gray700,
        fontSize: getFontSize(
          16,
        ),
        fontWeight: FontWeight.w500,
      );
  static get titleMediumWhiteA70001SemiBold =>
      theme.textTheme.titleMedium!.copyWith(
        color: appTheme.whiteA70001,
        fontSize: getFontSize(
          16,
        ),
        fontWeight: FontWeight.w600,
      );
  static get titleMediumIndigo60001SemiBold16 =>
      theme.textTheme.titleMedium!.copyWith(
        color: appTheme.indigo60001,
        fontSize: getFontSize(
          16,
        ),
        fontWeight: FontWeight.w600,
      );
  static get titleMediumWhiteA70001 => theme.textTheme.titleMedium!.copyWith(
        color: appTheme.whiteA70001,
      );
  static get titleSmallGray700 => theme.textTheme.titleSmall!.copyWith(
        color: appTheme.gray700,
        fontWeight: FontWeight.w500,
      );
  static get titleMediumGray800 => theme.textTheme.titleMedium!.copyWith(
        color: appTheme.gray800,
      );
  static get titleSmallGray10001 => theme.textTheme.titleSmall!.copyWith(
        color: appTheme.gray10001,
        fontWeight: FontWeight.w500,
      );
  static get titleMediumGray600 => theme.textTheme.titleMedium!.copyWith(
        color: appTheme.gray600,
        fontWeight: FontWeight.w500,
      );
  static get titleSmallGray700_1 => theme.textTheme.titleSmall!.copyWith(
        color: appTheme.gray700,
      );
  static get titleMediumGray800SemiBold =>
      theme.textTheme.titleMedium!.copyWith(
        color: appTheme.gray800,
        fontWeight: FontWeight.w600,
      );
  static get titleLargeWhiteA70001 => theme.textTheme.titleLarge!.copyWith(
        color: appTheme.whiteA70001,
      );
  static get titleMediumIndigoA400SemiBold =>
      theme.textTheme.titleMedium!.copyWith(
        color: appTheme.indigoA400,
        fontSize: getFontSize(
          16,
        ),
        fontWeight: FontWeight.w600,
      );
  static get titleLargeGray800 => theme.textTheme.titleLarge!.copyWith(
        color: appTheme.gray800,
      );
  static get titleMedium16 => theme.textTheme.titleMedium!.copyWith(
        fontSize: getFontSize(
          16,
        ),
      );
  static get titleMediumIndigoA400 => theme.textTheme.titleMedium!.copyWith(
        color: appTheme.indigoA400,
        fontSize: getFontSize(
          16,
        ),
      );
  static get titleSmallIndigo60001_1 => theme.textTheme.titleSmall!.copyWith(
        color: appTheme.indigo60001,
      );
  static get titleMediumPoppinsWhiteA70001 =>
      theme.textTheme.titleMedium!.poppins.copyWith(
        color: appTheme.whiteA70001,
        fontSize: getFontSize(
          16,
        ),
        fontWeight: FontWeight.w500,
      );
  static get titleSmallIndigo60001 => theme.textTheme.titleSmall!.copyWith(
        color: appTheme.indigo60001,
        fontWeight: FontWeight.w700,
      );
  static get titleSmallGray600 => theme.textTheme.titleSmall!.copyWith(
        color: appTheme.gray600,
        fontWeight: FontWeight.w500,
      );
  static get titleSmallIndigoA400 => theme.textTheme.titleSmall!.copyWith(
        color: appTheme.indigoA400,
        fontWeight: FontWeight.w700,
      );
  static get titleSmallMedium => theme.textTheme.titleSmall!.copyWith(
        fontWeight: FontWeight.w500,
      );
  static get titleMediumRedA200 => theme.textTheme.titleMedium!.copyWith(
        color: appTheme.redA200,
        fontWeight: FontWeight.w600,
      );
  static get titleMediumGray500 => theme.textTheme.titleMedium!.copyWith(
        color: appTheme.gray500,
        fontWeight: FontWeight.w500,
      );
  static get titleMediumGray700 => theme.textTheme.titleMedium!.copyWith(
        color: appTheme.gray700,
        fontWeight: FontWeight.w600,
      );
  static get titleSmallIndigoA400_1 => theme.textTheme.titleSmall!.copyWith(
        color: appTheme.indigoA400,
      );
  static get titleMediumWhiteA7000116 => theme.textTheme.titleMedium!.copyWith(
        color: appTheme.whiteA70001,
        fontSize: getFontSize(
          16,
        ),
      );
  // Body text style
  static get bodyLargeGray800 => theme.textTheme.bodyLarge!.copyWith(
        color: appTheme.gray800,
        fontSize: getFontSize(
          18,
        ),
      );
  static get bodyMediumGray700 => theme.textTheme.bodyMedium!.copyWith(
        color: appTheme.gray700,
      );
  static get bodyMediumGray800_1 => theme.textTheme.bodyMedium!.copyWith(
        color: appTheme.gray800,
      );
  static get bodySmallPoppinsWhiteA70001 =>
      theme.textTheme.bodySmall!.poppins.copyWith(
        color: appTheme.whiteA70001,
        fontSize: getFontSize(
          8,
        ),
      );
  static get bodyLargeIndigo60001 => theme.textTheme.bodyLarge!.copyWith(
        color: appTheme.indigo60001,
      );
  static get bodyLarge18 => theme.textTheme.bodyLarge!.copyWith(
        fontSize: getFontSize(
          18,
        ),
      );
  static get bodySmallGray700 => theme.textTheme.bodySmall!.copyWith(
        color: appTheme.gray700,
        fontSize: getFontSize(
          10,
        ),
      );
  static get bodyLargeGray700 => theme.textTheme.bodyLarge!.copyWith(
        color: appTheme.gray700,
      );
  static get bodySmallGray900 => theme.textTheme.bodySmall!.copyWith(
        color: appTheme.gray900,
      );
  static get bodySmallGray700_1 => theme.textTheme.bodySmall!.copyWith(
        color: appTheme.gray700,
      );
  static get bodyMediumGray600 => theme.textTheme.bodyMedium!.copyWith(
        color: appTheme.gray600,
      );
  static get bodyMediumGray800 => theme.textTheme.bodyMedium!.copyWith(
        color: appTheme.gray800,
      );
  static get bodyMediumGray400 => theme.textTheme.bodyMedium!.copyWith(
        color: appTheme.gray400,
      );
  // Display text style
  static get displayMediumMontserratBluegray900SemiBold =>
      theme.textTheme.displayMedium!.montserrat.copyWith(
        color: appTheme.blueGray900,
        fontSize: getFontSize(
          42,
        ),
        fontWeight: FontWeight.w600,
      );
  static get displayMediumIndigo60001 =>
      theme.textTheme.displayMedium!.copyWith(
        color: appTheme.indigo60001,
        fontSize: getFontSize(
          42,
        ),
        fontWeight: FontWeight.w600,
      );
  static get displayMediumMontserratBluegray900 =>
      theme.textTheme.displayMedium!.montserrat.copyWith(
        color: appTheme.blueGray900,
        fontSize: getFontSize(
          42,
        ),
        fontWeight: FontWeight.w600,
      );
  static get displayMediumIndigoA10001 =>
      theme.textTheme.displayMedium!.copyWith(
        color: appTheme.indigoA10001,
      );
  static get displayMediumMontserratIndigo60001 =>
      theme.textTheme.displayMedium!.montserrat.copyWith(
        color: appTheme.indigo60001,
        fontSize: getFontSize(
          42,
        ),
        fontWeight: FontWeight.w600,
      );
  static get displayMediumIndigoA10003 =>
      theme.textTheme.displayMedium!.copyWith(
        color: appTheme.indigoA10003,
      );
  // Headline text style
  static get headlineSmallLatoWhiteA700 =>
      theme.textTheme.headlineSmall!.lato.copyWith(
        color: appTheme.whiteA700,
      );
  static get headlineLargeMontserratOnPrimary =>
      theme.textTheme.headlineLarge!.montserrat.copyWith(
        color: theme.colorScheme.onPrimary,
      );
  static get headlineLargeIndigo60001 =>
      theme.textTheme.headlineLarge!.copyWith(
        color: appTheme.indigo60001,
      );
  static get headlineSmallSFProDisplay =>
      theme.textTheme.headlineSmall!.sFProDisplay.copyWith(
        fontWeight: FontWeight.w500,
      );
  static get headlineLargeIndigo600 => theme.textTheme.headlineLarge!.copyWith(
        color: appTheme.indigo600,
      );
  static get headlineSmallRedA200 => theme.textTheme.headlineSmall!.copyWith(
        color: appTheme.redA200,
      );
  static get headlineSmallIndigo60001 =>
      theme.textTheme.headlineSmall!.copyWith(
        color: appTheme.indigo60001,
      );
  // Label text style
  static get labelMediumRedA200 => theme.textTheme.labelMedium!.copyWith(
        color: appTheme.redA200,
        fontWeight: FontWeight.w600,
      );
  static get labelMediumIndigo60001 => theme.textTheme.labelMedium!.copyWith(
        color: appTheme.indigo60001,
        fontWeight: FontWeight.w600,
      );
  static get labelMediumWhiteA70001 => theme.textTheme.labelMedium!.copyWith(
        color: appTheme.whiteA70001,
        fontWeight: FontWeight.w600,
      );
  static get labelLargeGray600 => theme.textTheme.labelLarge!.copyWith(
        color: appTheme.gray600,
        fontWeight: FontWeight.w500,
      );
  static get labelMediumGreen400 => theme.textTheme.labelMedium!.copyWith(
        color: appTheme.green400,
        fontWeight: FontWeight.w600,
      );
  static get labelMediumIndigo60001Bold =>
      theme.textTheme.labelMedium!.copyWith(
        color: appTheme.indigo60001,
        fontWeight: FontWeight.w700,
      );
  static get labelMediumIndigoA400 => theme.textTheme.labelMedium!.copyWith(
        color: appTheme.indigoA400,
        fontWeight: FontWeight.w600,
      );
  // Urbanist text style
  static get urbanistIndigo60001 => TextStyle(
        color: appTheme.indigo60001,
        fontSize: getFontSize(
          70,
        ),
        fontWeight: FontWeight.w900,
      ).urbanist;
}

extension on TextStyle {
  TextStyle get lato {
    return copyWith(
      fontFamily: 'Lato',
    );
  }

  TextStyle get montserrat {
    return copyWith(
      fontFamily: 'Montserrat',
    );
  }

  TextStyle get sFProDisplay {
    return copyWith(
      fontFamily: 'SF Pro Display',
    );
  }

  TextStyle get urbanist {
    return copyWith(
      fontFamily: 'Urbanist',
    );
  }

  TextStyle get poppins {
    return copyWith(
      fontFamily: 'Poppins',
    );
  }
}
