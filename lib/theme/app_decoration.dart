import 'package:flutter/material.dart';
import 'package:parking_app/core/app_export.dart';

class AppDecoration {
  static BoxDecoration get gradientnamedeeppurple100nameindigoA10004 =>
      BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(
            0.53,
            0,
          ),
          end: Alignment(
            0.5,
            1,
          ),
          colors: [
            appTheme.deepPurple100,
            appTheme.indigoA10004,
          ],
        ),
      );
  static BoxDecoration
      get gradientnameredA20001opacity025nameredA100opacity025 => BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(
                1,
                1,
              ),
              end: Alignment(
                0,
                0,
              ),
              colors: [
                appTheme.redA20001.withOpacity(0.25),
                appTheme.redA100.withOpacity(0.25),
              ],
            ),
          );
  static BoxDecoration get fill => BoxDecoration(
        color: appTheme.whiteA70001,
      );
  static BoxDecoration get fill16 => BoxDecoration(
        color: appTheme.gray5004,
      );
  static BoxDecoration get fill15 => BoxDecoration(
        color: appTheme.gray200,
      );
  static BoxDecoration get txtFill2 => BoxDecoration(
        color: appTheme.indigo60001,
      );
  static BoxDecoration get txtFill1 => BoxDecoration(
        color: appTheme.indigoA10004,
      );
  static BoxDecoration get gradientnameprimarynameindigoA100 => BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(
            0.5,
            0,
          ),
          end: Alignment(
            0.5,
            1,
          ),
          colors: [
            theme.colorScheme.primary,
            appTheme.indigoA100,
          ],
        ),
      );
  static BoxDecoration get txtFill => BoxDecoration(
        color: appTheme.gray100,
      );
  static BoxDecoration
      get gradientnameindigoA400opacity025nameindigoA10002opacity025 =>
          BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(
                1,
                1,
              ),
              end: Alignment(
                0,
                0,
              ),
              colors: [
                appTheme.indigoA400.withOpacity(0.25),
                appTheme.indigoA10002.withOpacity(0.25),
              ],
            ),
          );
  static BoxDecoration get outline10 => BoxDecoration(
        color: appTheme.whiteA70001,
        border: Border.all(
          color: appTheme.indigo60001,
          width: getHorizontalSize(
            2,
          ),
        ),
        boxShadow: [
          BoxShadow(
            color: theme.colorScheme.onPrimaryContainer,
            spreadRadius: getHorizontalSize(
              2,
            ),
            blurRadius: getHorizontalSize(
              2,
            ),
            offset: Offset(
              0,
              4,
            ),
          ),
        ],
      );
  static BoxDecoration get txtGradientnameprimarynameindigoA100 =>
      BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(
            0.5,
            0,
          ),
          end: Alignment(
            0.5,
            1,
          ),
          colors: [
            theme.colorScheme.primary,
            appTheme.indigoA100,
          ],
        ),
      );
  static BoxDecoration get outline11 => BoxDecoration(
        color: appTheme.whiteA70001,
        border: Border.all(
          color: appTheme.whiteA70001,
          width: getHorizontalSize(
            1,
          ),
        ),
      );
  static BoxDecoration get outline12 => BoxDecoration(
        border: Border.all(
          color: appTheme.indigoA400,
          width: getHorizontalSize(
            1,
          ),
        ),
      );
  static BoxDecoration get txtOutline => BoxDecoration();
  static BoxDecoration get gradientnameindigo60001nameindigoA10004 =>
      BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(
            1,
            1,
          ),
          end: Alignment(
            0,
            0,
          ),
          colors: [
            appTheme.indigo60001,
            appTheme.indigoA10004,
          ],
        ),
      );
  static BoxDecoration get gradientnameredA20001nameredA100 => BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(
            1,
            1,
          ),
          end: Alignment(
            0,
            0,
          ),
          colors: [
            appTheme.redA20001,
            appTheme.redA100,
          ],
        ),
      );
  static BoxDecoration get outline => BoxDecoration(
        color: appTheme.whiteA70001,
        boxShadow: [
          BoxShadow(
            color: appTheme.black900.withOpacity(0.44),
            spreadRadius: getHorizontalSize(
              2,
            ),
            blurRadius: getHorizontalSize(
              2,
            ),
            offset: Offset(
              0,
              3.03,
            ),
          ),
        ],
      );
  static BoxDecoration get gradientnamedeeppurple100nameindigo60001 =>
      BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(
            1,
            1,
          ),
          end: Alignment(
            0,
            0,
          ),
          colors: [
            appTheme.deepPurple100,
            appTheme.indigo60001,
          ],
        ),
      );
  static BoxDecoration get fill9 => BoxDecoration(
        color: appTheme.deepPurple100,
      );
  static BoxDecoration get fill8 => BoxDecoration(
        color: appTheme.indigo60001,
      );
  static BoxDecoration get outline2 => BoxDecoration(
        color: appTheme.whiteA70001,
      );
  static BoxDecoration get fill5 => BoxDecoration(
        color: appTheme.indigoA40063,
      );
  static BoxDecoration get outline1 => BoxDecoration(
        color: appTheme.whiteA70001,
        border: Border.all(
          color: appTheme.gray200,
          width: getHorizontalSize(
            1,
          ),
        ),
      );
  static BoxDecoration get fill4 => BoxDecoration(
        color: appTheme.gray100,
      );
  static BoxDecoration get outline4 => BoxDecoration(
        color: appTheme.whiteA70001,
        boxShadow: [
          BoxShadow(
            color: theme.colorScheme.onPrimaryContainer,
            spreadRadius: getHorizontalSize(
              2,
            ),
            blurRadius: getHorizontalSize(
              2,
            ),
            offset: Offset(
              0,
              4,
            ),
          ),
        ],
      );
  static BoxDecoration get fill7 => BoxDecoration(
        color: appTheme.indigoA10004,
      );
  static BoxDecoration get outline3 => BoxDecoration(
        border: Border.all(
          color: appTheme.indigo60001,
          width: getHorizontalSize(
            2,
          ),
        ),
      );
  static BoxDecoration get fill6 => BoxDecoration(
        color: appTheme.gray5003,
      );
  static BoxDecoration get fill1 => BoxDecoration(
        color: appTheme.gray5005,
      );
  static BoxDecoration get outline6 => BoxDecoration(
        border: Border.all(
          color: appTheme.indigoA400,
          width: getHorizontalSize(
            2,
          ),
        ),
      );
  static BoxDecoration get fill12 => BoxDecoration(
        color: appTheme.deepPurple500,
      );
  static BoxDecoration get outline5 => BoxDecoration(
        color: appTheme.whiteA70001,
        border: Border.all(
          color: appTheme.gray10001,
          width: getHorizontalSize(
            1,
          ),
        ),
      );
  static BoxDecoration get fill11 => BoxDecoration(
        color: appTheme.indigoA400.withOpacity(0.39),
      );
  static BoxDecoration get fill3 => BoxDecoration(
        color: appTheme.indigoA200,
      );
  static BoxDecoration get outline8 => BoxDecoration(
        color: appTheme.whiteA70001,
        border: Border.all(
          color: appTheme.indigo60001,
          width: getHorizontalSize(
            3,
          ),
        ),
        boxShadow: [
          BoxShadow(
            color: theme.colorScheme.onPrimaryContainer,
            spreadRadius: getHorizontalSize(
              2,
            ),
            blurRadius: getHorizontalSize(
              2,
            ),
            offset: Offset(
              0,
              4,
            ),
          ),
        ],
      );
  static BoxDecoration get fill14 => BoxDecoration(
        color: appTheme.indigoA10004.withOpacity(0.42),
      );
  static BoxDecoration get fill2 => BoxDecoration(
        color: appTheme.blue50,
      );
  static BoxDecoration get outline7 => BoxDecoration(
        color: appTheme.whiteA70001,
        border: Border.all(
          color: appTheme.gray200,
          width: getHorizontalSize(
            1,
          ),
        ),
      );
  static BoxDecoration get fill13 => BoxDecoration(
        color: appTheme.indigo60001.withOpacity(0.39),
      );
  static BoxDecoration get outline9 => BoxDecoration(
        color: appTheme.whiteA70001,
        border: Border.all(
          color: appTheme.gray200,
          width: getHorizontalSize(
            1,
          ),
          strokeAlign: strokeAlignOutside,
        ),
      );
  static BoxDecoration get txtOutline2 => BoxDecoration(
        border: Border.all(
          color: appTheme.indigoA400,
          width: getHorizontalSize(
            2,
          ),
        ),
      );
  static BoxDecoration get fill10 => BoxDecoration(
        color: appTheme.indigoA70063,
      );
  static BoxDecoration get txtOutline1 => BoxDecoration(
        border: Border.all(
          color: appTheme.indigo60001,
          width: getHorizontalSize(
            2,
          ),
        ),
      );
}

class BorderRadiusStyle {
  static BorderRadius customBorderTL40 = BorderRadius.only(
    topLeft: Radius.circular(
      getHorizontalSize(
        40,
      ),
    ),
    topRight: Radius.circular(
      getHorizontalSize(
        40,
      ),
    ),
  );

  static BorderRadius txtCircleBorder19 = BorderRadius.circular(
    getHorizontalSize(
      19,
    ),
  );

  static BorderRadius customBorderTL24 = BorderRadius.only(
    topLeft: Radius.circular(
      getHorizontalSize(
        24,
      ),
    ),
    topRight: Radius.circular(
      getHorizontalSize(
        24,
      ),
    ),
  );

  static BorderRadius txtRoundedBorder22 = BorderRadius.circular(
    getHorizontalSize(
      22,
    ),
  );

  static BorderRadius circleBorder19 = BorderRadius.circular(
    getHorizontalSize(
      19,
    ),
  );

  static BorderRadius txtRoundedBorder12 = BorderRadius.circular(
    getHorizontalSize(
      12,
    ),
  );

  static BorderRadius circleBorder150 = BorderRadius.circular(
    getHorizontalSize(
      150,
    ),
  );

  static BorderRadius roundedBorder16 = BorderRadius.circular(
    getHorizontalSize(
      16,
    ),
  );

  static BorderRadius roundedBorder27 = BorderRadius.circular(
    getHorizontalSize(
      27,
    ),
  );

  static BorderRadius roundedBorder4 = BorderRadius.circular(
    getHorizontalSize(
      4,
    ),
  );

  static BorderRadius roundedBorder24 = BorderRadius.circular(
    getHorizontalSize(
      24,
    ),
  );

  static BorderRadius roundedBorder10 = BorderRadius.circular(
    getHorizontalSize(
      10,
    ),
  );

  static BorderRadius circleBorder70 = BorderRadius.circular(
    getHorizontalSize(
      70,
    ),
  );

  static BorderRadius roundedBorder31 = BorderRadius.circular(
    getHorizontalSize(
      31,
    ),
  );

  static BorderRadius roundedBorder40 = BorderRadius.circular(
    getHorizontalSize(
      40,
    ),
  );
}

// Comment/Uncomment the below code based on your Flutter SDK version.
    
// For Flutter SDK Version 3.7.2 or greater.
    
double get strokeAlignInside => BorderSide.strokeAlignInside;

double get strokeAlignCenter => BorderSide.strokeAlignCenter;

double get strokeAlignOutside => BorderSide.strokeAlignOutside;

// For Flutter SDK Version 3.7.1 or less.

// StrokeAlign get strokeAlignInside => StrokeAlign.inside;
//
// StrokeAlign get strokeAlignCenter => StrokeAlign.center;
//
// StrokeAlign get strokeAlignOutside => StrokeAlign.outside;
    